# StereoPhotoView #

Viewer/editor for stereoscopic 3d photo and video.

[Website](https://stereophotoview.bitbucket.io/en/)

## Install ##

[Get the distribution for your system](https://stereophotoview.bitbucket.io/en/download.html)

# Build #

## Sources ##

Use the [instructions to get the source code](https://stereophotoview.bitbucket.io/download.html).

## Linux ##

Below you will find instructions for building StereoPhotoView under Debian-based distributions.

### Install dependencies ###

For build:

```
sudo apt install qt5-default qtdeclarative5-dev qtmultimedia5-dev libavdevice-dev libavfilter-dev libopencv-dev qttools5-dev-tools
```

For running:

```
sudo apt install qml-module-qtquick-layouts qml-module-qtquick-controls2 qml-module-qtquick-dialogs qml-module-qt-labs-settings qml-module-qt-labs-folderlistmodel qml-module-qtqml-models2 qml-module-qt-labs-platform libqt5multimedia5-plugins
```

### Build ###

```
cd stereophotoview*
mkdir build
cd build
qmake ../
make
```

### Install ###

```
cd stereophotoview*
mkdir build
cd build
qmake [PREFIX=~/.local] [LIB=lib64] [CONFIG+=register-mime-types] [CONFIG+=post-install] [CONFIG+=without_opencv] ../
make
[sudo ][INSTALL_ROOT=... ]make install
```

* [PREFIX=<path>] - specify the installation path, than / usr.
* [LIB=lib64] - specify the installation path to lib directory in PREFIX, than lib.
* [CONFIG+=register-mime-types] - to register types of files "mpo" and "jps", otherwise the stereophotoview attached to the jpeg files.
* [CONFIG+=post-install] - run update-desktop-database and update-mime-database after installation.
* [CONFIG+=without_opencv] - build without opencv. Auto-align will not be available.

### Uninstall ###

```
[sudo ]make uninstall
```

## Windows ##

Below you can find instructions for building Stereo Photo View for MSVC 2017.

### Installing the necessary software for the build ###

* Download `Build Tools for Visual Studio 2017` or `Visual Studio 2017`, and install the `Visual C++ Build Tools`;
* Download [Qt for open source users](http://www.qt.io/download/), and install the `QT 5.14.1` -> `MSVC 2017 64-bit`;
* Download and extract [shared](https://bitbucket.org/stereophotoview/stereophotoview/downloads/ffmpeg-3.4.2-win64-shared.zip) and [dev](https://bitbucket.org/stereophotoview/stereophotoview/downloads/ffmpeg-3.4.2-win64-dev.zip) versions of the FFMpeg;
* Download and extract [OpenCV 3.4.x](https://opencv.org/releases.html).

### Build ###

* Open stereophotoview.pro in the Qt creator.
* Add additional arguments for qmake:
    ```
    ffmpeg_dev=<path_to_ffmpeg_dev> ffmpeg_shared=<path_to_ffmpeg_shared> opencv=<path_to_opencv/build> opencvVer=<version of the opencv>
    ```

    Example for opencv 3.4.12:
    ```
    ffmpeg_dev=C:\lib\ffmpeg-3.4.2-win64-dev ffmpeg_shared=C:\lib\ffmpeg-3.4.2-win64-shared opencv=C:\lib\opencv\build opencvVer=3412
    ```

* Build the project.

### Create an archive with portable binaries ###

This only works with PowerShell >= 5.

* Add the "Make" build stage with the arguments:
    ```
    archive
    ```
* Run the project build

### Make installer ###

* Install [Inno Setup](http://www.jrsoftware.org/isdl.php);
* Add additional arguments for qmake, for example:
    ```
    "InnoSetupDir=C:\Program Files (x86)\Inno Setup 5" <build arguments>
    ```

    Example:
    ```
    "InnoSetupDir=C:\Program Files (x86)\Inno Setup 6" ffmpeg_dev=C:\lib\ffmpeg-3.4.2-win64-dev ffmpeg_shared=C:\lib\ffmpeg-3.4.2-win64-shared opencv=C:\lib\opencv\build opencvVer=3412
    ```

* Add build step "Make" with arguments:
    ```
    installer
    ```

* Build the project

## Additional qmake parameters ##

Enable debugging output of the matching key points during auto-alignment:

```
CONFIG+=opencv_debug
```
