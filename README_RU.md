# Просмотр стерео фотографий (StereoPhotoView) #

Средство для просмотра и редактирования стереоскопических 3D фотографий и видео файлов.

[Сайт](https://stereophotoview.bitbucket.io/)

# Установка #

[Загрузите дистрибутив для вашей системы](https://stereophotoview.bitbucket.io/download.html)

# Сборка #

## Исходный код ##

Воспользуйтесь [инструкцией для получения исходников](https://stereophotoview.bitbucket.io/download.html).

## Linux ##

Ниже представлена инструкция для сборки StereoPhotoView под дистрибутивами, основанными на Debian.

### Установка зависимостей ###

Для сборки:

```
sudo apt install qt5-default qtdeclarative5-dev qtmultimedia5-dev libavdevice-dev libavfilter-dev libopencv-dev qttools5-dev-tools
```

Для запуска:

```
sudo apt install qml-module-qtquick-layouts qml-module-qtquick-controls2 qml-module-qtquick-dialogs qml-module-qt-labs-settings qml-module-qt-labs-folderlistmodel qml-module-qtqml-models2 qml-module-qt-labs-platform libqt5multimedia5-plugins
```

### Сборка ###

```
cd stereophotoview*
mkdir build
cd build
qmake ../
make
```

### Установка ###

```
cd stereophotoview*
mkdir build
cd build
qmake [PREFIX=~/.local] [LIB=lib64] [CONFIG+=register-mime-types] [CONFIG+=post-install] [CONFIG+=without_opencv] ../
make
[sudo ][INSTALL_ROOT=... ]make install
```

* [PREFIX=<path>] - указать путь для установки, отличный от /usr.
* [LIB=lib64] - указать путь для установки библиотек в PREFIX, отличный от lib.
* [CONFIG+=register-mime-types] - регистрировать отдельные типы файлов "mpo" и "jps", иначе stereophotoview привязывается к типу файлов jpeg.
* [CONFIG+=post-install] - выполнить update-desktop-database и update-mime-database после установки.
* [CONFIG+=without_opencv] - собрать без opencv. Авто-выравнивание будет недоступно.

### Удаление установленных файлов ###

```
[sudo ]make uninstall
```

## Windows ##

Ниже представлена инструкция по сборке StereoPhotoView под MSVC2017.

### Установка необходимого ПО для сборки ###

* Скачайте `Build Tools for Visual Studio 2017` или `Visual Studio 2017`, и установите `Visual C++ Build Tools`;
* Скачайте [Qt for open source users](http://www.qt.io/download/), и установите компонент `QT 5.14.1` -> `MSVC 2017 64-bit`;
* Скачайте и распакуйте [shared](https://bitbucket.org/stereophotoview/stereophotoview/downloads/ffmpeg-3.4.2-win64-shared.zip) и [dev](https://bitbucket.org/stereophotoview/stereophotoview/downloads/ffmpeg-3.4.2-win64-dev.zip) версии FFMpeg;
* Скачайте и распакуйте [OpenCV 3.4.x](https://opencv.org/releases.html).

### Сборка ###

* Откройте в Qt creator проект stereophotoview.pro из папки с исходниками.
* Настройте сборку.
* На вкладке "проекты" добавьте дополнительные параметры для qmake:
    ```
    ffmpeg_dev=<путь к dev версии ffmpeg> ffmpeg_shared=<путь к shared версии ffmpeg> opencv=<путь к папке opencv/build> opencvVer=<версия opencv>
    ```

    Пример для opencv 3.4.12:
    ```
    ffmpeg_dev=C:\lib\ffmpeg-3.4.2-win64-dev ffmpeg_shared=C:\lib\ffmpeg-3.4.2-win64-shared opencv=C:\lib\opencv\build opencvVer=3412
    ```

* Запустите сборку проекта.

### Создать архив с переносимыми бинарниками ###

Это работает только с PowerShell >= 5.

* Добавьте этап сборки "Make" с аргументами:
    ```
    archive
    ```
* Запустите сборку проекта

### Сделать установщик ###

* Установите [Inno Setup](http://www.jrsoftware.org/isdl.php);
* На вкладке "проекты" добавьте дополнительные параметры для qmake:
    ```
    InnoSetupDir=<Путь к папке установки Inno Setup> <параметры сборки>
    ```

    Пример:
    ```
    "InnoSetupDir=C:\Program Files (x86)\Inno Setup 6" ffmpeg_dev=C:\lib\ffmpeg-3.4.2-win64-dev ffmpeg_shared=C:\lib\ffmpeg-3.4.2-win64-shared opencv=C:\lib\opencv\build opencvVer=3412
    ```

* Добавьте этап сборки "Make" с аргументами:
    ```
    installer
    ```
* Запустите сборку проекта

## Дополнительные параметры qmake ##

Включение отладочного вывода совпадающих ключевых точек при авто-выравнивании:

```
CONFIG+=opencv_debug
```

