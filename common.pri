VERSION = 1.21.3
QMAKE_TARGET_COPYRIGHT= (c) 2015-2022 Alexander Mamzikov
QMAKE_TARGET_COMPANY = Alexander Mamzikov

APP_VERSION = $$VERSION
system(git describe):APP_VERSION = $$system(git describe)

DEFINES += APP_VERSION=\\\"$$APP_VERSION\\\"
DEFINES += APP_NAME=\\\"$$QMAKE_TARGET_PRODUCT\\\"
CONFIG += c++17
CONFIG(debug, release|debug):DEFINES += DEBUG

DISTFILES += \
    $$PWD/COPYING
