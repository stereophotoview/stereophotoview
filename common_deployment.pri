windows {
    isEmpty(PREFIX) {
        PREFIX = $$OUT_PWD/../install
    }
    isEmpty(target.path) {
        BINPATH = $$PREFIX/stereophotoview-$$APP_VERSION
        BINPATH ~= s,/,\\,g
    }
}
