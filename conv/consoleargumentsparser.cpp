#include "consoleargumentsparser.h"

#include <QCommandLineParser>
#include <QUrl>
#include <QMetaEnum>
#include <QIncompatibleFlag>
#include <QTextStream>
#include "fileFormats/video/mux.h"
#include "fileFormats/video/videooptions.h"
#include <QFileInfo>
#include <QTextCodec>

using namespace stereoscopic;
using namespace stereoscopic::fileFormats::video;
using namespace stereoscopic::ui;

ConsoleArgumentsParser::ConsoleArgumentsParser()
    : QObject(nullptr)
{

}

ConsoleArgumentsParser::ConsoleArgumentsParser(QCoreApplication &app)
    : QObject(nullptr)
{
    QMetaEnum layoutEnum = QMetaEnum::fromType<StereoFormat::Layout>();

    QCommandLineParser parser;

    parser.setApplicationDescription(tr("Converter for stereoscopic photos and videos.") + "\r\n"
                                     + "https://stereophotoview.bitbucket.io" + tr("/en"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("input", tr("Input file."));
    parser.addPositionalArgument("input2", tr("Optional second input file to load a separate stereo pair."));

    VideoOptions vo;

    QCommandLineOption outputFileOption(QStringList() << "o" << "output",
            tr("Output file."), "file");
    parser.addOption(outputFileOption);

    QCommandLineOption inputLayoutOption(QStringList() << "input-layout",
            tr("Input stereo layout (see --layouts) - optinal."), "layout", "Auto");
    parser.addOption(inputLayoutOption);

    QCommandLineOption inputRightFirstOption(QStringList() << "input-revert",
              tr("Right-eye view first in the input file - optional."), "1|0");
    parser.addOption(inputRightFirstOption);

    QCommandLineOption timeShiftOption(QStringList() << "time-shift" << "t",
            tr("Time shift of second video stream"), "frames", "0");
    parser.addOption(timeShiftOption);

    QCommandLineOption outputLayoutOption(QStringList() << "output-layout",
            tr("Output stereo layout (see --layouts). Default - Monoscopic."), "layout", "Monoscopic");
    parser.addOption(outputLayoutOption);

    QCommandLineOption outputRightFirstOption(QStringList() << "output-revert",
              tr("Right-eye view first in the output file - optional."), "1|0");
    parser.addOption(outputRightFirstOption);

    QCommandLineOption sizeOption(QStringList() << "size",
              tr("Output image size."), "width*height");
    parser.addOption(sizeOption);

    QCommandLineOption jpegQualityOption(QStringList() << "jpeg-quality",
              tr("Jpeg quality."), "0..100", "75");
    parser.addOption(jpegQualityOption);

    QCommandLineOption crfOption(QStringList() << "crf",
              tr("Constant rate factor. Default - %1.").arg(vo.crf()), "1..51");
    parser.addOption(crfOption);

    QCommandLineOption presetOption(QStringList() << "preset",
              tr("Preset for H264 codec (see --presets). Default - %1.").arg(vo.preset()), "preset");
    parser.addOption(presetOption);

    QCommandLineOption operationsOption(QStringList() << "operations",
              tr("List of operations separated by a semicolon (see --help-operations)."), "operations");
    parser.addOption(operationsOption);

    QCommandLineOption progressOption(QStringList() << "p" << "progress",
            tr("Display progress when saving video."));
    parser.addOption(progressOption);

    QCommandLineOption layoutListOption(QStringList() << "l" << "layouts",
            tr("Display a list of layouts."));
    parser.addOption(layoutListOption);

    QCommandLineOption videoFormatsListOption(QStringList() << "video-formats",
            tr("Display a list of video formats."));
    parser.addOption(videoFormatsListOption);

    QCommandLineOption presetListOption(QStringList() << "presets",
            tr("Display a list of presets."));
    parser.addOption(presetListOption);

    QCommandLineOption operationsListOption(QStringList() << "help-operations",
            tr("Display a list of operations."));
    parser.addOption(operationsListOption);

    parser.process(app);

    QTextStream out(stdout);

    if(parser.isSet(layoutListOption))
    {
        out << tr("Input layouts") << ":" << endl;
        for(int i = 0; i < layoutEnum.keyCount(); i++)
            out << "- " << layoutEnum.key(i) << endl;
        out << endl << tr("Output layouts") << ":" << endl;
        for(int i = 0; i < layoutEnum.keyCount(); i++)
            if(layoutEnum.value(i) != StereoFormat::Separate)
                out << "- " << layoutEnum.key(i) << endl;
        out.flush();
        exit(0);
    }

    if(parser.isSet(videoFormatsListOption))
    {
        out << "Video formats:" << endl;
        out << "name\tvideo_codec\taudio_codec" << endl;
        foreach(AVOutputFormat* format, stereoscopic::fileFormats::video::Mux::supportedFormats())
        {
            out << format->name << "\t"
                << stereoscopic::fileFormats::video::Mux::codecName(stereoscopic::fileFormats::video::Mux::defaultVideoCodec(format)) << endl
                << stereoscopic::fileFormats::video::Mux::codecName(stereoscopic::fileFormats::video::Mux::defaultAudioCodec(format)) << endl;
        }

        out.flush();
        exit(0);
    }

    if(parser.isSet(presetListOption))
    {
        QStringList list;
        list << "ultrafast" << "superfast" << "veryfast" << "faster" << "fast" << "medium" << "slow" << "slower" << "veryslow";
        foreach(auto s, list)
            out << "- " << s << endl;
        out.flush();
        exit(0);
    }

    if(parser.isSet((operationsListOption)))
    {
        QString prog = QFileInfo(QCoreApplication::applicationFilePath()).fileName();
        out << tr("Operations") << endl << endl;
#ifdef OPENCV
        out << "\tauto-align:h|v|r[,quality]  - " << tr("Auto alignment") << endl;
#endif
        out << "\talign:<x>,<y>               - " << tr("Alignment") << endl;
        out << "\trotate:<left>,<right>       - " << tr("Rotate") << endl;
        out << "\tcrop:<x>,<y>,<w>,<h>        - " << tr("Crop") << endl;
        out << endl << tr("Examples") << endl;
#ifdef OPENCV
        out << endl << tr("Automatic vertical alignment") << ":" << endl
            << "    auto-align:v" << endl;
        out << endl << tr("Automatic vertical alignment with quality of 80%") << ":" << endl
            << "    auto-align:v,80" << endl;
        out << endl << tr("Automatic alignment of horizontal, vertical and angle") << ":" << endl
            << "    auto_align:hvr" << endl;
#endif
        out << endl << tr("Apply a horizontal offset to 0.03 part by the width and the vertical offset to -0.01 part of the height") << ":" << endl
            << "    align:0.03,-0.01" << endl;
        out << endl << tr("Rotate the left view of 1 degree, and the right view at -1 degree") << ":" << endl
            << "    rotate:1,-1" << endl;
        out << endl << tr("Crop image") << ":" << endl
            << "    crop:10,20,200,100" << endl;
        out.flush();
        exit(0);
    }

    const QStringList args = parser.positionalArguments();
    if(args.length() > 0)
        m_input.append(parseFilePath(args[0]));
    if(args.length() > 1)
        m_input.append(parseFilePath(args[1]));

    if(parser.isSet(outputFileOption))
        m_output = parseFilePath(parser.value(outputFileOption));

    if(parser.isSet(inputLayoutOption))
        m_inputLayout = (StereoFormat::Layout)layoutEnum.keyToValue(parser.value(inputLayoutOption).toLatin1().data());
    if(parser.isSet(inputRightFirstOption))
        m_inputRightFirst = parser.value(inputRightFirstOption).toInt();

    if(parser.isSet(timeShiftOption))
        m_timeShift = parser.value(timeShiftOption).toInt();

    if(parser.isSet(outputLayoutOption))
        m_outputLayout = (StereoFormat::Layout)layoutEnum.keyToValue(parser.value(outputLayoutOption).toLatin1().data());
    if(parser.isSet(outputRightFirstOption))
        m_outputRightFirst = parser.value(outputRightFirstOption).toInt();

    if(parser.isSet(jpegQualityOption))
        m_jpegQuality = parser.value(jpegQualityOption).toInt();

    if(parser.isSet(sizeOption))
    {
        QStringList s = parser.value(sizeOption).split('*');
        if(s.length() == 2)
            m_size = QSize(s.at(0).toInt(), s.at(1).toInt());
    }

    if(parser.isSet(crfOption))
        m_crf = parser.value(crfOption).toInt();

    if(parser.isSet(presetOption))
        m_preset = parser.value(presetOption);

    if(parser.isSet(operationsOption))
    {
        QString str = parser.value(operationsOption);
        QStringList strs = str.split(';');
        foreach (QString sign, strs)
            m_operations.append(sign);
    }

    m_progress = parser.isSet(progressOption);

    if(m_output.isEmpty())
    {
        QTextStream out(stdout);
        out << parser.helpText();
        out.flush();
        exit(1);
    }
}

QString ConsoleArgumentsParser::parseFilePath(QString argv)
{
    QUrl url(argv);
    if(url.isValid() && url.isLocalFile())
        return url.toLocalFile();
    return argv;
}
