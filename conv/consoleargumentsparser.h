#ifndef CONSOLEARGUMENTSPARSER_H
#define CONSOLEARGUMENTSPARSER_H

#include <QObject>
#include <QStringList>
#include <QCoreApplication>
#include "stereoformat.h"
#include <memory>

using namespace std;
using namespace stereoscopic;

namespace stereoscopic::ui
{
    // Консольный конвертер стерео фотографий
    class ConsoleArgumentsParser : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QStringList inputFilePaths READ inputFilePaths)
            Q_PROPERTY(StereoFormat::Layout inputLayout READ inputLayout)
            Q_PROPERTY(int inputRightFirst READ inputRightFirst)
            Q_PROPERTY(int timeShift READ timeShift)
        public:
            explicit ConsoleArgumentsParser();
            explicit ConsoleArgumentsParser(QCoreApplication &app);
            QStringList inputFilePaths(){ return m_input; }
            QString outputFilePath(){ return m_output; }
            StereoFormat::Layout inputLayout(){ return m_inputLayout; }
            StereoFormat::Layout outputLayout(){ return m_outputLayout; }
            /// -1: not set, 0: false, 1: true
            int inputRightFirst(){ return m_inputRightFirst; }
            /// -1: not set, 0: false, 1: true
            int outputRightFirst(){ return m_outputRightFirst; }
            int timeShift(){ return m_timeShift; }
            int jpegQuality(){ return m_jpegQuality; }
            QSize size(){ return m_size; }
            int crf(){ return m_crf; }
            QString preset(){ return m_preset; }
            bool progress(){ return m_progress; }
            QStringList operations(){ return m_operations; }

        private:
            QStringList m_input;
            QString m_output;
            QString parseFilePath(QString argv);
            StereoFormat::Layout m_inputLayout = (StereoFormat::Layout)-1;
            StereoFormat::Layout m_outputLayout = (StereoFormat::Layout)-1;
            int m_inputRightFirst = -1;
            int m_timeShift = 0;
            int m_outputRightFirst = -1;
            int m_jpegQuality = -1;
            int m_crf = -1;
            QString m_preset = "medium";
            QSize m_size;
            bool m_progress = false;
            QStringList m_operations;
    };
};
#endif // CONSOLEARGUMENTSPARSER_H
