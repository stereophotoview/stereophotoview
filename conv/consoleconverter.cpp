#include "consoleconverter.h"

#include "fileFormats/mpoimageformat.h"
#include "fileFormats/separateimageformat.h"
#include "fileFormats/jpsimageformat.h"
#include "fileFormats/videoformat.h"
#include <QtConcurrent/QtConcurrent>
#include <autoalign.h>

using namespace stereoscopic::fileFormats;
using namespace stereoscopic::fileFormats::video;
using namespace stereoscopic::ui;
using namespace stereoscopic::ui::operations;

ConsoleConverter::ConsoleConverter(ConsoleArgumentsParser *args, QObject *parent)
    : QObject(parent)
{
    m_args = args;
    out = new QTextStream(stdout);
    err = new QTextStream(stderr);
}

ConsoleConverter::~ConsoleConverter()
{
    delete out;
    delete err;
}

int ConsoleConverter::execute()
{
    bool res = executeImpl();
    out->flush();
    err->flush();
    return res;
}

void ConsoleConverter::saveVideoProgressChanged(int p)
{
    *out << "\r" << p << "%";
    out->flush();
}

int ConsoleConverter::executeImpl()
{
    StereoImageSource *src = load();
    if(src == nullptr)
    {
        *err << tr("Can not load input file") + "\n";
        return 1;
    }
    StereoFormat::Layout layout = m_args->inputLayout();
    if(layout >= 0 || m_args->inputRightFirst() >= 0)
    {
        src->setUseOriginalFormat(false);
        src->activeFormat()->setLayout(layout);
        src->activeFormat()->setLeftFirst(!m_args->inputRightFirst());
    }
    if(src != nullptr)
    {
        foreach(QString opSign, m_args->operations())
        {
            auto opPtr = OperationFactory::create(opSign);
            auto op = opPtr.get();
            if(op)
                op->execute(src);
        }
        if(!save(src))
            return 2;
    }
    return 0;
}

StereoImageSource *ConsoleConverter::load()
{
    QStringList inputFiles = m_args->inputFilePaths();
    if(inputFiles.length() == 2)
    {
        SeparateImageFormat f;
        return f.load(inputFiles[0] + "\n" + inputFiles[1], nullptr);
    }
    MpoImageFormat mpo;
    JpsImageFormat jps;
    VideoFormat video;
    QString filePath = inputFiles[0];
    if(mpo.checkFileName(filePath))
        return mpo.load(filePath, nullptr);
    if(jps.checkFileName(filePath))
        return jps.load(filePath, nullptr);
    if(video.checkFileName(filePath))
    {
        StereoVideoSource* vs = dynamic_cast<StereoVideoSource*>(video.load(filePath, nullptr, false));
        vs->setTimeShift(m_args->timeShift());
        return vs;
    }
    return nullptr;
}

bool ConsoleConverter::save(StereoImageSource *src)
{
    MpoImageFormat mpo;
    JpsImageFormat jps;
    VideoFormat video;
    QString filePath = m_args->outputFilePath();
    int q = m_args->jpegQuality();
    QSize size = m_args->size();
    if(mpo.checkFileName(filePath))
    {
        mpo.save(src, filePath, !m_args->outputRightFirst(), size, q);
    }
    else if(jps.checkFileName(filePath))
    {
        StereoFormat f;
        applyOutputStereoFormat(&f);
        jps.save(src, filePath, q, &f, size);
    }
    else if(video.checkFileName(filePath))
    {
        VideoOptions opts;
        applyOutputStereoFormat(opts.stereoFormat());
        StereoVideoSource* vs = dynamic_cast<StereoVideoSource*>(src);
        if(vs != nullptr)
        {
            int formatIndex = opts.findFormatByFilePath(filePath);
            if(formatIndex < 0)
            {
                *err << "Can not detect output format" << "\n";
                return false;
            }
            opts.setCurrentFormatIndex(formatIndex);
            if(m_args->crf() > 0)
                opts.setCrf(m_args->crf());
            if(!m_args->preset().isNull())
                opts.setPreset(m_args->preset());
            SaveVideoTask *task = video.createSaveTask(vs, filePath, &opts);
            if(m_args->progress())
                connect(task, &SaveVideoTask::progressChanged, this, &ConsoleConverter::saveVideoProgressChanged, Qt::DirectConnection);
            QFuture<bool> f = QtConcurrent::run(task, &SaveVideoTask::execute);
            bool res = f.result();
            if(m_args->progress())
                *out << "\n";
            if(!res)
                *err << task->resultStr() << "\n";
            return res;
        }
        return false;
    }
    else
    {
        *err << "Can not detect output format\n";
        return false;
    }
    return true;
}

void ConsoleConverter::applyOutputStereoFormat(StereoFormat *f)
{
    StereoFormat::Layout layout = m_args->outputLayout();
    if(layout >= 0)
        f->setLayout(layout);
    if(m_args->outputRightFirst() >= 0)
        f->setLeftFirst(!m_args->outputRightFirst());
}
