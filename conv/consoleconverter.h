#ifndef CONSOLECONVERTER_H
#define CONSOLECONVERTER_H

#include <QObject>
#include "consoleargumentsparser.h"
#include "stereoimagesource.h"
#include <QtDebug>
#include "operations.h"

namespace stereoscopic::ui
{
    class ConsoleConverter : public QObject
    {
            Q_OBJECT
        public:
            ConsoleConverter(ConsoleArgumentsParser *args, QObject *parent = nullptr);
            ~ConsoleConverter();
            int execute();

        signals:

        public slots:
            void saveVideoProgressChanged(int);

        private:
            ConsoleArgumentsParser* m_args;
            QTextStream *out;
            QTextStream *err;

            int executeImpl();
            StereoImageSource* load();
            bool save(StereoImageSource* src);
            void applyOutputStereoFormat(StereoFormat* f);
    };
};
#endif // CONSOLECONVERTER_H
