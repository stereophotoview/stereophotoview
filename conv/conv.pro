TEMPLATE = app

QT -= gui
QT += widgets multimedia

TARGET = stereo-conv

CONFIG += c++17 console
CONFIG -= app_bundle

QMAKE_TARGET_PRODUCT = StereoConverter
QMAKE_TARGET_DESCRIPTION = Stereoscopic photo and video converter
include(../common.pri)

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    consoleconverter.cpp \
    consoleargumentsparser.cpp \
    operations.cpp

HEADERS += \
    consoleconverter.h \
    consoleargumentsparser.h \
    operations.h

# Подключаем сторонние библиотеки
include(../external_libs.pri)

DEPENDPATH += ../lib
INCLUDEPATH += ../lib

windows {
    LIBS += -lstereophotoview1
    CONFIG(debug):LIBS += -L../lib/debug
    CONFIG(release):LIBS += -L../lib/release
}
unix {
    LIBS += -L../lib -lstereophotoview
    QMAKE_RPATHDIR += $ORIGIN/../lib
}

include(deployment.pri)

include(translations.prf)
