include(../common_deployment.pri)

unix:!android {
    isEmpty(PREFIX) {
        PREFIX = /usr
    }

    isEmpty(target.path) {
        target.path = $$PREFIX/bin
    }
    INSTALLS += target
}

windows {
    target.path = $$BINPATH
    INSTALLS += target
    QMAKE_EXTRA_TARGETS = install
}
