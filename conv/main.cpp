#include <QCoreApplication>
#include <QLocale>
#include <QTranslator>
#include <QTextCodec>
#include <QTextStream>

#include "consoleargumentsparser.h"
#include "consoleconverter.h"

using namespace stereoscopic::ui;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    QString app_version = APP_VERSION;
    QCoreApplication::setApplicationName(APP_NAME);
    QCoreApplication::setOrganizationName(APP_NAME);
    QCoreApplication::setApplicationVersion(app_version);

    QString lang = QLocale::system().name();
    QLocale::setDefault(QLocale(lang));

    QTranslator internalTranslator;
    if(internalTranslator.load(lang, ":/translations"))
        app.installTranslator(&internalTranslator);

    QTranslator libTranslator;
    if(libTranslator.load(lang, ":/lib_translations"))
        app.installTranslator(&libTranslator);

#ifdef _WIN32
    if(lang == "ru_RU")
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("CP866"));
#endif

    ConsoleArgumentsParser args(app);
    ConsoleConverter conv(&args);
    return conv.execute();
}
