#include "operations.h"

#include <QDebug>

using namespace stereoscopic;
using namespace stereoscopic::ui::operations;

shared_ptr<BaseOperation> OperationFactory::create(QString sign)
{
    QStringList l = sign.split(':');
    if(l.length() < 1)
        return shared_ptr<BaseOperation>();
    QString name = l.first();
    QStringList parameters;
    if(l.length() > 1)
        parameters = l[1].split(',');
    if(name == "auto-align")
        return shared_ptr<BaseOperation>((BaseOperation*)new AutoAlignOp(parameters));
    if(name == "align")
        return shared_ptr<BaseOperation>((BaseOperation*)new AlignOp(parameters));
    if(name == "rotate")
        return shared_ptr<BaseOperation>((BaseOperation*)new RotateOp(parameters));
    if(name == "crop")
        return shared_ptr<BaseOperation>((BaseOperation*)new CropOp(parameters));
    return shared_ptr<BaseOperation>();
}

AutoAlignOp::AutoAlignOp(QStringList parameters)
{
    QString p = parameters.first();
    horizontal = p.contains('h');
    vertical = p.contains('v');
    rotate = p.contains('r');
    if(parameters.length() > 1)
        quality = parameters.at(1).toInt();
}

void AutoAlignOp::execute(StereoImageSource* src)
{
    double q = 0.01 * quality;
    AlignParams p = AutoAlign::calcOffset(src, InputAlignParams(QRect(), horizontal, vertical, rotate, q));
    StereoFrame frame = src->displayFrame()->aligned(p.offset()).rotated(p.leftAngle(), p.rightAngle());
    src->setOriginalFrame(frame);
}

AlignOp::AlignOp(QStringList parameters)
{
    if(parameters.length() > 0)
        hor = parameters.at(0).toDouble();
    if(parameters.length() > 1)
        ver = parameters.at(1).toDouble();
}

void AlignOp::execute(StereoImageSource *src)
{
    QPointF offset(hor, ver);
    StereoFrame frame = src->displayFrame()->aligned(offset);
    src->setOriginalFrame(frame);
}

RotateOp::RotateOp(QStringList parameters)
{
    if(parameters.length() > 0)
        leftA = parameters.at(0).toDouble();
    if(parameters.length() > 1)
        rightA = parameters.at(1).toDouble();
}

void RotateOp::execute(StereoImageSource *src)
{
    StereoFrame frame = src->displayFrame()->rotated(leftA, rightA);
    src->setOriginalFrame(frame);
}

CropOp::CropOp(QStringList parameters)
{
    if(parameters.length() == 4)
        rect = QRect(parameters[0].toInt(), parameters[1].toInt(), parameters[2].toInt(), parameters[3].toInt());
}

void CropOp::execute(StereoImageSource *src)
{
    if(!rect.isEmpty())
    {
        StereoFrame frame = src->displayFrame()->cropped(rect);
        src->setOriginalFrame(frame);
    }
}

BaseOperation::~BaseOperation()
{

}
