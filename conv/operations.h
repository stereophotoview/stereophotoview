#ifndef OPERATIONS_H
#define OPERATIONS_H

#include <QString>
#include <QStringList>
#include <memory>
#include "stereoimagesource.h"
#include "autoalign.h"

using namespace std;
using namespace stereoscopic;

namespace stereoscopic::ui::operations {
    class BaseOperation
    {
        public:
            QString name;
            virtual void execute(StereoImageSource *src) = 0;
            virtual ~BaseOperation();
    };

    class AutoAlignOp : BaseOperation
    {
        public:
            AutoAlignOp(QStringList parameters);
            bool horizontal = false;
            bool vertical = false;
            bool rotate = false;
            int quality = 20;
            virtual void execute(StereoImageSource *src);
    };

    class AlignOp : BaseOperation
    {
        public:
            AlignOp(QStringList parameters);
            double hor = 0;
            double ver = 0;
            virtual void execute(StereoImageSource *src);
    };

    class RotateOp : BaseOperation
    {
        public:
            RotateOp(QStringList parameters);
            double leftA = 0;
            double rightA = 0;
            virtual void execute(StereoImageSource *src);
    };

    class CropOp : BaseOperation
    {
        public:
            CropOp(QStringList parameters);
            QRect rect;
            virtual void execute(StereoImageSource *src);
    };

    class OperationFactory
    {
        public:
            static shared_ptr<BaseOperation> create(QString sign);
    };
}
#endif // OPERATIONS_H
