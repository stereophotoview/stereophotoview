<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>ConsoleArgumentsParser</name>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="30"/>
        <source>Converter for stereoscopic photos and videos.</source>
        <translation>Конвертер для стереоскопических фотографий и видео файлов.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="31"/>
        <source>/en</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="34"/>
        <source>Input file.</source>
        <translation>Входной файл.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="35"/>
        <source>Optional second input file to load a separate stereo pair.</source>
        <translation>Необязательный второй входной файл для загрузки раздельной стерео-пары.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="40"/>
        <source>Output file.</source>
        <translation>Выходной файл.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="44"/>
        <source>Input stereo layout (see --layouts) - optinal.</source>
        <translation>Компоновка стерео-пары во входном файле (см. --layouts) - не обязательный.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="48"/>
        <source>Right-eye view first in the input file - optional.</source>
        <translation>Указывает, что во входном файле сначала идёт ракурс для правого глаза - не обязательно.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="52"/>
        <source>Time shift of second video stream</source>
        <translation>Сдвиг времени второго видео потока</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="56"/>
        <source>Output stereo layout (see --layouts). Default - Monoscopic.</source>
        <translation>Компоновка стерео-пары в выходном файле (см. --layouts) - По умолчанию - Monoscopic.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="60"/>
        <source>Right-eye view first in the output file - optional.</source>
        <translation>Указывает, что в выходном файле сначала идёт ракурс для правого глаза - не обязательно.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="64"/>
        <source>Output image size.</source>
        <translation>Размер кадра в выходном файле.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="68"/>
        <source>Jpeg quality.</source>
        <translation>Качество JPEG.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="72"/>
        <source>Constant rate factor. Default - %1.</source>
        <translation>Постоянный коэффициент скорости для сохранения видео. По умолчанию - %1.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="76"/>
        <source>Preset for H264 codec (see --presets). Default - %1.</source>
        <translation>Пресет для кодека H264 (см. --presets). По умолчанию - %1.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="80"/>
        <source>List of operations separated by a semicolon (see --help-operations).</source>
        <translation>Список операций, разделённый точкой с запятой (см. --help-operations).</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="84"/>
        <source>Display progress when saving video.</source>
        <translation>Отображать прогресс сохранения видео.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="88"/>
        <source>Display a list of layouts.</source>
        <translation>Отобразить список возможных компоновок стерео-пар.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="92"/>
        <source>Display a list of video formats.</source>
        <translation>Отобразить список форматов видео файлов.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="96"/>
        <source>Display a list of presets.</source>
        <translation>Отобразить список пресетов.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="100"/>
        <source>Display a list of operations.</source>
        <translation>Отобразить список операций.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="109"/>
        <source>Input layouts</source>
        <translation>Расположения, доступные для входных стерео-пар</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="112"/>
        <source>Output layouts</source>
        <translation>Расположения, доступные для выходных стерео-пар</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="148"/>
        <source>Operations</source>
        <translation>Оперции</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="150"/>
        <source>Auto alignment</source>
        <translation>Авто-выравнивание</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="152"/>
        <source>Alignment</source>
        <translation>Выравнивание</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="153"/>
        <source>Rotate</source>
        <translation>Вращение</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="154"/>
        <source>Crop</source>
        <translation>Кадрировать</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="155"/>
        <source>Examples</source>
        <translation>Примеры</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="157"/>
        <source>Automatic vertical alignment</source>
        <translation>Автоматическое вертикальное выравнивание</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="159"/>
        <source>Automatic vertical alignment with quality of 80%</source>
        <translation>Автоматическое вертикальное выравнивание с качеством 80%</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="161"/>
        <source>Automatic alignment of horizontal, vertical and angle</source>
        <translation>Автоматическое выравнивание по горизонтали, вертикали, и углу</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="164"/>
        <source>Apply a horizontal offset to 0.03 part by the width and the vertical offset to -0.01 part of the height</source>
        <translation>Применить горизонтальное смещение на 0.03 части ширины, и вертикальное смещение на -0.01 части высоты</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="166"/>
        <source>Rotate the left view of 1 degree, and the right view at -1 degree</source>
        <translation>Повернуть левый ракурс на 1 градус, и правый ракурс - на -1 градус</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="168"/>
        <source>Crop image</source>
        <translation>Кадрировать изображение</translation>
    </message>
</context>
<context>
    <name>ConsoleConverter</name>
    <message>
        <location filename="../consoleconverter.cpp" line="48"/>
        <source>Can not load input file</source>
        <translation>Не удалось загрузить входной файл</translation>
    </message>
</context>
<context>
    <name>stereoscopic::ui::ConsoleArgumentsParser</name>
    <message>
        <source>Converter for stereoscopic photos and videos.</source>
        <translation type="vanished">Конвертер для стереоскопических фотографий и видео файлов.</translation>
    </message>
    <message>
        <source>/en</source>
        <translation type="vanished">/</translation>
    </message>
    <message>
        <source>Input file.</source>
        <translation type="vanished">Входной файл.</translation>
    </message>
    <message>
        <source>Optional second input file to load a separate stereo pair.</source>
        <translation type="vanished">Необязательный второй входной файл для загрузки раздельной стерео-пары.</translation>
    </message>
    <message>
        <source>Output file.</source>
        <translation type="vanished">Выходной файл.</translation>
    </message>
    <message>
        <source>Input stereo layout (see --layouts) - optinal.</source>
        <translation type="vanished">Компоновка кадров во входном файле (см. --layouts) - не обязательный.</translation>
    </message>
    <message>
        <source>Right-eye view first in the input file - optional.</source>
        <translation type="vanished">Указывает, что во входном файле сначала идёт ракурс для правого глаза - не обязательно.</translation>
    </message>
    <message>
        <source>Time shift of second video stream</source>
        <translation type="vanished">Сдвиг времени второго видео потока</translation>
    </message>
    <message>
        <source>Output stereo layout (see --layouts). Default - Monoscopic.</source>
        <translation type="vanished">Компоновка кадров в выходном файле (см. --layouts) - По умолчанию - Monoscopic.</translation>
    </message>
    <message>
        <source>Right-eye view first in the output file - optional.</source>
        <translation type="vanished">Указывает, что в выходном файле сначала идёт ракурс для правого глаза - не обязательно.</translation>
    </message>
    <message>
        <source>Output image size.</source>
        <translation type="vanished">Размер кадра в выходном файле.</translation>
    </message>
    <message>
        <source>Jpeg quality.</source>
        <translation type="vanished">Качество JPEG.</translation>
    </message>
    <message>
        <source>Constant rate factor. Default - %1.</source>
        <translation type="vanished">Постоянный коэффициент скорости для сохранения видео. По умолчанию - %1.</translation>
    </message>
    <message>
        <source>Preset for H264 codec (see --presets). Default - %1.</source>
        <translation type="vanished">Пресет для кодека H264 (см. --presets). По умолчанию - %1.</translation>
    </message>
    <message>
        <source>List of operations separated by a semicolon (see --help-operations).</source>
        <translation type="vanished">Список операций, разделённый точкой с запятой (см. --help-operations).</translation>
    </message>
    <message>
        <source>Display progress when saving video.</source>
        <translation type="vanished">Отображать прогресс сохранения видео.</translation>
    </message>
    <message>
        <source>Display a list of layouts.</source>
        <translation type="vanished">Отобразить список возможных компоновок кадров.</translation>
    </message>
    <message>
        <source>Display a list of video formats.</source>
        <translation type="vanished">Отобразить список форматов видео файлов.</translation>
    </message>
    <message>
        <source>Display a list of presets.</source>
        <translation type="vanished">Отобразить список пресетов.</translation>
    </message>
    <message>
        <source>Display a list of operations.</source>
        <translation type="vanished">Отобразить список операций.</translation>
    </message>
    <message>
        <source>Input layouts</source>
        <translation type="vanished">Компановки кадров, доступные для входных стерео-пар</translation>
    </message>
    <message>
        <source>Output layouts</source>
        <translation type="vanished">Компановки кадров, доступные для выходных стерео-пар</translation>
    </message>
    <message>
        <source>Operations</source>
        <translation type="vanished">Оперции</translation>
    </message>
    <message>
        <source>Auto alignment</source>
        <translation type="vanished">Авто-выравнивание</translation>
    </message>
    <message>
        <source>Alignment</source>
        <translation type="vanished">Выравнивание</translation>
    </message>
    <message>
        <source>Rotate</source>
        <translation type="vanished">Вращение</translation>
    </message>
    <message>
        <source>Crop</source>
        <translation type="vanished">Кадрировать</translation>
    </message>
    <message>
        <source>Examples</source>
        <translation type="vanished">Примеры</translation>
    </message>
    <message>
        <source>Automatic vertical alignment</source>
        <translation type="vanished">Автоматическое вертикальное выравнивание</translation>
    </message>
    <message>
        <source>Automatic vertical alignment with quality of 80%</source>
        <translation type="vanished">Автоматическое вертикальное выравнивание с качеством 80%</translation>
    </message>
    <message>
        <source>Automatic alignment of horizontal, vertical and angle</source>
        <translation type="vanished">Автоматическое выравнивание по горизонтали, вертикали, и углу</translation>
    </message>
    <message>
        <source>Apply a horizontal offset to 0.03 part by the width and the vertical offset to -0.01 part of the height</source>
        <translation type="vanished">Применить горизонтальное смещение на 0.03 части ширины, и вертикальное смещение на -0.01 части высоты</translation>
    </message>
    <message>
        <source>Rotate the left view of 1 degree, and the right view at -1 degree</source>
        <translation type="vanished">Повернуть левый ракурс на 1 градус, и правый ракурс - на -1 градус</translation>
    </message>
    <message>
        <source>Crop image</source>
        <translation type="vanished">Кадрировать изображение</translation>
    </message>
</context>
<context>
    <name>stereoscopic::ui::ConsoleConverter</name>
    <message>
        <source>Can not load input file</source>
        <translation type="vanished">Не удалось загрузить входной файл</translation>
    </message>
</context>
</TS>
