DISTFILES += \
    unix-utils/postinst \
    unix-utils/postrm \
    installer/windows.iss \
    installer/windows_64.iss


unix:!android {
    QMAKE_EXTRA_TARGETS = install
}

windows {
    isEmpty(PREFIX) {
        PREFIX = $$OUT_PWD/install
    }
    isEmpty(target.path) {
        BINPATH = $$PREFIX/stereophotoview-$$APP_VERSION
        BINPATH ~= s,/,\\,g
    }

    win32-msvc* {
        !equals(MSVC_VER, 15.0){
            msvcp.path = $$BINPATH
            vcruntime.path = $$BINPATH
            concrt.path = $$BINPATH

            !contains(QMAKE_TARGET.arch, x86_64) {
                SYSTEM_DIR = $$(SYSTEMROOT)\SysWOW64
            } else {
                SYSTEM_DIR = $$(SYSTEMROOT)\Sysnative
            }

            MSVC_VER = $$(VisualStudioVersion)
            equals(MSVC_VER, 12.0){
                MSDLLVER = 120
            }
            equals(MSVC_VER, 14.0){
                MSDLLVER = 140
            }

            msvcp.commands = $$QMAKE_COPY "$${SYSTEM_DIR}\msvcp$${MSDLLVER}.dll" "$$BINPATH"
            vcruntime.commands = $$QMAKE_COPY "$${SYSTEM_DIR}\vcruntime$${MSDLLVER}.dll" "$$BINPATH"
            concrt.commands = $$QMAKE_COPY "$${SYSTEM_DIR}\concrt$${MSDLLVER}.dll" "$$BINPATH"
            INSTALLS += msvcp
            INSTALLS += vcruntime
            INSTALLS += concrt
        }
    }

    FFMPEG_DLL = $$ffmpeg_shared/bin/*.dll
    FFMPEG_DLL ~= s,/,\\,g

    ffmpeg.path = $$BINPATH
    ffmpeg.commands =  $$QMAKE_COPY $$FFMPEG_DLL $$BINPATH

    INSTALLS += ffmpeg

    !isEmpty(opencv) {
        equals(MSVC_VER, 12.0){
            OPENCV_VSVER = vc12
        }
        equals(MSVC_VER, 14.0){
            OPENCV_VSVER = vc14
        }
        equals(MSVC_VER, 15.0){
            OPENCV_VSVER = vc15
        }

        !contains(QMAKE_TARGET.arch, x86_64) {
            OPENCV_BIN = $$opencv\x86\\$$OPENCV_VSVER\bin
        } else {
            OPENCV_BIN = $$opencv\x64\\$$OPENCV_VSVER\bin
        }
        CONFIG( debug, debug|release ) {
            OPENCV_DLLS = opencv_world$${opencvVer}d
        } else {
            OPENCV_DLLS = opencv_world$${opencvVer}
        }
        opencvt.path = $$BINPATH
        opencvt.commands = for %I in ($$OPENCV_DLLS) do $$QMAKE_COPY $$OPENCV_BIN\\%I.dll $$BINPATH
        INSTALLS += opencvt
    }

    removelibdev.path = $$BINPATH
    removelibdev.commands = $$QMAKE_DEL_FILE $$BINPATH\stereophotoview*.lib

    INSTALLS += removelibdev

    installscript.path = $$PREFIX

    !contains(QMAKE_TARGET.arch, x86_64) {
        INSTALL_SCRIPT_NAME = windows.iss
        installscript.files = installer/$$INSTALL_SCRIPT_NAME
    } else {
        INSTALL_SCRIPT_NAME = windows_64.iss
        installscript.files = installer/$$INSTALL_SCRIPT_NAME installer/windows.iss
    }
    installscript.files = installer/$$INSTALL_SCRIPT_NAME installer/windows.iss

    INSTALLS += installscript

    archive.path = $$PREFIX
    archive.commands = powershell Compress-Archive -Path \"$$BINPATH\" -DestinationPath \"$${BINPATH}.zip\" -CompressionLevel Optimal -Force
    archive.depends = install

    version_iss.path = $$PREFIX
    version_iss.commands = echo AppVersion=$$APP_VERSION > $$PREFIX/version.iss
    version_iss.depends = install

    installer.path = $$PREFIX
    !isEmpty(InnoSetupDir):installer.commands += $$InnoSetupDir/Compil32.exe /cc $$PREFIX/$$INSTALL_SCRIPT_NAME
    installer.depends = version_iss

    QMAKE_EXTRA_TARGETS = archive version_iss installer install

}
