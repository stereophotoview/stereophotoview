# Подключаем ffmpeg...
windows {
    INCLUDEPATH += $$ffmpeg_dev/include
    LIBS += -L$$ffmpeg_dev/lib
    LIBS += -L$$ffmpeg_shared/bin
}
LIBS += -lavdevice -lavformat -lavfilter -lavcodec -lswresample -lswscale -lavutil

# Подключаем opencv...
unix:!android:!CONFIG(without_opencv) {
    CONFIG += link_pkgconfig
    !isEmpty(opencv_pkg) {
        PKGCONFIG += $$opencv_pkg
    }
    else {
        PKGCONFIG += opencv4
    }
    DEFINES += OPENCV
    CONFIG(opencv_debug): DEFINES += CV_DEBUG
}
!isEmpty(opencv) {
    INCLUDEPATH += $$opencv\include

    MSVC_VER = $$(VisualStudioVersion)
    equals(MSVC_VER, 12.0){
        OPENCV_VSVER = vc12
    }
    equals(MSVC_VER, 14.0){
        OPENCV_VSVER = vc14
    }
    equals(MSVC_VER, 15.0){
        OPENCV_VSVER = vc15
    }
    !contains(QMAKE_TARGET.arch, x86_64) {
        OPENCV_ARCH = x86
    } else {
        OPENCV_ARCH = x64
    }

    LIBS += -L$$opencv\\$$OPENCV_ARCH\\$$OPENCV_VSVER\lib
    LIBS += -L$$opencv\\$$OPENCV_ARCH\\$$OPENCV_VSVER\bin

    CONFIG( debug, debug|release ) {
        LIBS += -lopencv_world$${opencvVer}d
    } else {
        LIBS += -lopencv_world$${opencvVer}
    }
    DEFINES += OPENCV
    CONFIG(opencv_debug) {
        DEFINES += CV_DEBUG
    }
}
