#include "baseeditor.h"

using namespace stereoscopic::gui;

BaseEditor::BaseEditor(int undoLimit, QObject *parent) : QObject(parent)
{
    m_undoStack = new QUndoStack(this);
    m_undoStack->setUndoLimit(undoLimit);
    connect(m_undoStack, SIGNAL(canUndoChanged(bool)), this, SLOT(onCanUndoChanged(bool)));
    connect(m_undoStack, SIGNAL(canRedoChanged(bool)), this, SLOT(onCanRedoChanged(bool)));
    connect(m_undoStack, SIGNAL(indexChanged(int)), this, SLOT(onIndexChanged(int)));

}

BaseEditor::~BaseEditor()
{
    disconnect(m_undoStack, SIGNAL(canUndoChanged(bool)), this, SLOT(onCanUndoChanged(bool)));
    disconnect(m_undoStack, SIGNAL(canRedoChanged(bool)), this, SLOT(onCanRedoChanged(bool)));
    disconnect(m_undoStack, SIGNAL(indexChanged(int)), this, SLOT(onIndexChanged(int)));
    if(m_undoStack)
        delete m_undoStack;
}

void BaseEditor::undo()
{
    if(!canModify()) return;
    m_undoStack->undo();
}

void BaseEditor::redo()
{
    if(!canModify()) return;
    m_undoStack->redo();
}

void BaseEditor::onSave()
{
    m_saveIndex = m_undoStack->index();
    emit modifiedChanged(false);
}

void BaseEditor::push(QUndoCommand *command)
{
    // Если заменяется состояние сохранения, сбрасываем индекс
    if(m_saveIndex == m_undoStack->index() + 1)
        m_saveIndex = 0;
    m_undoStack->push(command);
}
