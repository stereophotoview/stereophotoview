#ifndef BASEEDITOR_H
#define BASEEDITOR_H

#include <QObject>
#include <QUndoStack>

namespace stereoscopic::gui
{
    class BaseEditor : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(bool canUndo READ canUndo NOTIFY canUndoChanged)
            Q_PROPERTY(bool canRedo READ canRedo NOTIFY canRedoChanged)
            Q_PROPERTY(bool modified READ modified NOTIFY modifiedChanged)

        public:
            explicit BaseEditor(int undoLimit, QObject *parent = nullptr);

            virtual ~BaseEditor();

            bool canUndo(){ return m_undoStack->canUndo(); }
            bool canRedo() { return m_undoStack->canRedo(); }
            bool modified() { return m_undoStack->index() != m_saveIndex; }

        public slots:
            void undo();
            void redo();
            void onCanUndoChanged(bool v)
            {
                emit canUndoChanged(v);
            }

            void onCanRedoChanged(bool v)
            {
                emit canRedoChanged(v);
            }

            void onIndexChanged(int)
            {
                emit modifiedChanged(modified());
            }

            void clear()
            {
                m_undoStack->clear();
                m_saveIndex = 0;
                onIndexChanged(0);
            }
            void onSave();


        signals:
            void canUndoChanged(bool);
            void canRedoChanged(bool);
            void modifiedChanged(bool);

        protected:
            int m_saveIndex = 0;
            virtual bool canModify() { return false; }
            void push(QUndoCommand* command);

        private:
            QUndoStack* m_undoStack = nullptr;

    };
};
#endif // BASEEDITOR_H
