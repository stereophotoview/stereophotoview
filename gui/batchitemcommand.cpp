#include "batchitemcommand.h"
#include <QDebug>
#include <QtConcurrent/QtConcurrent>
#include <QFile>

#include <stereoformat.h>
#include <fileFormats/mpoimageformat.h>
#include <fileFormats/jpsimageformat.h>
#include <fileFormats/separateimageformat.h>
#include <colors/coloroperations.h>

using namespace std;
using namespace stereoscopic;
using namespace stereoscopic::gui;
using namespace stereoscopic::fileFormats;
using namespace stereoscopic::colors;

BatchItemCommand::BatchItemCommand(QObject *parent) : QObject(parent)
{
    connect(&w, &QFutureWatcher<int>::finished, this, &BatchItemCommand::handleFinished);
}

void BatchItemCommand::startExecute(QVariantMap parameters)
{
    setRunning(true);
    w.setFuture(QtConcurrent::run(&execute, parameters));
}

BatchItemCommand::BatchResult BatchItemCommand::execute(QVariantMap parameters)
{
    auto destination = parameters["destination"].value<QString>();
    auto existingFileProcessing = parameters["existingFileProcessing"].value<int>();
    if(existingFileProcessing == 0 && QFile::exists(destination))
        return ResultSkipped;

    auto source = parameters["source"].value<QString>();
    auto source2 = parameters["source2"].value<QString>();
    auto destFormat = parameters["destFormat"].value<QString>();

    StereoFormat* sourceFormat = nullptr;
    StereoFormat sf;
    auto sourceLayout = parameters["sourceLayout"].value<int>();
    if(sourceLayout >= 0)
    {
        sf.setLayout((StereoFormat::Layout)sourceLayout);
        sf.setLeftFirst(parameters["sourceLeftFirst"].value<bool>());
        sourceFormat = &sf;
    }

    StereoFormat format(parameters["layout"].value<StereoFormat::Layout>());
    format.setLeftFirst(parameters["leftFirst"].value<bool>());

    Preset preset;
    preset.setStdSize(StandardSizesModel::instance()->find(parameters["stdSizeCode"].value<QString>()));
    preset.setWidth(parameters["width"].value<int>());
    preset.setHeight(parameters["height"].value<int>());
    preset.setJpegQuality(parameters["jpegQuality"].value<int>());
    preset.setStereoFormat(&format);

    shared_ptr<StereoImageSource> imageSource = load(source, source2, sourceFormat);
    if(imageSource.get() == nullptr)
        return ResultLoadError;

    if(parameters["autoAlignEnabled"].value<bool>())
        applyAutoAlign(imageSource, parameters);
    if(parameters["autoColorAdjustEnabled"].value<bool>())
        applyAutoColorAdjust(imageSource, parameters);

    // Для рассчёта результирующего размера
    preset.setLeftSourceSize(imageSource->leftSize());
    preset.setRightSourceSize(imageSource->rightSize());
    return save(imageSource.get(), destFormat, destination, &preset, &format);
}

shared_ptr<StereoImageSource> BatchItemCommand::load(QString source1, QString source2, StereoFormat* format)
{
    QStringList inputFiles;
    if(source1.length() > 0)
        inputFiles << source1;
    if(source2.length() > 0)
        inputFiles << source2;

    if(inputFiles.length() == 2)
    {
        SeparateImageFormat f;
        return shared_ptr<StereoImageSource>(f.load(inputFiles[0] + "\n" + inputFiles[1], format));
    }
    MpoImageFormat mpo;
    JpsImageFormat jps;
    QString filePath = inputFiles[0];
    if(mpo.checkFileName(filePath))
        return shared_ptr<StereoImageSource>(mpo.load(filePath, format));
    if(jps.checkFileName(filePath))
        return shared_ptr<StereoImageSource>(jps.load(filePath, format));
    return shared_ptr<StereoImageSource>(nullptr);
}

void BatchItemCommand::applyAutoAlign(shared_ptr<StereoImageSource> imageSource, QVariantMap &parameters)
{
    AlignParams p = AutoAlign::calcOffset(imageSource.get(), getAutoAlignParams(imageSource, parameters));
    StereoFrame newFrame = imageSource->displayFrame()
            ->aligned(QPointF(p.offset().x(), p.offset().y()))
            .rotated(p.leftAngle(), p.rightAngle());
    imageSource->setOriginalFrame(newFrame);
}

InputAlignParams BatchItemCommand::getAutoAlignParams(shared_ptr<StereoImageSource> imageSource, QVariantMap &parameters)
{
    double estimationSize = parameters["autoAlignEstimationSize"].value<double>();
    double estiationCenterX = parameters["autoAlignEstimationCenterX"].value<double>();
    double estimationCenterY = parameters["autoAlignEstimationCenterY"].value<double>();
    QSize rectSize((double)imageSource->width() * estimationSize, (double)imageSource->height() * estimationSize);
    QPoint rectTopLeft((int)(estiationCenterX * imageSource->width() - rectSize.width() / 2),
                       (int)(estimationCenterY * imageSource->height() - rectSize.height() / 2));
    QRect estimationRect(rectTopLeft, rectSize);
    return InputAlignParams(estimationRect,
                            parameters["autoAlignHorizontalEnabled"].value<bool>(),
            parameters["autoAlignVerticalEnabled"].value<bool>(),
            parameters["autoAlignRotateEnabled"].value<bool>(),
            parameters["autoAlignAccuracy"].value<double>() / 100);
}

void BatchItemCommand::applyAutoColorAdjust(shared_ptr<StereoImageSource> imageSource, QVariantMap &parameters)
{
    StereoFrame *sourceFrame = imageSource.get()->displayFrame();
    StereoFrame frame;
    if(parameters["autoColorAdjustLeft"].value<bool>())
        frame = StereoFrame(
                    ColorOperations::adjustImageColorsToReference(sourceFrame->leftFullView(), sourceFrame->rightFullView(), 1),
                    sourceFrame->rightFullView());
    else
        frame = StereoFrame(
                    sourceFrame->leftFullView(),
                    ColorOperations::adjustImageColorsToReference(sourceFrame->rightFullView(), sourceFrame->leftFullView(), 1));
    imageSource->setOriginalFrame(frame);
}

BatchItemCommand::BatchResult BatchItemCommand::save(StereoImageSource *src, QString destFileFormat, QString filePath, Preset* preset, StereoFormat* format)
{
    QSize destSize = preset->size();
    if(format->layout() == StereoFormat::Default)
    {
        format->setLayout(src->originalFormat()->layout());
        format->setLeftFirst(src->originalFormat()->leftFirst());
    }
    if(destFileFormat == "mpo")
    {
        MpoImageFormat mpo;
        mpo.save(src, filePath, format->leftFirst(), destSize, preset->jpegQuality());
    }
    else if(destFileFormat == "jps" || destFileFormat == "jpeg")
    {
        JpsImageFormat jps;
        jps.save(src, filePath, preset->jpegQuality(), format, destSize);
    }
    else
    {
        return BatchResult::ResultUnknownDestFileFormat;
    }
    return BatchResult::ResultSucess;
}

void BatchItemCommand::handleFinished()
{
    setRunning(false);
    emit finished(w.result());
}

void BatchItemCommand::setRunning(bool value)
{
    if(m_running != value)
    {
        m_running = value;
        emit runningChanged();
    }
}
