#ifndef BATCHITEMCOMMAND_H
#define BATCHITEMCOMMAND_H

#include <QObject>
#include <QVariantMap>
#include <QFutureWatcher>
#include <memory>

#include "stereoimagesource.h"
#include <presetslistmodel.h>
#include <autoalign.h>

using namespace std;

namespace stereoscopic::gui
{
    /**
     * @brief Класс для выполнения команды пактеной обработки
     */
    class BatchItemCommand : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)
        public:
            explicit BatchItemCommand(QObject *parent = nullptr);

            /// Результат выполнения команды
            enum BatchResult
            {
                ResultEmpty,                  //< Задача ещё не запускалась
                ResultInProgress,             //< Процесс выполняется
                ResultSucess,                 //< Успешное выполнена
                ResultUnknownDestFileFormat,  //< Неизвестный формат выходного файла
                ResultLoadError,              //< Ошибка загрузки источника
                ResultSkipped,                //< Пропущен, файл назначения существует
            };
            Q_ENUM(BatchResult)

            bool running() { return m_running; }

        signals:
            void finished(BatchResult result);
            void runningChanged();

        public slots:
            void startExecute(QVariantMap parameters);

        private:
            QFutureWatcher<BatchResult> w;
            bool m_running = false;
            void handleFinished();
            void setRunning(bool value);
            static shared_ptr<StereoImageSource> load(QString source1, QString source2, StereoFormat *format);
            static BatchResult save(StereoImageSource *src, QString destFileFormat, QString filePath, Preset* preset, StereoFormat* format);
            static BatchResult execute(QVariantMap parameters);
            static void applyAutoAlign(shared_ptr<StereoImageSource> imageSource, QVariantMap &parameters);
            static void applyAutoColorAdjust(shared_ptr<StereoImageSource> imageSource, QVariantMap &parameters);
            static InputAlignParams getAutoAlignParams(shared_ptr<StereoImageSource> imageSource, QVariantMap &parameters);
    };
}
#endif // BATCHITEMCOMMAND_H
