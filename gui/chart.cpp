#include "chart.h"
#include <QRect>
#include <QPainter>
#include <QJsonObject>
#include <QMap>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wold-style-cast"
#endif

using namespace stereoscopic::gui;

Chart::Chart(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{

}

QVariantMap Chart::colors()
{
    QVariantMap obj;
    foreach(auto key, m_colors.keys())
        obj[key] = QVariant::fromValue(m_colors[key]);
    return obj;
}

void Chart::setColors(QVariantMap colors)
{
    m_colors.clear();
    foreach(auto key, colors.keys())
        m_colors[key] = colors[key].value<QColor>();
}

void Chart::paint(QPainter *painter)
{
    painter->setPen(Qt::transparent);
    painter->setBrush(background());
    painter->drawRect(0, 0, (int)width() - 1, (int)height() - 1);

    // Выясняем границы диапазонов значений
    QMap<QString, double> minValues;
    QMap<QString, double> maxValues;
    foreach(auto v, m_markers)
    {
        auto point = v.value<QVariantMap>();
        foreach(auto key, point.keys())
        {
            if(key == "pos")
                continue;
            double curValue = point.value(key).toDouble(0);
            if(!minValues.contains(key))
                minValues[key] = curValue;
            if(!maxValues.contains(key))
                maxValues[key] = curValue;
            if(minValues[key] > curValue)
                minValues[key] = curValue;
            if(maxValues[key] < curValue)
                maxValues[key] = curValue;
        }
    }
    // Рисуем графики
    foreach(auto key, minValues.keys())
    {
        if(minValues[key] < maxValues[key])
        {
            double diff = maxValues[key] - minValues[key];
            double minValue = minValues[key];
            painter->setPen(m_colors[key]);
            QPointF oldPoint(-1, -1);
            // Рисуем только те графики, значения которых меняются
            foreach(auto v, m_markers)
            {
                auto point = v.value<QVariantMap>();
                double pos = point["pos"].toDouble(0);
                double value = point[key].toDouble(0);
                QPointF p(pos * width() / m_maxPos, height() - (value - minValue) * height() / diff);
                if(oldPoint.x() >= 0)
                    painter->drawLine(QLineF(oldPoint, p));
                oldPoint = p;
            }
        }
    }
}
