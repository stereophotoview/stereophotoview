#ifndef CHART_H
#define CHART_H

#include <QQuickPaintedItem>
#include <QVariant>
#include <../lib/mathhelper.h>

using namespace stereoscopic::math;

namespace stereoscopic::gui
{
    /**
 * @brief Класс для отрисовки графика
 */
    class Chart : public QQuickPaintedItem
    {
            Q_OBJECT
            Q_PROPERTY(QVariantList markers READ markers WRITE setMarkers NOTIFY markersChanged)
            Q_PROPERTY(double maxPos READ maxPos WRITE setMaxPos NOTIFY maxPosChanged)
            Q_PROPERTY(QVariantMap colors READ colors WRITE setColors NOTIFY colorsChanged)
            Q_PROPERTY(QColor background READ background WRITE setBackground NOTIFY backgroundChanged)

        public:
            Chart(QQuickItem *parent = nullptr);

            QVariantList markers() { return m_markers; }
            void setMarkers(QVariantList markers)
            {
                if(m_markers != markers)
                {
                    m_markers = markers;
                    emit markersChanged();
                    update();
                }
            }

            double maxPos() { return m_maxPos; }
            void setMaxPos(double maxX)
            {
                if(!equal(m_maxPos, maxX))
                {
                    m_maxPos = maxX;
                    emit maxPosChanged();
                }
            }

            QVariantMap colors();
            void setColors(QVariantMap colors);

            QColor background() { return m_background; }
            void setBackground(QColor background)
            {
                if(background != m_background)
                {
                    m_background = background;
                    emit backgroundChanged();
                }
            }

            void paint(QPainter *painter) override;

        signals:
            void markersChanged();
            void maxPosChanged();
            void colorsChanged();
            void backgroundChanged();

        public slots:
        private:
            QVariantList m_markers;
            QColor m_background;
            double m_maxPos;
            QMap<QString, QColor> m_colors;
    };
};
#endif // CHART_H
