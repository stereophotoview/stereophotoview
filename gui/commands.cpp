#include "commands.h"

#include "stereoimagesource.h"
#include "imagehelper.h"
#include "autoalign.h"
#include "colors/coloroperations.h"

using namespace stereoscopic::gui::commands;
using namespace stereoscopic::colors;

void BaseCommand::undo()
{
    m_editor->source()->setOriginalFrame(frameBeforeExecute);
}

void BaseCommand::redo()
{
    if(!frameAfterExecute.isNull())
    {
        m_editor->source()->setOriginalFrame(frameAfterExecute);
    }
    else
    {
        frameBeforeExecute = StereoFrame(*m_editor->source()->displayFrame());
        frameAfterExecute = execute(frameBeforeExecute);
        m_editor->source()->setOriginalFrame(frameAfterExecute);
    }
}

StereoFrame ApplyAlign::execute(StereoFrame source)
{
    return source
            .rotated(m_leftAngle, m_rightAngle)
            .aligned(QPointF(m_parallax, m_vOffset));
}

StereoFrame Crop::execute(StereoFrame source)
{
    return source.cropped(m_rect).withFeatheredBorder(m_borderWidthPercent);
}

StereoFrame SetView::execute(StereoFrame source)
{
    QImage leftImage = !m_leftSource.isNull() ? m_leftSource : source.leftFullView();
    QImage rightImage = !m_rightSource.isNull() ? m_rightSource : source.rightFullView();
    return source.modified(leftImage, rightImage);
}

StereoFrame AutoColorAdjust::execute(StereoFrame source)
{
    if(toRight)
    {
        QImage newLeftImage = ColorOperations::adjustImageColorsToReference(source.leftFullView(), source.rightFullView(), type);
        return source.modified(newLeftImage, source.rightFullView());
    }
    else
    {
        QImage newRightImage = ColorOperations::adjustImageColorsToReference(source.rightFullView(),source.leftFullView(), type);
        return source.modified(source.leftFullView(), newRightImage);
    }
}
