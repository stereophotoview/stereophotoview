#ifndef COMMANDS_H
#define COMMANDS_H

#include <QUndoCommand>
#include <memory>

#include "stereoimageeditor.h"
#include "stereoimagesource.h"
#include "stereoimagesource.h"

namespace stereoscopic::gui::commands {

    class BaseCommand : public QUndoCommand
    {
        public:
            BaseCommand(StereoImageEditor* editor, QUndoCommand *parent = nullptr)
                : QUndoCommand(parent)
            {
                m_editor = editor;
            }

            ~BaseCommand() override
            {
            }

            void redo() Q_DECL_OVERRIDE;
            void undo() Q_DECL_OVERRIDE;

            virtual StereoFrame execute(StereoFrame source) = 0;

        protected:
            StereoImageEditor* m_editor;
            StereoFrame frameBeforeExecute, frameAfterExecute;
            void beforeRedo();
    };

    class ApplyAlign : public BaseCommand
    {
        public:
            ApplyAlign(StereoImageEditor* model, double parallax, double vOffset, double leftAngle, double rightAngle, QUndoCommand *parent = nullptr)
                : BaseCommand(model, parent)
            {
                m_parallax = parallax;
                m_vOffset = vOffset;
                m_leftAngle = leftAngle;
                m_rightAngle = rightAngle;
            }

            StereoFrame execute(StereoFrame source) Q_DECL_OVERRIDE;

        private:
            double m_parallax, m_vOffset;
            double m_leftAngle = 0;
            double m_rightAngle = 0;

    };

    class Crop : public BaseCommand
    {
        public:
            Crop(StereoImageEditor* model, QRect rect, int borderWidthPercents, QUndoCommand *parent = nullptr)
                : BaseCommand(model, parent)
            {
                m_rect = rect;
                m_borderWidthPercent = borderWidthPercents;
            }

            StereoFrame execute(StereoFrame source) Q_DECL_OVERRIDE;

        private:
            QRect m_rect;
            int m_borderWidthPercent;

    };

    class AutoColorAdjust : public BaseCommand
    {
        public:
            AutoColorAdjust(StereoImageEditor* model, bool toRight, int type, QUndoCommand *parent = nullptr)
                : BaseCommand(model, parent)
            {
                this->toRight = toRight;
                this->type = type;
            }

            StereoFrame execute(StereoFrame source) Q_DECL_OVERRIDE;

        private:
            bool toRight;
            int type = 0;

    };

    class SetView : public BaseCommand
    {
        public:
            SetView(StereoImageEditor* model, QImage *leftSource, QImage *rightSource, QUndoCommand *parent = nullptr)
                : BaseCommand(model, parent)
            {
                if(leftSource)
                    m_leftSource = QImage(*leftSource);
                if(rightSource)
                    m_rightSource = QImage(*rightSource);
            }

            StereoFrame execute(StereoFrame source) Q_DECL_OVERRIDE;

        private:
            QImage m_leftSource;
            QImage m_rightSource;

    };

}

#endif // COMMANDS_H
