#include "consoleargumentsparser.h"

#include <QCommandLineParser>
#include <QUrl>
#include <QMetaEnum>
#include <QIncompatibleFlag>
#include <QTextStream>
#include "fileFormats/video/mux.h"
#include "fileFormats/video/videooptions.h"
#include <QFileInfo>
#include <QTextCodec>
#include <QProcess>

using namespace stereoscopic::fileFormats::video;
using namespace stereoscopic::gui;

ConsoleArgumentsParser::ConsoleArgumentsParser()
    : QObject(nullptr)
{

}

ConsoleArgumentsParser::ConsoleArgumentsParser(QApplication &app)
    : QObject(nullptr)
{
    m_app = &app;
    QMetaEnum layoutEnum = QMetaEnum::fromType<StereoFormat::Layout>();

    QCommandLineParser parser;

    parser.setApplicationDescription(tr("A viewer/editor for stereoscopic photo and video.") + "\r\n"
                                     + "https://stereophotoview.bitbucket.io" + tr("/en"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("input", tr("Input file."));
    parser.addPositionalArgument("input2", tr("Optional second input file to load a separate stereo pair."));

    VideoOptions vo;

    QCommandLineOption batchOption("batch", tr("Run in batch mode."));
    parser.addOption(batchOption);

    QCommandLineOption inputLayoutOption(QStringList() << "input-layout",
            tr("Input stereo layout (see --layouts) - optinal."), "layout", "Auto");
    parser.addOption(inputLayoutOption);

    QCommandLineOption inputRightFirstOption(QStringList() << "input-revert",
              tr("Right-eye view first in the input file - optional."), "1|0");
    parser.addOption(inputRightFirstOption);

    QCommandLineOption layoutListOption(QStringList() << "l" << "layouts",
            tr("Display a list of layouts."));
    parser.addOption(layoutListOption);

    parser.process(app);

    QTextStream out(stdout);

    if(parser.isSet(layoutListOption))
    {
        out << tr("Input layouts") << ":" << endl;
        for(int i = 0; i < layoutEnum.keyCount(); i++)
            out << "- " << layoutEnum.key(i) << endl;
        out.flush();
        exit(0);
    }

    if(parser.isSet(batchOption))
        m_batchMode = true;

    const QStringList args = parser.positionalArguments();
    for(int i = 0; i < args.length(); i++)
        m_input.append(parseFilePath(args[i]));

    if(parser.isSet(inputLayoutOption))
        m_inputLayout = (StereoFormat::Layout)layoutEnum.keyToValue(parser.value(inputLayoutOption).toLatin1().data());
    if(parser.isSet(inputRightFirstOption))
        m_inputRightFirst = parser.value(inputRightFirstOption).toInt();
}

void ConsoleArgumentsParser::startBatchProcessing()
{
    QProcess p;
    p.startDetached(m_app->applicationFilePath(), QStringList("--batch"));
}

QString ConsoleArgumentsParser::parseFilePath(QString argv)
{
    QUrl url(argv);
    if(url.isValid() && url.isLocalFile())
        return url.toLocalFile();
    return argv;
}
