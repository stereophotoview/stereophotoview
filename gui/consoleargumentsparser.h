#ifndef CONSOLEARGUMENTSPARSER_H
#define CONSOLEARGUMENTSPARSER_H

#include <QObject>
#include <QStringList>
#include <QApplication>
#include "stereoformat.h"
#include <memory>

using namespace std;
using namespace stereoscopic;

namespace stereoscopic::gui
{
    // Консольный конвертер стерео фотографий
    class ConsoleArgumentsParser : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QStringList inputFilePaths READ inputFilePaths)
            Q_PROPERTY(StereoFormat::Layout inputLayout READ inputLayout)
            Q_PROPERTY(int inputRightFirst READ inputRightFirst)
            Q_PROPERTY(bool batchMode READ batchMode)
        public:
            explicit ConsoleArgumentsParser();
            explicit ConsoleArgumentsParser(QApplication &app);
            QStringList inputFilePaths(){ return m_input; }
            StereoFormat::Layout inputLayout(){ return m_inputLayout; }
            // -1: not set, 0: false, 1: true
            int inputRightFirst(){ return m_inputRightFirst; }
            /// Режим пакетной обработки
            bool batchMode() { return m_batchMode; }

        public slots:
            void startBatchProcessing();

        private:
            QStringList m_input;
            QString parseFilePath(QString argv);
            StereoFormat::Layout m_inputLayout = (StereoFormat::Layout)-1;
            int m_inputRightFirst = -1;
            bool m_batchMode = false;
            QApplication* m_app;
    };
};
#endif // CONSOLEARGUMENTSPARSER_H
