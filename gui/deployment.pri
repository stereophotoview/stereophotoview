include(../common_deployment.pri)

unix:!android {
    isEmpty(PREFIX) {
        PREFIX = /usr
    }

    isEmpty(target.path) {
        target.path = $$PREFIX/bin

        desktop.path = $$PREFIX/share/applications
        desktop.files = applications/stereophotoview.desktop \
            applications/stereophotobatchprocessing.desktop

        appicons.path = $$PREFIX/share/icons/hicolor/scalable/apps
        appicons.files = images/stereophotoview.svg

        INSTALLS += target
        INSTALLS += desktop
        INSTALLS += appicons

        CONFIG(register-mime-types){
            mime.path = $$PREFIX/share/mime/packages
            mime.files = mime/stereophotoview.xml
            mimeicons.path = $$PREFIX/share/icons/hicolor/scalable/mimetypes
            mimeicons.files += images/image-mpo.svg
            INSTALLS += mime
            INSTALLS += mimeicons
        }

        CONFIG(post-install) {
            post-install.path = $$INSTALL_ROOT$$PREFIX/share/mime
            post-install.commands = update-desktop-database \${INSTALL_ROOT}$$PREFIX/share/applications; update-mime-database \${INSTALL_ROOT}$$PREFIX/share/mime
            post-install.uninstall = mkdir \${INSTALL_ROOT}$$PREFIX/share/mime/packages; update-desktop-database \${INSTALL_ROOT}$$PREFIX/share/applications; update-mime-database \${INSTALL_ROOT}$$PREFIX/share/mime
            INSTALLS += post-install
        }
    }
    QMAKE_EXTRA_TARGETS = install
}

windows {
    target.path = $$BINPATH
    INSTALLS += target

    windeploy.path = $$BINPATH
    windeploy.commands = $$[QT_INSTALL_BINS]/windeployqt.exe $$BINPATH/stereophotoview.exe --qmldir $$[QT_INSTALL_PREFIX]/qml
    INSTALLS += windeploy

    # workaround: windeployqt does not copy Qt.labs.platform
    platforms.path = $$BINPATH
    platforms.commands = $$QMAKE_COPY_DIR \"$$[QT_INSTALL_QML]/Qt/labs/platform\" \"$$BINPATH/Qt/labs/platform\"
    INSTALLS += platforms

    QMAKE_EXTRA_TARGETS = install
}

