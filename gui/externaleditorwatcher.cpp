#include "externaleditorwatcher.h"
#include <QImage>
#include <QUrl>
#include <QDir>
#include <QFile>
#include <QApplication>
#include <QFileInfo>
#include <stdlib.h>
#include <QProcessEnvironment>
#include <QRegularExpression>
#ifdef Q_OS_WIN
#include <Windows.h>
#include <QTextCodec>
#endif

using namespace stereoscopic::gui;

ExternalEditorWatcher::ExternalEditorWatcher(QImage *pm, QString command, QString name, QObject *parent) : QObject(parent)
{
    m_command = command;
    m_name = name;
    createTempFile();
    if(!pm->save(tempFilePath, "JPEG", 100))
    {

    }
}

ExternalEditorWatcher::~ExternalEditorWatcher()
{
    removeTempFile();
}


void ExternalEditorWatcher::openExternal()
{
#ifdef Q_OS_LINUX
    QString cmd = m_command;
    cmd.replace("%u", tempFilePath, Qt::CaseInsensitive);
    cmd.replace("%f", tempFilePath, Qt::CaseInsensitive);
    cmd.replace("%c", tempFilePath, Qt::CaseInsensitive);
    cmd += "&";
    setenv("LD_LIBRARY_PATH", "", 1);
    system(cmd.toLatin1().data());
#endif
#ifdef Q_OS_WIN
    QDir::setCurrent(QDir::tempPath());
    QString cmd = insertEnv(m_command).arg(tempFilePath);
    QTextCodec *codec = QTextCodec::codecForName("CP1251");
    WinExec(codec->fromUnicode(cmd), SW_SHOWNORMAL);
#endif
}

bool ExternalEditorWatcher::isChanged()
{
    QFileInfo inf(tempFilePath);
    return inf.lastModified() > tempFileCreated;
}

void ExternalEditorWatcher::checkChanged()
{
    QFileInfo inf(tempFilePath);
    auto l = inf.lastModified();
    if(l > lastChangedTime)
    {
        lastChangedTime = l;
        emit isChangedChanged(true);
        emit changed();
    }
}

void ExternalEditorWatcher::confirm()
{
    QImage pixmap;
    pixmap.load(tempFilePath);
    emit import(&pixmap);
}

void ExternalEditorWatcher::createTempFile()
{
    QString tempPath = QDir::tempPath();
    QString appName = QApplication::applicationName();
    QString filePath;
    QFile* f = 0;
    do{
        filePath = QDir::cleanPath(tempPath + QDir::separator() + appName + getRandomString(10) + ".jpg");
        if(f) delete f;
        f = new QFile(filePath);
    }while(f->exists());
    f->open(QIODevice::WriteOnly);
    f->close();
    delete f;
    tempFilePath = filePath;
    QFileInfo inf(tempFilePath);
    lastChangedTime = tempFileCreated = inf.lastModified();
}

void ExternalEditorWatcher::removeTempFile()
{
    QFile f(tempFilePath);
    f.remove();
    f.close();
}

QString ExternalEditorWatcher::getRandomString(int length)
{
    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

    QString randomString;
    for(int i = 0; i < length; ++i)
    {
        int index = qrand() % possibleCharacters.length();
        QChar nextChar = possibleCharacters.at(index);
        randomString.append(nextChar);
    }
    return randomString;
}

QString ExternalEditorWatcher::insertEnv(QString str)
{
    QString res = str;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
#ifdef Q_OS_WIN
    QRegularExpression r("\\%(.*?)\\%");
#else
    QRegularExpression r("\\$(.*)");
#endif
    QRegularExpressionMatchIterator i = r.globalMatch(str);
    while (i.hasNext())
    {
        QRegularExpressionMatch match = i.next();
        QString key = match.captured(1);
        QString val = env.value(key);
#ifdef Q_OS_WIN
        res = res.replace("%" + key + "%", val);
#else
        res = res.replace("$" + key, val);
#endif
    }
    return res;
}
