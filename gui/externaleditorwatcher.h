#ifndef EXTERNALEDITORWATCHER_H
#define EXTERNALEDITORWATCHER_H

#include <QObject>
#include <QDateTime>

namespace stereoscopic::gui
{
    class ExternalEditorWatcher : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QString filePath READ filePath NOTIFY filePathChanged)
            Q_PROPERTY(bool isChanged READ isChanged NOTIFY isChangedChanged)

        public:
            explicit ExternalEditorWatcher(QObject *parent = nullptr): QObject(parent) { }
            explicit ExternalEditorWatcher(QImage* pm, QString command, QString name, QObject *parent = nullptr);
            ~ExternalEditorWatcher();

            QImage* pixmap(){ return m_pixmap; }
            QString filePath(){ return tempFilePath; }
            bool isChanged();

        signals:
            void pixmapChanged(QImage*);
            void filePathChanged(QString);
            void isChangedChanged(bool);
            void changed();
            void import(QImage*);

        public slots:
            void openExternal();
            void checkChanged();
            void confirm();

        private:
            QString tempFilePath;
            QImage* m_pixmap = nullptr;
            QDateTime tempFileCreated;
            QDateTime lastChangedTime;
            QString m_command;
            QString m_name;

            void createTempFile();
            void removeTempFile();
            QString getRandomString(int length);
            QString insertEnv(QString str);
    };
};
#endif // EXTERNALEDITORWATCHER_H
