#include "favoritesmodel.h"
#include <QFileInfo>
#include "settings.h"
#include "urlconv.h"

using namespace stereoscopic::gui;

FavoritesModel::FavoritesModel(QObject *parent)
    : QAbstractItemModel(parent)
{
    QStringList list = Settings::instance().favorites();
    foreach(QString path, list)
    {
        QFileInfo f(path);
        m_list.append(QPair<QString, QString>(f.fileName(), path));
    }
}

QVariant FavoritesModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > m_list.count())
            return QVariant();
    const QPair<QString,QString> & p = m_list[index.row()];
    if (role == NameRole)
        return p.first;
    else if (role == PathRole)
        return p.second;
    return QVariant();
}

int FavoritesModel::rowCount(const QModelIndex& parent) const
{
    if(!parent.isValid())
        return m_list.length();
    return 0;
}

QModelIndex FavoritesModel::index(int row, int column, const QModelIndex &) const
{
    return QAbstractItemModel::createIndex(row, column, (quintptr)row);
}

QModelIndex FavoritesModel::parent(const QModelIndex &) const
{
    return QAbstractItemModel::createIndex(-1, -1, (quintptr)-1);
}

int FavoritesModel::columnCount(const QModelIndex &) const
{
    return 2;
}

QHash<int, QByteArray> FavoritesModel::roleNames() const
{
    QHash<int, QByteArray> r;
    r.insert(NameRole, "Name");
    r.insert(PathRole, "Path");
    return r;
}

void FavoritesModel::add(QString path)
{
    beginResetModel();
    QFileInfo f(path);
    m_list.append(QPair<QString, QString>(f.fileName(), path));
    saveSettings();
    endResetModel();
}

void FavoritesModel::remove(int row)
{
    beginResetModel();
    m_list.removeAt(row);
    saveSettings();
    endResetModel();
}

QModelIndex FavoritesModel::pathIndex(QString path)
{
    UrlConv u;
    int p = -1;
    int length = -1;
    for(int i = 0; i < m_list.length(); i++)
    {
        QString cp = path;
        while(!cp.isEmpty())
        {
            if(u.equals(cp, m_list[i].second))
            {
                int l = cp.length();
                if(l > length)
                {
                    p = i;
                    length = l;
                }
                break;
            }
            cp = u.parent(cp);
        }
    }
    return index(p, 0);
}

void FavoritesModel::saveSettings()
{
    QStringList list;
    for(int i = 1; i < m_list.length(); i++)
        list.append(m_list[i].second);
    Settings::instance().setFavorites(list);
}
