#ifndef FAVORITESMODEL_H
#define FAVORITESMODEL_H

#include <QAbstractItemModel>
#include <QList>
#include <QPair>
#include <QString>

namespace stereoscopic::gui
{
    class FavoritesModel : public QAbstractItemModel
    {
            Q_OBJECT
            Q_PROPERTY(QString recentFoldersPath READ recentFoldersPath)
        public:
            explicit FavoritesModel(QObject *parent = nullptr);
            enum Roles
            {
                NameRole = Qt::DisplayRole,
                PathRole = Qt::UserRole + 1,
            };
            Q_ENUM(Roles)
            QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
            int rowCount(const QModelIndex &) const override;
            QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
            QModelIndex parent(const QModelIndex &index) const override;
            int columnCount(const QModelIndex &parent = QModelIndex()) const override;
            QHash<int,QByteArray> roleNames() const override;
            QString recentFoldersPath(){ return _recentFoldersPath; }

        public slots:
            void add(QString path);
            void remove(int row);
            QModelIndex pathIndex(QString path);
        private:
            const QString _recentFoldersPath = "recentFolders:";
            QList<QPair<QString,QString>> m_list = {
                QPair<QString,QString>(tr("Recent folders"), _recentFoldersPath),
            };
            void saveSettings();
    };
};
#endif // FAVORITESMODEL_H
