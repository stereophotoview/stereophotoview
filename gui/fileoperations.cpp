#include "fileoperations.h"
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>

using namespace stereoscopic::gui;

#ifdef Q_OS_WIN32

#include "Windows.h"

bool FileOperations::moveToTrash(QString file)
{
    QFileInfo fileinfo( file );
    if( !fileinfo.exists() )
    {
        qWarning() << "File doesnt exists, cant move to trash";
        return false;
    }
    WCHAR from[ MAX_PATH ];
    memset( from, 0, sizeof( from ));
    int l = fileinfo.absoluteFilePath().toWCharArray( from );
    Q_ASSERT( 0 <= l && l < MAX_PATH );
    from[ l ] = '\0';
    SHFILEOPSTRUCT fileop;
    memset( &fileop, 0, sizeof( fileop ) );
    fileop.wFunc = FO_DELETE;
    fileop.pFrom = from;
    fileop.fFlags = FOF_ALLOWUNDO | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT;

    int rv = SHFileOperation( &fileop );
    if( 0 != rv ){
        qWarning() << rv << QString::number( rv ).toInt( 0, 8 );
        return false;
    }
    return true;
}


#endif

#ifdef Q_OS_LINUX

#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QDateTime>
#include <QUrl>

bool FileOperations::moveToTrash(QString file)
{
    QString TrashPath;
    QString TrashPathInfo;
    QString TrashPathFiles;

    QStringList paths;
    const char* xdg_data_home = getenv( "XDG_DATA_HOME" );
    if( xdg_data_home ){
        qWarning() << "XDG_DATA_HOME not yet tested";
        QString xdgTrash( xdg_data_home );
        paths.append( xdgTrash + "/Trash" );
    }
    QString home = QStandardPaths::writableLocation( QStandardPaths::HomeLocation );
    paths.append( home + "/.local/share/Trash" );
    paths.append( home + "/.trash" );
    foreach( QString path, paths ){
        if( TrashPath.isEmpty() ){
            QDir dir( path );
            if( dir.exists() ){
                TrashPath = path;
            }
        }
    }
    if( TrashPath.isEmpty() )
    {
        qWarning() << "Cant detect trash folder";
        return false;
    }
    TrashPathInfo = TrashPath + "/info";
    TrashPathFiles = TrashPath + "/files";
    if( !QDir( TrashPathInfo ).exists() || !QDir( TrashPathFiles ).exists() )
    {
        qWarning() << "Trash doesnt looks like FreeDesktop.org Trash specification";
        return false;
    }

    QFileInfo original( file );
    if( !original.exists() )
    {
        qWarning() << "File doesnt exists, cant move to trash";
        return false;
    }
    QString info;
    info += "[Trash Info]\nPath=";
    info += QString(QUrl::toPercentEncoding(original.absoluteFilePath(), QByteArray("/", 1)));
    info += "\nDeletionDate=";
    info += QDateTime::currentDateTime().toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    info += "\n";
    QString trashname = original.fileName();
    QString infopath = TrashPathInfo + "/" + trashname + ".trashinfo";
    QString filepath = TrashPathFiles + "/" + trashname;
    int nr = 0;
    while( QFileInfo( infopath ).exists() || QFileInfo( filepath ).exists() ){
        nr++;
        trashname = original.baseName() + " (" + QString::number( nr ) + ")";
        if( !original.completeSuffix().isEmpty() ){
            trashname += QString( "." ) + original.completeSuffix();
        }
        infopath = TrashPathInfo + "/" + trashname + ".trashinfo";
        filepath = TrashPathFiles + "/" + trashname;
    }
    QDir dir;
    if( !dir.rename( original.absoluteFilePath(), filepath ) )
    {
        qWarning() << "move to trash failed";
        return false;
    }
    QFile infofile(infopath);
    infofile.open(QFile::Truncate|QFile::Text|QFile::WriteOnly);
    QTextStream outStream(&infofile);
    outStream << info;
    outStream.flush();
    infofile.close();
    return true;
}
#endif

bool FileOperations::exists(QString fileName)
{
    QFile file(fileName);
    return file.exists();
}

FileOperations::FileOperations() : QObject()
{

}

bool FileOperations::copy(QString source, QString dest)
{
#if TRACE
    qDebug() << "copy" << source << dest;
#endif
    return QFile::copy(source, dest);
}

bool FileOperations::move(QString source, QString dest)
{
#if TRACE
    qDebug() << "move" << source << dest;
#endif
    QFile f(source);
    return f.rename(dest);
}

