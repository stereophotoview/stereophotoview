#ifndef FILEOPERATIONS_H
#define FILEOPERATIONS_H

#include <QObject>

namespace stereoscopic::gui
{
    class FileOperations : public QObject
    {
            Q_OBJECT

        public:
            explicit FileOperations();

        signals:

        public slots:
            bool copy(QString, QString);
            bool move(QString, QString);
            bool moveToTrash(QString);
            bool exists(QString);

        private:
    };
};
#endif // FILEOPERATIONS_H
