#include "folderstreemodel.h"

using namespace stereoscopic::gui;

FoldersTreeModel::FoldersTreeModel(QObject *parent)
    : QFileSystemModel(parent)
{
#ifdef Q_OS_WIN
    setRootPath("");
#else
    setRootPath("/");
#endif
    setResolveSymlinks(true);
    setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
}

QVariant FoldersTreeModel::data(const QModelIndex &index, int role) const
{
    return QFileSystemModel::data(index, role);
}

Qt::ItemFlags FoldersTreeModel::flags(const QModelIndex &index) const
{
    return QFileSystemModel::flags(index);
}

QVariant FoldersTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    return QFileSystemModel::headerData(section, orientation, role);
}

QModelIndex FoldersTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    return QFileSystemModel::index(row, column, parent);
}

QModelIndex FoldersTreeModel::parent(const QModelIndex &index) const
{
    return QFileSystemModel::parent(index);
}

int FoldersTreeModel::rowCount(const QModelIndex &parent) const
{
    return QFileSystemModel::rowCount(parent);
}

int FoldersTreeModel::columnCount(const QModelIndex &parent) const
{
    return QFileSystemModel::columnCount(parent);
}

QHash<int, QByteArray> FoldersTreeModel::roleNames() const
{
    return QFileSystemModel::roleNames();
}

QModelIndex FoldersTreeModel::rootIndex()
{
    return QFileSystemModel::index(rootPath());
}

QModelIndex FoldersTreeModel::pathIndex(QString path)
{
    return QFileSystemModel::index(path, 0);
}

QString FoldersTreeModel::filePath(const QModelIndex &index)
{
    return QFileSystemModel::filePath(index);
}

bool FoldersTreeModel::isValidIndex(const QModelIndex &index)
{
    return index.isValid();
}
