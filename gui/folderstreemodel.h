#ifndef FOLDERSTREEMODEL_H
#define FOLDERSTREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include "standardpaths.h"
#include <QFileSystemModel>

namespace stereoscopic::gui
{
    class FoldersTreeModel : public QFileSystemModel
    {
            Q_OBJECT
            Q_PROPERTY(QModelIndex rootIndex READ rootIndex)

        public:
            explicit FoldersTreeModel(QObject* parent = NULL);

            QVariant data(const QModelIndex &index, int role) const override;
            Qt::ItemFlags flags(const QModelIndex &index) const override;
            QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const override;
            QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const override;
            QModelIndex parent(const QModelIndex &index) const override;
            int rowCount(const QModelIndex &parent = QModelIndex()) const override;
            int columnCount(const QModelIndex &parent = QModelIndex()) const override;
            QHash<int,QByteArray> roleNames() const override;

            QModelIndex rootIndex();
            Q_INVOKABLE QModelIndex pathIndex(QString path);
            Q_INVOKABLE QString filePath(const QModelIndex &index);
            Q_INVOKABLE bool isValidIndex(const QModelIndex &index);

    };
};
#endif // FOLDERSTREEMODEL_H
