TEMPLATE = app

TARGET = stereophotoview

RC_ICONS = images/appicon.ico
QMAKE_TARGET_PRODUCT = StereoPhotoView
QMAKE_TARGET_DESCRIPTION = Viewer/editor for stereoscopic photo and video
include(../common.pri)

QT += qml quick widgets multimedia

SOURCES += main.cpp \
    batchitemcommand.cpp \
    stereoimage.cpp \
    settings.cpp \
    fileoperations.cpp \
    myfiledialog.cpp \
    standardpaths.cpp \
    commands.cpp \
    stereoimageeditor.cpp \
    externaleditorwatcher.cpp \
    mimetypeinfo.cpp \
    previewimageprovider.cpp \
    stereovideoeditor.cpp \
    baseeditor.cpp \
    videocommands.cpp \
    qpaletteserializer.cpp \
    consoleargumentsparser.cpp \
    folderstreemodel.cpp \
    favoritesmodel.cpp \
    recentfolderslistmodel.cpp \
    onlineversionrequest.cpp \
    presetslistmodel.cpp \
    chart.cpp

lupdate_only{
    SOURCES += qml/*.qml \
        qml/*.js
}

RESOURCES += \
    qml/qml.qrc \
    images.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

HEADERS += \
    batchitemcommand.h \
    stereoimage.h \
    settings.h \
    fileoperations.h \
    myfiledialog.h \
    standardpaths.h \
    commands.h \
    stereoimageeditor.h \
    externaleditorwatcher.h \
    mimetypeinfo.h \
    previewimageprovider.h \
    stereovideoeditor.h \
    baseeditor.h \
    videocommands.h \
    qpaletteserializer.h \
    consoleargumentsparser.h \
    folderstreemodel.h \
    favoritesmodel.h \
    recentfolderslistmodel.h \
    onlineversionrequest.h \
    presetslistmodel.h \
    chart.h


DISTFILES += \
    applications/stereophotoview.desktop \
    images/appicon.ico \
    images/appicon.xcf \
    images/image-mpo.svg \
    README.md \
    README_RU.md \
    mime/stereophotoview.xml \
    images/mode-interlaced \
    stereophotoview.xmi \
    applications/stereophotobatchprocessing.desktop

DEPLOYMENT_PLUGIN += qjpeg

unix:!android: include(themes.prf)

DEPENDPATH += ../lib
INCLUDEPATH += ../lib
windows {
    LIBS += -lstereophotoview1
    CONFIG(debug):LIBS += -L../lib/debug
    CONFIG(release):LIBS += -L../lib/release
}
unix {
    LIBS += -L../lib -lstereophotoview

    QMAKE_RPATHDIR += $ORIGIN/../lib
}

# Подключаем сторонние библиотеки
include(../external_libs.pri)

# update translation files
include(translations.prf)

# Default rules for deployment.
include(deployment.pri)
