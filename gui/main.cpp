#include <QApplication>
#include <QtQuick/QQuickView>
#include <QQmlApplicationEngine>
#include <QDir>
#include <QQmlContext>
#include <QTranslator>
#include <QStyleFactory>
#include <QDebug>
#include <QPalette>
#include <QLocale>

#ifdef Q_OS_UNIX
#include <QProcessEnvironment>
#endif

#include "stereoimage.h"
#include "settings.h"
#include "urlconv.h"
#include "standardpaths.h"
#include "fileoperations.h"
#include "myfiledialog.h"
#include "stereoimageeditor.h"
#include "stereovideoeditor.h"
#include "externaleditorwatcher.h"
#include "mimetypeinfo.h"
#include "previewimageprovider.h"
#include "mediaplayer/mediaplayer.h"
#include "fileFormats/video/taskqueue.h"
#include "qpaletteserializer.h"
#include "consoleargumentsparser.h"
#include "lib_global.h"
#include <stereolib.h>
#include <folderstreemodel.h>
#include <favoritesmodel.h>
#include <recentfolderslistmodel.h>
#include <presetslistmodel.h>
#include "onlineversionrequest.h"
#include <presetslistmodel.h>
#include <chart.h>
#include <batchitemcommand.h>

using namespace stereoscopic::gui;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QString app_version = APP_VERSION;
    QCoreApplication::setApplicationName(APP_NAME);
    QCoreApplication::setOrganizationName(APP_NAME);
    QCoreApplication::setApplicationVersion(app_version);

    MimeTypeInfo jpegMimeType("image/jpeg");

    QString qt_version = QT_VERSION_STR;
    QString ffmpeg_version = FFMPEG_VERSION;

    Settings settings;
    QString lang = settings.language();
    if(lang.length() == 0)
        lang = QLocale::system().name();
    QLocale::setDefault(QLocale(lang));

    QTranslator appTranslator;
    if(appTranslator.load(lang, ":/translations"))
        app.installTranslator(&appTranslator);

    QTranslator libTranslator;
    if(libTranslator.load(lang, ":/lib_translations"))
        app.installTranslator(&libTranslator);

    StereoLib::qmlRegisterTypes();
    qmlRegisterType<StereoImage>("StereoImage", 1, 0, "StereoImage");
    qmlRegisterUncreatableType<BaseEditor>("StereoImage", 1, 0, "BaseEditor", "It is abstract class");
    qmlRegisterType<StereoImageEditor>("StereoImage", 1, 0, "StereoImageEditor");
    qmlRegisterType<StereoVideoEditor>("StereoImage", 1, 0, "StereoVideoEditor");
    qmlRegisterType<stereoscopic::gui::ExternalEditorWatcher>("StereoImage", 1, 0, "ExternalEditorWatcher");
    qmlRegisterType<MyFileDialog>("MyDialogs", 1, 0, "MyFileDialog");
    qmlRegisterType<ConsoleArgumentsParser>("StereoImage", 1, 0, "ConsoleArgumentsParser");
    qmlRegisterType<OnlineVersionRequest>("StereoImage", 1, 0, "OnlineVersionRequest");
    qmlRegisterUncreatableType<StandardSizeItem>("StereoImage", 1, 0, "StandardSizeItem", "Can not create");
    qmlRegisterUncreatableType<StandardSizesModel>("StereoImage", 1, 0, "StandardSizesModel", "Can not create");
    qmlRegisterType<Preset>("StereoImage", 1, 0, "Preset");
    qmlRegisterType<PresetsListModel>("PresetsListModel", 1, 0, "Preset");
    qmlRegisterType<Chart>("StereoImage", 1, 0, "Chart");
    qmlRegisterType<BatchItemCommand>("StereoImage", 1, 0, "BatchItemCommand");

    qmlRegisterUncreatableType<FoldersTreeModel>("StereoImage", 1, 0, "FolderTreeModel", "Can not create");
    qmlRegisterUncreatableType<FavoritesModel>("StereoImage", 1, 0, "FavoritesModel", "Can not create");
    qmlRegisterUncreatableType<RecentFoldersListModel>("StereoImage", 1, 0, "RecentFoldersListModel", "Can not create");
    qmlRegisterUncreatableType<PresetsListModel>("StereoImage", 1, 0, "PresetsListModel", "Can not create");

    ConsoleArgumentsParser args(app);

    QQmlApplicationEngine engine;

#ifdef ALLOW_COLOR_THEMES
    QString theme = settings.colorTheme();
    if(theme.length() > 0)
    {
        QString filePath = ":/themes/" + theme + ".ini";
        QPalette p = QPaletteSerializer::deserialize(filePath);
        app.setPalette(p);
    }
    engine.rootContext()->setContextProperty("ALLOW_COLOR_THEMES", true);
#else
    engine.rootContext()->setContextProperty("ALLOW_COLOR_THEMES", nullptr);
#endif

    UrlConv urlConv;
    StandardPaths standardPaths;
    FileOperations fileOpeartions;
    StereoFormat stereoFormat;

    engine.rootContext()->setContextProperty("appVersion", app_version);
    engine.rootContext()->setContextProperty("QtVersion", qt_version);
    engine.rootContext()->setContextProperty("FFmpegVersion", ffmpeg_version);
    engine.rootContext()->setContextProperty("startupArguments", &args);
    engine.rootContext()->setContextProperty("settings", &settings);
    engine.rootContext()->setContextProperty("urlConv", &urlConv);
    engine.rootContext()->setContextProperty("standardPaths", &standardPaths);
    engine.rootContext()->setContextProperty("stereoFormat", &stereoFormat);
    engine.rootContext()->setContextProperty("fileOperations", &fileOpeartions);
    engine.rootContext()->setContextProperty("jpegMimeType", &jpegMimeType);
    engine.rootContext()->setContextProperty("taskQueue", TaskQueue::instance());
    engine.rootContext()->setContextProperty("app", &app);
    engine.addImageProvider(QLatin1String("preview"), new PreviewImageProvider());

    engine.rootContext()->setContextProperty("fileSystemModel", new FoldersTreeModel(&engine));
    engine.rootContext()->setContextProperty("favoritesModel", new FavoritesModel(&engine));
    engine.rootContext()->setContextProperty("recentFoldersModel", new RecentFoldersListModel(&engine));
    engine.rootContext()->setContextProperty("jpsPresetsModel", PresetsListModel::JPS(&engine));
    engine.rootContext()->setContextProperty("mpoPresetsModel", PresetsListModel::MPO(&engine));
    engine.rootContext()->setContextProperty("videoPresetsModel", PresetsListModel::Video(&engine));
    engine.rootContext()->setContextProperty("standardSizes", StandardSizesModel::instance());

    if(args.batchMode())
        engine.load(QUrl(QStringLiteral("qrc:/qml/BatchProcessingWindow.qml")));
    else
        engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    app.setWindowIcon(QIcon(":/images/appicon.ico"));
    app.setQuitOnLastWindowClosed(true);
    return app.exec();
}
