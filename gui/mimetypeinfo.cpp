#include "mimetypeinfo.h"

#include <QSettings>
#include <QDir>
#include <QByteArray>
#include <QLocale>
#include "settings.h"

using namespace stereoscopic::gui;

MimeTypeInfo::MimeTypeInfo(QString mimeType, QObject *parent) : QObject(parent)
{
    m_mimeType = mimeType;
}

QVariant MimeTypeInfo::openWithList()
{
    if(!m_openWithList_filled)
    {
        fillOpenWithList();
        m_openWithList_filled = true;
    }
    return QVariant::fromValue(m_openWithList);
}

#ifdef Q_OS_LINUX

void MimeTypeInfo::fillOpenWithList()
{
    QList<AppInfo*> list;

    Settings settings;
    QString locale = settings.language();
    if(locale.length() == 0)
        locale = QLocale::system().name();
    QString lang = locale.split("_")[0];

    foreach(QString path, app_paths)
    {
        QString mimeCachePath = QDir::cleanPath(path + QDir::separator() + "mimeinfo.cache");
        QStringList desktopFiles = readIniParam(mimeCachePath, m_mimeType).split(';');
        foreach(QString desktopFile, desktopFiles)
        {
            QString desktopFilePath = QDir::cleanPath(path + QDir::separator() + desktopFile);
            QSettings desktop(desktopFilePath, QSettings::IniFormat);
            desktop.setIniCodec("UTF-8");
            QString name = desktop.value("Desktop Entry/Name[" + locale + "]").toString();
            if(name.length() == 0)
                name = desktop.value("Desktop Entry/Name[" + lang + "]").toString();
            if(name.length() == 0)
                name = desktop.value("Desktop Entry/Name").toString();
            QString icon = desktop.value("Desktop Entry/Icon").toString();
            QString command = desktop.value("Desktop Entry/Exec").toString();
            if(name.length() != 0 && command.length() != 0)
                list.append(new AppInfo(name, icon, command));
        }
    }
    foreach(AppInfo* a, list)
        m_openWithList.append(a);
}

QString MimeTypeInfo::readIniParam(QString filePath, QString paramName)
{
    QFile f(filePath);
    f.open(QFile::ReadOnly);
    QByteArray line;

    while((line = f.readLine()).length() > 0)
    {
        QString str(line);
        QStringList l = str.split('=');
        if(l.length() == 2 && l[0] == paramName)
            return l[1];
    }
    return QString();
}

#else

#ifdef Q_OS_WIN

void MimeTypeInfo::fillOpenWithList()
{
    QList<AppInfo*> list;
    QStringList progIds;

    QSettings mimeType("HKEY_CLASSES_ROOT\\Mime\\Database\\Content Type\\image/jpeg", QSettings::NativeFormat);
    QString extension = mimeType.value("Extension").toString();

    QString jpegExtPath = "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\" + extension;

    QSettings userChoice(jpegExtPath + "\\UserChoice", QSettings::NativeFormat);
    QString progId = userChoice.value("Progid").toString();
    if(progId.length() > 0)
        progIds.append(progId);

    QSettings openWithList(jpegExtPath + "\\OpenWithList", QSettings::NativeFormat);
    QStringList keys = openWithList.childKeys();
    keys.sort(Qt::CaseInsensitive);
    foreach(QString k, keys)
        progIds.append("Applications\\" + openWithList.value(k).toString());

    QSettings openWithProgids(jpegExtPath + "\\OpenWithProgids", QSettings::NativeFormat);
    keys = openWithProgids.childKeys();
    foreach(QString k, keys)
        progIds.append(k);

    foreach(QString progId, progIds)
    {
        // HKEY_CLASSES_ROOT\Applications\QtProject.QtCreator.c\shell
        QString shellPath = "HKEY_CLASSES_ROOT\\" + progId + "\\shell";
        QSettings shell(shellPath, QSettings::NativeFormat);
        //shell.setIniCodec("UTF-8");
        keys = shell.childGroups();
        foreach(QString s, keys)
        {
            QString name = shell.value(s + "/.").toString();
            QString command = shell.value(s + "/Command/.").toString();
            if(name.length() == 0)
                name = progId.split('\\').last() + " - " + s;
            if(command.length() != 0 && s != "printto" && s != "print")
                list.append(new AppInfo(name, "", command));
        }
    }
    foreach(AppInfo* a, list)
        m_openWithList.append(a);
}

#else

void MimeTypeInfo::fillOpenWithList() { }

#endif

#endif

