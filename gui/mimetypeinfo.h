#ifndef MIMETIMEINFO_H
#define MIMETIMEINFO_H

#include <QObject>
#include <QVariant>
#include <QList>
#include <QString>
#include <QStringList>

namespace stereoscopic::gui
{
    class AppInfo : public QObject {

            Q_OBJECT
            Q_PROPERTY(QString name READ name WRITE setName)
            Q_PROPERTY(QString iconName READ iconName WRITE setIconName)
            Q_PROPERTY(QString command READ command WRITE setCommand)
        public:

            AppInfo(QString name, QString icon, QString command)
            {
                m_name = name;
                m_iconName = icon;
                m_command = command;
            }

            QString name() { return m_name; }

            void setName(QString name) { m_name = name; }

            QString command() { return m_command; }

            void setCommand(QString command) { m_command = command; }

            QString iconName() { return m_iconName; }

            void setIconName(QString iconName) { m_iconName = iconName; }

        private:
            QString m_name;
            QString m_command;
            QString m_iconName;

    };

    class MimeTypeInfo : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QVariant openWithList READ openWithList)

        public:
            explicit MimeTypeInfo(QString mimeType, QObject *parent = 0);
            ~MimeTypeInfo()
            {
                foreach(auto o, m_openWithList)
                    delete o;
            }

            QVariant openWithList();

        signals:

        public slots:

        private:
            QString m_mimeType;
            QList<QObject*> m_openWithList;
            bool m_openWithList_filled = false;
            void fillOpenWithList();

            QStringList app_paths = {
                "/usr/share/applications",
                "/usr/local/share/applications",
                "~/.local/share/applications"
            };
            QString readIniParam(QString filePath, QString paramName);
    };
}
#endif // MIMETIMEINFO_H
