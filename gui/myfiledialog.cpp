#include "myfiledialog.h"
#include "settings.h"

using namespace stereoscopic::gui;

MyFileDialog::MyFileDialog() : QObject(nullptr)
{

}

int MyFileDialog::selectedNameFilterIndex()
{
    for(int i = 0; i < nameFilters_.size(); i++)
    {
        if(nameFilters_[i] == m_selectedNameFilter)
            return i;
    }
    return -1;
}

void MyFileDialog::setSelectedNameFilterIndex(int value)
{
    if(value >= 0 && value < nameFilters_.size())
        setSelectedNameFilter(nameFilters_[value]);
}

void MyFileDialog::open(const QString defaultPath)
{
    if(defaultPath.length() > 0)
    {
        QFileInfo f(defaultPath);
        QFileDialog d(nullptr, title(), f.path());
        d.selectFile(f.fileName());
        exec(d);
    }
    else
    {
        QFileDialog d(nullptr, title(), path());
        exec(d);
    }
}

void MyFileDialog::exec(QFileDialog &d)
{
    QFileDialog::FileMode mode = QFileDialog::AnyFile;
    if(selectExisting())
        mode = allowMultipleFiles() ? QFileDialog::ExistingFiles : QFileDialog::ExistingFile;
    d.setFileMode(mode);
    d.setAcceptMode((QFileDialog::AcceptMode)acceptMode_);
    if(acceptLabel_.length() > 0)
        d.setLabelText(QFileDialog::Accept, acceptLabel_);
    d.setNameFilters(nameFilters_);
    d.selectNameFilter(m_selectedNameFilter);
    d.setDefaultSuffix(defaultSuffix_);

    d.setOption(QFileDialog::DontUseNativeDialog, Settings::instance().dontUseNativeDialog());
    if(d.exec() == QDialog::Accepted)
    {
        QStringList files = d.selectedFiles();
        QString file = files.first();
        setSelectedFile(file);
        setSelectedFiles(files);
        setPath(QFileInfo(file).path());
        setSelectedNameFilter(d.selectedNameFilter());
        emit accepted(file);
    }
    else
    {
        setSelectedNameFilter(d.selectedNameFilter());
        emit rejected();
    }
}

