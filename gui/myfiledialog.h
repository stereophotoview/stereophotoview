#ifndef MYFILEDIALOG_H
#define MYFILEDIALOG_H

#include <QObject>
#include <QFileDialog>

namespace stereoscopic::gui
{
    class MyFileDialog : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
            Q_PROPERTY(bool selectExisting READ selectExisting WRITE setSelectExisting NOTIFY selectExistingChanged)
            Q_PROPERTY(bool allowMultipleFiles READ allowMultipleFiles WRITE setAllowMultipleFiles NOTIFY allowMultipleFilesChanged)
            Q_PROPERTY(QString selectedFile READ selectedFile WRITE setSelectedFile NOTIFY selectedFileChanged)
            Q_PROPERTY(QStringList selectedFiles READ selectedFiles WRITE setSelectedFiles NOTIFY selectedFilesChanged)
            Q_PROPERTY(QString acceptLabel READ acceptLabel WRITE setAcceptLabel NOTIFY acceptLabelChanged)
            Q_PROPERTY(AcceptMode acceptMode READ acceptMode WRITE setAcceptMode NOTIFY acceptModeChanged)
            Q_PROPERTY(QStringList nameFilters READ nameFilters WRITE setNameFilters NOTIFY nameFiltersChanged)
            Q_PROPERTY(QString defaultSuffix READ defaultSuffix WRITE setDefaultSuffix NOTIFY defaultSuffixChanged)
            Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
            Q_PROPERTY(QString selectedNameFilter READ selectedNameFilter WRITE setSelectedNameFilter NOTIFY selectedNameFilterChanged)
            Q_PROPERTY(int selectedNameFilterIndex READ selectedNameFilterIndex WRITE setSelectedNameFilterIndex NOTIFY selectedNameFilterChanged)

        public:
            enum AcceptMode { AcceptOpen, AcceptSave };
            Q_ENUMS(AcceptMode)

            explicit MyFileDialog();

            QString title(){ return title_; }
            void setTitle(QString value)
            {
                title_ = value;
                emit titleChanged(title_);
            }

            bool selectExisting(){
                return selectExisting_;
            }
            void setSelectExisting(bool value)
            {
                selectExisting_ = value;
                emit selectExistingChanged(selectExisting_);
            }

            bool allowMultipleFiles(){
                return allowMultipleFiles_;
            }
            void setAllowMultipleFiles(bool value)
            {
                allowMultipleFiles_ = value;
                emit allowMultipleFilesChanged();
            }

            QString selectedFile(){ return selectedFile_; }
            void setSelectedFile(QString value)
            {
                selectedFile_ = value;
                emit selectedFileChanged(selectedFile_);
            }

            QStringList selectedFiles(){ return selectedFiles_; }
            void setSelectedFiles(QStringList value)
            {
                selectedFiles_ = value;
                emit selectedFilesChanged();
            }

            QString acceptLabel(){ return acceptLabel_; }
            void setAcceptLabel(QString value)
            {
                acceptLabel_ = value;
                emit acceptLabelChanged(acceptLabel_);
            }

            AcceptMode acceptMode(){ return acceptMode_; }
            void setAcceptMode(AcceptMode value)
            {
                acceptMode_ = value;
                emit acceptModeChanged(acceptMode_);
            }

            QStringList nameFilters(){ return nameFilters_; }
            void setNameFilters(QStringList value)
            {
                nameFilters_ = value;
                emit nameFiltersChanged(nameFilters_);
            }

            QString defaultSuffix(){ return defaultSuffix_; }
            void setDefaultSuffix(QString value)
            {
                defaultSuffix_ = value;
                emit defaultSuffixChanged(defaultSuffix_);
            }

            QString path(){ return path_; }

            QString selectedNameFilter(){ return m_selectedNameFilter; }
            void setSelectedNameFilter(QString value)
            {
                m_selectedNameFilter = value;
                emit selectedNameFilterChanged(m_selectedNameFilter);
            }
            int selectedNameFilterIndex();
            void setSelectedNameFilterIndex(int value);

            void setPath(QString path){ path_ = path; }

        signals:
            void accepted(QString file);
            void rejected();
            void titleChanged(QString);
            void selectExistingChanged(bool);
            void selectedFileChanged(QString);
            void selectedFilesChanged();
            void acceptLabelChanged(QString);
            void acceptModeChanged(AcceptMode);
            void nameFiltersChanged(QStringList);
            void defaultSuffixChanged(QString);
            void pathChanged(QString);
            void selectedNameFilterChanged(QString);
            void allowMultipleFilesChanged();

        public slots:
            void open(const QString defaultPath = "");

        private:
            QString title_;
            bool selectExisting_ = false;
            bool allowMultipleFiles_ = false;
            QString selectedFile_;
            QStringList selectedFiles_;
            QString acceptLabel_;
            AcceptMode acceptMode_ = AcceptSave;
            QStringList nameFilters_;
            QString defaultSuffix_;
            QString path_;
            QString m_selectedNameFilter;
            void exec(QFileDialog &);
    };
};
#endif // MYFILEDIALOG_H
