#include "onlineversionrequest.h"
#include <QStringList>

using namespace stereoscopic::gui;

OnlineVersionRequest::OnlineVersionRequest(QObject *parent) : QObject(parent)
{
}

void OnlineVersionRequest::onVersionRecieved(QString v)
{
    m_newVersion  = v;
    m_isNew = isNew(v, APP_VERSION);
    emit newVersionChecked(m_isNew, v);
}

bool OnlineVersionRequest::isNew(QString newVersion, QString curVersion)
{
    QStringList curVer = curVersion.split(".");
    QStringList newVer = newVersion.split(".");
    if(curVer.length() == newVer.length())
    {
        for(int i = 0; i < curVer.length(); i++)
        {
            int curN = curVer.at(i).toInt();
            int newN = newVer.at(i).toInt();
            if(newN > curN)
                return true;
            if(newN < curN)
                return false;
        }
    }
    return false;
}

#ifdef Q_OS_WIN32

void OnlineVersionRequest::beginCheckNewVersion()
{
    QStringList args;
    m_process = new QProcess(this);
    connect(m_process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(processFinished(int, QProcess::ExitStatus)));
    args.append("[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; echo((New-Object system.Net.WebClient).downloadString('" + urlStr + "'))");
    m_process->start("powershell", args);
}

void OnlineVersionRequest::processFinished(int, QProcess::ExitStatus exitStatus)
{
    if(exitStatus == QProcess::NormalExit)
        onVersionRecieved(QString(m_process->readAllStandardOutput()).trimmed());
}
#else
#include <QNetworkReply>

void OnlineVersionRequest::beginCheckNewVersion()
{
    QUrl url(urlStr);
    QNetworkRequest request(url);
    QNetworkAccessManager *mngr = new QNetworkAccessManager(this);
    connect(mngr, &QNetworkAccessManager::finished, this, &OnlineVersionRequest::getResponse);
    mngr->get(request);
}

void OnlineVersionRequest::getResponse(QNetworkReply *reply)
{
    onVersionRecieved(QString(reply->readAll()).trimmed());
}
#endif
