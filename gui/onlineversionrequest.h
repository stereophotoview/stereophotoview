#ifndef ONLINEvERSIONREQUEST_H
#define ONLINEvERSIONREQUEST_H

#include <QObject>
#ifdef Q_OS_WIN32
#include <QProcess>
#else
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#endif

namespace stereoscopic::gui
{
    class OnlineVersionRequest : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QString version READ version NOTIFY newVersionChecked)
            Q_PROPERTY(bool isNew READ isNew NOTIFY newVersionChecked)

        public:
            explicit OnlineVersionRequest(QObject *parent = nullptr);
            QString version(){ return m_newVersion; }
            bool isNew(){ return m_isNew; }

        signals:
            void newVersionChecked(bool isNew, QString version);

        public slots:
            void beginCheckNewVersion();

        private:
            void onVersionRecieved(QString v);
            bool isNew(QString newVersion, QString curVersion);

            const QString urlStr = "https://stereophotoview.bitbucket.io/current-version";
            QString m_newVersion;
            bool m_isNew = false;

#ifdef Q_OS_WIN32
        private slots:
            void processFinished(int exitCode, QProcess::ExitStatus exitStatus);

        private:
            QProcess *m_process;

#else
        private slots:
            void getResponse(QNetworkReply*);

#endif
    };
};
#endif // ONLINEvERSIONREQUEST_H
