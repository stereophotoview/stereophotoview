#include "presetslistmodel.h"

#include <math.h>

using namespace stereoscopic::gui;

QSize StandardSizeItem::Size(StereoFormat*, QSize, QSize)
{
    return QSize(m_width, m_height);
}

QSize AutoStandardSizeItem::Size(StereoFormat *format, QSize leftSourceSize, QSize rightSourceSize)
{
    if(format != nullptr)
    {
        auto frameSize = format->fullFrameSize(leftSourceSize, rightSourceSize);
        return frameSize;
    }
    return QSize();
}

QSize AutoMaxStandardSizeItem::Size(StereoFormat *format, QSize leftSourceSize, QSize rightSourceSize)
{
    if(format != nullptr)
    {
        auto frameSize = format->fullFrameSize(leftSourceSize, rightSourceSize);
        auto factor = format->widthFactor() * format->heightFactor();
        return QSize(frameSize.width() * factor, frameSize.height() * factor);
    }
    return QSize();
}

QQmlListProperty<StandardSizeItem> StandardSizesModel::data()
{
    return QQmlListProperty<StandardSizeItem>(static_cast<QObject *>(this), static_cast<void *>(&m_data),
                                              nullptr, &StandardSizesModel::countData,
                                              &StandardSizesModel::atData, nullptr);
}

int StandardSizesModel::size()
{
    return m_data.length();
}

StandardSizesModel *StandardSizesModel::instance()
{
    if(m_instance == nullptr)
        m_instance = new StandardSizesModel();
    return m_instance;
}

int StandardSizesModel::indexOf(StandardSizeItem *item)
{
    for(int i = 0 ; i < m_data.length(); i++)
        if(m_data[i] == item)
            return i;
    return 0;
}

StandardSizeItem* StandardSizesModel::find(QString code)
{
    for(int i = 0 ; i < m_data.length(); i++)
        if(m_data[i]->code() == code)
            return m_data[i];
    return m_data.first();
}

StandardSizeItem *StandardSizesModel::at(int index)
{
    return m_data.at(index);
}

int StandardSizesModel::countData(QQmlListProperty<StandardSizeItem> *list)
{
    QList<StandardSizeItem*> *data = static_cast<QList<StandardSizeItem*> *>(list->data);
    return data->size();
}

StandardSizeItem *StandardSizesModel::atData(QQmlListProperty<StandardSizeItem> *list, int index)
{
    QList<StandardSizeItem*> *data = static_cast<QList<StandardSizeItem*> *>(list->data);
    return data->at(index);
}

StandardSizesModel* StandardSizesModel::m_instance = nullptr;

Preset::Preset(QObject *parent)
    : QObject(parent)
{

}

void Preset::setSizeLinked(bool value)
{
    if(m_sizeLinked != value)
    {
        m_sizeLinked = value;
        emit sizeLinkedChanged();
        calcAspectRatio();
    }

}

void Preset::setStdSize(StandardSizeItem* value)
{
    if(m_stdSize != value)
    {
        m_stdSize = value;
        emit stdSizeChanged();
        updateSizeByStd();
        calcAspectRatio();
    }
}

void Preset::setWidth(int value, bool calcHeight)
{
    if(value != m_width)
    {
        m_width = value;
        emit widthChanged();
        if(calcHeight && m_stdSize->isManual() && m_sizeLinked && !equal(m_aspectRatio, 0))
            setHeight((int)std::round((double)m_width / m_aspectRatio), false);
    }
}

void Preset::setHeight(int value, bool calcWidth)
{
    if(value != m_height)
    {
        m_height = value;
        emit heightChanged();
        if(calcWidth && m_stdSize->isManual() && m_sizeLinked && !equal(m_aspectRatio, 0))
            setWidth((int)std::round((double)(m_height * m_aspectRatio)), false);
    }
}

void Preset::setLeftSourceSize(QSize value)
{
    if(m_leftSourceSize != value)
    {
        m_leftSourceSize = value;
        emit leftSourceSizeChanged();
        updateSizeByStd();
    }
}

void Preset::setRightSourceSize(QSize value)
{
    if(m_rightSourceSize != value)
    {
        m_rightSourceSize = value;
        emit rightSourceSizeChanged();
        updateSizeByStd();
    }
}

void Preset::setStereoFormat(StereoFormat *value)
{
    if(m_stereoFormat != value)
    {
        if(m_stereoFormat != nullptr)
            disconnect(m_stereoFormat, &StereoFormat::changed, this, &Preset::updateSizeByStd);
        m_stereoFormat = value;
        if(m_stereoFormat != nullptr)
            connect(m_stereoFormat, &StereoFormat::changed, this, &Preset::updateSizeByStd);
        updateSizeByStd();
        emit stereoFormatChanged();
    }
}

void Preset::copyFrom(Preset *src)
{
    setLeftFirst(src->leftFirst());
    setJpegQuality(src->jpegQuality());
    setSizeLinked(false);
    setStdSize(src->stdSize());
    if(stdSize()->isManual())
    {
        setWidth(src->width(), false);
        setHeight(src->height(), false);
    }
    setLayout(src->layout());
    setSizeLinked(!stdSize()->isAutoSize() ? false : src->sizeLinked());
    setVideoFormatName(src->videoFormatName());
    setCrf(src->crf());
    setVideoPreset(src->videoPreset());
}

void Preset::updateSizeByStd()
{
    StandardSizeItem* std = stdSize();
    if(!std->isManual())
    {
        QSize s = std->Size(m_stereoFormat, m_leftSourceSize, m_rightSourceSize);
        setWidth(s.width(), false);
        setHeight(s.height(), false);
    }
}

void Preset::calcAspectRatio()
{
    if(m_stdSize->isManual() && m_sizeLinked)
        m_aspectRatio = (double)m_width / m_height;
    else
        m_aspectRatio = 0;
}

PresetsListModel::PresetsListModel(QObject *parent)
    : QObject(parent)
{

}

PresetsListModel::PresetsListModel(QString settingsname, Preset* defaultPreset, bool isVideo, QObject *parent)
    : PresetsListModel(parent)
{
    m_isVideo = isVideo;
    m_settingsName = settingsname;
    m_data.append(defaultPreset);
}

QQmlListProperty<Preset> PresetsListModel::data()
{
    return QQmlListProperty<Preset>(static_cast<QObject *>(this), static_cast<void *>(&m_data),
                                    &PresetsListModel::appendData, &PresetsListModel::countData,
                                    &PresetsListModel::atData, &PresetsListModel::clearData);
}

int PresetsListModel::size()
{
    return m_data.size();
}

Preset *PresetsListModel::elementAt(int index)
{
    if(index < m_data.size())
        return m_data[index];
    return nullptr;
}

void PresetsListModel::add(QString name, Preset *preset)
{
    Preset *p = new Preset(this);
    p->setName(name);
    p->copyFrom(preset);
    m_data.append(p);
    save();
    emit dataChanged();
}

void PresetsListModel::deleteAt(int index)
{
    if(index < m_data.size())
    {
        m_data[index]->deleteLater();
        m_data.removeAt(index);
        save();
        emit dataChanged();
    }
}

void PresetsListModel::load()
{
    int size = m_settings.beginReadArray(m_settingsName);
    for(int i = 0; i < size; i++)
    {
        m_settings.setArrayIndex(i);
        if(m_data.size() <= i)
            m_data.append(new Preset(this));
        Preset* p = m_data[i];
        p->setName(m_settings.value("name").toString());
        p->setLayout((StereoFormat::Layout)m_settings.value("layout", p->layout()).toInt());
        p->setLeftFirst(m_settings.value("leftFirst", p->leftFirst()).toBool());
        QString stdSizeCode = m_settings.value("stdSize", p->stdSize()->code()).toString();
        p->setWidth(m_settings.value("width", p->width()).toInt(), false);
        p->setHeight(m_settings.value("height", p->height()).toInt(), false);
        p->setStdSize(StandardSizesModel::instance()->find(stdSizeCode));
        p->setSizeLinked(m_settings.value("sizeLinked", p->sizeLinked()).toBool());
        p->setJpegQuality(m_settings.value("jpegQuality", p->jpegQuality()).toInt());
        if(m_isVideo)
        {
            p->setVideoFormatName(m_settings.value("videoFormatName", p->videoFormatName()).toString());
            p->setCrf(m_settings.value("crf", p->crf()).toInt());
            p->setVideoPreset(m_settings.value("videoPreset", p->videoPreset()).toString());
        }
    }
    m_settings.endArray();
    while(m_data.size() > 1 && m_data.size() > size)
    {
        m_data.last()->deleteLater();
        m_data.removeLast();
    }
    emit dataChanged();
}

void PresetsListModel::save()
{
    m_settings.beginWriteArray(m_settingsName);
    m_settings.remove("");
    for (int i = 0; i < m_data.size(); ++i) {
        m_settings.setArrayIndex(i);
        Preset* p = m_data[i];
        m_settings.setValue("name", p->name());
        m_settings.setValue("layout", p->layout());
        m_settings.setValue("leftFirst", p->leftFirst());
        m_settings.setValue("stdSize", p->stdSize()->code());
        m_settings.setValue("width", p->width());
        m_settings.setValue("height", p->height());
        m_settings.setValue("sizeLinked", p->sizeLinked());
        m_settings.setValue("jpegQuality", p->jpegQuality());
        if(m_isVideo)
        {
            m_settings.setValue("videoFormatName", p->videoFormatName());
            m_settings.setValue("crf", p->crf());
            m_settings.setValue("videoPreset", p->videoPreset());
        }
    }
    m_settings.endArray();
}

PresetsListModel* PresetsListModel::JPS(QObject *parent)
{
    auto defaultPreset = new Preset(parent);
    defaultPreset->setName(tr("Default", "Default preset name"));
    defaultPreset->setLayout((StereoFormat::Layout)-1);
    return new PresetsListModel("JPSPresets", defaultPreset, false, parent);
}

PresetsListModel* PresetsListModel::MPO(QObject *parent)
{
    auto defaultPreset = new Preset(parent);
    defaultPreset->setName(tr("Default", "Default preset name"));
    defaultPreset->setLayout(StereoFormat::Separate);
    return new PresetsListModel("MPOPresets", defaultPreset, false, parent);
}

PresetsListModel* PresetsListModel::Video(QObject *parent)
{
    auto defaultPreset = new Preset(parent);
    defaultPreset->setName(tr("Default", "Default preset name"));
    defaultPreset->setLayout((StereoFormat::Layout)-1);
    defaultPreset->setVideoFormatName("mp4");
    return new PresetsListModel("VideoPresets", defaultPreset, true, parent);
}

void PresetsListModel::appendData(QQmlListProperty<Preset> *list, Preset *value)
{
    QList<Preset*> *data = static_cast<QList<Preset*> *>(list->data);
    data->append(value);
}

int PresetsListModel::countData(QQmlListProperty<Preset> *list)
{
    QList<Preset*> *data = static_cast<QList<Preset*> *>(list->data);
    return data->size();
}

Preset *PresetsListModel::atData(QQmlListProperty<Preset> *list, int index)
{
    QList<Preset*> *data = static_cast<QList<Preset*> *>(list->data);
    return data->at(index);
}

void PresetsListModel::clearData(QQmlListProperty<Preset> *list)
{
    QList<Preset*> *data = static_cast<QList<Preset*> *>(list->data);
    qDeleteAll(data->begin(), data->end());
    data->clear();
}
