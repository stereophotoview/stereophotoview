#ifndef PRESETSLISTMODEL_H
#define PRESETSLISTMODEL_H

#include <QSettings>
#include <QQmlListProperty>
#include <../lib/stereoformat.h>
#include <../lib/mathhelper.h>

using namespace stereoscopic;
using namespace stereoscopic::math;

namespace stereoscopic::gui
{
    /**
     * @brief Стандартный размер изображения
     */
    class StandardSizeItem : public QObject {
            Q_OBJECT
            Q_PROPERTY(QString code READ code)
            Q_PROPERTY(QString name READ name)
            Q_PROPERTY(QString width READ width)
            Q_PROPERTY(QString height READ height)
            Q_PROPERTY(bool isManual READ isManual)
            Q_PROPERTY(bool isAutoSize READ isAutoSize)
        public:
            /// Код для сохранения
            QString code() { return m_code; }

            /// Название для отборажения
            QString name() { return m_name; }

            /// Ширина изображения
            int width() { return m_width; }

            /// Высота изображения
            int height() { return  m_height; }

            /**
             * @brief Создаёт стандартный размер изображения
             * @param code Код для сохранения
             * @param name Название для отборажения
             * @param width Ширина изображения
             * @param height Высота изображения
             */
            StandardSizeItem(QString code, QString name, int width, int height, QObject* parent = nullptr)
                : QObject(parent)
            {
                m_code = code;
                m_name = name;
                m_width = width;
                m_height = height;
            }

            /// Возвращает признак того, что размер должен рассчитываться автоматически
            virtual bool isAutoSize() { return false; }

            /// Возвращает признак того, что размер должен задаваться вручную
            virtual bool isManual(){ return false; }

            /// Возвращает размер изображения
            virtual QSize Size(StereoFormat* format, QSize leftSourceSize, QSize rightSourceSize);
        private:
            QString m_code, m_name;
            int m_width, m_height;
    };

    /**
     * @brief Автоматически вычисляемый размер с потерей разрешения для анаморфных форматов
     */
    class AutoStandardSizeItem : public StandardSizeItem
    {
            Q_OBJECT
        public:
            AutoStandardSizeItem(QString name, QObject* parent = nullptr) : StandardSizeItem("auto", name, 0, 0, parent)
            {
            }

            bool isAutoSize() Q_DECL_OVERRIDE { return true; }
            QSize Size(StereoFormat* format, QSize leftSourceSize, QSize rightSourceSize) Q_DECL_OVERRIDE;
    };

    /**
     * @brief Автоматически вычисляемый размер без потери разрешения для анаморфных форматов
     */
    class AutoMaxStandardSizeItem : public StandardSizeItem
    {
            Q_OBJECT
        public:
            AutoMaxStandardSizeItem(QString name, QObject* parent = nullptr) : StandardSizeItem("autoMax", name, -1, -1, parent)
            {
            }
            bool isAutoSize() Q_DECL_OVERRIDE { return true; }
            QSize Size(StereoFormat* format, QSize leftSourceSize, QSize rightSourceSize) Q_DECL_OVERRIDE;
    };

    /**
     * @brief Предоставляет возможность указать размер вручную
     */
    class ManualStandardSizeItem : public StandardSizeItem
    {
            Q_OBJECT
        public:
            ManualStandardSizeItem(QString name, QObject* parent = nullptr) : StandardSizeItem("manual", name, -1, -1, parent)
            {
            }
            bool isManual() Q_DECL_OVERRIDE { return true; }
    };

    /**
     * @brief Класс модели списка стандартных размеров изображений
     */
    class StandardSizesModel : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QQmlListProperty<stereoscopic::gui::StandardSizeItem> data READ data)
            Q_CLASSINFO("DefaultProperty", "data")
            Q_PROPERTY(int size READ size)
        public:
            /// Возвращает список для qml
            QQmlListProperty<StandardSizeItem> data();

            /// Возвраает длинну списка
            int size();

            /// Экземпляр
            static StandardSizesModel* instance();

        public slots:
            /// Возвразает индекс элемента по коду
            int indexOf(stereoscopic::gui::StandardSizeItem* item);

            /// Возвразает индекс элемента по коду
            stereoscopic::gui::StandardSizeItem* find(QString code);

            /// Возвразает индекс элемента по коду
            stereoscopic::gui::StandardSizeItem* at(int index);

        private:
            StandardSizesModel(): QObject(nullptr) { }

            /// Варианты стандартных размеров изображения
            QList<StandardSizeItem*> m_data = {
                new AutoStandardSizeItem(QObject::tr("Auto"), this),
                new AutoMaxStandardSizeItem(QObject::tr("Auto lossless"), this),
                new StandardSizeItem("480p", QObject::tr("480p"), 720, 480, this),
                new StandardSizeItem("576p", QObject::tr("576p"), 720, 576, this),
                new StandardSizeItem("720p", QObject::tr("HD 720p"), 1280, 720, this),
                new StandardSizeItem("1080p", QObject::tr("Full HD 1080p"), 1920, 1080, this),
                new StandardSizeItem("2k", QObject::tr("2K"), 2048, 1080, this),
                new StandardSizeItem("4k", QObject::tr("4K UHD"), 3840, 2160, this),
                new StandardSizeItem("true4k", QObject::tr("True 4K"), 4096, 2160, this),
                new StandardSizeItem("8kUhd", QObject::tr("8K UHD"), 7680, 4320, this),
                new StandardSizeItem("true8k", QObject::tr("True 8K"), 8192, 4320, this),
                new ManualStandardSizeItem(QObject::tr("Manual"), this),
            };
            static int countData(QQmlListProperty<StandardSizeItem> *list);
            static StandardSizeItem *atData(QQmlListProperty<StandardSizeItem> *list, int index);
            static StandardSizesModel* m_instance;
    };

 /**
 * @brief Класс предустановленных параметров сохранения стереоскопического контента
 */
    class Preset: public QObject
    {
            Q_OBJECT
            /// Возвращает или устанавливает наименование
            Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
            /// Возвращает или устанавливает признак того, что ширина и высота связаны
            Q_PROPERTY(bool sizeLinked READ sizeLinked WRITE setSizeLinked NOTIFY sizeLinkedChanged)
            /// Выбранный вариант стандартного размера
            Q_PROPERTY(StandardSizeItem* stdSize READ stdSize WRITE setStdSize NOTIFY stdSizeChanged)
            /// Качество JPEG
            Q_PROPERTY(int jpegQuality READ jpegQuality WRITE setJpegQuality NOTIFY jpegQualityChanged)
            /// Ширина изображения
            Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
            /// Высота изображения
            Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)
            /// Выбранный вариант расположения ракурсов (-1 - исходный)
            Q_PROPERTY(int layout READ layout WRITE setLayout NOTIFY layoutChanged)
            /// Признак того, что левый кадр идёт впереди
            Q_PROPERTY(bool leftFirst READ leftFirst WRITE setLeftFirst NOTIFY leftFirstChanged)
            /// Размер исходного изображения в пикселах
            Q_PROPERTY(QSize leftSourceSize READ leftSourceSize WRITE setLeftSourceSize NOTIFY leftSourceSizeChanged)
            /// Размер исходного изображения в пикселах
            Q_PROPERTY(QSize rightSourceSize READ rightSourceSize WRITE setRightSourceSize NOTIFY rightSourceSizeChanged)
            /// Стерео формат
            Q_PROPERTY(StereoFormat* stereoFormat READ stereoFormat WRITE setStereoFormat NOTIFY stereoFormatChanged)
            /// Название формата видео
            Q_PROPERTY(QString videoFormatName READ videoFormatName WRITE setVideoFormatName NOTIFY VideoFormatNameChanged)
            /// Constant rate factor
            Q_PROPERTY(int crf READ crf WRITE setCrf NOTIFY crfChanged)
            /// Пресет кодека
            Q_PROPERTY(QString videoPreset READ videoPreset WRITE setVideoPreset NOTIFY videoPresetChanged)

        public:
            explicit Preset(QObject *parent = nullptr);

            QString name(){ return m_name; }
            void setName(QString value)
            {
                if(m_name != value)
                {
                    m_name = value;
                    emit nameChanged();
                }
            }

            bool sizeLinked(){ return m_sizeLinked; }
            void setSizeLinked(bool value);

            /// Возвращает выбранный стандартный размер
            StandardSizeItem* stdSize(){ return m_stdSize; }
            /// Устаналвивает выбранный стандартный размер
            void setStdSize(StandardSizeItem* value);

            int jpegQuality(){ return m_jpegQuality; }
            void setJpegQuality(int value)
            {
                if(value != m_jpegQuality)
                {
                    m_jpegQuality = value;
                    emit jpegQualityChanged();
                }
            }

            int width() { return m_width; }
            void setWidth(int value, bool calcHeight = true);

            int height(){ return m_height; }
            void setHeight(int value, bool calcWidth = true);

            int layout(){ return m_layout; }
            void setLayout(int value)
            {
                if(value != m_layout)
                {
                    m_layout = value;
                    emit layoutChanged();
                }
            }

            bool leftFirst(){ return m_leftFirst; }
            void setLeftFirst(bool value)
            {
                if(value != m_leftFirst)
                {
                    m_leftFirst = value;
                    emit leftFirstChanged();
                }
            }

            QSize leftSourceSize() { return m_leftSourceSize; }
            void setLeftSourceSize(QSize value);

            QSize rightSourceSize() { return m_rightSourceSize; }
            void setRightSourceSize(QSize value);

            StereoFormat* stereoFormat() { return m_stereoFormat; }
            void setStereoFormat(StereoFormat* value);

            QString videoFormatName(){ return m_videoFormatName; }
            void setVideoFormatName(QString value)
            {
                if(value != m_videoFormatName)
                {
                    m_videoFormatName = value;
                    emit VideoFormatNameChanged();
                }
            }

            int crf(){ return m_crf; }
            void setCrf(int value)
            {
                if(value != m_crf)
                {
                    m_crf = value;
                    emit crfChanged();
                }
            }

            QString videoPreset(){ return m_videoPreset; }
            void setVideoPreset(QString value)
            {
                if(value != m_videoPreset)
                {
                    m_videoPreset = value;
                    emit videoPresetChanged();
                }
            }

            Q_INVOKABLE void copyFrom(Preset* src);

        signals:
            void nameChanged();
            void sizeLinkedChanged();
            void stdSizeChanged();
            void jpegQualityChanged();
            void widthChanged();
            void heightChanged();
            void layoutChanged();
            void leftFirstChanged();
            void VideoFormatNameChanged();
            void crfChanged();
            void videoPresetChanged();
            void leftSourceSizeChanged();
            void rightSourceSizeChanged();
            void stereoFormatChanged();

        public slots:
            /// Вовзращает целевой размер изображения
            QSize size()
            {
                return QSize(width(), height());
            }

        private slots:
            /// Обновляет width и height по стандартному размеру и стерео формату
            void updateSizeByStd();

        private:
            QString m_name;
            bool m_sizeLinked = false;
            StandardSizeItem* m_stdSize = StandardSizesModel::instance()->at(0);
            int m_jpegQuality = 97;
            int m_width = 0;
            int m_height = 0;
            int m_layout = -1;
            bool m_leftFirst = true;
            QString m_videoFormatName;
            int m_crf = 23;
            QString m_videoPreset = "medium";
            QSize m_leftSourceSize, m_rightSourceSize;
            StereoFormat* m_stereoFormat = nullptr;
            double m_aspectRatio = 0;
            void calcAspectRatio();
    };

    class PresetsListModel : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QQmlListProperty<stereoscopic::gui::Preset> data READ data NOTIFY dataChanged)
            Q_CLASSINFO("DefaultProperty", "data")
            Q_PROPERTY(int size READ size NOTIFY dataChanged)

        public:
            PresetsListModel(QObject* parent = nullptr);
            PresetsListModel(QString settingsname, Preset *defaultPreset, bool isVideo, QObject* parent = nullptr);
            QQmlListProperty<Preset> data();
            int size();
            Q_INVOKABLE stereoscopic::gui::Preset* elementAt(int index);
            Q_INVOKABLE void add(QString name, stereoscopic::gui::Preset *preset);
            Q_INVOKABLE void deleteAt(int index);
            Q_INVOKABLE void load();
            Q_INVOKABLE void save();
            static PresetsListModel *JPS(QObject *parent);
            static PresetsListModel *MPO(QObject *parent);
            static PresetsListModel *Video(QObject *parent);

        signals:
            void dataChanged();

        private:
            QList<Preset*> m_data;
            QSettings m_settings;
            QString m_settingsName;
            bool m_isVideo = false;

            static void appendData(QQmlListProperty<Preset> *list, Preset *value);
            static int countData(QQmlListProperty<Preset> *list);
            static Preset *atData(QQmlListProperty<Preset> *list, int index);
            static void clearData(QQmlListProperty<Preset> *list);
    };
};
#endif // PRESETSLISTMODEL_H
