#include "previewimageprovider.h"

using namespace stereoscopic;
using namespace stereoscopic::gui;

PreviewImageProvider::PreviewImageProvider()
    : QQuickImageProvider(QQuickImageProvider::Pixmap)
{

}

QPixmap PreviewImageProvider::requestPixmap(const QString &filePath, QSize *size, const QSize &requestedSize)
{
    QSize resultSize = requestedSize;
    if(requestedSize.height() <= 0 || requestedSize.width() <= 0)
        resultSize = QSize(256, 256);

    QImage result;
    if(filePath.contains("%0A"))
    {
        // Рисуем предпросмотр для раздельных источников
        auto paths = filePath.split("%0A");
        QSize reqPartSize = resultSize / 1.5;
        QImage img1 = loadPreview(paths[0], reqPartSize);
        QImage img2 = loadPreview(paths[1], reqPartSize);
        QSize sizePart = img1.size();
        // Реальный размер sizePart может быть меньше запрашиваемого reqPartSize, если считываем картинку предпросмотра JPG
        // Рисуем на резултирующем изображении два источника
        int max = sizePart.width();
        if(sizePart.height() > max)
            max = sizePart.height();
        resultSize = QSize(max * 4 / 3, max * 4 / 3);
        result = QImage(resultSize, QImage::Format_ARGB32);
        result.fill(Qt::transparent);
        QPainter painter(&result);
        int frameWidth = result.width() / 30;
        painter.setPen(QPen(QBrush(Qt::white), frameWidth));
        QRectF rect1(result.width() - img2.width() - frameWidth, frameWidth - 1, img1.width(), img1.height());
        QRectF rect2(frameWidth - 1, result.height() - img1.height() - frameWidth, img2.width(), img2.height());
        painter.drawImage(rect1, img1, QRectF(QPoint(0,0), img1.size()));
        painter.drawRect(rect1);
        painter.drawImage(rect2, img2, QRectF(QPoint(0,0), img2.size()));
        painter.drawRect(rect2);
        painter.end();
    }
    else
    {
        // Рисуем предпросмотр для одного источника
        result = loadPreview(filePath, resultSize);
    }
    size->setWidth(result.width());
    size->setHeight(result.height());
    return QPixmap::fromImage(result);
}

QImage PreviewImageProvider::loadPreview(const QString &filePath, const QSize &requestedSize)
{
    QImage result;
    if(m_jpsFormat.checkFileName(filePath) || m_mpoFormat.checkFileName(filePath))
    {
        QExifImageHeader header;
        header.loadFromJpeg(filePath);
        result = header.thumbnail();
        int resArea = result.size().width() * result.size().height();
        int reqArea = requestedSize.width() * requestedSize.height();
        if(result.isNull() || resArea * 4 < reqArea)
            result.load(filePath);
    }
    if(m_videoFormat.checkFileName(filePath))
    {
        Demux demux;
        demux.open(filePath);
        InVideoStream* stream = demux.videoStreams().length() > 0 ? demux.videoStreams().first() : nullptr;
        if(stream == nullptr)
            return QImage();
        double duration = demux.duration();
        auto pos = duration / 100;
        // Перематываем на позицию 1/100 от длинны
        demux.seek(pos);
        InVideoFrame* videoFrame = demux.seekToKeyFrame(stream);
        if(videoFrame != nullptr)
        {
            result = videoFrame->image();
            delete videoFrame;
        }
    }
    result = ImageHelper::createThumbnail(&result, requestedSize);
    return result;
}
