#ifndef PREVIEWIMAGEPROVIDER_H
#define PREVIEWIMAGEPROVIDER_H

#include <QQuickImageProvider>

#include "fileFormats/jpeg/qexifimageheader.h"
#include "fileFormats/jpsimageformat.h"
#include "fileFormats/mpoimageformat.h"
#include "fileFormats/videoformat.h"
#include "fileFormats/video/demux.h"
#include "fileFormats/video/invideoframe.h"
#include "imagehelper.h"

using namespace stereoscopic::fileFormats;

namespace stereoscopic::gui
{
    class PreviewImageProvider : public QQuickImageProvider
    {
        public:
            PreviewImageProvider();

            QPixmap requestPixmap(const QString &filePath, QSize *size, const QSize &requestedSize);

        private:
            JpsImageFormat m_jpsFormat;
            MpoImageFormat m_mpoFormat;
            VideoFormat m_videoFormat;
            QImage loadPreview(const QString &filePath, const QSize &requestedSize);
    };
};
#endif // PREVIEWIMAGEPROVIDER_H
