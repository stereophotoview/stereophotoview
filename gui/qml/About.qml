import QtQuick 2.5
import QtQuick.Controls 1.4
import StereoImage 1.0

ApplicationWindow {
    id: window
    title: qsTr("About %1").arg("StereoPhotoView")
    modality: Qt.WindowModal
    flags: Qt.Dialog
    SystemPalette { id: palette }
    Column {
        id: column1
        anchors.fill: parent
        anchors.margins: 10
        spacing: 15
        Row {
            id: row1
            spacing: 15
            Image {
                width: 128
                height: 128
                source: "../images/stereophotoview.svg"
            }
            Label {
                wrapMode: Text.WordWrap
                width: 320
                linkColor: palette.highlight
                text: "<h1>StereoPhotoView</h1>"
                      + qsTr("Version %1").arg(appVersion)
                      + "<br/>"
                      + "<p>" + qsTr("A viewer/editor for stereoscopic photo and video") + ".</p>"
                      + "<br/><p>© 2015-2022 <a href='mailto:av.mamzikov@gmail.com'>" + qsTr("Alexander Mamzikov") + "</a>.</p>"
                      + "<br/>"
                      + "<p>"
                      + qsTr("StereoPhotoView is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.")
                      + " " + qsTr("See the %1 for more details.").arg("<a href='http://www.gnu.org/licenses/gpl.txt'>GNU General Public License</a>")
                      + "<br/>"
                      +"</p>"
                      + "<p>"+qsTr("Used projects")+":</p>"
                      + "<ul>"
                      + "<li><a href='https://www.qt.io' style='color:red'>Qt " + QtVersion + "</a></li>"
                      + "<li><a href='https://ffmpeg.org/'>FFmpeg " + FFmpegVersion + "</a></li>"
                      + (autoAlign.enabled ? "<li><a href='http://opencv.org'>OpenCV " + autoAlign.opencvVersion + "</a></li>" : "")
                      + "<li><a href='https://github.com/KDE/breeze-icons/'>Breeze Icons</a></li>"
                      + "</ul>"
                      + "<br/>"
                      + "<a href='https://stereophotoview.bitbucket.io" + qsTr("/en/") + "'>" + qsTr("Website") + "</a>"
                      + " | <a href='https://bitbucket.org/stereophotoview/stereophotoview/issues?status=new&status=open'>" + qsTr("Bug tracker") + "</a>"
                onLinkActivated: Qt.openUrlExternally(link)
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.NoButton
                    cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
                }
            }
        }

        Button {
            text: qsTr("OK")
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked:
                window.close()
        }
    }

    AutoAlign {
        id: autoAlign
    }

    Component.onCompleted: {
        window.minimumWidth = window.maximumWidth = window.width
        window.minimumHeight = window.maximumHeight = window.height
    }
}


