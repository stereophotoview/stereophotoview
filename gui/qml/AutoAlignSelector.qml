import QtQuick 2.5
import QtQuick.Controls 1.4
import StereoImage 1.0
import Qt.labs.settings 1.0

MouseArea {
    id: self
    anchors.fill: parent
    hoverEnabled: true
    acceptedButtons: Qt.LeftButton
    property StereoImage image: parent
    signal selected(rect rect)

    Settings {
        id: settings
        category: "AutoAlignSelector"
        property double scale: 0.1
    }

    onVisibleChanged: {
        var imageArea = image.width * image.height
        if(visible)
        {
            var scale = settings.scale
            if(scale < 0 || scale > 1)
                scale = 0.1
            rect.width = rect.height = Math.sqrt(imageArea * scale)
        }
        else
        {
            if(rect.width > 0)
            {
                var rectArea = rect.width * rect.height
                settings.scale = rectArea / imageArea
            }
        }
    }

    onWheel: {
        rect.width = rect.height = rect.width + wheel.angleDelta.y / 10;
    }

    onClicked: {
        selected(selectorRect())
    }

    Rectangle {
        id: rect
        x: parent.mouseX - width / 2
        y: parent.mouseY - height / 2
        border.color: palette.light
        border.width: 2
        color: "#00000000"
    }

    Action {
        enabled: self.visible
        shortcut: "Escape"
        onTriggered: currentTool = 0
    }

    function selectorRect()
    {
        return image.translateToSource(Qt.rect(rect.x, rect.y, rect.width, rect.height));
    }
}
