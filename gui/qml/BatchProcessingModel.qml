import QtQuick 2.0
import StereoImage 1.0
import Qt.labs.settings 1.0
import "functions.js" as F

QtObject {
    id: model

    property MPOFileFormat mpoFileFormat: MPOFileFormat { }
    property JpsImageFormat jpsFileFormat: JpsImageFormat { }

    property variant nameFilters: [
        qsTr("All supported files") + " (" + [mpoFileFormat, jpsFileFormat].map(F.getNameFilters).join(" ")  + ")",
        qsTr("MPO files") + " (" + F.getNameFilters(mpoFileFormat) + ")",
        qsTr("Stereo pairs") + " (" + F.getNameFilters(jpsFileFormat) + ")",
        qsTr("All files") + " (*)"]

    /** Очередь заданий: {source: string, source2: string, destination: string, result: BatchItemCommand::BatchResult} */
    property ListModel queueList: ListModel { }

    /** Форматы файлов */
    property variant fileFormats: [mpoFormat, jpsFormat, jpegFormat]

    /** Пресеты **/
    property int currentPresetIndex: 0
    property ListModel presetsModel: ListModel { }

    /** Формат файла источника */
    property FileFormatInfo sourceFormat : FileFormatInfo {
        currentLayout: StereoFormat.Monoscopic
        layoutsModel: jpgSourceLayouts
    }

    /** Индекс формата назначения (0 - mpo, 1 - jps, 2 - jpeg) */
    property int destFileFormatIndex: 0
    /** Формат файла назначения */
    property FileFormatInfo destFileFormat: fileFormats[destFileFormatIndex]

    /** Папка назначения */
    property string destinationFolder
    /** Шаблон файла-назначения */
    property string destinationFilePattern: "{source}.mpo"

    /** Процесс обработки существущих файлов */
    property int existingFileProcessing: 0
    property ListModel existingFileProcessingVariants: ListModel {
        ListElement { name: qsTr("Skip") }
        ListElement { name: qsTr("Overwrite") }
    }

    /** Автовыравнивание включено */
    property bool autoAlignEnabled: false
    /** Автовыравнивание по горизонтали включено */
    property bool autoAlignHorizontalEnabled: true
    /** Автовыравнивание по вертикали включено */
    property bool autoAlignVerticalEnabled: true
    /** Автовыравниванеи вращение включено */
    property bool autoAlignRotateEnabled: false
    /** Точность автовыравнивания */
    property double autoAlignAccuracy: 50
    /** Длина стороны области для оценки точек автовыравнивания в долях от длины стороны изображения */
    property double autoAlignEstimationSize: 0.3
    /** Центр области для оценки точек автовыравнивания в долях от длины стороны изображения */
    property double autoAlignEstimationCenterX: 0.5
    /** Центр области для оценки точек автовыравнивания в долях от длины стороны изображения */
    property double autoAlignEstimationCenterY: 0.5

    property bool autoColorAdjustEnabled: false
    property bool autoColorAdjustLeft: true

    /** Свойства для управления очердью: */
    /** Запуск очереди возможен */
    property bool startEnabled: queueList.count > 0 && destinationFolder !== "" && destinationFilePattern !== "" && worker.state === 0
    /** Остановка очереди возомжна */
    property bool stopEnabled: worker.state === 1 || worker.state === 2
    /** Поставить паузу возможно */
    property bool pauseEnabled: worker.state === 1 || worker.state === 2 && !worker.running
    /** Работа приостановлена */
    property bool workPaused: worker.state === 2

    /// Признак того, что параметры выбранного пресета были изменены
    property bool presetChanged: curSettings.changed

    onDestinationFolderChanged: updateDestination()
    onDestinationFilePatternChanged: updateDestination()
    onDestFileFormatChanged: destinationFilePattern = urlConv.changeSuffix(destinationFilePattern, destFileFormat.ext)

    property FileFormatInfo mpoFormat: FileFormatInfo {
        property string name: "MPO"
        ext: "mpo"
        layoutsModel: mpoLayouts
    }

    property FileFormatInfo jpsFormat: FileFormatInfo {
        property string name: "JPS"
        ext: "jps"
        layoutsModel: jpsLayouts
    }

    property FileFormatInfo jpegFormat: FileFormatInfo{
        property string name: "JPEG"
        ext: "jpeg"
        layoutsModel: jpegLayouts
    }

    property BatchItemCommand worker: BatchItemCommand{
        property int index
        /** Статус: 0 - остановлено, 1 - работает, 2 - пауза  */
        property int state: 0

        // Обработка оконачания работы одной задачи из списка
        onFinished: {
            var item = queueList.get(index).result = result
            if (state == 0)
            {
                // Если процесс остановлен, сбрасываем состояние на начало
                index = 0;
            }
            else
            {
                // Если работа в процессе или паузе, переключаем на следующую задачу
                if(index < model.queueList.count - 1)
                {
                    index++
                    // Если процесс идёт запускаем следующую задачу
                    if(state == 1)
                        next();
                }
                else
                {
                    // Если задачи кончились, останавливаем и сбрасываем состояние
                    index = 0
                    state = 0
                }
            }
        }

        function next()
        {
            if(running)
                return;
            var item = queueList.get(index)
            item.result = BatchItemCommand.ResultInProgress
            var parameters = {
                'source' : item.source,
                'source2' : item.source2,
                'sourceLayout': sourceFormat.currentLayout,
                'sourceLeftFirst': sourceFormat.leftFirst,
                'destination' : item.destination,
                'destFormat': destFileFormat.ext,
                'stdSizeCode': destFileFormat.preset.stdSize.code,
                'width': destFileFormat.preset.width,
                'height': destFileFormat.preset.height,
                'layout': destFileFormat.preset.stereoFormat.layout,
                'leftFirst': destFileFormat.preset.stereoFormat.leftFirst,
                'jpegQuality': destFileFormat.preset.jpegQuality,
                'autoAlignEnabled': autoAlignEnabled,
                'autoAlignAccuracy': autoAlignAccuracy,
                'autoAlignHorizontalEnabled': autoAlignHorizontalEnabled,
                'autoAlignVerticalEnabled': autoAlignVerticalEnabled,
                'autoAlignRotateEnabled': autoAlignRotateEnabled,
                'autoAlignEstimationCenterX': autoAlignEstimationCenterX,
                'autoAlignEstimationCenterY': autoAlignEstimationCenterY,
                'autoAlignEstimationSize': autoAlignEstimationSize,
                'autoColorAdjustEnabled': autoColorAdjustEnabled,
                'autoColorAdjustLeft': autoColorAdjustLeft,
                'existingFileProcessing': existingFileProcessing,
            }
            startExecute(parameters)
        }

        function start()
        {
            for(var i = 0; i < queueList.count; i++)
            {
                var item = queueList.get(i)
                item.result = BatchItemCommand.ResultEmpty
            }
            index = 0
            resume()
        }

        function pause()
        {
            state = 2
        }

        function resume()
        {
            state = 1
            next();
        }

        function stop()
        {
            state = 0
        }
    }

    property Settings presetsListSettings: Settings {
        category: "BatchPresets"
        property int size: 0

        function add(name)
        {
            setName(size, name)
            presetsModel.append({'name': name})
            size++;
        }

        function removeAt(index)
        {
            // Копируем все следующие пресеты в предыдущие
            for(var i = index + 1; i < size; i++)
                settings.copyAll(category + "/" + (i + 1).toString(), category + "/" + (i + 0).toString())
            // Удаляем последнюю папку настроек
            settings.remove(category + "/" + size.toString())
            // Уменьшаем количество элементов
            size--;
            presetsModel.remove(index)
        }

        function name(i)
        {
            return settings.value(category + "/" + (i + 1).toString() + "/name", qsTr("Default"))
        }

        function setName(i, name)
        {
            settings.setValue(category + "/" + (i + 1).toString() + "/name", name)
        }

        Component.onCompleted:
        {
            if(size == 0)
                add(qsTr("Default"))
            else
                for(var i = 0; i < size; i++)
                    presetsModel.append({'name': name(i)})
        }
    }

    property Settings curSettings: Settings {
        category: presetsListSettings.category + "/" + (currentPresetIndex + 1).toString()

        property int sourceLayout
        property bool sourceLeftFirst
        property string destinationFolder
        property string destinationFilePattern: "{source}.mpo"
        property string destFileFormat: "mpo"
        property int destLayout
        property bool destLeftFirst
        property string destStdSizeCode
        property int destWidth
        property int destHeight
        property bool destSizeLinked
        property int destJpegQuality
        property int existingFileProcessing
        property bool autoAlignEnabled
        property bool autoAlignHorizontalEnabled
        property bool autoAlignVerticalEnabled
        property bool autoAlignRotateEnabled
        property double autoAlignAccuracy
        property double autoAlignEstimationCenterX
        property double autoAlignEstimationCenterY
        property double autoAlignEstimationSize
        property bool autoColorAdjustEnabled
        property bool autoColorAdjustLeft

        onSourceLayoutChanged: model.sourceFormat.currentLayout = sourceLayout
        onSourceLeftFirstChanged: model.sourceFormat.leftFirst = sourceLeftFirst
        onDestinationFolderChanged: model.destinationFolder = destinationFolder
        onDestinationFilePatternChanged: model.destinationFilePattern = destinationFilePattern
        onDestFileFormatChanged: model.destFileFormatIndex = searchFileFormat(destFileFormat)
        onDestLayoutChanged: model.destFileFormat.currentLayout = destLayout
        onDestLeftFirstChanged: model.destFileFormat.leftFirst = destLeftFirst
        onDestStdSizeCodeChanged: model.destFileFormat.preset.stdSize = standardSizes.find(destStdSizeCode)
        onDestWidthChanged: model.destFileFormat.preset.width = destWidth
        onDestHeightChanged: model.destFileFormat.preset.height = destHeight
        onDestSizeLinkedChanged: model.destFileFormat.preset.sizeLinked = destSizeLinked
        onDestJpegQualityChanged: model.destFileFormat.preset.jpegQuality = destJpegQuality
        onExistingFileProcessingChanged: model.existingFileProcessing = existingFileProcessing
        onAutoAlignEnabledChanged: model.autoAlignEnabled = autoAlignEnabled
        onAutoAlignHorizontalEnabledChanged: model.autoAlignHorizontalEnabled = autoAlignHorizontalEnabled
        onAutoAlignVerticalEnabledChanged: model.autoAlignVerticalEnabled = autoAlignVerticalEnabled
        onAutoAlignRotateEnabledChanged: model.autoAlignRotateEnabled = autoAlignRotateEnabled
        onAutoAlignAccuracyChanged: model.autoAlignAccuracy = autoAlignAccuracy
        onAutoAlignEstimationCenterXChanged: model.autoAlignEstimationCenterX = autoAlignEstimationCenterX
        onAutoAlignEstimationCenterYChanged: model.autoAlignEstimationCenterY = autoAlignEstimationCenterY
        onAutoAlignEstimationSizeChanged: model.autoAlignEstimationSize = autoAlignEstimationSize
        onAutoColorAdjustEnabledChanged: model.autoColorAdjustEnabled = autoColorAdjustEnabled
        onAutoColorAdjustLeftChanged: model.autoColorAdjustLeft

        property bool changed:
            destinationFilePattern != model.destinationFilePattern
            || destFileFormat !== model.destFileFormat.ext
            || sourceLayout != model.sourceFormat.currentLayout
            || sourceLeftFirst != model.sourceFormat.leftFirst
            || destinationFolder != model.destinationFolder
            || destLayout != model.destFileFormat.currentLayout
            || destLeftFirst != model.destFileFormat.leftFirst
            || destStdSizeCode != model.destFileFormat.preset.stdSize.code
            || destWidth != model.destFileFormat.preset.width
            || destHeight != model.destFileFormat.preset.height
            || destJpegQuality != model.destFileFormat.preset.jpegQuality
            || destSizeLinked != model.destFileFormat.preset.sizeLinked
            || existingFileProcessing != model.existingFileProcessing
            || autoAlignEnabled != model.autoAlignEnabled
            || autoAlignHorizontalEnabled != model.autoAlignHorizontalEnabled
            || autoAlignVerticalEnabled != model.autoAlignVerticalEnabled
            || autoAlignRotateEnabled != model.autoAlignRotateEnabled
            || autoAlignAccuracy != model.autoAlignAccuracy
            || autoAlignEstimationCenterX != model.autoAlignEstimationCenterX
            || autoAlignEstimationCenterY != model.autoAlignEstimationCenterY
            || autoAlignEstimationSize != model.autoAlignEstimationSize
            || autoColorAdjustEnabled != model.autoColorAdjustEnabled
            || autoColorAdjustLeft != model.autoColorAdjustLeft

        function searchFileFormat(ext)
        {
            for(var i = 0; i < model.fileFormats.length; i++)
                if(model.fileFormats[i].ext === ext)
                    return i;
            return 0;
        }

        function save()
        {
            destinationFilePattern = model.destinationFilePattern
            destFileFormat = model.destFileFormat.ext
            sourceLayout = model.sourceFormat.currentLayout
            sourceLeftFirst = model.sourceFormat.leftFirst
            destinationFolder = model.destinationFolder
            destLayout = model.destFileFormat.currentLayout
            destLeftFirst = model.destFileFormat.leftFirst
            destStdSizeCode = model.destFileFormat.preset.stdSize.code
            destWidth = model.destFileFormat.preset.width
            destHeight = model.destFileFormat.preset.height
            destJpegQuality = model.destFileFormat.preset.jpegQuality
            destSizeLinked = model.destFileFormat.preset.sizeLinked
            existingFileProcessing = model.existingFileProcessing
            autoAlignEnabled = model.autoAlignEnabled
            autoAlignHorizontalEnabled = model.autoAlignHorizontalEnabled
            autoAlignVerticalEnabled = model.autoAlignVerticalEnabled
            autoAlignRotateEnabled = model.autoAlignRotateEnabled
            autoAlignAccuracy = model.autoAlignAccuracy
            autoAlignEstimationCenterX = model.autoAlignEstimationCenterX
            autoAlignEstimationCenterY = model.autoAlignEstimationCenterY
            autoAlignEstimationSize = model.autoAlignEstimationSize
            autoColorAdjustEnabled = model.autoColorAdjustEnabled
            autoColorAdjustLeft = model.autoColorAdjustLeft
        }
    }

    function appendUrlSources(urls, role)
    {
        var files = urls.map(function(u){ return urlConv.getLocalFile(u) })
        appendSources(files, role)
    }

    /** Добавляет источники в очередь */
    function appendSources(selectedFiles, role)
    {
        var fi = 0;
        for(var i = 0; i < queueList.count; i++)
        {
            var curItem = queueList.get(i)
            if(curItem[role] === "")
            {
                curItem[role] = selectedFiles[fi++]
            }
        }
        for(fi; fi < selectedFiles.length; fi++)
        {
            var item = { source: "", source2: "", destination: "", result: BatchItemCommand.ResultEmpty }
            item[role] = selectedFiles[fi]
            model.queueList.append(item)
        }
        updateDestination()
    }

    /** Удаляет источники */
    function removeSources(selection, role)
    {
        // Очищаем соответсвующее свойство
        selection.forEach(function(index)
        {
            var item = queueList.get(index)
            item[role] = ""
        })
        // Удаляем все пустые элементы очереди
        for(var i = queueList.count - 1; i >= 0; i--)
        {
            var item = queueList.get(i)
            if(item.source.toString() === "" && item.source2.toString() === "")
                queueList.remove(i)
        }
        updateDestination();
    }

    /** Обновляет путь к файлу назначения в очереди заданий */
    function updateDestination()
    {
        var count = queueList.count
        var numberFormat = count.toString().replace(/./g, '0')
        for(var i = 0; i < queueList.count; i++)
        {
            var item = queueList.get(i)
            item.destination = model.destinationFolder + "/" + model.destinationFilePattern
                .replace(/\{number\}/g, F.strFormat((i + 1), numberFormat))
                .replace(/\{source\}/g, urlConv.getPathWithoutSuffix(urlConv.getFileName(item.source)))
                .replace(/\{source2\}/g, urlConv.getPathWithoutSuffix(urlConv.getFileName(item.source2)))
        }
    }

    /// Запускает обработку очереди сначала
    function startWork()
    {
        worker.start()
    }

    /// Останавливает обработку очереди
    function stopWork()
    {
        worker.stop()
    }

    /// Приостанавливает или возобновляет обработку очереди
    function pauseWork()
    {
        if(workPaused)
            worker.resume()
        else
            worker.pause()
    }

    /// Добавляет новый пресет
    function addPreset(name)
    {
        presetsListSettings.add(name)
        currentPresetIndex = presetsModel.rowCount(0) - 1
        saveCurrentPreset()
    }

    function deleteCurrentPreset()
    {
        presetsListSettings.removeAt(currentPresetIndex)
        currentPresetIndex = 0
    }

    /// Сохраняет настройки текущего пресета
    function saveCurrentPreset()
    {
        curSettings.save()
    }

    Component.onCompleted: {
        if(startupArguments.inputFilePaths.length > 0)
            appendSources(startupArguments.inputFilePaths, "source")
    }
}
