import QtQuick 2.5
import QtQuick.Controls 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import Qt.labs.platform 1.0
import MyDialogs 1.0
import StereoImage 1.0
import "functions.js" as F
import Qt.labs.settings 1.0

ApplicationWindow {
    id: window
    title: qsTr("Stereo Photo Batch Processing")
    minimumWidth: 700
    minimumHeight: 700
    visible: true

    Settings {
        id: windowSettings
        category: "BatchWindow"
        property alias width : window.width
        property alias height: window.height
        property alias optionsWidth: optionsBar.width
    }

    SystemPalette {
        id: palette
        colorGroup: SystemPalette.Active
    }
    SystemPalette {
        id: inactivePalette
        colorGroup: SystemPalette.Inactive
    }

    Component {
        id: textColumnDelegate
        Text {
            anchors.verticalCenter: parent.verticalCenter
            color: styleData.textColor
            elide: Text.ElideLeft
            text: styleData.value
            anchors.fill: parent
        }
    }

    Component {
        id: resultColumnDelegate
        Item {
            id: item1
            BusyIndicator {
                anchors.left: parent.left
                anchors.leftMargin: -10
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                running: styleData.value === BatchItemCommand.ResultInProgress
            }
            Image {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 5
                source: switch(styleData.value)
                        {
                        case BatchItemCommand.ResultSucess:
                            return F.getIconSource(palette, "../images/mark-checked.svg")
                        case BatchItemCommand.ResultLoadError:
                        case BatchItemCommand.ResultSkipped:
                        case BatchItemCommand.ResultUnknownDestFileFormat:
                            return F.getIconSource(palette, "../images/error.svg")
                        default:
                            return ""
                        }
                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    ToolTip.visible: containsMouse
                    ToolTip.delay: 500
                    ToolTip.text: switch(styleData.value)
                                  {
                                  case BatchItemCommand.ResultSucess:
                                      return qsTr("Success")
                                  case BatchItemCommand.ResultLoadError:
                                      return qsTr("Load error")
                                  case BatchItemCommand.ResultSkipped:
                                      return qsTr("Omitted, the file exists")
                                  case BatchItemCommand.ResultUnknownDestFileFormat:
                                      return qsTr("Unknown destination format")
                                  default:
                                      return ""
                                  }

                }
            }
        }
    }
    Item{
        anchors.fill: parent
        ColumnLayout {
            anchors.fill: parent
            spacing: 5
            anchors.margins: 10
            SplitView {
                Layout.fillHeight: true
                Layout.fillWidth: true
                ColumnLayout {
                    Layout.fillWidth: true
                    TableView {
                        id: queueTable
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.rightMargin: 5
                        headerVisible: true
                        selectionMode: SelectionMode.ExtendedSelection

                        TableViewColumn {
                            title: qsTr("Source")
                            role: "source"
                            width: (queueTable.width - 75) / 3 - 2
                            delegate: textColumnDelegate
                        }
                        TableViewColumn {
                            title: qsTr("Second source")
                            role: "source2"
                            width: (queueTable.width - 75) / 3 - 2
                            delegate: textColumnDelegate
                        }
                        TableViewColumn {
                            title: qsTr("Destination")
                            role: "destination"
                            width: (queueTable.width - 75) / 3 - 2
                            delegate: textColumnDelegate
                        }
                        TableViewColumn {
                            title: qsTr("Result")
                            role: "result"
                            width: 75
                            delegate: resultColumnDelegate
                        }
                        model: model.queueList
                    }
                    RowLayout {
                        Layout.fillWidth: true
                        Layout.rightMargin: 5
                        Label { text: qsTr("Sources") + ":" }
                        ToolButton { action: actions.addSources }
                        ToolButton { action: actions.removeSources }
                        ToolSeparator { }
                        Label { text: qsTr("Second sources") + ":" }
                        ToolButton { action: actions.addSources2 }
                        ToolButton { action: actions.removeSources2 }
                    }
                }
                ColumnLayout {
                    id: optionsBar
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.fillWidth: true
                    width: 300
                    enabled: !model.stopEnabled
                    GroupBox {
                        id: groupBox
                        title: qsTr("Presets")
                        Layout.fillWidth: true
                        Layout.leftMargin: 5

                        RowLayout {
                            anchors.fill: parent

                            ComboBox {
                                id: presetsListControl
                                Layout.fillWidth: true
                                implicitWidth: 50
                                textRole: "name"
                                currentIndex: model.currentPresetIndex
                                onCurrentIndexChanged: model.currentPresetIndex = currentIndex
                                Binding {
                                    target: presetsListControl
                                    property: "model"
                                    value: model.presetsModel
                                }
                            }
                            Button {
                                tooltip: qsTr("Save")
                                iconSource: F.getIconSource(palette, "../images/document-save.svg")
                                enabled: model.presetChanged
                                onClicked: model.saveCurrentPreset()
                            }
                            Button {
                                tooltip: qsTr("Save as…")
                                iconSource: F.getIconSource(palette, "../images/document-save-as.svg")
                                onClicked:  {
                                    var curName = model.presetsModel.get(model.currentPresetIndex).name
                                    var name = qsTr("%1 copy").arg(curName)
                                    savePresetAsWindow.open(name)
                                }
                            }
                            Button {
                                tooltip: qsTr("Delete")
                                iconSource: "../images/entry-delete.svg"
                                enabled: model.currentPresetIndex > 0
                                onClicked: model.deleteCurrentPreset()
                            }
                        }
                    }
                    GroupBox {
                        Layout.fillWidth: true
                        Layout.fillHeight: false
                        Layout.leftMargin: 5
                        title: qsTr("Default source format")

                        GridLayout {
                            anchors.fill: parent
                            columns: 2
                            Label { text: qsTr("Layout") + ":" }
                            ComboBox {
                                id: sourceLayoutCombo
                                Layout.fillWidth: true
                                implicitWidth: 50
                                ToolTip.text: qsTr("If the format cannot be detected automatically, this format is used")
                                ToolTip.visible: hovered
                                textRole: "name"
                                model: model.sourceFormat.layoutsModel
                                onCurrentIndexChanged: model.sourceFormat.currentLayoutIndex = currentIndex
                                Binding {
                                    target: sourceLayoutCombo
                                    property: "currentIndex"
                                    value: model.sourceFormat.currentLayoutIndex
                                }
                            }
                            Label { text: qsTr("Reverse") + ":" }
                            CheckBox {
                                id: sourcereverseCheckBox
                                onCheckedChanged: model.sourceFormat.leftFirst = !checked
                                ToolTip.text: qsTr("If the format cannot be detected automatically, this format is used")
                                ToolTip.visible: hovered
                                Binding {
                                    target: sourcereverseCheckBox
                                    property: "checked"
                                    value: !model.sourceFormat.leftFirst
                                }
                            }
                        }
                    }
                    GroupBox {
                        Layout.fillWidth: true
                        Layout.fillHeight: false
                        Layout.leftMargin: 5
                        title: qsTr("Actions")

                        ColumnLayout {
                            anchors.fill: parent
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            CheckBox {
                                id: autoAlignEnabledCheckBox
                                text: qsTr("Auto align")
                                onCheckedChanged: model.autoAlignEnabled = checked
                                Binding {
                                    target: autoAlignEnabledCheckBox
                                    property: "checked"
                                    value: model.autoAlignEnabled
                                }
                            }
                            ColumnLayout {
                                Layout.fillWidth: true
                                visible: model.autoAlignEnabled
                                Layout.leftMargin: 20
                                CheckBox {
                                    id: autoAlignHorizontalEnabledCheckBox
                                    text: qsTr("Horizontal");
                                    onCheckedChanged: model.autoAlignHorizontalEnabled = checked
                                    Binding {
                                        target: autoAlignHorizontalEnabledCheckBox
                                        property: "checked"
                                        value: model.autoAlignHorizontalEnabled
                                    }
                                }
                                CheckBox {
                                    id: autoAlignVerticalEnabledCheckBox
                                    text: qsTr("Vertical");
                                    onCheckedChanged: model.autoAlignVerticalEnabled = checked
                                    Binding {
                                        target: autoAlignVerticalEnabledCheckBox
                                        property: "checked"
                                        value: model.autoAlignVerticalEnabled
                                    }
                                }
                                CheckBox {
                                    id: autoAlignRotateEnabledCheckBox
                                    text: qsTr("Rotate");
                                    onCheckedChanged: model.autoAlignRotateEnabled = checked
                                    Binding {
                                        target: autoAlignRotateEnabledCheckBox
                                        property: "checked"
                                        value: model.autoAlignRotateEnabled
                                    }
                                }
                                RowLayout {
                                    Label {
                                        text: qsTr("Accuracy") + ":"
                                    }
                                    Slider {
                                        id: autoAlignAccuracySlider
                                        Layout.fillWidth: true
                                        implicitWidth: 50
                                        maximumValue: 100
                                        minimumValue: 0
                                        stepSize: 10
                                        onValueChanged: model.autoAlignAccuracy = value
                                        Binding {
                                            target: autoAlignAccuracySlider
                                            property: "value"
                                            value: model.autoAlignAccuracy
                                        }
                                    }
                                }
                                RowLayout {
                                    Label {
                                        text: qsTr("Area of estimation") + ": "
                                    }
                                    Rectangle {
                                        id: imageArea
                                        property SystemPalette palette: enabled ? palette : inactivePalette

                                        width: 120
                                        height: width * 2/3
                                        color: palette.light
                                        border.color: palette.dark
                                        Rectangle {
                                            id: estimationArea
                                            color: parent.palette.highlight
                                            width: parent.width * model.autoAlignEstimationSize
                                            height: parent.height * model.autoAlignEstimationSize
                                            x: model.autoAlignEstimationCenterX * parent.width - estimationArea.width / 2
                                            y: model.autoAlignEstimationCenterY * parent.height - estimationArea.height / 2
                                            onXChanged: model.autoAlignEstimationCenterX = (estimationArea.x + estimationArea.width / 2) / imageArea.width
                                            onYChanged: model.autoAlignEstimationCenterY = (estimationArea.y + estimationArea.height / 2) / imageArea.height
                                        }
                                        MouseArea {
                                            anchors.fill: parent
                                            hoverEnabled: true
                                            ToolTip.text: qsTr("Drag to move, mouse wheel to resize")
                                            ToolTip.visible: containsMouse
                                            ToolTip.delay: 500
                                            onWheel: {
                                                var k = model.autoAlignEstimationSize + wheel.angleDelta.y / 10000
                                                if(k > 1) k = 1
                                                if(k < 0.05) k = 0.05
                                                model.autoAlignEstimationSize = k
                                            }
                                            drag {
                                                target: estimationArea
                                                minimumX: 0
                                                minimumY: 0
                                                maximumX: imageArea.width - estimationArea.width
                                                maximumY: imageArea.height - estimationArea.height
                                                smoothed: true
                                                axis: Drag.XAxis | Drag.YAxis

                                            }
                                        }
                                    }
                                }
                            }
                            CheckBox {
                                id: autoColorAdjustEnabledCheckBox
                                text: qsTr("Auto Color Adjustment")
                                onCheckedChanged: model.autoColorAdjustEnabled = checked
                                Binding {
                                    target: autoColorAdjustEnabledCheckBox
                                    property: "checked"
                                    value: model.autoColorAdjustEnabled
                                }
                            }
                            ColumnLayout {
                                visible: model.autoColorAdjustEnabled
                                Layout.leftMargin: 20
                                ExclusiveGroup {
                                    id: colorAdjustSelect
                                }
                                RadioButton {
                                    id: autoColorAdjustLeftRadio
                                    text: qsTr("Adjust left view")
                                    exclusiveGroup: colorAdjustSelect
                                    onCheckedChanged: model.autoColorAdjustLeft = checked
                                    Binding {
                                        target: autoColorAdjustLeftRadio
                                        property: "checked"
                                        value: model.autoColorAdjustLeft
                                    }
                                }
                                RadioButton {
                                    id: autoColorAdjustRightRadio
                                    text: qsTr("Adjust right view")
                                    exclusiveGroup: colorAdjustSelect
                                }
                            }
                        }
                    }
                    GroupBox {
                        Layout.fillWidth: true
                        Layout.fillHeight: false
                        Layout.leftMargin: 5
                        title: qsTr("Save options")
                        ColumnLayout
                        {
                            anchors.fill: parent
                            GroupBox {
                                title: qsTr("Format")
                                Layout.fillWidth: true
                                GridLayout {
                                    anchors.fill: parent
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    columns: 2
                                    Label { text: qsTr("File type") + ":" }
                                    ComboBox {
                                        Layout.fillWidth: true
                                        implicitWidth: 50
                                        textRole: "name"
                                        currentIndex: model.destFileFormatIndex
                                        model: model.fileFormats
                                        onCurrentIndexChanged: model.destFileFormatIndex = currentIndex
                                    }
                                    Label { text: qsTr("Layout") + ":" }
                                    ComboBox {
                                        id: destLayoutCombo
                                        Layout.fillWidth: true
                                        implicitWidth: 50
                                        textRole: "name"
                                        enabled: model.destFileFormat.layoutsModel.count > 1
                                        model: model.destFileFormat.layoutsModel

                                        onCurrentIndexChanged: model.destFileFormat.currentLayoutIndex = currentIndex
                                        Binding {
                                            target: destLayoutCombo
                                            property: "currentIndex"
                                            value: model.destFileFormat.currentLayoutIndex
                                        }
                                    }
                                    Label { text: qsTr("Reverse") + ":" }
                                    CheckBox {
                                        id: destReverseCheckBox
                                        enabled: model.destFileFormat.currentLayout !== StereoFormat.Default
                                        onCheckedChanged: model.destFileFormat.leftFirst = !checked
                                        Binding {
                                            target: destReverseCheckBox
                                            property: "checked"
                                            value: !model.destFileFormat.leftFirst
                                        }
                                    }
                                    Label {
                                        id: sizeLabel
                                        text: qsTr("Size") + ":"
                                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                                    }
                                    ColumnLayout {
                                        Layout.fillWidth: true
                                        spacing: 5
                                        ComboBox {
                                            id: stdSizeCombo
                                            Layout.fillWidth: true
                                            implicitWidth: 50
                                            model: standardSizes.data
                                            textRole: "name"
                                            onCurrentIndexChanged: model.destFileFormat.preset.stdSize = standardSizes.at(currentIndex)
                                            Binding {
                                                target: stdSizeCombo
                                                property: "currentIndex"
                                                value: standardSizes.indexOf(model.destFileFormat.preset.stdSize)
                                            }
                                        }
                                        RowLayout {
                                            spacing: 5
                                            visible: !model.destFileFormat.preset.stdSize.isAutoSize
                                            SpinBox {
                                                id: widthEditor
                                                Layout.fillWidth: true
                                                enabled: model.destFileFormat.preset.stdSize.isManual
                                                maximumValue: 10000
                                                minimumValue: 0
                                                onValueChanged: model.destFileFormat.preset.width = value
                                                ToolTip.text: qsTr("Width")
                                                ToolTip.visible: hovered
                                                ToolTip.delay: 500
                                                Binding {
                                                    target: widthEditor
                                                    property: "value"
                                                    value: model.destFileFormat.preset.width
                                                }
                                            }
                                            Button {
                                                id: linkButton
                                                enabled: model.destFileFormat.preset.stdSize.isManual
                                                iconSource: F.getIconSource(palette, "../images/link.svg")
                                                implicitWidth: implicitHeight
                                                anchors.verticalCenter: parent.verticalCenter
                                                checkable: true
                                                onCheckedChanged: model.destFileFormat.preset.sizeLinked = checked
                                                ToolTip.text: qsTr("Keep aspect ratio") + (checked ? " (" + qsTr("enabled") + ")" : "")
                                                ToolTip.visible: hovered
                                                ToolTip.delay: 500
                                                Binding {
                                                    target: linkButton
                                                    property: "checked"
                                                    value: model.destFileFormat.preset.sizeLinked
                                                }
                                            }
                                            SpinBox {
                                                id: heightEditor
                                                Layout.fillWidth: true
                                                enabled: model.destFileFormat.preset.stdSize.isManual
                                                maximumValue: 10000
                                                minimumValue: 0
                                                onValueChanged: model.destFileFormat.preset.height = value
                                                ToolTip.text: qsTr("Height")
                                                ToolTip.visible: hovered
                                                ToolTip.delay: 500
                                                Binding {
                                                    target: heightEditor
                                                    property: "value"
                                                    value: model.destFileFormat.preset.height
                                                }
                                            }
                                        }
                                    }
                                    Label { text: qsTr("Jpeg quality") + ":" }
                                    RowLayout {
                                        id: row
                                        spacing: 5

                                        Slider {
                                            id: qualitySlider
                                            Layout.fillWidth: true
                                            implicitWidth: 50
                                            minimumValue: 0
                                            maximumValue: 100
                                            stepSize: 1
                                            onValueChanged: model.destFileFormat.preset.jpegQuality = qualitySlider.value
                                            Binding{
                                                target: qualitySlider
                                                property: "value"
                                                value: model.destFileFormat.preset.jpegQuality
                                            }
                                        }

                                        Label{
                                            text : F.strFormat(qualitySlider.value, "000")
                                            anchors.verticalCenter: parent.verticalCenter
                                        }
                                    }
                                }
                            }
                            GroupBox {
                                title: qsTr("Location")
                                Layout.fillWidth: true
                                GridLayout {
                                    anchors.fill: parent
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    columns: 2
                                    Label { text:qsTr("Folder") + ":" }
                                    RowLayout {
                                        TextField {
                                            id: folderText
                                            Layout.fillWidth: true
                                            implicitWidth: 50
                                            onTextChanged: model.destinationFolder = text
                                            Binding {
                                                target: folderText
                                                property: "text"
                                                value: model.destinationFolder
                                            }
                                        }
                                        Button {
                                            Layout.fillHeight: true
                                            Layout.preferredWidth: height
                                            iconSource: F.getIconSource(palette, "../images/document-open.svg")
                                            tooltip: qsTr("Open")
                                            onClicked: folderDialog.execute(folderText.text)
                                        }
                                    }
                                    Label { text: qsTr("File name pattern") + ":" }
                                    TextField {
                                        id: filePattern
                                        Layout.fillWidth: true
                                        implicitWidth: 50
                                        ToolTip.text: qsTr("The following variables are available") + ":"
                                                      + "\n• {source} - " + qsTr("source file name without extension") + ";"
                                                      + "\n• {source2} - " + qsTr("second source file name without extension") + ";"
                                                      + "\n• {number} - " + qsTr("order number") + "."
                                        ToolTip.visible: hovered
                                        onTextChanged: model.destinationFilePattern = text
                                        Binding {
                                            target: filePattern
                                            property: "text"
                                            value: model.destinationFilePattern
                                        }
                                    }
                                    Label { text: qsTr("Existing Files") + ":" }
                                    ComboBox {
                                        id: existsFileProcessingCombo
                                        Layout.fillWidth: true
                                        implicitWidth: 50
                                        model: model.existingFileProcessingVariants
                                        onCurrentIndexChanged: model.existingFileProcessing = currentIndex
                                        Binding {
                                            target: existsFileProcessingCombo
                                            property: "currentIndex"
                                            value: model.existingFileProcessing
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            RowLayout {
                Layout.fillWidth: true
                Button { action: actions.about }
                Item { Layout.fillWidth: true }
                Button { action: actions.start }
                Button { action: actions.stop }
                Button { action: actions.pause }
                Button { action: actions.close }
            }
        }

        DropArea {
            id: leftDropArea
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width / 2

            Rectangle {
                color: leftDropArea.containsDrag ? palette.highlight : palette.base
                border.color: palette.dark
                visible: leftDropArea.containsDrag || rightDropArea.containsDrag
                anchors.fill: parent
                Label {
                    anchors.centerIn: parent
                    font.bold: true
                    color: leftDropArea.containsDrag ? palette.highlightedText : palette.text
                    text: qsTr("Add Sources")
                }
            }
            onDropped: if(drop.hasUrls) model.appendUrlSources(drop.urls, "source")
        }
        DropArea {
            id: rightDropArea
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            width: parent.width / 2
            Rectangle {
                color: rightDropArea.containsDrag ? palette.highlight : palette.base
                border.color: palette.dark
                visible: leftDropArea.containsDrag || rightDropArea.containsDrag
                anchors.fill: parent
                Label {
                    anchors.centerIn: parent
                    font.bold: true
                    color: rightDropArea.containsDrag ? palette.highlightedText : palette.text
                    text: qsTr("Add second sources")
                }
            }
            onDropped: if(drop.hasUrls) model.appendUrlSources(drop.urls, "source2")
        }
    }

    QtObject {
        id: actions

        property Action start: Action {
            text: qsTr("Start")
            enabled: model.startEnabled
            onTriggered: model.startWork()
        }
        property Action stop: Action {
            text: qsTr("Stop")
            enabled: model.stopEnabled
            onTriggered: model.stopWork()
        }
        property Action pause: Action {
            text: model.workPaused ? qsTr("Resume") : qsTr("Pause")
            enabled: model.pauseEnabled
            onTriggered: model.pauseWork()
        }
        property Action close: Action {
            text: qsTr("Close")
            onTriggered: window.close()
        }
        property Action addSources: Action {
            iconSource: F.getIconSource(palette, "../images/document-open.svg")
            text: qsTr("Add Sources")
            onTriggered: addSourcesFileDialog.open()
        }
        property Action addSources2: Action {
            iconSource: F.getIconSource(palette, "../images/document-open.svg")
            text: qsTr("Add second sources")
            onTriggered: addSources2FileDialog.open()
        }
        property Action removeSources: Action {
            iconSource: "../images/entry-delete.svg"
            text: qsTr("Remove selected sources")
            enabled: queueTable.selection.count > 0
            onTriggered: model.removeSources(queueTable.selection, "source")
        }
        property Action removeSources2: Action {
            iconSource: "../images/entry-delete.svg"
            text: qsTr("Remove selected second sources")
            enabled: queueTable.selection.count > 0
            onTriggered: model.removeSources(queueTable.selection, "source2")
        }
        property Action about: Action {
            text: qsTr("About")
            onTriggered: {
                var component = Qt.createComponent("About.qml")
                var aboutDialog = component.createObject(window)
                aboutDialog.show()
            }
        }
    }

    NewPresetWindow {
        id: savePresetAsWindow
        onAccepted: model.addPreset(name)
    }

    MyFileDialog {
        id: addSourcesFileDialog
        title: qsTr("Add Sources")
        selectExisting: true
        acceptMode: MyFileDialog.AcceptOpen
        path: standardPaths.pictures
        allowMultipleFiles: true
        nameFilters: model.nameFilters
        onAccepted: model.appendSources(selectedFiles, "source")
    }

    MyFileDialog {
        id: addSources2FileDialog
        title: qsTr("Add second sources")
        selectExisting: true
        acceptMode: MyFileDialog.AcceptOpen
        path: standardPaths.pictures
        allowMultipleFiles: true
        nameFilters: model.nameFilters
        onAccepted: model.appendSources(selectedFiles, "source2")
    }

    FolderDialog {
        id: folderDialog
        onAccepted: folderText.text = urlConv.getLocalFile(currentFolder)

        function execute(folder)
        {
            currentFolder = urlConv.getUrl(folder)
            open()
        }
    }

    BatchProcessingModel {
        id: model
    }

    onClosing: model.stopWork()
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:900;width:700}
}
##^##*/
