import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

import "functions.js" as F

RowLayout {
    id: self

    property alias quality: qualitySlider.value
    property alias value: bitrateSpinBox.value
    property double rawBirate: 0

    onQualityChanged:
    {
        p.isChnaging = true
        bitrateSpinBox.value = p.getBitrate()
        p.isChnaging = false
    }
    onRawBirateChanged:
    {
        p.isChnaging = true
        bitrateSpinBox.value = p.getBitrate()
        p.isChnaging = false
    }

    Slider {
        id: qualitySlider
        Layout.fillWidth: true
        minimumValue: 0
        maximumValue: 0.2
        stepSize: 0.01
        value: 0
        tickmarksEnabled: true
        onValueChanged: {
            if(!p.isChnaging)
            {
                p.isChnaging = true
                bitrateSpinBox.value = p.getBitrate()
                p.isChnaging = false
            }
        }
    }

    Label {
        text: "(" + F.strFormat((qualitySlider.value * 100).toFixed(), "00") + "%)"
    }

    SpinBox {
        id: bitrateSpinBox
        implicitWidth: 80
        minimumValue: 0
        maximumValue: 1000000000000
        onValueChanged: {
            if(!p.isChnaging)
            {
                p.isChnaging = true
                qualitySlider.value = bitrateSpinBox.value * 8 * 1024 / rawBirate
                p.isChnaging = false
            }
        }
    }
    Label { text: qsTr("KiB/s") }

    QtObject {
        id: p
        function getBitrate()
        {
            return rawBirate * qualitySlider.value / 8 / 1024;
        }

        property bool isChnaging: false;
    }
}
