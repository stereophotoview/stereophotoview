import QtQuick 2.0
import StereoImage 1.0
import QtQuick.Controls 1.4

ApplicationWindow {
    visible: true
    width: 400
    height: 100
    Chart {
        anchors.fill: parent
        colors: { "offsetX": "red", "offsetY": "blue", "leftAngle": "green", "rightAngle": "magenta" }
        maxPos: 84
        markers: [
            {pos: 0, offsetX: 0, offsetY: 0, leftAngle: 0, rightAngle: 0},
            {pos: 40, offsetX: -2, offsetY: 0, leftAngle: 0, rightAngle: 0},
            {pos: 84, offsetX: 0, offsetY: 0, leftAngle: 0, rightAngle: 0}
        ]
    }

}
