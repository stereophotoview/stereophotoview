import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: button
    property string imageSource : ""
    property string text : action.text
    property alias textColor : buttonLabel.color
    property Action action : Action { }
    signal clicked()

    width: buttonLabel.paintedWidth
    height: buttonLabel.paintedHeight
    Text {
        id: buttonLabel
        text: (button.imageSource.length > 0 ? "<img src='../images/" + imageSource + "'/> " : "") + button.text
    }
    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked: {
            button.clicked()
            if(action)
                action.trigger()
        }
    }
}

