import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import Qt.labs.settings 1.0

Item {
    id: self

    height: row.height + 12
    enabled: self.visible // Чтобы снимать фокус с элементов ввода при закрытии, для последующей работы горячих клавиш

    property double borderWidthPercents

    property Selector selector
    property color textColor
    signal crop
    signal canceled
    property int buttonHeight: 24
    property int buttonWidth: 100

    Settings {
        id: settings
        category: "CropEditor"
        property alias borderEnabled: borderCheckbox.checked
        property alias borderWidthPercents: borderWithEdit.value
    }

    Binding {
        target: self
        property: "borderWidthPercents"
        value: borderCheckbox.checked ? borderWithEdit.value : 0
    }

    Binding {
        target: selector
        property: "borderWidthPercents"
        value: self.borderWidthPercents
    }

    RowLayout {
        id: row
        anchors.centerIn: parent

        Action {
            id: cropAction
            text: qsTr("Crop")
            shortcut: "Return"
            tooltip: qsTr("%1 [%2]").arg(text).arg(shortcut)
            enabled: self.visible
            onTriggered: self.crop()
        }

        Action {
            id: cancel
            text: qsTr("Cancel")
            shortcut: "Escape"
            tooltip: qsTr("%1 [%2]").arg(text).arg(shortcut)
            enabled: self.visible
            onTriggered: self.canceled()
        }

        ListModel {
            id : ratioModel
            ListElement { width: 0; height: 0; name: qsTr("Free") }
            ListElement { width: 4; height: 3; name: qsTr("Album 4:3") }
            ListElement { width: 3; height: 2; name: qsTr("Album 3:2") }
            ListElement { width: 2; height: 1; name: qsTr("Album 2:1") }
            ListElement { width: 14; height: 9; name: qsTr("Album 14:9") }
            ListElement { width: 16; height: 9; name: qsTr("Album 16:9") }
            ListElement { width: 16; height: 10; name: qsTr("Album 16:10") }

            ListElement { width: 1; height: 1; name: qsTr("Square 1:1") }

            ListElement { width: 3; height: 4; name: qsTr("Portrait 3:4") }
            ListElement { width: 2; height: 3; name: qsTr("Portrait 2:3") }
            ListElement { width: 1; height: 2; name: qsTr("Portrait 1:2") }
            ListElement { width: 9; height: 14; name: qsTr("Portrait 9:14") }
            ListElement { width: 9; height: 16; name: qsTr("Portrait 9:16") }
            ListElement { width: 10; height: 16; name: qsTr("Portrait 10:16") }
        }

        Text {
            color: textColor
            text: qsTr("Aspect ratio") + ":"
        }

        ComboBox {
            id: ratioCombo
            model: ratioModel
            implicitWidth: 150
            implicitHeight: buttonHeight
            textRole: "name"
            onCurrentIndexChanged: setAspectRatio()
        }

        CheckBox {
            id: borderCheckbox
            text: qsTr("Border") + ":"
            Layout.leftMargin: 6
        }

        SpinBox {
            id: borderWithEdit
            enabled: borderCheckbox.checked
            value: 5
            maximumValue: 60
            minimumValue: 0
        }

        Text {
            color: textColor
            text: qsTr("%")
        }

        Button {
            Layout.leftMargin: 6
            action: cropAction
            implicitHeight: buttonHeight
            implicitWidth: buttonWidth
        }

        Button {
            action: cancel
            implicitHeight: buttonHeight
            implicitWidth: buttonWidth
        }
    }

    onVisibleChanged: {
        if(visible)
        {
            selector.fill()
            setAspectRatio()
        }
    }

    function setAspectRatio()
    {
        var currentItem = ratioModel.get(ratioCombo.currentIndex)
        selector.setRatio(currentItem.width, currentItem.height)
    }    
}
