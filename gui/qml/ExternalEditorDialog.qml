import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import StereoImage 1.0

ApplicationWindow {
    id: window
    modality: Qt.WindowModal
    flags: Qt.Dialog
    minimumHeight: 270
    minimumWidth: 600
    width: 600
    height: 270

    property ExternalEditorWatcher editor

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10
        Item {
            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: true
            Layout.preferredHeight: 20
            Label {
                text: qsTr("Before:")
                anchors.right: parent.horizontalCenter
                anchors.rightMargin: 5
                anchors.left: parent.left
                anchors.leftMargin: 0
            }
            Label {
                text: qsTr("After:")
                horizontalAlignment: Text.AlignLeft
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.horizontalCenter
                anchors.leftMargin: 5
            }
        }
        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 0
            Image {
                id: beforeImage
                source: editor ? urlConv.getUrl(editor.filePath) : ""
                fillMode: Image.PreserveAspectFit
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.right: parent.horizontalCenter
                anchors.rightMargin: 5
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
            }
            Image {
                id: afterImage
                source: editor ? urlConv.getUrl(ditor.filePath) : ""
                fillMode: Image.PreserveAspectFit
                anchors.left: parent.horizontalCenter
                anchors.leftMargin: 5
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
            }
        }
        Row {
            spacing: 10
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Button {
                enabled: editor && editor.isChanged
                text: qsTr("Import")
                onClicked: onImportClick()
            }
            Button {
                text: qsTr("Cancel")
                onClicked: window.close()
            }
        }
    }

    function init(editor, title)
    {
        window.title = title
        window.editor = editor
        window.editor.openExternal()
        afterImage.source = urlConv.getUrl(editor.filePath)
        show()
    }

    function onImportClick()
    {
        editor.confirm()
        window.close()
    }

    onActiveChanged: {
        if(active)
        {
            editor.checkChanged()
        }
    }

    Connections {
        target: editor
        onChanged: {
            afterImage.cache = false
            afterImage.source = ""
            afterImage.source = urlConv.getUrl(editor.filePath)
        }
    }

    onClosing: if(window.editor) window.editor.destroy()
}
