import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: self
    property double borderWidthPercents: 5

    property double borderWidthPixels: Math.min(width, height) * borderWidthPercents / 100

    LinearGradient {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: borderWidthPixels
        start: Qt.point(0, 0)
        end: Qt.point(0, borderWidthPixels)
        gradient: Gradient {
            GradientStop { position: 0.0; color: "black" }
            GradientStop { position: 1; color: "#00000000" }
        }
    }
    LinearGradient {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: borderWidthPixels
        start: Qt.point(borderWidthPixels, 0)
        end: Qt.point(0, 0)
        gradient: Gradient {
            GradientStop { position: 0.0; color: "black" }
            GradientStop { position: 1; color: "#00000000" }
        }
    }
    LinearGradient {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: borderWidthPixels
        start: Qt.point(0, borderWidthPixels)
        end: Qt.point(0, 0)
        gradient: Gradient {
            GradientStop { position: 0.0; color: "black" }
            GradientStop { position: 1; color: "#00000000" }
        }
    }
    LinearGradient {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: borderWidthPixels
        start: Qt.point(0, 0)
        end: Qt.point(borderWidthPixels, 0)
        gradient: Gradient {
            GradientStop { position: 0.0; color: "black" }
            GradientStop { position: 1; color: "#00000000" }
        }
    }
}
