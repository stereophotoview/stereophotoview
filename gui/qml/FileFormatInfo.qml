import QtQuick 2.0
import StereoImage 1.0

/** Информация о формате файла */
Item {
    /** Текущие установки */
    property Preset preset: preset

    property alias currentLayout: stereoFormat.layout
    onCurrentLayoutChanged: setLayout(currentLayout)

    property alias leftFirst: stereoFormat.leftFirst

    /** Индекс текущего варианта расположения ракурсов */
    property int currentLayoutIndex: 0
    onCurrentLayoutIndexChanged: updateLayout()

    property string ext

    /** Список возможных вариантов расположения ракурсов */
    property ListModel layoutsModel : ListModel { }
    onLayoutsModelChanged: updateLayout()

    /** Варианты расположения ракурсов для jps файлов */
    property ListModel jpsLayouts: ListModel {
        ListElement { targetType: StereoFormat.Default; name: "" }
        ListElement { targetType: StereoFormat.RowInterlaced; name: "" }
        ListElement { targetType: StereoFormat.ColumnInterlaced; name: "" }
        ListElement { targetType: StereoFormat.AnaglyphRC; name: "" }
        ListElement { targetType: StereoFormat.AnaglyphYB; name: "" }
        ListElement { targetType: StereoFormat.Horizontal; name: "" }
        ListElement { targetType: StereoFormat.AnamorphHorizontal; name: "" }
        ListElement { targetType: StereoFormat.Vertical; name: "" }
        ListElement { targetType: StereoFormat.AnamorphVertical; name: "" }
        ListElement { targetType: StereoFormat.Monoscopic; name: "" }
    }

    /** Варианты расположения ракурсов для загрузки jps файлов */
    property ListModel jpgSourceLayouts: ListModel {
        ListElement { targetType: StereoFormat.Monoscopic; name: "" }
        ListElement { targetType: StereoFormat.RowInterlaced; name: "" }
        ListElement { targetType: StereoFormat.ColumnInterlaced; name: "" }
        ListElement { targetType: StereoFormat.AnaglyphRC; name: "" }
        ListElement { targetType: StereoFormat.AnaglyphYB; name: "" }
        ListElement { targetType: StereoFormat.Horizontal; name: "" }
        ListElement { targetType: StereoFormat.AnamorphHorizontal; name: "" }
        ListElement { targetType: StereoFormat.Vertical; name: "" }
        ListElement { targetType: StereoFormat.AnamorphVertical; name: "" }
    }

    /** Варианты расположения ракурсов для mpo файлов */
    property ListModel mpoLayouts: ListModel {
        ListElement { targetType: StereoFormat.Separate; name: "" }
    }

    /** Варианты расположения ракурсов для jpeg файлов */
    property ListModel jpegLayouts: ListModel {
        ListElement { targetType: StereoFormat.Monoscopic; name: "" }
    }

    Preset {
        id: preset
        stereoFormat: stereoFormat
    }

    StereoFormat {
        id: stereoFormat
    }

    Component.onCompleted: {
        fillNames(jpsLayouts)
        fillNames(jpgSourceLayouts)
        fillNames(mpoLayouts)
        fillNames(jpegLayouts)
    }

    function fillNames(listModel)
    {
        for(var i = 0; i < listModel.count; i++)
        {
            var item = listModel.get(i)
            if(item.name.length === 0)
                item.name = stereoFormat.layoutName(item.targetType)
        }
    }

    function updateLayout()
    {
        preset.stereoFormat.layout = layoutsModel.get(currentLayoutIndex).targetType
    }

    function setLayout(layout)
    {
        currentLayoutIndex = getLayoutIndex(layout)
    }

    function getLayoutIndex(layout)
    {
        for(var i = 0; i < layoutsModel.count; i++)
        {
            if(layoutsModel.get(i).targetType === layout)
                return i;
        }
        return 0;
    }
}
