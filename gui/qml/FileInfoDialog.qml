import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import StereoImage 1.0

ApplicationWindow {
    id: window

    property ListModel leftMetadata: ListModel {}
    property ListModel rightMetadata: ListModel {}

    SystemPalette { id: palette; colorGroup: SystemPalette.Active; }

    title: qsTr("Information")
    modality: Qt.WindowModal
    flags: Qt.Dialog
    width: 500
    height: 450

    Action {
        shortcut: "Escape"
        onTriggered: window.close()
    }

    ColumnLayout {
        id: columnLayout1
        anchors.fill: parent
        anchors.margins: 10
        spacing: 0

        Row {
            id: row1
            spacing: 10
            Layout.topMargin: 10
            ExclusiveGroup { id: imageSelect }
            RadioButton {
                id: leftImage
                text: qsTr("Left image")
                checked: true
                exclusiveGroup: imageSelect
            }

            RadioButton {
                id: rightImage
                text: qsTr("Right image")
                exclusiveGroup: imageSelect
            }
        }

        ScrollView {
            id: scrollbar
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.topMargin: 10
            Layout.bottomMargin: 10

            ListView {
                id: exifList
                width: scrollbar.viewport.width
                model: leftImage.checked ? leftMetadata : rightMetadata
                interactive: false

                delegate: Rectangle {
                    width: parent.width
                    height: row.height
                    color: "transparent"
                    Row {
                        id: row
                        anchors.left: parent.left
                        anchors.leftMargin: 5
                        width: parent.width
                        spacing: 5
                        Text{
                            id: attrColumn
                            color: palette.text
                            width: parent.width / 2
                            wrapMode: Text.Wrap
                            text: attribute + ":"
                        }
                        TextEdit {
                            color: palette.text
                            width: parent.width - attrColumn.width - 10
                            readOnly: true
                            wrapMode: Text.Wrap
                            selectByMouse: true
                            text: value
                            onFocusChanged: if(!focus) select(0,0)
                        }
                    }
                }
                section.property: "section"
                section.criteria: ViewSection.FullString
                section.delegate:
                    Rectangle {
                        width: parent.width
                        height: text.height + 4
                        color: palette.highlight
                        Text {
                            id: text
                            horizontalAlignment: Text.AlignLeft
                            verticalAlignment: Text.AlignVCenter
                            text: section
                            font.bold: true
                            color: palette.highlightedText
                            anchors.left: parent.left
                            anchors.leftMargin: 5
                            anchors.top: parent.top
                            anchors.topMargin: 2
                        }
                    }
            }
        }
        Row {
            //Layout.topMargin: 5
            Layout.alignment: Qt.AlignCenter | Qt.AlignVCenter
            Layout.margins: 5
            spacing: 5
            Button {
                text: qsTr("OK")
                onClicked: {
                    window.close()
                    leftMetadata = 0
                    rightMetadata = 0
                }
            }
        }
    }
}
