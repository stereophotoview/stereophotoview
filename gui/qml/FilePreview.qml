import QtQuick 2.5
import QtQuick.Controls 1.4
import StereoImage 1.0


Item {
    id: self

    property string filePath
    property bool isVideo
    property double imageOffsetX: 0
    property double imageOffsetY: 0

    BusyIndicator {
        anchors.centerIn: parent
        running: originalImage.status === Image.Loading
    }

    Image {
        id: originalImage
        antialiasing: true
        anchors.centerIn: parent
        anchors.verticalCenterOffset: imageOffsetX
        anchors.horizontalCenterOffset: imageOffsetY
        width: self.width
        height: self.height
        sourceSize.width: getSourceSize(width)
        sourceSize.height: getSourceSize(height)
        source: self.filePath ? "image://preview/" + self.filePath : ""
        asynchronous: true
        fillMode: Image.PreserveAspectFit;
        visible: status == Image.Ready
        Rectangle {
            visible: self.isVideo
            radius: 4
            color: "#40000000"
            anchors.centerIn: parent
            width: 32
            height: 32
            Image {
                anchors.centerIn: parent
                source: "../images/media-playback-start32-dark.svg"
            }
        }

        function getSourceSize(v)
        {
            if(v <= 256)
                return 256
            return 400
        }
    }
}
