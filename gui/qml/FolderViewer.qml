import QtQuick 2.5
import QtQuick.Controls 1.4
import Qt.labs.folderlistmodel 2.1
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import "functions.js" as F

SplitView {
    id: self
    property color textColor
    property Model model
    property alias selectedFolder: foldersTree.selectedFolder

    Settings {
        category: "Interface"
        property alias folderTreeWidth: foldersTree.width
        property alias preViewSize: sizeSlider.value
    }

    FolderListModel {
        id: folderModel
        showDirs: true
        sortField: FolderListModel.Name
        nameFilters: self.model.supportednameFilters || []
        onFolderChanged: grid.foldersModel = folderModel
    }

    FoldersTree {
        id: foldersTree
        width: 200
        Layout.minimumWidth: 100
        onSelectedFolderChanged: self.openDir(selectedFolder)
    }

    ColumnLayout
    {
        Layout.margins: 5
        Layout.minimumWidth: 220
        Label {
            height: 22
            padding: 2
            visible: self.isRecentFolders()
            text: qsTr("Recent folders")
        }
        Flickable {
            visible: !self.isRecentFolders()
            Layout.fillWidth: true
            height: 22
            contentHeight: pathRow.height
            contentWidth: pathRow.width
            clip: true
            Row
            {
                id: pathRow
                Repeater {
                    model: splitPath(foldersTree.selectedFolder)

                    Row{
                        Label {
                            text: modelData == "/" ? "/" : urlConv.getFileName(modelData)
                            padding: 2;

                            MouseArea {
                                hoverEnabled: true
                                anchors.fill: parent
                                cursorShape: Qt.PointingHandCursor
                                onClicked: foldersTree.selectedFolder = modelData
                                onContainsMouseChanged: parent.font.underline = containsMouse
                            }
                        }
                        Label {
                            padding: 2;
                            visible: modelData != foldersTree.selectedFolder
                            text: ">"
                        }
                    }
                    function splitPath(path)
                    {
                        var paths = [];
                        while(path.length > 0)
                        {
                            paths[paths.length] = path
                            path = urlConv.parent(path)
                        }
                        return paths.reverse();
                    }
                }
            }
        }
        FoldersGridView {
            id: grid
            Layout.fillHeight: true
            Layout.fillWidth: true
            textColor: self.textColor
            cellSize: sizeSlider.value
            model: self.model
            onSelected: {
                if(urlConv.isDir(filePath))
                {
                    foldersTree.setSelectedFolder(filePath)
                }
                else
                    model.loadFile(filePath)
            }
            itemContextMenu: folderContextmenu
        }
        RowLayout {
            height: 16
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.preferredWidth: 100
            Layout.rightMargin: 10
            Slider {
                id: sizeSlider
                Layout.preferredWidth: 200
                minimumValue: 64
                value: 200
                maximumValue: 600
            }
        }
    }

    Menu {
        id: folderContextmenu
        property string context: ""
        MenuItem {
            visible: self.isRecentFolders()
            text: qsTr("Forget this folder")
            onTriggered: recentFoldersModel.remove(folderContextmenu.context)
        }
        MenuItem {
            visible: self.isRecentFolders()
            text: qsTr("Forget all")
            onTriggered: recentFoldersModel.clear()
        }
        MenuItem {
            iconSource: F.getIconSource(palette, "../images/star.svg")
            visible: urlConv.isDir(folderContextmenu.context)
            text: qsTr("Add to favorites")
            onTriggered: favoritesModel.add(folderContextmenu.context)
        }
        MenuSeparator { visible: urlConv.isDir(folderContextmenu.context) }
        MenuItem { action: open }
        MenuItem { action: openSeparate }
        MenuSeparator {}
        MenuItem {
            action: extraAction
        }
        MenuSeparator {}
        MenuItem {
            action: quitAction
        }
    }

    function openDir(path)
    {
        var url = urlConv.getUrl(path)
        if(folderModel.folder === url && grid.model === folderModel)
            return
        if(path === favoritesModel.recentFoldersPath)
        {
            folderModel.folder = urlConv.invalidUrl()
            grid.foldersModel = recentFoldersModel
        }
        else
        {
            folderModel.folder = url
        }
    }

    function isRecentFolders()
    {
        return foldersTree.selectedFolder == favoritesModel.recentFoldersPath
    }
}
