import QtQuick 2.5
import Qt.labs.folderlistmodel 2.1
import QtQuick.Controls 1.4
import StereoImage 1.0

Item {
    id: self

    property color textColor
    property Menu contextMenu : null
    property variant nameFilter: []
    property Model model
    property string curFilePath: typeof(modelData) !== 'undefined' ? modelData : filePath

    signal selected(string filePath)

    MouseArea {
        anchors.fill: column
        acceptedButtons: Qt.RightButton
        enabled: contextMenu !== null
        onClicked: {
            contextMenu.context = self.curFilePath
            contextMenu.popup()
        }
    }

    MouseArea {
        anchors.fill: column
        cursorShape: Qt.PointingHandCursor
        acceptedButtons: Qt.LeftButton
        onClicked: self.selected(self.curFilePath)
    }

    Column {
        id: column

        FolderListModel {
            id: folderModel
            nameFilters: self.nameFilter || []
            showDirs: false
            sortField: "Name"
            folder: self.curFilePath ? urlConv.getUrl(self.curFilePath) : urlConv.invalidUrl()
        }

        Item {
            width: self.width - 20
            height: self.height - 20
            anchors.horizontalCenter: parent.horizontalCenter
            Image {
                anchors.centerIn: parent;
                visible: urlConv.isDir(self.curFilePath)
                anchors.fill: parent
                sourceSize.width: width
                sourceSize.height: height
                source: "../images/folder.svg"

                PathView {
                    id: pathView
                    visible: urlConv.isDir(self.curFilePath)
                    pathItemCount: 5
                    anchors.centerIn: parent;
                    path: Path {
                        startX: 0; startY: 0
                        PathAttribute { name: "z"; value: 5.0 }
                        PathLine { x: 1; y: 1 }
                        PathAttribute { name: "z"; value: 0.0 }
                    }
                    model: folderModel
                    delegate: FilePreview {
                        id: imageDelegate
                        filePath: model.filePath
                        isVideo: self.model.isVideo(model.filePath)
                        property double randomAngle: Math.random() * (2 * 8) - 8
                        property double randomX: Math.random()
                        property double randomY: Math.random()
                        x: 0; y: 0; width: self.width - 0.5; height: self.height * 0.4
                        z: PathView.z
                        rotation: imageDelegate.randomAngle
                        imageOffsetX: randomX * self.width / 4 - self.width / 8
                        imageOffsetY: randomY * self.height / 4 - self.height / 8
                    }
                }

            }
            FilePreview {
                anchors.centerIn: parent
                visible: !urlConv.isDir(self.curFilePath)
                filePath: visible ? self.curFilePath : ""
                isVideo: self.model.isVideo(self.curFilePath)
                width: self.width - 20
                height: self.height - 20
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        Text {
            width: self.width
            horizontalAlignment: Text.AlignHCenter
            color: self.textColor
            text: urlConv.getFileName(self.curFilePath)
            elide: Text.ElideRight
        }
    }
}

