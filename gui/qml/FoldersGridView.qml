import QtQuick 2.5
import QtQml.Models 2.1
import QtQuick.Controls 1.4

Rectangle
{
    id: self
    SystemPalette { id: palette; colorGroup: SystemPalette.Active }
    color: palette.base

    property color textColor
    property Model model
    property Menu itemContextMenu : null
    property Menu contextMenu : null
    property alias foldersModel: gridView.model
    signal selected(string filePath)
    property int cellSize: 54;

    ScrollView {
        anchors.fill: parent
        GridView {
            id: gridView
            anchors.margins: 5

            cellWidth: self.cellSize;
            cellHeight: self.cellSize

            delegate: FoldersGridDelegate {
                width: self.cellSize
                height: self.cellSize
                model: self.model
                textColor: palette.text
                onSelected: self.selected(filePath)
                contextMenu: itemContextMenu
                nameFilter: self.model.supportednameFilters
            }
        }
    }
}
