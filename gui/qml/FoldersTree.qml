import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQml.Models 2.2
import StereoImage 1.0
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "functions.js" as F

SplitView {
    id: self

    orientation: Qt.Vertical

    property string selectedFolder

    SystemPalette {
        id: palette;
        colorGroup: SystemPalette.Active;
    }

    Settings {
        category: "Interface"
        property alias favoritesHeight: favoritesColumn.height
    }

    Component {
        id: folderItemDelegate
        Row {
            height: 22
            spacing: 5
            Image {
                anchors.verticalCenter: parent.verticalCenter
                source: F.getIconSource(palette, "../images/document-open.svg")
            }
            Label {
              anchors.verticalCenter: parent.verticalCenter
              color: styleData.textColor
              elide: styleData.elideMode
              text: styleData.value
          }
        }
    }

    ColumnLayout {
        id: favoritesColumn
        Layout.margins: 5
        Layout.fillHeight: false
        Layout.fillWidth: true

        Label {
            padding: 2
            text: qsTr("Favorites")
        }

        TreeView {
            id: favoritesTree
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 50
            alternatingRowColors: false
            headerVisible: false
            model: self.visible ? favoritesModel : null
            property bool skipSetFolder
            selection: ItemSelectionModel {
                model: favoritesTree.model
                onModelChanged: favoritesTree.updateSelection()
                onCurrentChanged:
                {
                    if(!favoritesTree.skipSetFolder)
                        self.setSelectedFolder(favoritesModel.data(current, FavoritesModel.PathRole))
                }
            }
            TableViewColumn {
                title: "Name"
                role: "Name"
            }
            itemDelegate: folderItemDelegate

            function updateSelection()
            {
                var index = favoritesModel.pathIndex(self.selectedFolder)
                skipSetFolder = true
                favoritesTree.selection.setCurrentIndex(index, ItemSelectionModel.ClearAndSelect)
                skipSetFolder = false
                resizeColumnsToContents()
            }

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.RightButton
                onClicked: {
                    var index = parent.indexAt(mouse.x, mouse.y)
                    if (index.valid)
                    {
                        favoritesTree.selection.setCurrentIndex(index, ItemSelectionModel.ClearAndSelect)
                        if(index.row > 0)
                            itemContextMenu.popup()
                    }
                }
            }
            Menu {
                id: itemContextMenu

                MenuItem {
                    text: qsTr("Delete from favorites")
                    iconSource: "../images/entry-delete.svg"
                    onTriggered: favoritesModel.remove(favoritesTree.selection.currentIndex.row)
                }
            }
        }
    }

    ColumnLayout {
        Layout.margins: 5
        Layout.fillWidth: true

        Label {
            padding: 2
            text: qsTr("File system")
        }

        TreeView {
            id: fileSystemTree
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 50
            alternatingRowColors: false
            model: self.visible ? fileSystemModel : null
            rootIndex: fileSystemModel.rootIndex
            headerVisible: false
            property bool skipSetFolder
            selection: ItemSelectionModel {
                model: fileSystemTree.model
                onModelChanged: fileSystemTree.updateSelection()
                onCurrentChanged:
                {
                    if(!fileSystemTree.skipSetFolder)
                    {
                        var p = fileSystemModel.filePath(current)
                        if(p !== "")
                            self.setSelectedFolder(p)
                    }
                }
            }
            TableViewColumn {
                title: "Name"
                role: "fileName"
            }

            itemDelegate: folderItemDelegate

            Timer {
                id: selectTimer
                interval: 100
                repeat: false
                onTriggered: fileSystemTree.selection.setCurrentIndex(fileSystemModel.pathIndex(self.selectedFolder), ItemSelectionModel.ClearAndSelect)
            }

            function updateSelection()
            {
                skipSetFolder = true
                if(self.selectedFolder === favoritesModel.recentFoldersPath)
                    fileSystemTree.selection.clear()
                else
                {
                    var path = self.selectedFolder
                    var paths = [];
                    while(path.length > 0)
                        paths[paths.length] = path = urlConv.parent(path)
                    for(var i = paths.length - 1; i >= 0; i--)
                    {
                        fileSystemTree.expand(fileSystemModel.pathIndex(paths[i]));
                    }
                }
                resizeColumnsToContents()
                skipSetFolder = false
                selectTimer.start()
            }

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.RightButton
                onClicked:
                {
                    var index = parent.indexAt(mouse.x, mouse.y)
                    if (index.valid)
                        fileSystemTree.selection.setCurrentIndex(index, ItemSelectionModel.ClearAndSelect)
                    fsItemContextMenu.popup()
                }
            }
            Menu {
                id: fsItemContextMenu

                property int rowNumber

                MenuItem {
                    iconSource: F.getIconSource(palette, "../images/star.svg")
                    text: qsTr("Add to favorites")
                    onTriggered: {
                        var path = fileSystemModel.filePath(fileSystemTree.selection.currentIndex)
                        favoritesModel.add(path)
                    }
                }
            }
        }
    }

    function setSelectedFolder(path)
    {
        if(!urlConv.equals(path, self.selectedFolder))
        {
            self.selectedFolder = path
        }
    }

    onSelectedFolderChanged:
    {
        favoritesTree.updateSelection()
        fileSystemTree.updateSelection()
    }
}
