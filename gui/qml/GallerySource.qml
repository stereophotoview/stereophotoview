import QtQuick 2.0
import Qt.labs.folderlistmodel 2.2
/**
  Список источников стерео контента в указанных папках
  Нужен для поддержки галереи миниатюр при открытии контента с раздельными источниками
*/

Item {
    id: self
    property var source     //< Пути к открытым файлам (один или два)

    property variant nameFilters
    property alias model: listModel

    // Уведомляет об изменении списка
    signal reset();

    ListModel {
        id: listModel
    }

    FolderListModel {
        id: folderModel1
        nameFilters: self.nameFilters || []
        showDirs: false
        folder: self.source.length > 0 && self.source[0] ? urlConv.getUrl(urlConv.getDirPath(self.source[0])) : urlConv.invalidUrl()
        sortField: FolderListModel.Name
        onRowCountChanged: p.refresh()
        onRowsMoved: p.refresh()
        onDataChanged: p.refresh()
    }

    FolderListModel {
        id: folderModel2
        nameFilters: self.nameFilters || []
        showDirs: false
        folder: self.source.length > 1 && self.source[1] ? urlConv.getUrl(urlConv.getDirPath(self.source[1])) : urlConv.invalidUrl()
        sortField: FolderListModel.Name
        onRowCountChanged: p.refresh()
        onRowsMoved: p.refresh()
        onDataChanged: p.refresh()
    }

    QtObject {
        id: p

        function refresh()
        {
            p.updateList()
            self.reset()
        }

        function updateList()
        {
            listModel.clear()
            var i = 0;
            if(self.source.length > 1 && source[0] && source[1])
            {
                // Если открыт контент из раздельных источников
                // Формируем единый список пар файлов
                var count = folderModel1.count
                var index1 = indexOf(folderModel1, self.source[0])
                var index2 = indexOf(folderModel2, self.source[1])
                if(index1 < 0 || index2 < 0)
                    return;
                var diff = index2 - index1
                var startIndex = 0;
                if(folderModel1.folder == folderModel2.folder)
                {
                    // Открыты файлы из одной папки
                    var fragmentLength = diff + 1;
                    startIndex = index1 % fragmentLength
                    for(i = startIndex; i < count - diff; i += fragmentLength)
                    {
                        var file1 = folderModel1.get(i, "filePath")
                        var file2 = folderModel2.get(i + diff, "filePath")
                        listModel.append({"filePath" : file1 + "\n" + file2 })
                    }
                }
                else
                {
                    // Открыты файлы из разных папок
                    if(folderModel2.count > count)
                        count = folderModel2.count
                    if(diff < startIndex)
                        startIndex = diff
                    for(i = startIndex; i < count; i++)
                    {
                        var data1 = folderModel1.get(i - diff, "filePath")
                        var data2 = folderModel2.get(i, "filePath")
                        if(data1 && data2)
                            listModel.append({"filePath" : data1 + "\n" + data2 })
                    }
                }
            }
            else if (source.length > 0 && source[0])
            {
                // Если открыт контент из одного источника - просто копируем список файлов из первой папки
                for(i = 0; i < folderModel1.count; i++)
                    listModel.append({"filePath" : folderModel1.get(i, "filePath") })
            }
        }

        function indexOf(folderModel, filePath)
        {
            for(var i = 0; i < folderModel.count; i++)
            {
                var itemPath = folderModel.get(i, "filePath")
                if(urlConv.equals(itemPath, filePath))
                    return i;
            }
            return -1;
        }
    }
}
