import QtQuick 2.5
import QtQuick.Controls 1.4
import StereoImage 1.0
import QtQuick.Layouts 1.1
import MyDialogs 1.0
import "functions.js" as F

ColumnLayout {
    id: column1
    spacing: 5

    width: 300

    property alias filePath: filePathEditor.text
    property alias quality: qualitySlider.value
    property variant formats;
    property int selectedFormatIndex;

    SystemPalette {
        id: palette;
        colorGroup: SystemPalette.Active;
    }

    MyFileDialog {
        id: saveFileDialog
        title: qsTr("Save image file", "File dialog title")
        selectExisting: false
        acceptMode: MyFileDialog.AcceptSave
        nameFilters: ""
        defaultSuffix: ""
        onAccepted: filePathEditor.text = selectedFile
    }


    Label { text: qsTr("File path") + ":" }
    RowLayout {
        Layout.fillWidth: true
        spacing: 5
        TextField {
            id: filePathEditor
            Layout.fillWidth: true
            onTextChanged: {
                if(p.textChanging)
                    return;
                var index = options.findFormatByFilePath(text)
                if(index > 0)
                {
                    p.textChanging = true;
                    try{
                        options.currentFormatIndex = index;
                    }catch(e){ }

                    p.textChanging = false;
                }
            }
        }
        Button {
            id: openButton
            iconSource: F.getIconSource(palette, "qrc:/images/document-open.svg")
            Layout.fillWidth: false
            implicitWidth: 26
            implicitHeight: 26
            onClicked: saveFileDialog.open(filePathEditor.text)
        }
    }
    Label { text: qsTr("Format") + ":" }
    ComboBox {
        id: formatSelector
        Layout.fillWidth: true
        model: options.supportedFormats
        textRole: "long_name"
        currentIndex: options.currentFormatIndex

        Binding {
            target: options
            property: "currentFormatIndex"
            value: formatSelector.currentIndex
        }

        onCurrentIndexChanged:
        {
            if(!p.textChanging)
            {
                p.textChanging = true;
                try{
                    filePathEditor.text = options.changeSuffix(filePathEditor.text, model[currentIndex].name)
                }catch(e){ }
                p.textChanging = false;
            }
        }
    }

    Label { text: qsTr("Video bitrate") + ":" }
    BitrateEditor {
        id: videoBitrateEditor
        rawBirate: column1.options.frameSize.width * column1.options.frameSize.height * 3 * fps

        Binding {
            target: options
            property: "videoBitrate"
            value: videoBitrateEditor.value
        }

    }

    Label { text: qsTr("Audio bitrate") + ":" }
    BitrateEditor {
        id: audioBitrateEditor
        rawBirate: channels * sampleRate * bytesPerSample * 8

        Binding {
            target: options
            property: "audioBitrate"
            value: audioBitrateEditor.value
        }

    }

    function open()
    {
        if(options.currentFormatIndex <= 0)
            options.currentFormatIndex = options.findFormatByFilePath(filePath);
    }

    QtObject {
       id: p
       property bool textChanging : false
    }
}
