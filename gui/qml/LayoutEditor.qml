import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import StereoImage 1.0
import QtQuick.Controls.Styles 1.4
import "functions.js" as F

ColumnLayout {
    id: self

    SystemPalette {
        id: palette;
        colorGroup: SystemPalette.Active;
    }

    property StereoImage image : null
    property size imageSize: Qt.size(image.source.width*2, image.source.height*2)

    property VerticalSliderEditor verticalEditor: null
    property RotateEditor rotateEditor: null

    property color textColor
    property double stepXSize: 1
    property double stepYSize: 1
    property double bigStepXSize: 10
    property double bigStepYSize: 10
    property double minimumValue: -0.5
    property double maximumValue: 0.5
    property int buttonSize: 24
    signal canceled
    signal applyed

    Binding {
        target: verticalEditor
        property: "visible"
        value: self.visible
    }
    Binding {
        target: rotateEditor
        property: "visible"
        value: self.visible
    }
    Binding {
        target: verticalEditor
        property: "minimumValue"
        value: self.minimumValue * imageSize.height
    }
    Binding {
        target: verticalEditor
        property: "maximumValue"
        value: self.maximumValue * imageSize.height
    }
    Binding {
        target: verticalEditor
        property: "stepSize"
        value: self.stepYSize
    }
    Binding {
        target: verticalEditor
        property: "bigStepSize"
        value: self.bigStepYSize
    }
    Binding {
        target: verticalEditor
        property: "value"
        value: self.image.vOffset * imageSize.height
    }
    Binding {
        target: self.image
        property: "vOffset"
        value: self.verticalEditor.value / imageSize.height
    }
    Binding {
        target: rotateEditor
        property: "leftAngle"
        value: self.image.leftAngle
    }
    Binding {
        target: rotateEditor
        property: "rightAngle"
        value: self.image.rightAngle
    }
    Binding {
        target: self.image
        property: "leftAngle"
        value: rotateEditor.leftAngle
    }
    Binding {
        target: self.image
        property: "rightAngle"
        value: rotateEditor.rightAngle
    }

    Action {
        id: decreaseAction
        enabled: self.visible
        shortcut: "Left"
        tooltip: qsTr("Decrease [%1]").arg(shortcut)
        iconSource: F.getIconSource(palette, "../images/left.svg")
        onTriggered: slider.value -= self.stepXSize
    }

    Action {
        id: decreaseBigAction
        enabled: self.visible
        shortcut: "Ctrl+Left"
        tooltip: qsTr("Decrease [%1]").arg(shortcut)
        iconSource: F.getIconSource(palette, "../images/left2.svg")
        onTriggered: slider.value -= self.bigStepXSize
    }

    Action {
        id: increaseAction
        enabled: self.visible
        shortcut: "Right"
        tooltip: qsTr("Increase [%1]").arg(shortcut)
        iconSource: F.getIconSource(palette, "../images/right.svg")
        onTriggered: slider.value += self.stepXSize
    }

    Action {
        id: increaseBigAction
        enabled: self.visible
        shortcut: "Ctrl+Right"
        tooltip: qsTr("Increase [%1]").arg(shortcut)
        iconSource: F.getIconSource(palette, "../images/right2.svg")
        onTriggered: slider.value += self.bigStepXSize
    }

    Action {
        id: resetAction
        enabled: self.visible
        tooltip: qsTr("Reset")
        text: "0"
        onTriggered: slider.value = 0
    }

    Action {
        id: applyAction
        text: qsTr("Apply")
        shortcut: "Return"
        tooltip: qsTr("%1 [%2]").arg(text).arg(shortcut)
        enabled: self.visible
        onTriggered: {
            self.apply()
        }
    }

    Action {
        id: cancelAction
        text: qsTr("Cancel")
        shortcut: "Escape"
        enabled: self.visible
        tooltip: qsTr("%1 [%2]").arg(text).arg(shortcut)
        onTriggered: {
            self.cancel()
        }
    }

    RowLayout {

        Layout.fillWidth: true

        Button {
            action: decreaseBigAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }

        Button {
            action: decreaseAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }

        Slider {
            id: slider
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignVCenter
            stepSize: self.stepXSize
            minimumValue: self.minimumValue * imageSize.width
            maximumValue: self.maximumValue * imageSize.width
            Binding {
                target: self.image
                property: "parallax"
                value: slider.value / imageSize.width
            }
            Binding {
                target: slider
                property: "value"
                value: self.image.parallax * imageSize.width
            }
        }

        Button {
            action: increaseAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }

        Button {
            action: increaseBigAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }

        Button {
            action: resetAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }

        Label {
            text : Math.round(slider.value) + ":" + Math.round(self.verticalEditor.value)
            Layout.minimumWidth: 40
            horizontalAlignment: Text.AlignHCenter
        }

        Button {
            id: applyButton
            action: applyAction
            implicitHeight: self.buttonSize
        }

        Button {
            action: cancelAction
            implicitHeight: self.buttonSize
        }
    }

    function apply()
    {
        applyed()
        reset()
    }

    function cancel()
    {
        self.reset()
        self.canceled()
    }

    function reset()
    {
        image.parallax = 0
        image.vOffset = 0
        image.leftAngle = 0
        image.rightAngle = 0
        slider.value = 0
        verticalEditor.value = 0
        rotateEditor.reset()
    }

    onVisibleChanged: if(!visible)
                      {
                          reset()
                          applyButton.focus = true
                      }
}
