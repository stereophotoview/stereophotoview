import QtQuick 2.5
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import StereoImage 1.0

Item {

    id: self

    SystemPalette { id: palette; colorGroup: SystemPalette.Active; }

    property alias currentIndex: list.currentIndex

    signal selected(string filePath)
    property Model mainModel;

    ScrollView {
        id: scroll
        anchors.fill: parent

        ListView {
            id: list
            model: self.mainModel.folderModel
            spacing: 2
            highlightMoveDuration: 2000
            height: 70
            delegate: Item {
                height: 70
                width: 70
                BusyIndicator {
                    anchors.centerIn: parent
                    running: originalImage.status === Image.Loading
                }
                Image {
                    id: originalImage
                    source: "image://preview/" + filePath
                    asynchronous: true
                    anchors.fill: parent
                    anchors.margins: 5
                    fillMode: Image.PreserveAspectCrop
                    Rectangle {
                        visible: self.mainModel.isVideo(filePath)
                        radius: 4
                        color: "#40000000"
                        anchors.centerIn: parent
                        width: 32
                        height: 32
                        Image {
                            anchors.centerIn: parent
                            source: "../images/media-playback-start32-dark.svg"
                        }
                    }
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: self.selected(filePath)
                }
            }

            highlight: Rectangle { color: palette.highlight; radius: 5; width: 70; height: 70 }
            focus: true
            orientation: ListView.Horizontal
        }

    }

}
