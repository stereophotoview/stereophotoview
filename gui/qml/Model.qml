import QtQuick 2.5
import StereoImage 1.0
import "functions.js" as F

Item {
    id: self

    property BaseEditor editor : imageEditor
    property StereoImageSource source;
    property StereoVideoSource videosource;
    property bool modified : editor.modified
    property bool canUndo : editor.canUndo
    property bool canRedo : editor.canRedo

    property variant nameFiltersToOpen : [
        qsTr("All supported files") + " (" + [mpoFormat, jpsFormat, videoFormat].map(F.getNameFilters).join(" ")  + ")",
        qsTr("MPO files") + " (" + F.getNameFilters(mpoFormat) + ")",
        qsTr("Stereo pairs") + " (" + F.getNameFilters(jpsFormat) + ")",
        qsTr("Video files") + " (" + F.getNameFilters(videoFormat) + ")",
        qsTr("All files") + " (*)"]
    property variant nameFiltersToOpenImages : [
        qsTr("All supported files") + " (" + [mpoFormat, jpsFormat].map(F.getNameFilters).join(" ")  + ")",
        qsTr("MPO files") + " (" + F.getNameFilters(mpoFormat) + ")",
        qsTr("Stereo pairs") + " (" + F.getNameFilters(jpsFormat) + ")",
        qsTr("All files") + " (*)"]
    property variant nameFiltersToOpenSeparate : [
        qsTr("All supported files") + " (" + [separateImageFormat, videoFormat].map(F.getNameFilters).join(" ")  + ")",
        qsTr("Images") + " (" + F.getNameFilters(separateImageFormat) + ")",
        qsTr("Video files") + " (" + F.getNameFilters(videoFormat) + ")",
        qsTr("All files") + " (*)"]
    property string nameFilterToSaveMPO : qsTr("MPO files") + " (" + F.getNameFilters(mpoFormat) + ")"
    property string nameFilterToSaveJPS : qsTr("JPS files") + " (" + F.getNameFilters(jpsFormat) + ")"
    property variant nameFiltersToSaveSeparate : [ qsTr("Images") + " (" + F.getNameFilters(separateImageFormat) + ")" ]
    property variant mpoExt : mpoFormat.extensions
    property variant jpsExt : jpsFormat.extensions
    property variant videoExt : videoFormat.extensions
    property variant separateExt : separateImageFormat.extensions
    property variant supportednameFilters: jpsFormat.extensions.map(F.getFilePattern)
        .concat(mpoFormat.extensions.map(F.getFilePattern))
        .concat(videoFormat.extensions.map(F.getFilePattern))
        .concat(jpsFormat.extensions.map(F.getFilePattern).map(p.toupper))
        .concat(mpoFormat.extensions.map(F.getFilePattern).map(p.toupper))
        .concat(videoFormat.extensions.map(F.getFilePattern).map(p.toupper))

    property string filePath
    property variant filePaths: filePath.split("\n")
    readonly property string fileName : self.filePaths.map(urlConv.getFileName).join("; ")
    property ListModel folderModel: gallery.model
    property int currentIndex: -1;
    property QtObject curFormat;

    property ListModel leftMetadata : ListModel { }
    property ListModel rightMetadata : ListModel { }

    property int mode: -1
    property int modeStartScreen: 0
    property int modeView: 1

    property bool isLoaded: mode == modeView && source !== emptySource
    property bool isImageLoaded: isLoaded && !source.isVideo
    property bool isVideoLoaded: isLoaded && source.isVideo
    property bool isMpoLoaded: isLoaded && curFormat === mpoFormat
    property bool isJpsLoaded: isLoaded && curFormat === jpsFormat

    property bool zoomToFit: true
    property double zoom: 1

    property double zoomWidth : zoomToFit ? viewPortWidth : p.max(zoom * source.width, viewPortWidth)
    property double zoomHeight : zoomToFit ? viewPortHeight : p.max(zoom * source.height, viewPortHeight)

    property int viewPortWidth;
    property int viewPortHeight;

    signal loaded(string filePath)
    signal loadError(string filePath)

    MPOFileFormat { id: mpoFormat }
    JpsImageFormat { id: jpsFormat }
    SeparateImageFormat { id: separateImageFormat }
    VideoFileFormat { id: videoFormat }
    StereoImageSource { id: emptySource }

    StereoImageEditor {
        id: imageEditor
        source: emptySource
    }

    StereoVideoEditor {
        id: videoEditor
        source: emptySource
    }

    GallerySource {
        id: gallery
        nameFilters: supportednameFilters
        source: self.filePaths
        onReset:
        {
            p.updateCurrentIndex(true)
        }
    }

    ExternalEditorDialog {
        id: externalEditorDialog
    }

    // private functions
    QtObject {
        id: p

        function getbaseName(filePath)
        {
            if(filePath === null)
                return null
            return filePath.substr(filePath.lastIndexOf("/") + 1)
        }

        function getFolderPath(filePath)
        {
            if(filePath === null)
                return null
            return filePath.substr(0, filePath.lastIndexOf("/"))
        }

        function updateCurrentIndex(l)
        {
            if(self.filePath.length < 1)
                return;
            var currentBaseName = urlConv.getFileName(self.filePath);
            for(var i = 0; i < gallery.model.count; i++){
                var itemFileName = urlConv.getFileName(gallery.model.get(i).filePath);
                if(urlConv.equals(itemFileName, currentBaseName)){
                    currentIndex = -1
                    currentIndex = i;
                    return;
                }
            }

            if(!l || filePaths.length != 1)
                return

            if (currentIndex > -1 && currentIndex >= gallery.model.count)
            {
                currentIndex = currentIndex - 1
            }
            else
            {
                var oldi = currentIndex
                currentIndex = -1
                currentIndex = oldi
            }

            loadByIndex(currentIndex)
        }

        function toupper(str)
        {
            return str.toUpperCase()
        }

        function checkImageSourceAfterLoad(filePath, curFormat, newSource)
        {
            self.mode = self.modeView
            if(newSource !== null)
            {
                setImageSource(newSource)
                self.curFormat = curFormat
                var paths = filePaths;
                fillmetadata(leftMetadata, self.source.leftExifHeaders, paths[0])
                fillmetadata(rightMetadata, self.source.rightExifHeaders, paths.length > 1 ? paths[1] : paths[0])
                zoomToFit = true
                zoom = 1
                loaded(filePath)
                return true
            }
            else
            {
                leftMetadata.clear()
                rightMetadata.clear()
                setImageSource(emptySource)
                loadError(filePath)
                return false
            }
        }

        function fillmetadata(dest, source, filePath)
        {
            dest.clear();
           var mainSection = qsTr("Main");
            var exifSection = qsTr("Exif");
            dest.append({"section": mainSection, "attribute": qsTr("Original source type"), "value": self.source.originalFormat.displayName})
            dest.append({"section": mainSection, "attribute": qsTr("File path"), "value": filePath})
            if(source === undefined || source === null)
                return;
            var attributes = source.attributes
            if(attributes === undefined )
                return;
            for(var i=0; i< attributes.length; i++)
            {
                var av = attributes[i];
                dest.append({"section": exifSection, "attribute": av.attribute, "value": av.value})
            }
        }

        function max(a, b)
        {
            return a > b ? a : b
        }
    }

    function saveMPOAs(filePath, leftFirst, size, jpegQuality)
    {
        mpoFormat.save(self.source, filePath, leftFirst, size, jpegQuality)
        if(isImageLoaded)
            editor.onSave();
    }

    function saveJPSAs(filePath, format, targetSize, jpegQuality)
    {
        jpsFormat.save(self.source, filePath, jpegQuality, format, targetSize)
        if(isImageLoaded)
            editor.onSave();
    }

    function saveVideoAs(filePath, options)
    {
        videoFormat.save(self.source, filePath, options)
        editor.onSave()
    }

    /// Запоминает сохранённое состояние
    function onSaveCurrent(selectedFile)
    {
        filePath = selectedFile
        curFormat = getfileFormatByFileName(selectedFile);
        editor.onSave()
        p.fillmetadata(leftMetadata, self.source.leftExifHeaders, filePaths[0])
        p.fillmetadata(rightMetadata, self.source.rightExifHeaders, filePaths.length > 1 ? filePaths[1] : filePaths[0])
    }

    function isVideo(filePath)
    {
        return getfileFormatByFileName(filePath) === videoFormat;
    }

    function getfileFormatByFileName(filePath)
    {
        if(separateImageFormat.checkFileName(filePath))
            return separateImageFormat;
        if(mpoFormat.checkFileName(filePath))
            return mpoFormat;
        if(jpsFormat.checkFileName(filePath))
            return jpsFormat;
        if(videoFormat.checkFileName(filePath))
            return videoFormat;
        return null;
    }

    function loadFile(filePath)
    {
        var curFormat = getfileFormatByFileName(filePath);
        var newSource = curFormat === null ? null : curFormat.load(filePath, settings.sourceFormat)
        self.filePath = filePath
        if(p.checkImageSourceAfterLoad(filePath, curFormat, newSource))
        {
            var paths = filePath.split("\n");
            recentFoldersModel.add(urlConv.parent(paths[0]))
            if(paths.length > 1)
                recentFoldersModel.add(urlConv.parent(paths[1]))
        }
    }

    function loadSeparate(leftFilePath, rightFilePath)
    {
    }

    function reload()
    {
        loadFile(filePath)
    }

    function close()
    {
        setImageSource(emptySource)
        self.filePath = ""
        self.mode = self.modeStartScreen
    }

    function loadPrevious()
    {
        loadByIndex(currentIndex - 1)
    }

    function loadNext()
    {
        loadByIndex(currentIndex + 1)
    }

    function loadCurrent()
    {
        loadByIndex(currentIndex)
    }

    function loadFirst()
    {
        loadByIndex(0)
    }

    function loadLast()
    {
        loadByIndex(gallery.model.count - 1)
    }

    function loadByIndex(index)
    {
        if(index > -1 && index < gallery.model.count)
            loadFile(gallery.model.get(index).filePath)
    }

    function toggleZoomToFit()
    {
        zoomToFit = !zoomToFit
    }

    function zoomtoActual()
    {
        zoomToFit = false
        zoom = 1
    }

    property double zoomStep: 1.05

    function zoomIn()
    {
        if(zoomToFit)
        {
            zoom = viewPortWidth / source.width
            zoomToFit = false
        }
        var newZoom = zoom * zoomStep
        if(newZoom * source.width  * newZoom * source.height < 30000000)
            zoom = newZoom
    }

    function zoomOut()
    {
        zoom = zoom / zoomStep
        var minZoom = viewPortWidth / source.width;
        if(zoom <= minZoom + 0.00001)
        {
            zoom = minZoom;
            zoomToFit = true;
        }
    }

    function crop(rect, borderWidthPercents)
    {
        editor.crop(rect, borderWidthPercents)
    }

    function applyAlign(parallax, vOffset, leftAngle, rightAngle)
    {
        editor.applyAlign(parallax, vOffset, leftAngle, rightAngle)
    }

    function colorAutoAdjust(toRight, type, checked)
    {
        if(isVideoLoaded)
            editor.colorAutoAdjust(toRight, checked)
        else
            editor.colorAutoAdjust(toRight, type)
    }

    function addTimestamp()
    {
        videoEditor.addTimestamp()
    }

    function removeTimestamp()
    {
        videoEditor.removeTimestamp()
    }

    function externalOpenLeftView(command, name)
    {
        externalEditorDialog.init(editor.createLeftExternalEditorWatcher(command, name), qsTr("Open left image with ") + name)
    }

    function externalOpenRightView(command, name)
    {
        externalEditorDialog.init(editor.createRightExternalEditorWatcher(command, name), qsTr("Open right image with ") + name)
    }

    function undo()
    {
        editor.undo();
    }

    function redo()
    {
        editor.redo();
    }

    function init()
    {
        var filePaths = startupArguments.inputFilePaths
        if(filePaths === null || filePaths.length === 0)
        {
            self.mode = self.modeStartScreen
        }
        else
        {
            if(filePaths.length === 2)
                self.loadFile(filePaths[0] + "\n" + filePaths[1])
            else
                self.loadFile(filePaths[0])
            if(startupArguments.inputLayout >= 0 || startupArguments.inputRightFirst >= 0)
            {
                if(startupArguments.inputLayout >= 0)
                    source.userFormat.layout = startupArguments.inputLayout
                else
                    source.userFormat.layout = source.originalFormat.layout
                if(startupArguments.inputRightFirst >= 0)
                    source.userFormat.leftFirst = !startupArguments.inputRightFirst
                else
                    source.userFormat.leftFirst = source.leftSize
            }

        }
    }

    /// Сигнал об окончательном изменении источника
    signal imageSourceEndChanged()

    function setImageSource(newSource)
    {
        var oldImageource = editor.source;
        editor.clear();
        editor.source = null;

        if(newSource.isVideo) {
            editor = videoEditor;
            videoEditor.source = videosource = source = newSource;
        }
        else {
            editor = imageEditor;
            videosource = null;
            imageEditor.source = source = newSource;
        }
        console.log("debug setImageSource", source, newSource);

        if(oldImageource !== emptySource)
        {
            try { oldImageource.destroy() }
            catch (error) { console.error(error) }
        }
        imageSourceEndChanged();
    }

    function setVolume(volume)
    {
        if(videosource && videosource.player)
            videosource.player.volume = volume
    }

    function setPosition(newPos)
    {
        var player = source.player
        if(player !== undefined)
            player.position = newPos
    }

    function setIsPlaying(isPlaying)
    {
        if(videosource && videosource.player)
            videosource.player.isPlaying = isPlaying
    }

    function stepFrame(step) {
        var player = source.player
        if(player !== undefined)
            player.framePosition = player.framePosition + step;
    }

    function setFramesPosition(numFrames) {
        var player = source.player
        if(player !== undefined)
            player.framePosition = numFrames;
    }

    onFilePathsChanged : {
        p.updateCurrentIndex(false)
    }
}

