/**
Окно выбора имени для сохранения нового пресета
*/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

ApplicationWindow {
    id: self
    modality: Qt.WindowModal
    maximumWidth: minimumWidth
    maximumHeight: minimumHeight
    minimumWidth: 300
    minimumHeight: contentItem.implicitHeight

    visible: false

    flags: Qt.Dialog
    title: qsTr("Save preset as…")

    property alias name: nametextBox.text
    signal accepted(string name)

    ColumnLayout {
        id:column
        spacing: 5
        anchors.fill: parent
        anchors.margins: 10

        Label {
            text: qsTr("Name of the new Preset:")
            Layout.alignment: Qt.AlignBottom
        }
        TextField {
            id: nametextBox
            Layout.fillWidth: true
        }
        RowLayout {
            Layout.topMargin: 5
            spacing: 5
            Layout.alignment: Qt.AlignRight
            Button { isDefault: true; action: saveAction }
            Button { action: cancelAction }
        }
    }

    Action {
        id: saveAction
        text: qsTr("OK")
        shortcut: "Return"
        onTriggered: {
            accepted(nametextBox.text)
            self.close()
        }
    }

    Action {
        id: cancelAction
        text: qsTr("Cancel")
        shortcut: StandardKey.Cancel
        onTriggered: {
            self.close()
        }
    }

    function open(newName)
    {
        nametextBox.text = newName
        nametextBox.selectAll()
        nametextBox.forceActiveFocus()
        show()
    }
}
