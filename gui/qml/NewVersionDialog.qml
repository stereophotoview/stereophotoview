import QtQuick 2.0
import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import StereoImage 1.0
import Qt.labs.settings 1.0

ApplicationWindow {
    id: window
    title: qsTr("Update Available")
    modality: Qt.WindowModal
    flags: Qt.Dialog
    maximumWidth: minimumWidth
    maximumHeight: minimumHeight
    property string skip: ""
    property string newVersion
    property string url : "https://stereophotoview.bitbucket.io" + qsTr("/en/download.html") + "?app"

    Settings {
        category: "CheckVersion"
        property alias skip: window.skip
    }

    Action {
        id: downloadAction
        text: qsTr("Open Download Page")
        onTriggered: Qt.openUrlExternally(window.url)
    }

    Action {
        id: cancelAction
        shortcut: StandardKey.Cancel
        text: qsTr("Close")
        onTriggered: {
            if(skipCheckBox.checked)
                window.skip += (window.skip.length > 0 ? ";"  : "") + window.newVersion
            window.close()
        }
    }

    ColumnLayout {
        id: column
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        Label {
            text: qsTr("A new version of StereoPhotoView is now available for download.")
        }

        Grid {
            columns: 2
            spacing: 2
            Label { text: qsTr("Current version") + ":" }
            Label { text: appVersion }
            Label { text: qsTr("New version") + ":" }
            Label { text: newVersion }
        }

        CheckBox {
            id: skipCheckBox
            text: qsTr("Skip this version")
        }

        Row {
            Layout.alignment: Qt.AlignRight
            spacing: 10
            Button { action: downloadAction; focus: true }
            Button { action: cancelAction }
        }
    }

    OnlineVersionRequest {
        id: request
        onNewVersionChecked: {
            if(isNew && skip.indexOf(version) < 0)
            {
                window.newVersion = version
                visible = true
            }
        }
    }

    function check(){
        request.beginCheckNewVersion()
    }
}
