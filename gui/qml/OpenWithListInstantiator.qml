import QtQuick 2.5
import QtQuick.Controls 1.4

Instantiator {
    property Menu parentMenu : null
    property Model imageModel: null
    property bool leftView : true

    model: jpegMimeType.openWithList
    onObjectAdded: { parentMenu.insertItem(index, object) }
    onObjectRemoved: { parentMenu.removeItem(object) }

    delegate: MenuItem {
        text: modelData.name
        iconName: modelData.iconName
        enabled: imageModel.isLoaded
        onTriggered: {
            if(leftView)
                imageModel.externalOpenLeftView(modelData.command, modelData.name)
            else
                imageModel.externalOpenRightView(modelData.command, modelData.name)
        }
    }
}
