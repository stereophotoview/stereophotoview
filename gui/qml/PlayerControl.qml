import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.2
import StereoImage 1.0
import "functions.js" as F

Rectangle {
    id: self
    height: 40
    color: "#60000000"

    property alias playAction: playAction
    property alias duration: timeSlider.maximumValue
    property alias position: timeSlider.value
    property alias volume: volumeSlider.value
    property alias isPlaying: playAction.checked;
    property double showInterval: 3000
    property color textColor
    property alias markers: chart.markers
    /// Положение текущего видео кадра
    property double videoPts: 0

    signal userPositionChanged(double newPos)

    SystemPalette {
        id: palette;
        colorGroup: SystemPalette.Active;
    }

    Action
    {
        id: playAction
        text: qsTr("Play")
        shortcut: "Space"
        checkable: true
    }

    MouseArea {
        id: mouseArea
        hoverEnabled: true
        anchors.fill: parent
    }

    Item
    {
        id: item1
        anchors.fill: parent
        Button {
            id: playButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 5
            implicitWidth: 32
            implicitHeight: 32
            text: ""
            style: ButtonStyle {
                background: Rectangle {
                    color: control.hovered ? palette.highlight : "transparent"
                    implicitWidth: 32
                    implicitHeight: 32
                    border.width: control.activeFocus ? 1 : 0
                    border.color: "#f2f2f2"
                    radius: 4
                }
            }
            iconSource: checked ? "../images/media-playback-pause32-dark.svg" : "../images/media-playback-start32-dark.svg"
            action: playAction
            tooltip: qsTr("Play") + " [" + playAction.shortcut + "]"
            Layout.leftMargin: 4
        }

        Label {
            id: posLabel
            color: textColor
            text: F.getTime(position)
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: playButton.right
            anchors.leftMargin: 5
        }

        Item {
            id: markersHost
            height: 12
            anchors.left: posLabel.right
            anchors.leftMargin: 5
            anchors.right: durationLabel.left
            anchors.rightMargin: 5
            anchors.bottom: timeSlider.top

            property double maxOx: Math.max.apply(null, self.markers.map(function(e) { return e.x; }))
            property double minOx: Math.min.apply(null, self.markers.map(function(e) { return e.x; }))

            Repeater {
                id: repeater
                model: self.markers.length

                Item {
                    property double pos: self.markers[modelData].pos
                    property bool isSelected: pos === self.videoPts
                    height: markersHost.height
                    anchors.bottom: markersHost.bottom
                    anchors.left: markersHost.left
                    anchors.leftMargin: pos * markersHost.width / self.duration - width / 2
                    width: 11

                    Rectangle {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        border.width: 1
                        border.color: palette.light
                        width: ma.containsMouse || isSelected ? 5 : 2
                        color: isSelected ? palette.highlight : palette.light
                    }

                    MouseArea {
                        id: ma
                        enabled: true
                        acceptedButtons: Qt.LeftButton
                        hoverEnabled: true
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        onClicked: timeSlider.setValue(self.markers[modelData].pos)
                    }
                }
            }
            Chart {
                id: chart
                anchors.fill: parent
                background: "transparent"
                colors: { "offsetX": "blue", "offsetY": "navy", "leftAngle": "green", "rightAngle": "lime" }
                maxPos: self.duration
            }
        }

        PlayerSlider {
            id: timeSlider
            anchors.left: posLabel.right
            anchors.leftMargin: 5
            anchors.right: durationLabel.left
            anchors.rightMargin: 5
            anchors.verticalCenter: parent.verticalCenter
            Layout.fillWidth: true
            updateInterval: 10
            onUserValueChanged: userPositionChanged(newValue)
            maximumValue: 84
        }

        Label {
            id: durationLabel
            color: textColor
            text: F.getTime(duration)
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: volumeButton.left
            anchors.rightMargin: 5
        }

        Image {
            id: volumeButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: volumeSlider.left
            anchors.rightMargin: 5
            source: volumeSlider.value > 0 ? "../images/player-volume.svg" : "../images/player-volume-muted.svg"
        }

        PlayerSlider {
            id: volumeSlider
            maximumValue: 1
            width: 80
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 10
        }
    }

    NumberAnimation {
        id: hideAnimate
        target: self
        properties: "opacity"
        from: 1
        to: 0
        loops: 1
        duration: 1000
    }
    NumberAnimation {
        id: showAnimate
        target: self
        properties: "opacity"
        from: 0
        to: 1
        loops: 1
        duration: 200
    }

    Timer
    {
        id: timer
        interval: self.showInterval
        running: !timeSlider.hovered && !playButton.hovered && !volumeSlider.hovered && !mouseArea.containsMouse
        onTriggered:
        {
            hideAnimate.start()
        }
    }

    function show()
    {
        if(opacity == 0)
            showAnimate.start()
        timer.restart()
    }
    function hide()
    {
        if(opacity == 1)
            hideAnimate.start()
    }
    function clone(src)
    {
        timeSlider.value = src.position
    }
}

