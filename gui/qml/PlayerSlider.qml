import QtQuick 2.0

Rectangle {
    id: self

    property double maximumValue: 0
    property double value: 0
    property alias updateInterval: updateTimer.interval
    signal userValueChanged(double newValue)

    SystemPalette {
        id: palette;
        colorGroup: SystemPalette.Active;
    }

    height: 12
    border.width: 2
    border.color: palette.light
    color: "transparent"

    Rectangle {
        property double w : parent.width - parent.border.width * 2
        color: palette.highlight
        anchors.fill: parent
        anchors.margins: self.border.width
        anchors.rightMargin: parent.width - parent.border.width - self.value * w / self.maximumValue
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton
        onPressed: setValue(getNewvalue(mouse.x))
        onPositionChanged: updateTimer.shedule(getNewvalue(mouse.x))

        function getNewvalue(x)
        {
            return Math.max(0, Math.min((x - self.border.width) * self.maximumValue / (self.width - parent.border.width * 2), self.maximumValue))
        }
    }
    Timer {
        id: updateTimer
        property double newValue;
        interval: 0
        onTriggered: setValue(newValue)

        function shedule(v)
        {
            newValue = v
            if(!running)
                start()
        }
    }

    function setValue(v)
    {
        self.value = v
        userValueChanged(v)
    }
}

