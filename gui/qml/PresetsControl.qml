/**
Элемент выбора пресета для сохранения
*/
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import StereoImage 1.0
import "functions.js" as F

RowLayout {
    id: self
    spacing: 5
    Layout.fillWidth: true
    /// Список пресетов
    property PresetsListModel presetsList
    /// Текущие параметры
    property Preset currentPreset
    /// Номер выбранного пресета
    property alias selectedIndex: presetsListControl.currentIndex
    /// Выбранный в списке пресет
    property Preset selectedPreset : self.selectedIndex >= 0 && self.selectedIndex < presetsList.size ?
                                         presetsList.elementAt(self.selectedIndex) : null

    NewPresetWindow {
        id: savePresetAsWindow
        onAccepted: {
            presetsList.add(name, currentPreset)
            presetsList.save()
            presetsListControl.currentIndex = presetsList.size - 1
        }
    }

    ComboBox {
        id: presetsListControl
        Layout.fillWidth: true
        model: presetsList.data
        textRole: "name"
        editable: false
    }
    Button {
        tooltip: qsTr("Save")
        iconSource: F.getIconSource(palette, "../images/document-save.svg")
        enabled:
            selectedPreset && currentPreset && (
                selectedPreset.layout != currentPreset.layout
                || selectedPreset.leftFirst  != currentPreset.leftFirst
                || selectedPreset.stdSize != currentPreset.stdSize
                || selectedPreset.jpegQuality != currentPreset.jpegQuality
                || (!selectedPreset.stdSize.isAutoSize
                    && (selectedPreset.width != currentPreset.width || selectedPreset.height != currentPreset.height || selectedPreset.sizeLinked != currentPreset.sizeLinked))
                || selectedPreset.videoFormatName != currentPreset.videoFormatName
                || selectedPreset.videoPreset != currentPreset.videoPreset
                || selectedPreset.crf != currentPreset.crf)
        onClicked: saveSelectedPreset()
    }
    Button {
        tooltip: qsTr("Save as…")
        iconSource: F.getIconSource(palette, "../images/document-save-as.svg")
        onClicked:  {
            var name = qsTr("%1 copy").arg(presetsList.elementAt(presetsListControl.currentIndex).name)
            savePresetAsWindow.open(name)
        }
    }
    Button {
        tooltip: qsTr("Delete")
        iconSource: "../images/entry-delete.svg"
        enabled: presetsListControl.currentIndex > 0
        onClicked: deleteSelectedPreset()
    }
    /*
    Column
    {
        Text { text:  selectedPreset.layout + " : " +  currentPreset.layout}
        Text { text:  selectedPreset.leftFirst + " : " +  currentPreset.leftFirst}
        Text { text:  selectedPreset.stdSize.code + " : " +  currentPreset.stdSize.code}
        Text { text:  selectedPreset.jpegQuality + " : " +  currentPreset.jpegQuality}
        Text { text:  selectedPreset.width + " : " +  currentPreset.width}
        Text { text:  selectedPreset.height + " : " +  currentPreset.height}
        Text { text:  selectedPreset.sizeLinked + " : " +  currentPreset.sizeLinked}
        Text { text:  selectedPreset.videoFormatName + " : " +  currentPreset.videoFormatName}
        Text { text:  selectedPreset.videoPreset + " : " +  currentPreset.videoPreset}
        Text { text:  selectedPreset.crf + " : " +  currentPreset.crf}
    }
    */
    onSelectedPresetChanged: {
        if(selectedPreset)
            currentPreset.copyFrom(selectedPreset)
    }

    function saveSelectedPreset()
    {
        selectedPreset.copyFrom(currentPreset)
        presetsList.save()
    }

    function deleteSelectedPreset()
    {
        presetsList.deleteAt(presetsListControl.currentIndex)
    }

    Component.onCompleted: {
        presetsList.load()
        currentPreset.copyFrom(presetsList.elementAt(0))
    }
}
