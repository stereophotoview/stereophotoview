import QtQuick 2.5
import QtQuick.Controls 1.4
import StereoImage 1.0
import "functions.js" as F

Action {
    id: self
    property StereoImage image
    property int targetType
    property SystemPalette palette

    text: stereoFormat.layoutName(targetType)
    iconSource: F.getIconSource(palette, stereoFormat.layoutIconSource(targetType))
    checkable: true
    exclusiveGroup: renderTypeGroup
    checked: image.renderFormat.layout === targetType
    enabled: model.isLoaded
    onTriggered: image.renderFormat.layout = targetType
}
