import QtQuick 2.5
import QtQuick.Layouts 1.1
import StereoImage 1.0

RowLayout {
    id: self
    property double leftAngle: 0
    property double rightAngle: 0

    SystemPalette { id: palette }

    spacing: 10

    RotateEditorOne {
        id: leftAngleEditor
        Layout.topMargin: 3
        Layout.bottomMargin: 0

        Binding {
            target: self
            property: "leftAngle"
            value: leftAngleEditor.angle
        }
        Binding {
            target: leftAngleEditor
            property: "angle"
            value: self.leftAngle
        }
    }

    RotateEditorOne {
        id: rightAngleEditor
        Layout.topMargin: 3
        Layout.bottomMargin: 0
        Binding {
            target: self
            property: "rightAngle"
            value: rightAngleEditor.angle
        }
        Binding {
            target: rightAngleEditor
            property: "angle"
            value: self.rightAngle
        }
    }
    function reset()
    {
        leftAngleEditor.angle = 0
        rightAngleEditor.angle = 0
    }
}
