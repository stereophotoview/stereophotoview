import QtQuick 2.0
import "functions.js" as F
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

RowLayout {
    id: self

    property double angle: 0
    spacing: 0

    SystemPalette {
        id: palette;
        colorGroup: SystemPalette.Active;
    }

    Image {
        source: F.getIconSource(palette, "../images/rotate.svg")
        Layout.leftMargin: 10
        Layout.rightMargin: 10
    }
    SpinBox {
        id: spin
        minimumValue: -180
        maximumValue: 180
        activeFocusOnPress: false
        stepSize: 0.01
        decimals: 2
        enabled: self.visible
        Binding {
            target: self
            property: "angle"
            value: spin.value
        }
        Binding {
            target: spin
            property: "value"
            value: self.angle
        }
    }

    Label {
        text: " °"
    }

    Slider {
        id: slider
        minimumValue: -180
        maximumValue: 180
        stepSize: 0.1
        value: self.angle

        Layout.fillWidth: true
        Layout.leftMargin: 10
        Layout.rightMargin: 10
        Binding {
            target: self
            property: "angle"
            value: slider.value
        }
    }
}
