import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import StereoImage 1.0
import "functions.js" as F

ApplicationWindow {
    id: window
    modality: Qt.ApplicationModal

    maximumWidth: minimumWidth
    maximumHeight: minimumHeight
    flags: Qt.Dialog

    signal accepted;

    property alias currentPreset: currentPreset
    property alias currentFormat: options.currentFormat
    property alias leftSize: currentPreset.leftSourceSize
    property alias rightSize: currentPreset.rightSourceSize
    property alias layoutsModel: options.layoutsModel
    property bool asCopy
    property alias presets: presetsControl.presetsList
    property alias originalFormat: options.originalFormat

    ColumnLayout {
        id: column1
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        Preset {
            id: currentPreset
            stereoFormat: currentFormat
        }

        ColumnLayout
        {
            Layout.fillWidth: true
            Label {
                text: qsTr("Preset") + ":"
            }
            PresetsControl {
                id: presetsControl
                Layout.fillWidth: true
                currentPreset: currentPreset
            }
        }

        StereoOptions {
            id: options
            leftSize: window.leftSize
            rightSize: window.rightSize
            Layout.fillWidth: true
            currentPreset: currentPreset
        }

        GridLayout {
            columns: 2
            anchors.margins: 5
            columnSpacing: 5
            rowSpacing: 5
            Label {
                text: qsTr("JPEG quality:")
            }
            Row {
                id: row
                spacing: 5

                Slider {
                    id: qualitySlider
                    minimumValue: 0
                    maximumValue: 100
                    stepSize: 1
                    value: currentPreset.jpegQuality
                    onValueChanged: currentPreset.jpegQuality = qualitySlider.value
                }

                Label{
                    text : F.strFormat(qualitySlider.value, "000")
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            anchors.margins: 5
            spacing: 5
            Button { isDefault: true; action: saveAction }
            Button { action: cancelAction }
        }
    }


    Action {
        id: saveAction
        text: qsTr("OK")
        shortcut: "Return"
        onTriggered: {
            accepted();
            close()
        }
    }

    Action {
        id: cancelAction
        text: qsTr("Cancel")
        shortcut: StandardKey.Cancel
        onTriggered: {
            window.close()
        }
    }

    // Сбрасывает состояние окна
    function reset()
    {
        // Загружаем настройки из пресета по умолчанию
        presetsControl.selectedIndex = 0
        currentPreset.copyFrom(presetsControl.selectedPreset)
    }

    function open(leftSize, rightSize)
    {
        window.leftSize = leftSize
        window.rightSize = rightSize
        show()
    }

    Component.onCompleted: {
        presets.load()
    }
}
