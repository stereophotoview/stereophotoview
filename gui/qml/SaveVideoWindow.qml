import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import StereoImage 1.0
import MyDialogs 1.0
import QtQuick.Dialogs 1.2

ApplicationWindow {
    id: window
    title: qsTr("Save video as…")
    modality: Qt.WindowModal

    maximumWidth: minimumWidth
    maximumHeight: minimumHeight
    flags: Qt.Dialog

    signal accepted(string path, VideoOptions options);

    property alias layoutsModel: options.layoutsModel
    property alias videoOptions: videooptions.options
    property alias leftSize: options.leftSize;
    property alias rightSize: options.rightSize;
    property alias presets: presetsControl.presetsList
    property alias originalFormat: options.originalFormat

    /// Формат по умолчанию для сохранения файлов
    property StereoFormat defaultFormat: StereoFormat {
        layout: StereoFormat.AnamorphHorizontal
    }

    Preset {
        id: currentPreset
        leftSourceSize: window.leftSize
        rightSourceSize: window.rightSize
        stereoFormat: options.currentFormat
    }

    ColumnLayout {
        id: column1
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        ColumnLayout {
            Label {
                text: qsTr("Presets") + ":"
            }
            Layout.fillWidth: true
            PresetsControl {
                id: presetsControl
                Layout.fillWidth: true
                currentPreset: currentPreset
                presetsList: videoPresetsModel
            }
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: 10

            GroupBox {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                title: qsTr("Stereo Settings")
                Layout.fillHeight: true
                StereoOptions {
                    id: options;
                    currentPreset: currentPreset

                    Binding{
                        target: videoOptions
                        property: "frameSize"
                        value: Qt.size(currentPreset.width, currentPreset.height)
                    }
                }
            }
            GroupBox {
                title: qsTr("Video Coding Settings")
                Layout.alignment: Qt.AlignTop
                Layout.preferredWidth: 320
                VideoSettings {
                    id: videooptions
                    currentPreset: currentPreset
                    Layout.alignment: Qt.AlignTop
                    anchors.fill: parent
                }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            anchors.margins: 5
            spacing: 5
            Button { isDefault: true; action: saveAction }
            Button { action: cancelAction }
        }
    }

    MessageDialog {
        id: sameFileDialog
        title: qsTr("Saving video")
        text: qsTr("Can not save video to source file") + "."
        icon: StandardIcon.Warning
        standardButtons: StandardButton.Ok
        onAccepted: saveFileDialog.open(defaultFilePath)
    }

    MessageDialog {
        id: notSupportedFormatDialog
        title: qsTr("Saving video")
        text: qsTr("The output format \"%1\" is not yet supported.")
            .arg(stereoFormat.layoutName(videoOptions.stereoFormat.layout))
        icon: StandardIcon.Warning
        standardButtons: StandardButton.Ok
    }

    MyFileDialog {
        id: saveFileDialog
        title: qsTr("Save video file", "File dialog title")
        selectExisting: false
        acceptMode: MyFileDialog.AcceptSave

        onAccepted: {
            if(urlConv.equals(selectedFile, defaultFilePath))
            {
                sameFileDialog.open()
            }
            else
            {
                close()
                window.accepted(saveFileDialog.selectedFile, window.videoOptions)
            }
        }
    }

    Action {
        id: saveAction
        text: qsTr("OK")
        shortcut: "Return"
        onTriggered: {
            window.videoOptions.stereoFormat.copyFrom(options.currentFormat)
            if(videoOptions.stereoFormat.layout == StereoFormat.Separate)
            {
                notSupportedFormatDialog.open()
            }
            else
            {
                saveFileDialog.nameFilters = window.videoOptions.nameFilters
                saveFileDialog.open(defaultFilePath)
            }
        }
    }

    Action {
        id: cancelAction
        text: qsTr("Cancel")
        shortcut: StandardKey.Cancel
        onTriggered: {
            window.close()
        }
    }

    property string defaultFilePath

    function open(defaultFilePath, frameSize)
    {
        window.defaultFilePath = defaultFilePath
        window.leftSize = frameSize
        window.rightSize = frameSize
        window.show()
    }
}
