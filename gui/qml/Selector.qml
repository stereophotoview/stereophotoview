import QtQuick 2.5
import StereoImage 1.0

Rectangle {
    id: self

    property StereoImage image: parent
    property rect viewRect: image.viewRect

    property double ratioWidth: 3
    property double ratioHeight: 2
    property int viewX: x - viewRect.x
    property int viewY: y - viewRect.y
    function setViewX(v) { x = viewRect.x + v; }
    function setViewY(v) { y = viewRect.y + v; }

    property bool isRatioActive: ratioHeight != 0 && ratioWidth != 0

    property int minWidth : 20
    property int minHeight : 20

    property alias borderWidthPercents: featheredBorder.borderWidthPercents

    SystemPalette { id: palette; colorGroup: SystemPalette.Active; }
    border.color: palette.light
    border.width: 2
    color: "#00000000"
    width: 100
    height: 100

    FeatheredBorder {
        id: featheredBorder
        anchors.fill: parent
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.SizeAllCursor
        drag {
            target: parent
            minimumX: self.viewRect.x
            minimumY: self.viewRect.y
            maximumX: self.viewRect.width + self.viewRect.x - parent.width
            maximumY: self.viewRect.height + self.viewRect.y - parent.height
            smoothed: true
        }
    }
    SelectorHandle {
        visible: !isRatioActive
        cursorShape: Qt.SizeFDiagCursor
        anchors.left: parent.left
        anchors.top: parent.top
        onMouseXDrag: {
            mouseX = range.left(mouseX)
            self.width = self.width - mouseX
            self.x = self.x + mouseX
        }
        onMouseYDrag: {
            mouseY = range.top(mouseY)
            self.height = self.height - mouseY
            self.y = self.y + mouseY
        }
    }
    SelectorHandle {
        visible: !isRatioActive
        cursorShape: Qt.SizeFDiagCursor
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        onMouseXDrag: self.width = self.width + range.right(mouseX)
        onMouseYDrag: self.height = self.height + range.bottom(mouseY)
    }
    SelectorHandle {
        visible: !isRatioActive
        cursorShape: Qt.SizeBDiagCursor
        anchors.right: parent.right
        anchors.top: parent.top
        onMouseXDrag: self.width = self.width + range.right(mouseX)
        onMouseYDrag: {
            mouseY = range.top(mouseY)
            self.height = self.height - mouseY
            self.y = self.y + mouseY
        }
    }
    SelectorHandle {
        visible: !isRatioActive
        cursorShape: Qt.SizeBDiagCursor
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        onMouseXDrag: {
            mouseX = range.left(mouseX)
            self.width = self.width - mouseX
            self.x = self.x + mouseX
        }
        onMouseYDrag: self.height = self.height + range.bottom(mouseY)
    }
    SelectorHandle {
        cursorShape: Qt.SizeVerCursor
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        onMouseYDrag: {
            mouseY = range.top(mouseY)
            self.y = self.y + mouseY
            setHeight(self.height - mouseY)
        }
    }
    SelectorHandle {
        cursorShape: Qt.SizeVerCursor
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        onMouseYDrag: setHeight(self.height + range.bottom(mouseY))
    }
    SelectorHandle {
        cursorShape: Qt.SizeHorCursor
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        onMouseXDrag: {
            mouseX = range.left(mouseX)
            self.x = self.x + mouseX
            setWidth(self.width - mouseX)
        }
    }
    SelectorHandle {
        cursorShape: Qt.SizeHorCursor
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        onMouseXDrag: setWidth(self.width + range.right(mouseX))
    }

    onWidthChanged: {
        if(width < minWidth)
            width = minWidth
    }
    onHeightChanged: {
        if(height < minHeight)
            height = minHeight
    }

    function setRatio(rwidth, rheight)
    {
        if(x < viewRect.x)
            x = viewRect.x
        if(y < viewRect.y)
            y = viewRect.y
        ratioWidth = rwidth
        ratioHeight = rheight
        if(rwidth === 0 || rheight === 0)
            return
        var maxWidth = viewRect.width - self.x
        var maxHeight = viewRect.height - self.y
        var newWidth = ratioWidth * height / ratioHeight
        if(newWidth > maxWidth)
            newWidth = maxWidth
        var newHeight = ratioHeight * newWidth / ratioWidth
        if(newHeight > maxHeight)
        {
            newHeight = maxHeight
            newWidth = ratioWidth * newHeight / ratioHeight
        }
        width = newWidth
        height = newHeight
    }

    function fill()
    {
        x = viewRect.x
        y = viewRect.y
        width = viewRect.width
        height = viewRect.height
        setRatio(ratioWidth, ratioHeight)
    }

    function setWidth(width)
    {
        self.width = width
        self.height = ratioHeight * width / ratioWidth
    }

    function setHeight(height)
    {
        self.height = height
        self.width = ratioWidth * height / ratioHeight
    }

    function selectorRect()
    {
        var cw = image.source.width / viewRect.width
        var ch = image.source.height / viewRect.height
        return Qt.rect(viewX * cw, viewY * ch, width * cw, height * ch)
    }

    function setSelectorRect(rect) {
        console.log("set", rect)

        var cw = image.source.width / viewRect.width
        var ch = image.source.height / viewRect.height
        setViewX(rect.left / cw);
        setViewY(rect.top / ch)
        width = rect.width / cw;
        height = rect.height / ch;
    }

    QtObject {
        id: range

        function left(mouseX){
            var minXDiff = viewRect.x - self.x
            if(mouseX < minXDiff)
                mouseX = minXDiff
            if(mouseX > width - minWidth)
                mouseX = width - minWidth
            if(isRatioActive)
            {
                var maxHeightDiff = getMaxHeight() - self.height
                var maxWidth = ratioWidth * maxHeightDiff / ratioHeight
                if(mouseX < -maxWidth)
                    mouseX = -maxWidth
            }
            return mouseX
        }

        function right(mouseX)
        {
            var maxWidthDiff = getMaxWidth() - self.width
            if(mouseX > maxWidthDiff)
                mouseX = maxWidthDiff
            if(isRatioActive)
            {
                var maxHeightDiff = getMaxHeight() - self.height
                var maxWidth = ratioWidth * maxHeightDiff / ratioHeight
                if(mouseX > maxWidth)
                    mouseX = maxWidth
            }
            return mouseX
        }

        function top(mouseY)
        {
            var minYDiff = viewRect.y - self.y
            if(mouseY < minYDiff)
                mouseY = minYDiff
            if(mouseY > height - minHeight)
                mouseY = height - minHeight
            if(isRatioActive)
            {
                var maxWidthDiff = getMaxWidth() - self.width
                var maxHeight = ratioHeight * maxWidthDiff / ratioWidth
                if(mouseY < -maxHeight)
                    mouseY = -maxHeight
            }
            return mouseY
        }

        function bottom(mouseY)
        {
            var maxHeightDiff = getMaxHeight() - self.height
            if(mouseY > maxHeightDiff)
                mouseY = maxHeightDiff
            if(isRatioActive)
            {
                var maxWidthDiff = getMaxWidth() - self.width
                var maxHeight = ratioHeight * maxWidthDiff / ratioWidth
                if(mouseY > maxHeight)
                    mouseY = maxHeight
            }
            return mouseY
        }

        function getMaxWidth()
        {
            return viewRect.width - viewX
        }

        function getMaxHeight()
        {
            return viewRect.height - viewY
        }
    }

    property rect oldViewRect

    onViewRectChanged: {
        if(oldViewRect.width !== 0)
        {
            var xCoeff = viewRect.width / oldViewRect.width
            var yCoeff = viewRect.height / oldViewRect.height
            x = (x - oldViewRect.x) * xCoeff + viewRect.x
            y = (y - oldViewRect.y) * yCoeff + viewRect.y
            width = width * xCoeff
            height = height * yCoeff
        }
        else
        {
            x = viewRect.x
            y = viewRect.y
            width = viewRect.width
            height = viewRect.height
        }
        oldViewRect = viewRect
    }
}
