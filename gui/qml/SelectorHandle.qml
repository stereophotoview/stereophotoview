import QtQuick 2.5

Rectangle {
    id: self

    property alias cursorShape: mouseArea.cursorShape

    SystemPalette { id: palette; colorGroup: SystemPalette.Active; }

    width: 15
    height: 15
    border.width: 1
    border.color: palette.light
    color: palette.button
    MouseArea {
        id: mouseArea
        anchors.fill: parent

        drag {
            target: parent
            smoothed: true
            threshold: 0
        }
        onMouseXChanged: if(drag.active) self.mouseXDrag(mouseX)
        onMouseYChanged: if(drag.active) self.mouseYDrag(mouseY)
    }
    signal mouseXDrag(int mouseX)
    signal mouseYDrag(int mouseY)
}
