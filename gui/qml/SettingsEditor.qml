import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import StereoImage 1.0
import Qt.labs.settings 1.0
import "functions.js" as F

ApplicationWindow {
    id: window
    title: qsTr("Settings")
    modality: Qt.WindowModal
    flags: Qt.Dialog
    maximumWidth: minimumWidth
    maximumHeight: minimumHeight

    AutoAlign { id: autoAlign }

    Settings {
        id: interfaceSettings
        category: "Interface"
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        GroupBox {
            Layout.fillWidth: true
            Layout.columnSpan: 2
            title: qsTr("User interface")

            GridLayout {
                id: grid2
                columns: 2
                anchors.margins: 5
                columnSpacing: 5
                rowSpacing: 5

                Label {
                    text: qsTr("Language:")
                }
                ComboBox {
                    id: langCombo
                    width: Layout.fillWidth
                    model: langModel
                    textRole: "name"
                }

                Label {
                    visible: ALLOW_COLOR_THEMES
                    text: qsTr("Color theme:")
                }
                ComboBox {
                    id: themeCombo
                    visible: ALLOW_COLOR_THEMES
                    width: Layout.fillWidth
                    model: themeModel
                    textRole: "name"
                }

                Label {
                    text: qsTr("Number of recent folders:")
                }
                SpinBox {
                    id: recentFolderLengthSpin
                    width: Layout.fillWidth
                }

                CheckBox {
                    id: dontUseNativeDialogCheckbox
                    Layout.columnSpan: 2
                    checked: true
                    text: qsTr("Don't use native dialogs")
                }
            }
        }
        GroupBox {
            Layout.fillWidth: true
            Layout.columnSpan: 2
            title: qsTr("Extended Auto Alignment")
            visible: autoAlign.enabled
            GridLayout {
                columns: 2
                anchors.margins: 5
                columnSpacing: 5
                rowSpacing: 5
                Label {
                    text: qsTr("Accuracy:")
                }
                Row {
                    spacing: 5
                    Slider {
                        id: autoAlignQualityslider
                        minimumValue: 10
                        maximumValue: 100
                        stepSize: 1
                    }

                    Label{
                        text : F.strFormat(autoAlignQualityslider.value, "   ") + " %"
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }

                CheckBox {
                    id: autoAlignParallaxCheckbox
                    Layout.columnSpan: 2
                    text: qsTr("Parallax")
                }
                CheckBox {
                    id: autoAlignVOffsetCheckbox
                    Layout.columnSpan: 2
                    text: qsTr("Vertical offset")
                }
                CheckBox {
                    id: autoAlignRotateCheckbox
                    Layout.columnSpan: 2
                    text: qsTr("Rotate")
                }

            }
        }
        Row {
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            anchors.margins: 5
            spacing: 5
            Button {
                text: qsTr("OK")
                onClicked: saveAndClose()
            }
            Button {
                text: qsTr("Cancel")
                onClicked: window.close()
            }
        }
    }

    MessageDialog {
        id: restartDialog
        icon: StandardIcon.Information
        title: qsTr("Settings")
        text: qsTr("The change will take effect the next time the application is started.")
    }

    ListModel {
        id : langModel
        ListElement { code: ""; name: qsTr("System", "Language") }
        ListElement { code: "en"; name: qsTr("English", "Language") }
        ListElement { code: "ru"; name: qsTr("Русский (Russian)", "Language") }
    }

    ListModel {
        id : themeModel
        ListElement { code: ""; name: qsTr("System", "Color theme") }
        ListElement { code: "breeze-light"; name: qsTr("Light", "Color theme") }
        ListElement { code: "breeze-dark"; name: qsTr("Dark", "Color theme") }
    }

    function open()
    {
        var settingsCode = settings.language
        for(var i = 0; i < langModel.count; i++)
        {
            var curItem = langModel.get(i)
            if(curItem.code === settingsCode)
            {
                langCombo.currentIndex = i;
                break;
            }
        }
        var themeCode = settings.colorTheme
        for(var i = 0; i < themeModel.count; i++)
        {
            var curItem = themeModel.get(i)
            if(curItem.code === themeCode)
            {
                themeCombo.currentIndex = i;
                break;
            }
        }
        recentFolderLengthSpin.value = settings.recentFoldersLength
        autoAlignParallaxCheckbox.checked = settings.autoAlignParallax
        autoAlignVOffsetCheckbox.checked = settings.autoAlignVOffset
        autoAlignRotateCheckbox.checked = settings.autoAlignRotate
        autoAlignQualityslider.value = settings.autoAlignQuality
        dontUseNativeDialogCheckbox.checked = settings.dontUseNativeDialog
        window.show()
    }

    function saveAndClose()
    {
        var oldLang = settings.language
        var oldTheme = settings.colorTheme
        var newLang = langModel.get(langCombo.currentIndex).code
        var newColortheme = themeModel.get(themeCombo.currentIndex).code
        settings.language = newLang
        settings.recentFoldersLength = recentFolderLengthSpin.value
        settings.colorTheme = newColortheme
        settings.autoAlignParallax = autoAlignParallaxCheckbox.checked
        settings.autoAlignVOffset = autoAlignVOffsetCheckbox.checked
        settings.autoAlignRotate = autoAlignRotateCheckbox.checked
        settings.autoAlignQuality = autoAlignQualityslider.value
        settings.dontUseNativeDialog = dontUseNativeDialogCheckbox.checked
        window.close()
        if(newLang !== oldLang || newColortheme !== oldTheme)
            restartDialog.open()
    }

}
