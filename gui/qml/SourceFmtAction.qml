import QtQuick 2.5
import QtQuick.Controls 1.4
import StereoImage 1.0
import "functions.js" as F

Action {
    id: self
    property Model model
    property int targetType
    property SystemPalette palette
    property StereoFormat activeFormat: model.source.activeFormat

    text: stereoFormat.layoutName(targetType)
    checkable: true
    checked: self.activeFormat.layout === targetType
    enabled: model.isLoaded
    onTriggered:
    {
        if(model.source.useOriginalFormat)
            model.source.userFormat.copyFrom(model.source.activeFormat)
        model.source.userFormat.layout = targetType
        settings.sourceFormat = model.source.userFormat
    }
    iconSource: F.getIconSource(palette, stereoFormat.layoutIconSource(targetType))
}

