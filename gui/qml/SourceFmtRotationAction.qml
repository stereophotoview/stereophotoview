import QtQuick 2.0
import QtQuick.Controls 1.4
import StereoImage 1.0
import "functions.js" as F

Action {
    id: self
    property Model model
    property int targetValue;
    property bool isLeft

    property StereoFormat format: self.model.source.userFormat
    property int currentValue: self.isLeft ? self.format.leftRotation : self.format.rightRotation
    function changeValue()
    {
        if(self.isLeft)
            self.format.leftRotation = self.targetValue;
        else
            self.format.rightRotation = self.targetValue;
    }

    enabled: self.model.isLoaded
    text: stereoFormat.rotationName(self.targetValue)
    checkable: true
    checked: self.currentValue == self.targetValue
    onTriggered: {
        if(self.model.source.useOriginalFormat)
            self.model.source.userFormat.copyFrom(self.model.source.activeFormat)
        self.changeValue()
        settings.sourceFormat = model.source.userFormat
    }
    iconSource: F.getIconSource(palette, stereoFormat.rotationIconSource(targetValue))
}
