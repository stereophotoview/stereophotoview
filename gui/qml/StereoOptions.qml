import QtQuick 2.5
import QtQuick.Controls 2.2
import QtQuick.Controls 1.4
import StereoImage 1.0
import QtQuick.Layouts 1.1
import "functions.js" as F

ColumnLayout {
    id: self
    spacing: 5

    SystemPalette { id: palette }

    property Preset currentPreset;
    property StereoFormat originalFormat
    property alias currentFormat: currentFormat
    property size leftSize
    property size rightSize

    property ListModel layoutsModel : ListModel {
        ListElement { targetType: StereoFormat.Default }
    }

    /// Текущий стерео формат (с учётом исходного)
    StereoFormat
    {
        id: currentFormat
    }
    Binding {
        target: currentFormat
        property: "layout"
        value: currentPreset.layout == StereoFormat.Default ? originalFormat.layout : currentPreset.layout
    }
    Binding {
        target: currentFormat
        property: "leftFirst"
        value: currentPreset.leftFirst
    }

    Label {
        text: qsTr("Layout") + ":"
    }
    Rectangle {
        border.color: palette.dark
        color: palette.base
        height: childrenRect.height + 10
        width: childrenRect.width + 10
        ListView {
            id: layoutList
            model: layoutsModel
            highlight: Rectangle { border.color: palette.highlight; border.width: 2; radius: 5; color: palette.base }
            height: childrenRect.height
            width: 320
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: 5

            Binding {
                target: layoutList
                property: "currentIndex"
                value: mainModel.currentLayoutIndex
            }

            delegate: Item {
                height: 18
                width: 320
                id: curItem
                property color textColor: palette.text
                property color baseColor: palette.base
                property int displayType: targetType == StereoFormat.Default ? originalFormat.layout : targetType

                RowLayout {
                    anchors.fill: parent
                    anchors.margins: 1
                    anchors.leftMargin: 5
                    spacing: 5

                    Image {
                        source: F.getIconSourceByColor(curItem.baseColor, stereoFormat.layoutIconSource(curItem.displayType))
                    }
                    Text {
                        property string layoutName: stereoFormat.layoutName(displayType)
                        color: curItem.textColor;
                        text:  targetType == StereoFormat.Default ? qsTr("Original (%1)").arg(layoutName) : layoutName
                        Layout.fillWidth: true
                        clip: true
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton
                    onClicked: {
                        layoutList.currentIndex = index
                        layoutList.focus = true
                    }
                }

            }
            focus: true
            onCurrentIndexChanged: {
                mainModel.setLayoutIndex(currentIndex)
            }
        }
    }

    CheckBox {
        id: reverseCheckBox
        text: qsTr("Reverse")

        Binding {
            target: reverseCheckBox
            property: "checked"
            value: !currentPreset.leftFirst
        }

        Binding {
            target: currentPreset
            property: "leftFirst"
            value: !reverseCheckBox.checked
        }
    }

    Label {
        text: qsTr("Resolution") + ":"
    }

    RowLayout {
        spacing: 5
        ComboBox {
            id: stdSizeCombo
            implicitWidth: 150
            model: standardSizes.data
            textRole: "name"
            currentIndex: standardSizes.indexOf(currentPreset.stdSize)
            onCurrentIndexChanged: currentPreset.stdSize = standardSizes.at(currentIndex)
        }
        SpinBox {
            id: widthEditor
            enabled: currentPreset.stdSize.isManual
            implicitWidth: 70
            maximumValue: 10000
            minimumValue: 0
            value: currentPreset.width
            onValueChanged: currentPreset.width = value
            ToolTip.text: qsTr("Width")
            ToolTip.visible: hovered
            ToolTip.delay: 500
        }
        Button {
            id: linkButton
            enabled: currentPreset.stdSize.isManual
            iconSource: F.getIconSource(palette, "../images/link.svg")
            implicitWidth: implicitHeight
            anchors.verticalCenter: parent.verticalCenter
            checkable: true
            checked: currentPreset.sizeLinked
            onCheckedChanged: currentPreset.sizeLinked = checked
            ToolTip.text: qsTr("Keep aspect ratio") + (checked ? " (" + qsTr("enabled") + ")" : "")
            ToolTip.visible: hovered
            ToolTip.delay: 500
        }
        SpinBox {
            id: heightEditor
            enabled: currentPreset.stdSize.isManual
            implicitWidth: 70
            maximumValue: 10000
            minimumValue: 0
            value: currentPreset.height
            onValueChanged: currentPreset.height = value
            ToolTip.text: qsTr("Height")
            ToolTip.visible: hovered
            ToolTip.delay: 500
        }
    }

    QtObject {
        id: mainModel

        /// Номер текущего варианта расположения ракурсов
        property int currentLayoutIndex: getLayoutIndex(currentPreset.layout)
        // Номер выбранного стандартного размера
        property StandardSizeItem stdSize: currentPreset.stdSize
        /// Ширина
        /// Вариант расопложения ракурса текущего формата (с учётом исходного)
        property int currentFormatLayout: currentFormat.layout

        property size leftSize: self.leftSize
        property size rightSize: self.rightSize

        /// Возвращает номер варианта расположения ракурсов
        function getLayoutIndex(layout)
        {
            for(var i = 0; i < layoutsModel.count; i++)
            {
               if(layoutsModel.get(i).targetType === layout)
                   return i
            }
            return -1;
        }

        /// Устанавливает вариант расположения ракурсов
        function setLayoutIndex(index)
        {
            var item = layoutsModel.get(index)
            if(item)
                currentPreset.layout = item.targetType
        }
    }
}
