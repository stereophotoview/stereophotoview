import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import StereoImage 1.0

import "functions.js" as F

ApplicationWindow {
    id: window
    title: qsTr("Active tasks: %1").arg(taskQueue.activeCount)

    SystemPalette { id: palette }

    width: 300
    height: 200

    minimumWidth: 200
    minimumHeight: 60

    ColumnLayout{
        anchors.fill: parent
        ScrollView {
            id: scrollbar
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.topMargin: 10
            Layout.bottomMargin: 10

            ListView {
                id: list
                width: scrollbar.viewport.width
                highlight: Rectangle {
                    color: palette.base
                    border.color: palette.highlight
                    border.width: 2
                    radius: 5
                }
                model: taskQueue.allTasks
                currentIndex: taskQueue.currentIndex
                delegate: Item {
                    width: parent.width
                    height: 40
                    id: item
                    property color textColor: palette.text

                    Image {
                        id: img
                        anchors.margins: 5
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        width: 16
                        height: 16
                        source: F.getIconSource(palette, getIconSource(modelData.isActive, modelData.result, modelData.isFinished))
                    }

                    Column {
                        anchors.rightMargin: 5
                        anchors.leftMargin: 5
                        anchors.topMargin: 2
                        anchors.right: buttons.left
                        anchors.left: img.right
                        anchors.top: parent.top
                        spacing: 0
                        Text {
                            color: item.textColor
                            text: urlConv.getFileName(modelData.dstFilePath)
                        }
                        ProgressBar {
                            implicitHeight: 16
                            implicitWidth: parent.width
                            visible: modelData.isActive
                            minimumValue: 0
                            maximumValue: 100
                            value: modelData.progress
                        }
                        TextEdit {
                            color: item.textColor
                            visible: !modelData.isActive
                            readOnly: true
                            width: parent.width
                            selectByMouse: true
                            wrapMode: Text.NoWrap
                            text: modelData.resultStr
                        }
                    }

                    Row {
                        id: buttons
                        spacing: 5
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.margins: 5
                        Button {
                            visible: modelData.isActive
                            implicitHeight: 22
                            implicitWidth: 22
                            tooltip: qsTr("Stop")
                            iconSource: F.getIconSource(palette, "qrc:/images/media-playback-stop.svg")
                            onClicked: modelData.stop()
                        }
                        Button {
                            visible: !modelData.isActive
                            implicitHeight: 22
                            implicitWidth: 22
                            tooltip: qsTr("Delete")
                            iconSource: "qrc:/images/entry-delete.svg"
                            onClicked: taskQueue.removeAt(index)
                        }
                    }
                }
            }
        }
    }
    function getIconSource(isActive, result, isFinished)
    {
        if(isFinished && result)
            return "qrc:/images/mark-checked.svg"
        if(isFinished && !result)
            return "qrc:/images/error.svg"
        if(isActive)
            return "qrc:/images/media-playback-start.svg"
        return "";
    }
}
