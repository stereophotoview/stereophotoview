import QtQuick 2.0
import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    id: window
    title: qsTr("Time Shift")
    modality: Qt.WindowModal
    flags: Qt.Dialog
    SystemPalette { id: palette }

    signal apply(real seconds)

    Column {
        spacing: 10
        anchors.fill: parent
        anchors.margins: 10
        Row {
            spacing: 5
            Text {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Time shift of the second stream:")
            }
            SpinBox {
                id: timeField
                stepSize: 0.01
                decimals: 2
                minimumValue: -60
                maximumValue: 60
            }
            Text {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("seconds")
            }
        }
        Row {
            spacing: 10
            anchors.right: parent.right
            Button {
                isDefault: true
                action: applyAction
            }
            Button {
                action: cancelAction
            }
        }
    }

    Action {
        id: cancelAction
        text: qsTr("Close")
        shortcut: StandardKey.Cancel
        onTriggered: {
            window.close()
        }
    }

    Action {
        id: applyAction
        text: qsTr("Apply")
        shortcut: "Return"
        onTriggered: {
            window.apply(timeField.value)
        }
    }

    function open(timeValue) {
        timeField.value = timeValue
        show()
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
