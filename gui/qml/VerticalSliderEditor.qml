import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Controls.Styles 1.4
import "functions.js" as F

Item {
    id: self

    SystemPalette {
        id: palette;
        colorGroup: SystemPalette.Active;
    }

    property color textColor
    property alias minimumValue: slider.minimumValue
    property alias maximumValue: slider.maximumValue
    property alias value: slider.value
    property alias stepSize: slider.stepSize
    property double bigStepSize: 0
    property int buttonSize: 24

    width: buttonSize

    ColumnLayout {
        anchors.fill: parent
        Action {
            id: increaseAction
            enabled: self.visible
            shortcut: "Up"
            tooltip: qsTr("Increase [%1]").arg(shortcut)
            iconSource: F.getIconSource(palette, "../images/up.svg")
            onTriggered: self.value += self.stepSize
        }

        Action {
            id: increaseBigAction
            enabled: self.visible
            shortcut: "Ctrl+Up"
            tooltip: qsTr("Increase [%1]").arg(shortcut)
            iconSource: F.getIconSource(palette, "../images/up2.svg")
            onTriggered: self.value += self.bigStepSize
        }

        Action {
            id: decreaseAction
            enabled: self.visible
            shortcut: "Down"
            tooltip: qsTr("Decrease [%1]").arg(shortcut)
            iconSource: F.getIconSource(palette, "../images/down.svg")
            onTriggered: self.value -= self.stepSize
        }

        Action {
            id: decreaseBigAction
            enabled: self.visible
            shortcut: "Ctrl+Down"
            tooltip: qsTr("Decrease [%1]").arg(shortcut)
            iconSource: F.getIconSource(palette, "../images/down2.svg")
            onTriggered: self.value -= self.bigStepSize
        }

        Action {
            id: resetAction
            enabled: self.visible
            tooltip: qsTr("Reset")
            text: "0"
            onTriggered: self.value = 0
        }

        Button {
            action: increaseBigAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }

        Button {
            action: increaseAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }

        Slider {
            id: slider
            orientation: Qt.Vertical
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.alignment: Qt.AlignCenter
        }

        Button {
            action: decreaseAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }

        Button {
            action: decreaseBigAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }

        Button {
            action: resetAction
            implicitWidth: self.buttonSize
            implicitHeight: self.buttonSize
        }
    }
}
