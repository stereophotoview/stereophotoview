import QtQuick 2.5
import QtQuick.Controls 1.4
import StereoImage 1.0
import QtQuick.Layouts 1.1
import MyDialogs 1.0
import "functions.js" as F

ColumnLayout {
    id: column1
    spacing: 5

    property VideoOptions options : options
    property Preset currentPreset

    VideoOptions {
        id: options
    }

    Binding{
        target: currentPreset
        property: "videoFormatName"
        value: options.currentFormatName
    }
    Binding{
        target: options
        property: "currentFormatName"
        value: currentPreset.videoFormatName
    }
    Binding{
        target: currentPreset
        property: "crf"
        value: options.crf
    }
    Binding{
        target: currentPreset
        property: "videoPreset"
        value: options.preset
    }
    Binding {
        target: options
        property: "preset"
        value: currentPreset.videoPreset
    }
    Binding {
        target: options
        property: "crf"
        value: currentPreset.crf
    }

    property bool isH264Codec: options.videoCodecID == VideoOptions.CodecH264

    ListModel {
        id: h264presets
        ListElement { name: "ultrafast" }
        ListElement { name: "superfast" }
        ListElement { name: "veryfast" }
        ListElement { name: "faster" }
        ListElement { name: "fast" }
        ListElement { name: "medium" }
        ListElement { name: "slow" }
        ListElement { name: "slower" }
        ListElement { name: "veryslow" }
    }

    SystemPalette {
        id: palette;
        colorGroup: SystemPalette.Active;
    }

    GroupBox {
        title: qsTr("File format")
        Layout.fillWidth: true
        ColumnLayout
        {
            anchors.fill: parent

            ComboBox {
                id: formatCombo
                Layout.fillWidth: true
                model: options.supportedFormats
                textRole: "long_name"
                Binding {
                    target: formatCombo
                    property: "currentIndex"
                    value: options.currentFormatIndex
                }
                Binding{
                    target: options
                    property: "currentFormatIndex"
                    value: formatCombo.currentIndex
                }
            }
            GridLayout {
                columns: 2
                anchors.left: parent.left
                anchors.right: parent.right
                Label {
                    text: qsTr("Video codec") + ":"
                }
                Label {
                    text: options.videoCodecName
                }
                Label {
                    text: qsTr("Audio codec") + ":"
                }
                Label {
                    text: options.audioCodecName
                }
                Label {
                    text: qsTr("Stereo descriptor") + ":"
                }
                Label {
                    text: isH264Codec ? qsTr("Yes") : qsTr("No");
                }
            }
        }
    }

    Column {
        visible: isH264Codec
        Label { text: qsTr("H264 Preset") + ":" }
        ComboBox
        {
            id: presetCombo
            Layout.fillWidth: true
            model: h264presets
            textRole: "name"

            Binding {
                target: presetCombo
                property: "currentIndex"
                value: p.getPresetIndex(options.preset)
            }
            onCurrentIndexChanged: options.preset = h264presets.get(currentIndex).name
        }
    }

    ColumnLayout {
        Layout.fillWidth: true
        spacing: 5
        Label {
            text: (isH264Codec ? qsTr("Constant Rate Factor") : qsTr("Compression factor")) + ":"
        }
       RowLayout {
            spacing: 5
            Layout.fillWidth: true
            Slider {
                id: crfSlider
                minimumValue: 0
                maximumValue: isH264Codec ? 61 : 99
                stepSize: 1
                Layout.fillWidth: true

                Binding {
                    target: crfSlider
                    property: "value"
                    value: options.crf
                }
                Binding{
                    target: options
                    property: "crf"
                    value: crfSlider.value
                }
            }
            Label {
                text: "(" + F.strFormat(crfSlider.value, "00") + ")"
            }
        }
        RowLayout {
            Layout.fillWidth: true
            Label {
                Layout.fillWidth: true
                text: qsTr("High quality")
            }
            Label {
                Layout.alignment: Qt.AlignRight
                text: qsTr("Small size")
            }
        }
    }

    QtObject {
       id: p
       property bool textChanging : false

       function getPresetIndex(name)
       {
            for(var i = 0; i < h264presets.count; i++)
            {
                if(h264presets.get(i).name === name)
                    return i;
            }
            return -1;
       }
    }
}
