import QtQuick 2.5
import StereoImage 1.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import MyDialogs 1.0
import Qt.labs.settings 1.0
import "functions.js" as F

Item {
    id: self
    focus: false


    SystemPalette {
        id: palette;
        colorGroup: SystemPalette.Active;
    }

    property bool miniaturesVisible
    property bool fullscreen

    property Model model
    property bool folderViewerVisible: model.mode == model.modeStartScreen
    property color textColor : palette.text
    property color color: palette.dark
    property Action extraAction
    property alias image: image
    property Action quitAction

    property ExclusiveGroup renderTypeGroup : ExclusiveGroup { id: renderTypeGroup }
    property ExclusiveGroup sourceFmtGroup : ExclusiveGroup { id: sourceFmtGroup }
    property VerticalSliderEditor vslider : vslider
    property ScrollView scroll: scroll
    property PlayerControl playerConrol: playerConrol

    property int currentTool: 0
    property int toolLayout: 1
    property int toolCrop: 2
    property int toolAutoAreaAlign: 3

    property alias renderFormat : image.renderFormat

    property Action open: Action{
        text: qsTr("Open…")
        // iconName: "document-open"
        iconSource: F.getIconSource(palette, "../images/document-open.svg")
        shortcut: StandardKey.Open
        onTriggered: checkModifiedAndExecute(openFileDialog.open)
    }

    property Action openSeparate: Action{
        text: qsTr("Open separate files…")
        iconSource: F.getIconSource(palette, "../images/document-open-separate.svg")
        shortcut: "Ctrl+Shift+O"
        onTriggered: checkModifiedAndExecute(openSeparateFileDialog.init)
    }

    /// Сохраняет изображение в тот же файл
    property Action saveImage: Action{
        text: qsTr("Save")
        iconSource: F.getIconSource(palette, "../images/document-save.svg")
        shortcut: StandardKey.Save
        enabled: model.isImageLoaded && (model.isMpoLoaded || model.isJpsLoaded) && model.modified
        onTriggered: imageOptionsDialog.open(model.filePaths[0], model.isJpsLoaded ? "jps" : "mpo", true)
    }

    /// Сохраняет изображение в другой файл и открывает его
    property Action saveImageAs: Action{
        text: qsTr("Save as...")
        iconSource: model.isVideoLoaded ? F.getIconSource(palette, "../images/camera-photo.svg") : F.getIconSource(palette, "../images/document-save-as.svg")
        shortcut: StandardKey.SaveAs
        enabled: model.isImageLoaded
        onTriggered: saveImageFileDialog.show(model.filePaths[0])
    }

    /// Сохраняет изображение в другой файл и Не открывает его
    /// если открыто видео, сохраняет снимок кадра
    property Action saveImageAsCopy: Action{
        text: model.isVideoLoaded ? qsTr("Save freeze frame as...") : qsTr("Save as copy...")
        iconSource: model.isVideoLoaded ? F.getIconSource(palette, "../images/camera-photo.svg") : F.getIconSource(palette, "../images/document-save-as-copy.svg")
        enabled: model.isLoaded
        shortcut: "Ctrl+E,S"
        onTriggered:  {
            var path = urlConv.getPathWithoutSuffix(model.filePaths[0])
            if(model.isVideoLoaded)
                path = path + " " + F.getTime(self.playerConrol.position).replace(/\:/g, "-")

            saveImageFileDialog.showAsCopy(path)
        }
    }

    property Action saveVideoAs: Action{
        text: qsTr("Save as copy…", "Save as Video file action")
        iconSource: F.getIconSource(palette, "../images/document-save-as.svg")
        enabled: model.isVideoLoaded
        shortcut: StandardKey.Save
        onTriggered: {
            showSaveVideoWindow()
        }
    }

    property Action fileCopyTo: Action{
        text: qsTr("Copy to…")
        // iconName: "edit-copy"
        iconSource: F.getIconSource(palette, "../images/edit-copy.svg")
        shortcut: "F5"
        enabled: model.mode == model.modeView && model.filePaths.length == 1
        onTriggered: {
            fileCopyToDialog.open(model.filePath)
        }
    }

    property Action fileMoveTo: Action{
        text: qsTr("Move to…")
        // iconName: "go-jump"
        iconSource: F.getIconSource(palette, "../images/go-jump.svg")
        shortcut: "F6"
        enabled: model.mode == model.modeView && model.filePaths.length == 1
        onTriggered: fileMoveToDialog.open(model.filePath)
    }

    property Action fileMoveToTrash: Action{
        text: qsTr("Move to Trash")
        // iconName: "entry-delete"
        iconSource: "../images/entry-delete.svg"
        shortcut: StandardKey.Delete
        enabled: model.mode == model.modeView
        onTriggered: moveToTrashDialog.open()
    }

    property Action fileInfo: Action{
        text: qsTr("Information…")
        // iconName: "exifinfo"
        iconSource: F.getIconSource(palette, "../images/exifinfo.svg")
        shortcut: "Ctrl+I"
        enabled: model.isLoaded
        onTriggered: showFileInfoDialog()
    }

    property Action close: Action{
        text: qsTr("Close")
        // iconName: "document-close"
        iconSource: "../images/document-close.svg"
        shortcut: StandardKey.Close
        enabled: model.mode == model.modeView
        onTriggered: checkModifiedAndExecute(self.model.close)
    }

    property Action renderRowInterlaced: RenderTypeAction {
        targetType: StereoFormat.RowInterlaced
        shortcut: "Ctrl+M,1"
        palette: palette
        image: image
        exclusiveGroup: renderTypeGroup
    }

    property Action renderColumnInterlaced: RenderTypeAction {
        targetType: StereoFormat.ColumnInterlaced
        shortcut: "Ctrl+M,2"
        palette: palette
        image: image
        exclusiveGroup: renderTypeGroup
    }

    property Action renderAnaglyphRC: RenderTypeAction {
        targetType: StereoFormat.AnaglyphRC
        shortcut: "Ctrl+M,3"
        palette: palette
        image: image
        exclusiveGroup: renderTypeGroup
    }

    property Action renderAnaglyphYB: RenderTypeAction {
        targetType: StereoFormat.AnaglyphYB
        shortcut: "Ctrl+M,4"
        palette: palette
        image: image
        exclusiveGroup: renderTypeGroup
    }

    property Action renderHorizontal: RenderTypeAction {
        targetType: StereoFormat.Horizontal
        shortcut: "Ctrl+M,5"
        palette: palette
        image: image
        exclusiveGroup: renderTypeGroup
    }

    property Action renderAnamorphHorizontal: RenderTypeAction {
        targetType: StereoFormat.AnamorphHorizontal
        shortcut: "Ctrl+M,6"
        palette: palette
        image: image
        exclusiveGroup: renderTypeGroup
    }

    property Action renderVertical: RenderTypeAction {
        targetType: StereoFormat.Vertical
        shortcut: "Ctrl+M,7"
        palette: palette
        image: image
        exclusiveGroup: renderTypeGroup
    }

    property Action renderAnamorphVertical: RenderTypeAction {
        targetType: StereoFormat.AnamorphVertical
        shortcut: "Ctrl+M,8"
        palette: palette
        image: image
        exclusiveGroup: renderTypeGroup
    }

    property Action renderMono: RenderTypeAction {
        targetType: StereoFormat.Monoscopic
        shortcut: "Ctrl+M,9"
        palette: palette
        image: image
        exclusiveGroup: renderTypeGroup
    }

    property Action renderReverse : Action {
        text: qsTr("Reverse")
        shortcut: "Ctrl+M,R"
        checkable: true
        enabled: self.model.isLoaded
        iconSource: F.getIconSource(palette, "qrc:/images/layout-reverse.svg")
        checked: !image.renderFormat.leftFirst
        onTriggered: image.renderFormat.leftFirst = !image.renderFormat.leftFirst
    }

    property alias playAction: playerConrol.playAction

    property Action gotoNextFrame: Action{
        text: qsTr("Go to next frame")
        shortcut: "Ctrl+Right"
        enabled: model.isVideoLoaded && !model.videosource.player.isPlaying && self.currentTool == 0
        onTriggered: model.stepFrame(+1)
    }

    property Action gotoPreviousframe: Action{
        text: qsTr("Go to previous frame")
        shortcut: "Ctrl+Left"
        enabled: model.isVideoLoaded && !model.videosource.player.isPlaying && self.currentTool == 0
        onTriggered: model.stepFrame(-1)
    }

    property Action gotoFirstImage: Action{
        text: qsTr("First")
        // iconName: "go-first"
        iconSource: F.getIconSource(palette, "../images/go-first.svg")
        shortcut: StandardKey.MoveToStartOfLine
        enabled: model.mode == model.modeView && self.currentTool == 0
        onTriggered: checkModifiedAndExecute(self.model.loadFirst)
    }

    property Action goToLeftImage: Action{
        text: qsTr("Previous")
        // iconName: "go-previous"
        iconSource: F.getIconSource(palette, "../images/go-previous.svg")
        shortcut: StandardKey.MoveToPreviousChar
        enabled: model.mode == model.modeView && self.currentTool == 0
        onTriggered: checkModifiedAndExecute(self.model.loadPrevious)
    }

    property Action gotoRightImage: Action{
        text: qsTr("Next")
        // iconName: "go-next"
        iconSource: F.getIconSource(palette, "../images/go-next.svg")
        shortcut: StandardKey.MoveToNextChar
        enabled: model.mode == model.modeView && self.currentTool == 0
        onTriggered: checkModifiedAndExecute(self.model.loadNext)
    }

    property Action gotoLastImage: Action{
        text: qsTr("Last")
        // iconName: "go-last"
        iconSource: F.getIconSource(palette, "../images/go-last.svg")
        shortcut: StandardKey.MoveToEndOfLine
        enabled: model.mode == model.modeView && self.currentTool == 0
        onTriggered: checkModifiedAndExecute(self.model.loadLast)
    }

    property Action miniaturesToggle: Action{
        text: qsTr("Thumbnail bar")
        // iconName: "view-preview"
        iconSource: F.getIconSource(palette, "../images/view-preview.svg")
        shortcut: "T"
        checked: self.miniaturesVisible
        checkable: true
        enabled: model.mode == model.modeView
        onTriggered: {
            if(self.fullscreen)
                miniaturesRect.show()
            else
                self.miniaturesVisible = !self.miniaturesVisible
        }
    }

    property Action sourceFmtUseOriginal : Action {
        text: qsTr("Use original") + " (" + self.model.source.originalFormat.displayName + ")"
        iconSource: F.getIconSource(palette, stereoFormat.layoutIconSource(self.model.source.originalFormat.layout))
        shortcut: "Ctrl+F,0"
        enabled: self.model.source.originalFormat.isLoaded
        checkable: true
        checked: self.model.source.useOriginalFormat
        onTriggered: self.model.source.useOriginalFormat = !self.model.source.useOriginalFormat
    }

    property Action sourceFmtRowInterlaced: SourceFmtAction {
        targetType: StereoFormat.RowInterlaced
        shortcut: "Ctrl+F,1"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtColumnInterlaced: SourceFmtAction {
        targetType: StereoFormat.ColumnInterlaced
        shortcut: "Ctrl+F,2"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtAnaglyphRC: SourceFmtAction {
        targetType: StereoFormat.AnaglyphRC
        shortcut: "Ctrl+F,3"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtAnaglyphYB: SourceFmtAction {
        targetType: StereoFormat.AnaglyphYB
        shortcut: "Ctrl+F,4"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtHorizontal: SourceFmtAction {
        targetType: StereoFormat.Horizontal
        shortcut: "Ctrl+F,5"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtAnamorphHorizontal: SourceFmtAction {
        targetType: StereoFormat.AnamorphHorizontal
        shortcut: "Ctrl+F,6"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtVertical: SourceFmtAction {
        targetType: StereoFormat.Vertical
        shortcut: "Ctrl+F,7"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtAnamorphVertical: SourceFmtAction {
        targetType: StereoFormat.AnamorphVertical
        shortcut: "Ctrl+F,8"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtMono: SourceFmtAction {
        targetType: StereoFormat.Monoscopic
        shortcut: "Ctrl+F,9"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtSeparate: SourceFmtAction {
        targetType: StereoFormat.Separate
        shortcut: "Ctrl+F,S"
        palette: palette
        model: self.model
        exclusiveGroup: sourceFmtGroup
    }

    property Action sourceFmtReverse : Action {
        text: qsTr("Reverse")
        shortcut: "Ctrl+F,R"
        checkable: true
        enabled: self.model.isLoaded
        iconSource: F.getIconSource(palette, "qrc:/images/layout-reverse.svg")
        checked: !self.model.source.activeFormat.leftFirst
        onTriggered: {
            if(self.model.source.useOriginalFormat)
                self.model.source.userFormat.copyFrom(self.model.source.activeFormat)
            self.model.source.userFormat.leftFirst = !self.model.source.userFormat.leftFirst
            settings.sourceFormat = model.source.userFormat
        }
    }
    property Action sourceFmtFirstRotationNone : SourceFmtRotationAction {
        model: self.model
        targetValue: StereoFormat.None
        isLeft: true
    }
    property Action sourceFmtFirstRotationLeft : SourceFmtRotationAction {
        model: self.model
        targetValue: StereoFormat.Left
        isLeft: true
    }
    property Action sourceFmtFirstRotationRight : SourceFmtRotationAction {
        model: self.model
        targetValue: StereoFormat.Right
        isLeft: true
    }
    property Action sourceFmtFirstRotationUpsideDown : SourceFmtRotationAction {
        model: self.model
        targetValue: StereoFormat.UpsideDown
        isLeft: true
    }

    property Action sourceFmtSecondRotationNone : SourceFmtRotationAction {
        model: self.model
        targetValue: StereoFormat.None
        isLeft: false
    }
    property Action sourceFmtSecondRotationLeft : SourceFmtRotationAction {
        model: self.model
        targetValue: StereoFormat.Left
        isLeft: false
    }
    property Action sourceFmtSecondRotationRight : SourceFmtRotationAction {
        model: self.model
        targetValue: StereoFormat.Right
        isLeft: false
    }
    property Action sourceFmtSecondRotationUpsideDown : SourceFmtRotationAction {
        model: self.model
        targetValue: StereoFormat.UpsideDown
        isLeft: false
    }

    property Action timeShift : Action {
        text: qsTr("Time Shift") + ": " + model.videosource.timeShift.toLocaleString() + " " + qsTr("seconds")
        shortcut: "Ctrl+V,T"
        enabled: self.model.isVideoLoaded && self.model.source.activeFormat.layout == StereoFormat.Separate
        onTriggered: {
            timeShiftWindow.open(model.videosource.timeShift)
        }
    }

    TimeShiftWindow {
        id: timeShiftWindow

        onApply: {
            model.videosource.timeShift = seconds
        }
    }

    AutoAlign {
        id: autoAlignExtended
        image: self.model.source
        onOffsetCalculated: {
            image.parallax = result.offset.x
            image.vOffset = result.offset.y
            image.leftAngle = result.leftAngle
            image.rightAngle = result.rightAngle
            currentTool = toolLayout
        }
    }

    AutoAlign {
        id: autoAlign
        image: self.model.source
        onOffsetCalculated: {
            currentTool = 0
            self.model.applyAlign(result.offset.x, result.offset.y, 0, 0)
        }
    }

    property Action editAutoAreaAlign : Action {
        text: qsTr("Auto Alignment")
        property alias allowed: autoAlign.enabled
        // iconName: "dialog-align-and-distribute.svg"
        iconSource: F.getIconSource(palette, "../images/edit-auto-area-align.svg")
        shortcut: "Ctrl+A"
        enabled: allowed && (model.isImageLoaded || model.isVideoLoaded) && !autoAlign.running && !autoAlignExtended.running
        checkable: true
        checked: currentTool == toolAutoAreaAlign
        onTriggered: toggleTool(toolAutoAreaAlign)
    }

    property Action editAutoAlign : Action {
        text: qsTr("Extended Auto Alignment")
        property alias allowed: autoAlignExtended.enabled
        // iconName: "dialog-align-and-distribute.svg"
        iconSource: F.getIconSource(palette, "../images/edit-auto-align.svg")
        shortcut: "Ctrl+E,A"
        enabled: allowed && (model.isImageLoaded || model.isVideoLoaded) && !autoAlign.running && !autoAlignExtended.running
        checkable: false
        onTriggered: {
            var s = settings
            autoAlignExtended.startCalcOffset(Qt.rect(0, 0, 0, 0), s.autoAlignParallax, s.autoAlignVOffset, s.autoAlignRotate, 0.01 * s.autoAlignQuality)
        }
    }

    property Action editAutoColorAdjustLeft : Action {
        text: qsTr("Adjust Left View")
        iconSource: F.getIconSource(palette, "../images/left.svg")
        shortcut: "Ctrl+C,L"
        enabled: model.isLoaded && !autoAlign.running && !autoAlignExtended.running
        checkable: model.isVideoLoaded
        onTriggered: model.colorAutoAdjust(true, 2, checked)
    }
    Binding {
        target: editAutoColorAdjustLeft
        property: "checked"
        when: model.isVideoLoaded
        value: model.videosource.colorAdjustment === 1
    }

    property Action editAutoColorAdjustRight : Action {
        text: qsTr("Adjust Right View")
        iconSource: F.getIconSource(palette, "../images/right.svg")
        shortcut: "Ctrl+C,R"
        enabled: model.isLoaded && !autoAlign.running && !autoAlignExtended.running
        checkable: model.isVideoLoaded
        onTriggered: {
            model.colorAutoAdjust(false, 2, checked);
        }
    }
    Binding {
        target: editAutoColorAdjustRight
        property: "checked"
        when: model.isVideoLoaded
        value: model.videosource.colorAdjustment === 2
    }

    property Action toggleTimestamp : Action {
        text: model.videosource && model.videosource.hasTimestamp ? qsTr("Delete Timestamp") : qsTr("Add Timestamp")
        shortcut: "Ctrl+T"
        enabled: model.isVideoLoaded
        onTriggered: {
            if(model.videosource.hasTimestamp)
                model.removeTimestamp()
            else
                model.addTimestamp()
            playerConrol.show()
        }
    }

    property Action editLayout : Action {
        text: qsTr("Alignment")
        // iconName: "dialog-align-and-distribute.svg"
        iconSource: F.getIconSource(palette, "../images/edit-align.svg")
        shortcut: "A"
        enabled: model.isImageLoaded || model.isVideoLoaded
        checkable: true
        checked: currentTool == toolLayout
        onTriggered: toggleTool(toolLayout)
    }

    property Action editCrop : Action {
        text: qsTr("Crop")
        // iconName: "transform-crop"
        iconSource: F.getIconSource(palette, "../images/transform-crop.svg")
        shortcut: "C"
        enabled: model.isLoaded && allowedRenderType()
        checkable: true
        checked: currentTool == toolCrop
        onTriggered: toggleTool(toolCrop)

        function allowedRenderType()
        {
            var r = image.renderFormat.layout
            return r !== StereoFormat.AnamorphHorizontal
                    && r !== StereoFormat.AnamorphVertical
                    && r !== StereoFormat.Horizontal
                    && r !== StereoFormat.Vertical
        }

        onEnabledChanged: if(!enabled && currentTool == toolCrop) currentTool = 0
    }

    property Action editUndo : Action {
        text: qsTr("Undo")
        // iconName: "edit-undo"
        iconSource: F.getIconSource(palette, "../images/edit-undo.svg")
        shortcut: StandardKey.Undo
        enabled: model.isLoaded && model.canUndo
        onTriggered: model.undo()
    }

    property Action editRedo : Action {
        text: qsTr("Redo")
        // iconName: "edit-redo"
        iconSource: F.getIconSource(palette, "../images/edit-redo.svg")
        shortcut: StandardKey.Redo
        enabled: model.isLoaded && model.canRedo
        onTriggered: model.redo()
    }

    property Action zoomToFit : Action {
        // iconName: "zoom-fit-best"
        iconSource: F.getIconSource(palette, "../images/zoom-fit-best.svg")
        shortcut: "F"
        checkable: true
        checked: self.model.zoomToFit
        text: qsTr("Zoom to Fit")
        onTriggered: model.toggleZoomToFit()
        enabled: model.isImageLoaded
    }

    property Action zoomActual : Action {
        text: qsTr("Original Size")
        // iconName: "zoom-original"
        iconSource: F.getIconSource(palette, "../images/zoom-original.svg")
        shortcut: "O"
        onTriggered: zoomToActualExecute()
        enabled: model.isImageLoaded
    }

    property Action zoomIn : Action {
        // iconName: "zoom-in"
        iconSource: F.getIconSource(palette, "../images/zoom-in.svg")
        shortcut:StandardKey.ZoomIn
        text: qsTr("Zoom In")
        onTriggered: {
            zoomInExecute()
        }
        enabled: model.isImageLoaded
    }

    property Action zoomOut : Action {
        // iconName: "zoom-out"
        iconSource: F.getIconSource(palette, "../images/zoom-out.svg")
        shortcut: StandardKey.ZoomOut
        text: qsTr("Zoom Out")
        onTriggered: zoomOutExecute()
        enabled: model.isImageLoaded
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.topMargin: !fullscreen && self.miniaturesVisible ? 85 : 0
        visible: !self.folderViewerVisible

        RowLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 0


            ColumnLayout {
                Layout.fillHeight: true
                Layout.fillWidth: true
                RotateEditor {
                    id: rotateEditor
                    visible: currentTool == toolLayout                    
                }
                Rectangle {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    color: palette.dark

                    ScrollView {
                        id: scroll
                        anchors.fill: parent

                        flickableItem.interactive: true
                        verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff
                        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
                        StereoImage {
                            id: image

                            source: self.model.source
                            width: model.zoomWidth
                            height: model.zoomHeight

                            MouseArea {
                                id: mouseArea
                                anchors.fill: parent
                                hoverEnabled: true
                                acceptedButtons: Qt.LeftButton | Qt.RightButton
                                onClicked: if(mouse.button & Qt.RightButton) imageContextMenu.popup()
                                onDoubleClicked: if(mouse.button & Qt.LeftButton) extraAction.trigger()

                                property bool hideCursor: false
                                cursorShape: hideCursor
                                             ? Qt.BlankCursor
                                             : model.zoom == 1
                                               ? Qt.ArrowCursor
                                               : Qt.OpenHandCursor

                                onWheel: {
                                    if(wheel.modifiers == Qt.ControlModifier || !model.zoomToFit)
                                    {
                                        // Масштабирование
                                        if(model.isImageLoaded)
                                        {
                                            if(wheel.angleDelta.y > 0)
                                                zoomInExecute()
                                            else
                                                zoomOutExecute()
                                        }
                                    }
                                    // Листать колёсиком мыши неудобно
                                    /*
                                    else if (model.zoomToFit && self.currentTool == 0) {
                                        // Навигация по изображениям
                                        if(wheel.angleDelta.y > 0)
                                            self.gotoRightImage.trigger()
                                        else
                                            self.goToLeftImage.trigger()
                                    }
                                    */
                                }
                                onPositionChanged:
                                {
                                    showCursor()
                                    playerConrol.show()
                                    if(self.fullscreen && self.currentTool == 0)
                                    {
                                        if(miniaturesRect.visible && mouse.y <= miniaturesRect.height
                                                || !miniaturesRect.visible && mouse.y <= 1)
                                        {
                                            miniaturesRect.show()
                                            hideMiniaturesTimer.stop()
                                        }
                                        else if(miniaturesRect.visible && !hideMiniaturesTimer.running)
                                        {
                                            hideMiniaturesTimer.start()
                                        }
                                    }
                                }

                                Timer {
                                    id: hideCursorTimer
                                    interval: 4000
                                    onTriggered: parent.hideCursor = true
                                }
                                Timer {
                                    id: hideMiniaturesTimer
                                    interval: 2000
                                    repeat: false
                                    onTriggered: miniaturesRect.hide()
                                }

                                function showCursor()
                                {
                                    hideCursor = false;
                                    hideCursorTimer.restart();
                                }
                            }
                            Selector {
                                id: selector
                                visible: currentTool == toolCrop
                            }
                            AutoAlignSelector {
                                visible: currentTool == toolAutoAreaAlign
                                onSelected: {
                                    currentTool = 0
                                    autoAlign.startCalcOffset(rect, true, true, false, 1);
                                }
                            }
                        }
                        flickableItem.onContentYChanged: image.handleYChanged()
                    }
                    PlayerControl {
                        id: playerConrol
                        color: Qt.rgba(self.color.r, self.color.g, self.color.b, 0.7)
                        textColor: self.textColor
                        visible: model.isVideoLoaded && currentTool == 0 && opacity != 0
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        anchors.right: parent.right

                        duration: model.videosource ? model.videosource.player.duration : 0
                        markers: model.videosource ? model.videosource.markers : []
                        videoPts: model.videosource ? model.videosource.pts : 0

                        isPlaying: model.videosource ? model.videosource.player.isPlaying : false
                        onIsPlayingChanged: model.setIsPlaying(isPlaying)

                        Binding {
                            target: playerConrol
                            property: "position"
                            value: model.videosource ? model.videosource.player.position : 0
                        }
                        onUserPositionChanged: model.setPosition(newPos)

                        Binding {
                            target: playerConrol
                            property: "volume"
                            value: model.videosource ? model.videosource.player.volume : 0
                        }
                        onVolumeChanged: model.setVolume(volume)
                    }
                    ZoomView {
                        id: zoomView
                        visible: !model.zoomToFit
                        w : self.model.source ? self.model.source.width : 0
                        h: self.model.source ? self.model.source.height : 0
                        anchors.right: parent.right
                        anchors.rightMargin: 10
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 10
                    }
                }
            }
            Binding {
                target: self.model
                property: "viewPortWidth"
                value: scroll.viewport.childrenRect.width
                when: !image.freeze
            }

            Binding {
                target: self.model
                property: "viewPortHeight"
                value: scroll.viewport.childrenRect.height
                when: !image.freeze
            }

            VerticalSliderEditor {
                id: vslider
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignRight
                Layout.topMargin: 5
                Layout.rightMargin: 5
                textColor: self.textColor
            }
        }

        LayoutEditor {
            id: layoutPanel

            verticalEditor: vslider
            rotateEditor: rotateEditor
            image: image
            visible: currentTool == toolLayout
            textColor: self.textColor
            Layout.fillWidth: true
            Layout.rightMargin: 5
            Layout.bottomMargin: 5
            Layout.leftMargin: 5
            onCanceled: currentTool = 0
            onApplyed: {
                self.model.applyAlign(image.parallax, image.vOffset, image.leftAngle, image.rightAngle)
                currentTool = 0
            }
        }

        CropEditor {
            id: cropPanel

            selector: selector
            visible: currentTool == toolCrop
            textColor: self.textColor
            Layout.fillWidth: true
            onCrop: {
                self.model.crop(selector.selectorRect(), cropPanel.borderWidthPercents)
                currentTool = 0
            }
            onCanceled: currentTool = 0
            onVisibleChanged:
                if(visible && model.videosource && model.videosource.isCropped()){
                    selector.setSelectorRect(model.videosource.cropRect());
                    selector.borderWidthPercents = model.videosource.borderWidthPercents();
                }
        }
    }
    Rectangle {
        id: miniaturesRect
        color: Qt.rgba(self.color.r, self.color.g, self.color.b, 0.7)
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        visible: model.isLoaded && (self.fullscreen ? self.model.mode === self.model.modeView && anchors.topMargin > -height : self.miniaturesVisible)
        anchors.topMargin: self.fullscreen ? -85 : 0
        height: 85

        NumberAnimation {
            id: hideMiniatures
            target: miniaturesRect
            properties: "anchors.topMargin"
            to: -height
            loops: 1
            duration: 1000
        }

        NumberAnimation {
            id: showMiniatures
            target: miniaturesRect
            properties: "anchors.topMargin"
            to: 0
            loops: 1
            duration: 1000
        }

        Miniatures {
            id: miniatures
            anchors.fill: parent
            mainModel: self.model
            currentIndex: self.model.currentIndex
            onSelected: checkModifiedAndExecute(self.model.loadFile, filePath)
        }

        function show()
        {
            showMiniatures.start()
        }
        function hide()
        {
            hideMiniatures.start()
        }
    }

    Menu {
        id: imageContextMenu
        MenuItem {
            visible: extraAction !== null
            action: extraAction
        }
        MenuSeparator { visible: model.isVideoLoaded }
        MenuItem{ action: control.playAction; visible: model.isVideoLoaded }
        MenuItem{ action: control.gotoNextFrame; visible: model.isVideoLoaded }
        MenuItem{ action: control.gotoPreviousframe; visible: model.isVideoLoaded }
        MenuSeparator { }
        MenuItem { action: gotoFirstImage }
        MenuItem { action: goToLeftImage }
        MenuItem { action: gotoRightImage }
        MenuItem { action: gotoLastImage }
        MenuSeparator { }
        MenuItem { action: zoomToFit }
        MenuItem { action: zoomActual }
        MenuItem { action: zoomIn }
        MenuItem { action: zoomOut }
        MenuSeparator { }
        Menu {
            title: qsTr("Source format")
            iconSource: model.source ? F.getIconSource(palette, stereoFormat.layoutIconSource(model.source.activeFormat.layout)) : ""
            MenuItem { action: control.sourceFmtUseOriginal }
            MenuSeparator { }
            MenuItem { action: control.sourceFmtRowInterlaced }
            MenuItem { action: control.sourceFmtColumnInterlaced }
            MenuItem { action: control.sourceFmtAnaglyphRC }
            MenuItem { action: control.sourceFmtAnaglyphYB }
            MenuItem { action: control.sourceFmtHorizontal }
            MenuItem { action: control.sourceFmtAnamorphHorizontal }
            MenuItem { action: control.sourceFmtVertical }
            MenuItem { action: control.sourceFmtAnamorphVertical }
            MenuItem { action: control.sourceFmtMono }
            MenuItem { action: control.sourceFmtSeparate }
            MenuSeparator { }
            Menu {
                title: qsTr("Left angle rotation")
                iconSource: F.getIconSource(palette, stereoFormat.rotationIconSource(model.source.activeFormat.leftRotation))
                visible: control.sourceFmtFirstRotationNone.enabled
                MenuItem { action: control.sourceFmtFirstRotationNone }
                MenuItem { action: control.sourceFmtFirstRotationLeft }
                MenuItem { action: control.sourceFmtFirstRotationRight }
                MenuItem { action: control.sourceFmtFirstRotationUpsideDown }
            }
            Menu {
                title: qsTr("Right angle rotation")
                iconSource: F.getIconSource(palette, stereoFormat.rotationIconSource(model.source.activeFormat.rightRotation))
                visible: control.sourceFmtSecondRotationNone.enabled
                MenuItem { action: control.sourceFmtSecondRotationNone }
                MenuItem { action: control.sourceFmtSecondRotationLeft }
                MenuItem { action: control.sourceFmtSecondRotationRight }
                MenuItem { action: control.sourceFmtSecondRotationUpsideDown }
            }
            MenuItem { action: control.sourceFmtReverse }
            MenuSeparator { }
            MenuItem {
                action: control.timeShift
                visible: enabled
            }
        }
        MenuSeparator { }
        Menu {
            title: qsTr("Display format")
            iconSource: F.getIconSource(palette, stereoFormat.layoutIconSource(control.renderFormat.layout))
            MenuItem { action: renderRowInterlaced }
            MenuItem { action: renderColumnInterlaced }
            MenuItem { action: renderAnaglyphRC }
            MenuItem { action: renderAnaglyphYB }
            MenuItem { action: renderHorizontal }
            MenuItem { action: renderAnamorphHorizontal }
            MenuItem { action: renderVertical }
            MenuItem { action: renderAnamorphVertical }
            MenuItem { action: renderMono }
            MenuSeparator { }
            MenuItem { action: control.renderReverse }
        }
        MenuSeparator {}
        MenuItem { action: editUndo }
        MenuItem { action: editRedo }
        MenuSeparator {}
        MenuItem { action: toggleTimestamp; visible: model.isVideoLoaded }
        MenuSeparator { visible: model.isVideoLoaded }
        MenuItem { action: editLayout }
        MenuItem { action: editAutoAreaAlign; visible: editAutoAreaAlign.allowed }
        MenuItem { action: editAutoAlign; visible: editAutoAlign.allowed }
        MenuItem { action: editCrop }
        Menu {
            iconSource: "../images/color-management.svg"
            title: qsTr("Auto Color Adjustment")
            enabled: editAutoColorAdjustRight.enabled || editAutoColorAdjustLeft.enabled
            MenuItem { action: editAutoColorAdjustLeft }
            MenuItem { action: editAutoColorAdjustRight }
        }
        MenuSeparator {}
        Menu {
            title: qsTr("File")
            MenuItem { action: open }
            MenuItem { action: openSeparate }
            MenuItem { action: saveImage }
            MenuItem { action: saveVideoAs; visible: enabled }
            MenuItem { action: saveImageAs }
            MenuItem { action: saveImageAsCopy }
            MenuSeparator {}
            MenuItem { action: fileCopyTo }
            MenuItem { action: fileMoveTo }
            MenuItem { action: fileMoveToTrash }
            MenuSeparator {}
            MenuItem { action: fileInfo }
            MenuItem { action: close }
        }
        MenuSeparator { }
        MenuItem { action: self.quitAction }
    }

    MessageDialog {
        id: loadErrorDialog
        icon: StandardIcon.Critical
        property string filePath
        title: qsTr("Error")
        text: qsTr("Unable to load file:\n%1").arg(filePath)
    }

    MessageDialog {
        id: checkModifiedDialog

        property var action
        property var param

        title: qsTr("Saving changes")
        text: qsTr("The current file was modified") + "."
        standardButtons: StandardButton.Save | StandardButton.Discard | StandardButton.Cancel
        icon: StandardIcon.Question
        onButtonClicked: {
            switch(clickedButton)
            {
            case StandardButton.Save:
                if(self.model.isVideoLoaded)
                    saveVideoAs.trigger()
                else
                    saveImage.trigger()
                break;
            case StandardButton.Discard:
                action(param)
                break;
            case StandardButton.Cancel:
                break;
            }
        }
    }

    MessageDialog {
        id: moveToTrashDialog
        title: qsTr("Move to Trash")
        text: qsTr("Move file '%1' to trash?").arg(self.model.fileName)
        icon: StandardIcon.Question
        standardButtons: StandardButton.Yes|StandardButton.No
        onYes:  {
            self.model.filePaths.forEach(function(path){ fileOperations.moveToTrash(path) });
        }
    }

    MyFileDialog {
        id: openFileDialog
        title: qsTr("Open file", "File dialog title")
        nameFilters: model.nameFiltersToOpen
        selectExisting: true
        acceptMode: MyFileDialog.AcceptOpen
        path: standardPaths.pictures
        onAccepted: self.model.loadFile(file);
    }

    MyFileDialog {
        id: openSeparateFileDialog
        title: isLeft ? qsTr("Open left view", "File dialog title") : qsTr("Open right view", "File dialog title")
        nameFilters: model.nameFiltersToOpenSeparate
        selectExisting: true
        acceptMode: MyFileDialog.AcceptOpen

        property string leftFile
        property bool isLeft : true
        property string leftPath
        property string rightPath

        function init(){
            leftFile = ""
            isLeft = true
            path = leftPath || standardPaths.pictures
            open()
        }

        onAccepted: {
            if(isLeft)
            {
                leftFile = file
                isLeft = false
                if(rightPath)
                    path = rightPath
                open()
            }
            else
            {
                self.model.loadFile(leftFile + "\n" + file);
                leftPath = urlConv.getDirPath(leftFile)
                rightPath = urlConv.getDirPath(file)
                leftFile = ""
                isLeft = true
            }
        }
    }

    Settings {
        id: saveImageAsCopySettings
        category: "SaveImageAsCopy"
        property string nameFilterIndex
        property string path
    }

    FileFormatInfo {
        id: fileFormatInfo
    }

    /// Окно выбора имени файла для сохранения изображения
    MyFileDialog {
        id: saveImageFileDialog
        title: qsTr("Save image", "File dialog title")
        selectExisting: false
        acceptMode: MyFileDialog.AcceptSave
        nameFilters: [ model.nameFilterToSaveMPO, model.nameFilterToSaveJPS ]
        property bool asCopy

        onAccepted:
        {
            imageOptionsDialog.open(selectedFile, selectedNameFilterIndex === 1 ? "jps" : "mpo", !asCopy)
            if(saveImageFileDialog.asCopy)
            {
                saveImageAsCopySettings.path = urlConv.getDirPath(selectedFile);
                saveImageAsCopySettings.nameFilterIndex = saveImageFileDialog.selectedNameFilterIndex
            }
        }

        /// Показывает диалог сохранения файла
        function show(filePath)
        {
            saveImageFileDialog.asCopy = false
            if(model.isJpsLoaded)
            {
                saveImageFileDialog.selectedNameFilter = model.nameFilterToSaveJPS
                filePath = urlConv.changeSuffix(filePath, model.jpsExt[0]);
            }
            else
            {
                saveImageFileDialog.selectedNameFilter = model.nameFilterToSaveMPO
                filePath = urlConv.changeSuffix(filePath, model.mpoExt[0]);
            }
            saveImageFileDialog.open(filePath)
        }

        function showAsCopy(filePath)
        {
            saveImageFileDialog.selectedNameFilterIndex = saveImageAsCopySettings.nameFilterIndex;
            saveImageFileDialog.asCopy = true;
            var dirPath = saveImageAsCopySettings.path;
            if(dirPath === "")
                dirPath = urlConv.getDirPath(filePath);
            filePath = dirPath + urlConv.separator + urlConv.getFileName(filePath);
            var suffix = saveImageFileDialog.selectedNameFilterIndex == 0 ? model.mpoExt[0] : model.jpsExt[0];
            filePath = urlConv.changeSuffix(filePath, suffix);
            saveImageFileDialog.open(filePath);
        }

    }

    /// Диалог настройки параметров сохранения файла
    QtObject {
        id: imageOptionsDialog

        /// Текущее имя файла
        property string fileName

        /// Загружать файл после сохранения
        property bool loadAfterSave

        /// Открывает диалог настройки параметров
        function open(fileName, format, loadAfterSave)
        {
            imageOptionsDialog.fileName = fileName
            imageOptionsDialog.loadAfterSave = loadAfterSave
            var optionsWindow = null
            if(format === "mpo")
            {
                optionsWindow = getMpoOptionsWindow()
            }
            else if (format === "jps")
            {
                optionsWindow = getJpsOptionsWindow()
                if(model.source.originalFormat.layout != StereoFormat.Separate)
                    optionsWindow.originalFormat = model.source.originalFormat
            }
            else
                return;
            optionsWindow.title = qsTr("Save %1").arg(urlConv.getFileName(fileName))
            optionsWindow.open(model.source.leftSize, model.source.rightSize)
        }

        /// Окно для выбора параметров сохранения MPO
        property SaveImageWindow mpoOptionsWindow

        /// Возвращает окно выбора параметров сохранения MPO
        function getMpoOptionsWindow()
        {
            if(!mpoOptionsWindow)
                mpoOptionsWindow = createMpoOptionsWindow()
            return mpoOptionsWindow
        }

        function createMpoOptionsWindow()
        {
            var mpoParams = {
                presets: mpoPresetsModel,
                layoutsModel: fileFormatInfo.mpoLayouts
            }
            var window = Qt.createComponent("SaveImageWindow.qml").createObject(saveImageFileDialog, mpoParams)
            window.accepted.connect(saveMPO)
            return window
        }

        /// Формат по умолчанию для сохранения файлов
        property StereoFormat defaultFormat: StereoFormat {
            layout: StereoFormat.Horizontal
        }

        /// Окно для выбора параметров сохранения JPS
        property SaveImageWindow jpsOptionsWindow

        /// Возвращает окно выбора параметров сохранения JPS
        function getJpsOptionsWindow()
        {
            if(!jpsOptionsWindow)
                jpsOptionsWindow = createJpsOptionsWindow()
            if(model.source.activeFormat.layout === StereoFormat.Separate)
                jpsOptionsWindow.originalFormat = defaultFormat
            else
                jpsOptionsWindow.originalFormat = model.source.activeFormat
            return jpsOptionsWindow
        }

        function createJpsOptionsWindow() {
            var jpsParams = {
                presets: jpsPresetsModel,
                layoutsModel: fileFormatInfo.jpsLayouts
            }
            var window = Qt.createComponent("SaveImageWindow.qml").createObject(saveImageFileDialog, jpsParams)
            window.accepted.connect(saveJPS)
            return window
        }

        /// Обработка подтверждения выбора параметров в окне сохранения параметров JPS
        function saveJPS()
        {
            var f = jpsOptionsWindow.currentFormat
            var p = jpsOptionsWindow.currentPreset
            self.model.saveJPSAs(fileName, f, p.size(), p.jpegQuality)
            if(loadAfterSave)
                model.onSaveCurrent(fileName)
        }

        /// Обработка подтверждения выбора параметров в окне сохранения параметров MPO
        function saveMPO()
        {
            var f = mpoOptionsWindow.currentFormat
            var p = mpoOptionsWindow.currentPreset
            self.model.saveMPOAs(fileName, f.leftFirst, p.size(), p.jpegQuality)
            if(loadAfterSave)
                model.onSaveCurrent(fileName)
        }
    }

    MessageDialog {
        id: unknownFormatDialog
        title: qsTr("Saving video")
        text: qsTr("Can not save video to source file") + "."
        icon: StandardIcon.Warning
        standardButtons: StandardButton.Ok
    }

    // Окно создаётся при первом обращении
    property SaveVideoWindow saveVideoWindow

    function showSaveVideoWindow()
    {
        var window = getSaveVideoWindow();
        window.open(model.filePaths[0], model.videosource.frameSizeForSave())
    }

    function getSaveVideoWindow()
    {
        if(!saveVideoWindow)
            saveVideoWindow = createSaveVideoWindow();
        if(model.source.activeFormat === StereoFormat.Separate)
            saveVideoWindow.originalFormat = saveVideoWindow.defaultFormat
        else
            saveVideoWindow.originalFormat = model.source.activeFormat;
        return saveVideoWindow;
    }

    function createSaveVideoWindow()
    {
        var params = {
            presets: videoPresetsModel,
            layoutsModel: fileFormatInfo.jpsLayouts
        }
        var window = Qt.createComponent("SaveVideoWindow.qml").createObject(self, params)
        window.accepted.connect(onSaveVideoAccepted)
        return window;
    }

    function onSaveVideoAccepted(path, options)
    {
        model.saveVideoAs(path, options)
    }

    MyFileDialog {
        id: fileCopyToDialog
        title: qsTr("Copy to")
        selectExisting: false
        acceptLabel: qsTr("Copy")
        acceptMode: MyFileDialog.AcceptSave
        onAccepted: fileOperations.copy(model.filePath, file);
    }

    MyFileDialog {
        id: fileMoveToDialog
        title: qsTr("Move to")
        acceptLabel: qsTr("Move")
        selectExisting: false
        acceptMode: MyFileDialog.AcceptSave
        onAccepted: fileOperations.move(model.filePath, file)
    }

    property FileInfoDialog fileInfoDialog

    function showFileInfoDialog()
    {
        if(!fileInfoDialog)
            fileInfoDialog = Qt.createComponent("FileInfoDialog.qml").createObject(self)
        fileInfoDialog.leftMetadata = model.leftMetadata
        fileInfoDialog.rightMetadata = model.rightMetadata
        fileInfoDialog.show()
    }

    Component {
        id: folderViewerComponent
        FolderViewer {
            anchors.fill: parent
            textColor: self.textColor
            model: self.model
            Component.onCompleted: {
                if(folderViewerLoader.lastFolder)
                    selectedFolder = folderViewerLoader.lastFolder
                else
                    selectedFolder = favoritesModel.recentFoldersPath
            }
        }
    }

    Loader {
        id: folderViewerLoader
        property string lastFolder
        anchors.fill: parent
        sourceComponent: self.folderViewerVisible ? folderViewerComponent : undefined
    }

    function getImage()
    {
        return image;
    }

    function checkModifiedAndExecute(action, param)
    {
        if(self.model.source !== null && self.model.modified)
        {
            checkModifiedDialog.param = param
            checkModifiedDialog.action = action
            checkModifiedDialog.open()
        }
        else
        {
            action(param)
        }
    }

    function changeZoom(action)
    {
        var oldWidth = image.width
        var oldHeight = image.height
        action()
        scroll.flickableItem.contentX += (image.width - oldWidth) / 2
        scroll.flickableItem.contentY += (image.height - oldHeight) / 2
        if(scroll.flickableItem.contentX < 0)
            scroll.flickableItem.contentX = 0;
        if(scroll.flickableItem.contentY < 0)
            scroll.flickableItem.contentY = 0;
    }

    function zoomInExecute()
    {
        changeZoom(self.model.zoomIn)
    }

    function zoomOutExecute()
    {
        changeZoom(self.model.zoomOut)
    }

    function zoomToActualExecute()
    {
        changeZoom(self.model.zoomtoActual)
    }

    function toggleTool(tool)
    {
        if(currentTool === tool)
            currentTool = 0
        else
            currentTool = tool
    }

    function clone(src)
    {
        miniaturesVisible = src.miniaturesVisible
        vslider.value = src.vslider.value
        playerConrol.clone(src.playerConrol)
        scroll.flickableItem.contentX = src.scroll.flickableItem.contentX
        scroll.flickableItem.contentY = src.scroll.flickableItem.contentY
        quitAction = src.quitAction
    }


    Connections {
        target: model
        onLoadError: {
            if(!image.freeze)
            {
                loadErrorDialog.filePath = filePath
                loadErrorDialog.open()
            }
        }
    }

    Connections {
        target: model
        onImageSourceEndChanged : {
            currentTool = 0
            if(model.isVideoLoaded)
            {
                model.setVolume(playerConrol.volume)
                playerConrol.show()
            }
            if(model.isLoaded)
                folderViewerLoader.lastFolder = urlConv.getDirPath(model.filePaths[0])
        }
    }

    BusyIndicator {
        id: busyIndicator
        anchors.centerIn: parent
        running: autoAlign.running || autoAlignExtended.running
    }
}
