import QtQuick 2.5

Rectangle {
    id: zoomView
    property double m : 5

    opacity: 0.6
    color: "#90000000"
    border.color: palette.light
    border.width: 2

    property double w : 0
    property double h: 0

    width: w > h ? scroll.width / m : zoomView.height * w / h
    height: w > h ? zoomView.width * h / w : scroll.height / m

    Rectangle {
        border.color: palette.light
        border.width: 2
        color: "#90FFFFFF"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: scroll.flickableItem.contentX / scroll.flickableItem.contentWidth * zoomView.width
        anchors.topMargin: scroll.flickableItem.contentY / scroll.flickableItem.contentHeight * zoomView.height
        width: scroll.viewport.width / scroll.flickableItem.contentWidth * zoomView.width
        height: scroll.viewport.height / scroll.flickableItem.contentHeight * zoomView.height
    }
}

