
function getIconSource(palette, path)
{
    if(palette === null)
        return;
    return getIconSourceByColor(palette.window, path)
}

function getIconSourceByColor(color, path)
{
    if(color === null || path === null || path.length === 0)
        return;
    var c = color
    var y = 0.299 * c.r + 0.587 * c.g + 0.114 * c.b
    var ext = path.substr(path.lastIndexOf("."))
    var basePath = path.substr(0, path.lastIndexOf("."))
    return basePath + (y < 0.5 ? "-dark" : "") + ext
}

function strFormat(v, format)
{
    var str = v.toString()
    return format.substring(0, format.length - str.length) + str
}

/// Возвращает время в строковом вормате по количеству секунд
function getTime(position)
{
    var totalSeconds = Math.round(position)
    var totalMinutes = totalSeconds / 60
    var hours = Math.floor(totalMinutes / 60)
    var minutes = Math.floor(totalMinutes) - hours * 60
    var seconds = totalSeconds - hours * 60 * 60 - minutes * 60
    return strFormat(hours, "00") + ":" + strFormat(minutes, "00") + ":" + strFormat(seconds, "00");
}

function getNameFilters(format)
{
    return format.extensions.map(getFilePattern).join(" ")
}

function getFilePattern(ext)
{
    return "*." + ext
}
