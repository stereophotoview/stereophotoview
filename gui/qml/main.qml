import QtQuick 2.5
import QtQuick.Controls 1.4
import StereoImage 1.0
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import MyDialogs 1.0
import Qt.labs.settings 1.0

import "functions.js" as F

ApplicationWindow {
    width: 700
    height: 500
    visible: true
    title: (model.fileName == "" || model.fileName == null
            ? qsTr("Stereo Photo View")
            : model.fileName + (model.modified ? " (" + qsTr("modified", "flag in the header") + ")" : "") + " - " + qsTr("Stereo Photo View"))

    id: window

    SystemPalette { id: palette; colorGroup: SystemPalette.Active }

    property TaskQueueWindow tqwindow

    Connections {
        target: taskQueue
        onTaskAdded: {
            createTaskQueueWindow()
            tqwindow.show()
        }
    }

    function createTaskQueueWindow()
    {
        if(!tqwindow)
            tqwindow = Qt.createComponent("TaskQueueWindow.qml").createObject(window);
    }

    menuBar: MenuBar {
        id: mainmenu
        Menu {
            title: qsTr("&File")
            visible: !control.fullscreen
            MenuItem { action: control.open }
            MenuItem { action: control.openSeparate }
            MenuItem { action: control.saveImage; visible: !model.isVideoLoaded }
            MenuItem { action: control.saveVideoAs; visible: model.isVideoLoaded }
            MenuItem { action: control.saveImageAs }
            MenuItem { action: control.saveImageAsCopy }
            MenuSeparator {}
            MenuItem { action: control.fileCopyTo }
            MenuItem { action: control.fileMoveTo }
            MenuItem { action: control.fileMoveToTrash }
            MenuSeparator {}
            MenuItem { action: control.fileInfo }
            MenuItem { action: control.close }
            MenuSeparator {}
            MenuItem { action: batchProcessingAction }
            MenuSeparator {}
            MenuItem { action: window.quitAction }
        }
        Menu {
            title: qsTr("&Source format")
            visible: !control.fullscreen
            MenuItem { action: control.sourceFmtUseOriginal }
            MenuSeparator { }
            MenuItem { action: control.sourceFmtRowInterlaced }
            MenuItem { action: control.sourceFmtColumnInterlaced }
            MenuItem { action: control.sourceFmtAnaglyphRC }
            MenuItem { action: control.sourceFmtAnaglyphYB }
            MenuItem { action: control.sourceFmtHorizontal }
            MenuItem { action: control.sourceFmtAnamorphHorizontal }
            MenuItem { action: control.sourceFmtVertical }
            MenuItem { action: control.sourceFmtAnamorphVertical }
            MenuItem { action: control.sourceFmtMono }
            MenuItem { action: control.sourceFmtSeparate }
            MenuSeparator { }
            Menu {
                title: qsTr("Left angle rotation")
                iconSource: F.getIconSource(palette, stereoFormat.rotationIconSource(model.source.activeFormat.leftRotation))
                visible: control.sourceFmtFirstRotationNone.enabled
                MenuItem { action: control.sourceFmtFirstRotationNone }
                MenuItem { action: control.sourceFmtFirstRotationLeft }
                MenuItem { action: control.sourceFmtFirstRotationRight }
                MenuItem { action: control.sourceFmtFirstRotationUpsideDown }
            }
            Menu {
                title: qsTr("Right angle rotation")
                iconSource: F.getIconSource(palette, stereoFormat.rotationIconSource(model.source.activeFormat.rightRotation))
                visible: control.sourceFmtSecondRotationNone.enabled
                MenuItem { action: control.sourceFmtSecondRotationNone }
                MenuItem { action: control.sourceFmtSecondRotationLeft }
                MenuItem { action: control.sourceFmtSecondRotationRight }
                MenuItem { action: control.sourceFmtSecondRotationUpsideDown }
            }
            MenuItem { action: control.sourceFmtReverse }

            MenuSeparator { }
            MenuItem {
                action: control.timeShift
                visible: enabled
            }
        }
        Menu {
            title: qsTr("&Edit")
            visible: !control.fullscreen
            MenuItem { action: control.editUndo }
            MenuItem { action: control.editRedo }
            MenuSeparator { }
            MenuItem { action: control.toggleTimestamp; visible: model.isVideoLoaded }
            MenuSeparator { visible: model.isVideoLoaded }
            MenuItem { action: control.editLayout }
            MenuItem { action: control.editAutoAreaAlign; visible: control.editAutoAreaAlign.allowed }
            MenuItem { action: control.editAutoAlign; visible: control.editAutoAlign.allowed }
            MenuItem { action: control.editCrop }
            Menu {
                iconSource: "../images/color-management.svg"
                title: qsTr("Auto Color Adjustment")
                enabled: control.editAutoColorAdjustRight.enabled || control.editAutoColorAdjustLeft.enabled
                MenuItem { action: control.editAutoColorAdjustLeft }
                MenuItem { action: control.editAutoColorAdjustRight }
            }
            MenuSeparator { }
            Menu {
                id: openLeftImageToolMenu
                iconSource: F.getIconSource(palette, "../images/ext-open-left.svg")
                title: qsTr("Open left image with…")
                enabled: model.isImageLoaded && items.length > 0
                OpenWithListInstantiator { parentMenu: openLeftImageToolMenu; imageModel: model; leftView: true }
            }
            Menu {
                title: qsTr("Open right image with…")
                id: openrightImageToolMenu
                iconSource: F.getIconSource(palette, "../images/ext-open-right.svg")
                enabled: model.isImageLoaded && items.length > 0
                OpenWithListInstantiator { parentMenu: openrightImageToolMenu; imageModel: model; leftView: false }
            }
            MenuSeparator { }
            MenuItem {
                text: qsTr("Settings…")
                // iconName: "configure"
                iconSource: F.getIconSource(palette, "../images/configure.svg")
                onTriggered: settingsEditor.open()
            }
        }
        Menu {
            title: qsTr("&View")
            visible: !control.fullscreen
            MenuItem {
                id: tqMenu
                text: qsTr("Active tasks: %1").arg(taskQueue.activeCount)
                checkable: true;
                checked: tqwindow && tqwindow.visible
                onCheckedChanged:
                {
                    if(tqMenu.checked)
                    {
                        createTaskQueueWindow()
                        tqwindow.show()
                    }
                    else if (tqwindow)
                    {
                        tqwindow.hide()
                    }
                }
            }
            MenuSeparator { }
            MenuItem{ action: control.miniaturesToggle }
            MenuItem { action: fullScreenAction }
            MenuSeparator { }
            Menu {
                title: qsTr("Toolbars")
                MenuItem {
                    id: toolbarFileVisible
                    checkable: true
                    text: qsTr("File")
                    checked: true
                }
                MenuItem {
                    id: toolbarFileOperationsVisible
                    checkable: true
                    text: qsTr("File operations")
                    checked: false
                }
                MenuItem {
                    id: toolbarEditVisible
                    checkable: true
                    text: qsTr("Edit")
                    checked: true
                }
                MenuItem {
                    id: toolbarViewVisible
                    checkable: true
                    text: qsTr("View")
                    checked: true
                }
                MenuItem {
                    id: toolbarGoVisible
                    checkable: true
                    text: qsTr("Go")
                    checked: true
                }
            }

            MenuSeparator { }
            MenuItem { action: control.zoomToFit }
            MenuItem { action: control.zoomActual }
            MenuItem { action: control.zoomIn }
            MenuItem { action: control.zoomOut }
            MenuSeparator { }
            MenuItem { action: control.renderRowInterlaced }
            MenuItem { action: control.renderColumnInterlaced }
            MenuItem { action: control.renderAnaglyphRC }
            MenuItem { action: control.renderAnaglyphYB }
            MenuItem { action: control.renderHorizontal }
            MenuItem { action: control.renderAnamorphHorizontal }
            MenuItem { action: control.renderVertical }
            MenuItem { action: control.renderAnamorphVertical }
            MenuItem { action: control.renderMono }
            MenuSeparator { }
            MenuItem { action: control.renderReverse }
        }

        Menu {
            title: qsTr("&Play")
            visible: !control.fullscreen && model.isVideoLoaded
            MenuItem{ action: control.playAction }
            MenuItem{ action: control.gotoNextFrame }
            MenuItem{ action: control.gotoPreviousframe }
        }

        Menu {
            title: qsTr("&Go")
            visible: !control.fullscreen
            MenuItem{ action: control.gotoFirstImage }
            MenuItem{ action: control.goToLeftImage }
            MenuItem{ action: control.gotoRightImage }
            MenuItem{ action: control.gotoLastImage }
        }

        Menu {
            title: qsTr("&Help")
            visible: !control.fullscreen
            MenuItem {
                text: qsTr("Online help")
                // iconName: "help-about"
                iconSource: F.getIconSource(palette, "../images/help.svg")
                onTriggered: Qt.openUrlExternally("https://stereophotoview.bitbucket.io" + qsTr("/en/help.html", "Help url"))
            }
            MenuSeparator { }
            MenuItem {
                text: qsTr("About %1…").arg("StereoPhotoView")
                // iconName: "help-about"
                iconSource: F.getIconSource(palette, "../images/help-about.svg")
                onTriggered: {
                    var component = Qt.createComponent("About.qml")
                    var aboutDialog = component.createObject(window)
                    aboutDialog.show()
                }
            }
        }
    }

    toolBar: ToolBar {
        visible: !control.fullscreen
                 && (toolbarFileVisible.checked || toolbarFileOperationsVisible.checked || toolbarEditVisible.checked || toolbarViewVisible.checked || toolbarGoVisible.checked)
        implicitHeight: contentItem.childrenRect.height
        Flow {
            anchors.left: parent.left
            anchors.right: parent.right
            Row {
                visible: toolbarFileVisible.checked
                ToolButton { action: control.open  }
                ToolButton { action: control.openSeparate }
                ToolButton { action: control.saveImage; visible: !model.isVideoLoaded }
                ToolButton { action: control.saveImageAs; visible: !model.isVideoLoaded }
                ToolButton { action: control.saveVideoAs; visible: model.isVideoLoaded  }
                ToolButton { action: control.saveImageAsCopy;  }
                ToolButton {
                    tooltip: qsTr("Source type") + ": " + stereoFormat.layoutName(model.source.activeFormat.layout)
                    iconSource: F.getIconSource(palette, stereoFormat.layoutIconSource(model.source.activeFormat.layout))
                    menu: Menu {
                        MenuItem { action: control.sourceFmtUseOriginal }
                        MenuSeparator { }
                        MenuItem { action: control.sourceFmtRowInterlaced }
                        MenuItem { action: control.sourceFmtColumnInterlaced }
                        MenuItem { action: control.sourceFmtAnaglyphRC }
                        MenuItem { action: control.sourceFmtAnaglyphYB }
                        MenuItem { action: control.sourceFmtHorizontal }
                        MenuItem { action: control.sourceFmtAnamorphHorizontal }
                        MenuItem { action: control.sourceFmtVertical }
                        MenuItem { action: control.sourceFmtAnamorphVertical }
                        MenuItem { action: control.sourceFmtMono }
                        MenuItem { action: control.sourceFmtSeparate }
                    }
                }
                ToolButton {
                    tooltip: qsTr("Left angle rotation") + ": " + stereoFormat.rotationName(model.source.activeFormat.leftRotation)
                    iconSource: F.getIconSource(palette, stereoFormat.rotationIconSource(model.source.activeFormat.leftRotation))
                    visible: control.sourceFmtFirstRotationNone.enabled
                    menu: Menu {
                        MenuItem { action: control.sourceFmtFirstRotationNone }
                        MenuItem { action: control.sourceFmtFirstRotationLeft }
                        MenuItem { action: control.sourceFmtFirstRotationRight }
                        MenuItem { action: control.sourceFmtFirstRotationUpsideDown }
                    }
                }
                ToolButton {
                    tooltip: qsTr("Right angle rotation") + ": " + stereoFormat.rotationName(model.source.activeFormat.rightRotation)
                    iconSource: F.getIconSource(palette, stereoFormat.rotationIconSource(model.source.activeFormat.rightRotation))
                    visible: control.sourceFmtSecondRotationNone.enabled
                    menu: Menu {
                        MenuItem { action: control.sourceFmtSecondRotationNone }
                        MenuItem { action: control.sourceFmtSecondRotationLeft }
                        MenuItem { action: control.sourceFmtSecondRotationRight }
                        MenuItem { action: control.sourceFmtSecondRotationUpsideDown }
                    }
                }
                ToolButton { action: control.sourceFmtReverse; tooltip: qsTr("Source type") + ": " + qsTr("Reverse") }
                ToolButton { action: control.fileInfo }
                ToolButton { action: control.close }
            }
            Row {
                id: fileOperationsToolBar
                visible: toolbarFileOperationsVisible.checked
                ToolButton { action: control.fileCopyTo }
                ToolButton { action: control.fileMoveTo }
                ToolButton { action: control.fileMoveToTrash }
            }
            Row {
                //todo Добавить регулировку цвета
                id: editToolBar
                visible: toolbarEditVisible.checked
                ToolButton { action: control.editUndo }
                ToolButton { action: control.editRedo }
                ToolButton { action: control.editLayout }
                ToolButton { action: control.editAutoAreaAlign; visible: control.editAutoAreaAlign.allowed }
                ToolButton { action: control.editAutoAlign; visible: control.editAutoAlign.allowed }
                ToolButton { action: control.editCrop }
                ToolButton {
                    iconSource: "../images/color-management.svg"
                    tooltip: qsTr("Auto Color Adjustment")
                    enabled: control.editAutoColorAdjustRight.enabled || control.editAutoColorAdjustLeft.enabled
                    menu: Menu {
                        MenuItem { action: control.editAutoColorAdjustLeft }
                        MenuItem { action: control.editAutoColorAdjustRight }
                    }
                }

                ToolButton {
                    iconSource: F.getIconSource(palette, "../images/ext-open-left.svg")
                    tooltip: qsTr("Open left image with…")
                    enabled: model.isImageLoaded && menu.items.length > 0
                    menu: Menu {
                        id: openLeftImageMenu
                        title: qsTr("Open left image with…")
                        OpenWithListInstantiator { parentMenu: openLeftImageMenu; imageModel: model; leftView: true }
                    }
                }
                ToolButton {
                    iconSource: F.getIconSource(palette, "../images/ext-open-right.svg")
                    tooltip: qsTr("Open right image with…")
                    enabled: model.isImageLoaded && menu.items.length > 0
                    menu: Menu {
                        id: openrightImageMenu
                        title: qsTr("Open right image with…")
                        OpenWithListInstantiator { parentMenu: openrightImageMenu; imageModel: model; leftView: false }
                    }
                }

            }
            Row {
                id: viewToolBar
                visible: toolbarViewVisible.checked
                ToolButton{ action: control.miniaturesToggle }
                ToolButton { action: fullScreenAction }
                ToolButton { action: control.zoomToFit }
                ToolButton { action: control.zoomActual }
                ToolButton { action: control.zoomIn }
                ToolButton { action: control.zoomOut }
                ToolButton{
                    tooltip: qsTr("Display format") + ": " + stereoFormat.layoutName(control.renderFormat.layout)
                    iconSource: F.getIconSource(palette, stereoFormat.layoutIconSource(control.renderFormat.layout))
                    menu: Menu {
                        MenuItem { action: control.renderRowInterlaced }
                        MenuItem { action: control.renderColumnInterlaced }
                        MenuItem { action: control.renderAnaglyphRC }
                        MenuItem { action: control.renderAnaglyphYB }
                        MenuItem { action: control.renderHorizontal }
                        MenuItem { action: control.renderAnamorphHorizontal }
                        MenuItem { action: control.renderVertical }
                        MenuItem { action: control.renderAnamorphVertical }
                        MenuItem { action: control.renderMono }
                    }
                }
                ToolButton { action: control.renderReverse; tooltip: qsTr("Display format") + ": " + qsTr("Reverse") }
            }
            Row {
                id: goToolBar
                visible: toolbarGoVisible.checked
                ToolButton{ action: control.gotoFirstImage }
                ToolButton{ action: control.goToLeftImage }
                ToolButton{ action: control.gotoRightImage }
                ToolButton{ action: control.gotoLastImage }
            }
        }
    }

    //todo динамическое создание
    SettingsEditor {
        id: settingsEditor
    }

    WorkSpace{
        id: control
        fullscreen: false
        focus: true
        anchors.fill: parent
        anchors.margins: 0
        model: model
        extraAction: fullScreenAction
        quitAction: window.quitAction
        onFullscreenChanged: {
            if(fullscreen)
                window.showFullScreen()
            else
                window.show()
        }
    }
    DropArea {
        anchors.fill: parent
        onDropped: {
            if(drop.hasUrls)
            {
                if(drop.urls.length === 1)
                    model.loadFile(urlConv.getLocalFile(drop.urls[0]))
                else if (drop.urls.length === 2)
                    model.loadFile(urlConv.getLocalFile(drop.urls[0]) + "\n" + urlConv.getLocalFile((drop.urls[1])))
            }
        }
    }

    Model{
        id: model
    }

    property bool closeAtTasksStopped: false

    MessageDialog {
        id: stopActiveTasksDialog
        title: qsTr("Active tasks")
        text: qsTr("There are %1 active tasks.\nDo you want to stop them?").arg(taskQueue.activeCount)
        standardButtons: StandardButton.Yes | StandardButton.Cancel
        icon: StandardIcon.Question
        onButtonClicked: {
            switch(clickedButton)
            {
            case StandardButton.Yes:
                window.closeAtTasksStopped = true
                taskQueue.stop()
                break;
            case StandardButton.Cancel:
                break;
            }
        }
    }

    property Action fullScreenAction : Action{
        id: fullScreenAction
        text: control.fullscreen ? qsTr("Window mode") : qsTr("Full screen")
        // iconName: "view-fullscreen"
        iconSource: F.getIconSource(palette, "../images/view-fullscreen.svg")
        shortcut: StandardKey.FullScreen
        onTriggered: control.fullscreen = !control.fullscreen
    }

    property Action quitAction : Action {
        text: qsTr("Quit")
        // iconName: "application-exit"
        iconSource: "../images/application-exit.svg"
        shortcut: StandardKey.Quit
        onTriggered: window.close()
    }

    Action {
        id: batchProcessingAction
        text: qsTr("Batch processing") + "…"
        onTriggered: startupArguments.startBatchProcessing()
    }

    property bool forceCloseChanges: false

    onClosing: {
        if(taskQueue.activeCount > 0)
        {
            stopActiveTasksDialog.open()
            close.accepted = false
        }
        else if(!window.forceCloseChanges)
        {
            control.checkModifiedAndExecute(closeFile, "parameter")
            close.accepted = false
        }
    }

    function closeFile(p)
    {
        control.model.close()
        window.forceCloseChanges = true
        window.close()
    }

    Settings {
        id: windowSettings
        category: "MainWindow"
        property double width: 700
        property double height: 500
        property alias fullScreen: control.fullscreen
    }

    Binding {
        target: windowSettings
        property: "width"
        value: window.width
        when: !control.fullscreen
    }
    Binding {
        target: windowSettings
        property: "height"
        value: window.height
        when: !control.fullscreen
    }
    Binding {
        target: window
        property: "width"
        value: windowSettings.width
        when: !windowSettings.fullscreen
    }
    Binding {
        target: window
        property: "height"
        value: windowSettings.height
        when: !windowSettings.fullscreen
    }

    Settings {
        category: "Toolbars"
        property alias file: toolbarFileVisible.checked
        property alias fileoperations: toolbarFileOperationsVisible.checked
        property alias edit: toolbarEditVisible.checked
        property alias view: toolbarViewVisible.checked
        property alias go: toolbarGoVisible.checked
    }

    Connections {
        target: taskQueue
        onStopped: {
            if(closeAtTasksStopped)
                window.close()
        }
    }

    Component.onCompleted: {
        model.init()
        if(control.fullscreen)
            window.showFullScreen()
    }
}

