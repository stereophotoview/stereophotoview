#include "qpaletteserializer.h"

#include <QMetaEnum>
#include <QDir>

using namespace stereoscopic::gui;

void QPaletteSerializer::serialize(const QPalette &palette, QTextStream &stream)
{
    auto mg = QMetaEnum::fromType<QPalette::ColorGroup>();
    auto mr = QMetaEnum::fromType<QPalette::ColorRole>();

    for(int gi = 0; gi < 3; gi++)
    {
        auto group = (QPalette::ColorGroup)mg.value(gi);
        QString groupKey = mg.valueToKey(group);
        stream << "[" << groupKey << "]\n";
        for(int ri = 0; ri < 20; ri++)
        {
            auto role = (QPalette::ColorRole)mr.value(ri);
            QString roleKey = mr.valueToKey(role);
            QColor color = palette.color(group, role);
            stream << roleKey << "=" << color.alpha() << "," << color.red() << "," << color.green() << "," << color.blue() << "\n";
        }
        stream << "\n";
    }

}

void QPaletteSerializer::serialize(const QPalette &palette, QIODevice *device)
{
    QTextStream stream(device);
    serialize(palette, stream);
}

void QPaletteSerializer::serialize(const QPalette &palette, QString fileName)
{
    QFile file(fileName);
    file.open(QFile::WriteOnly);
    serialize(palette, &file);
    file.close();
}

QPalette QPaletteSerializer::deserialize(QString fileName)
{
    QSettings s(fileName, QSettings::IniFormat);
    return deserialize(s);
}

QPalette QPaletteSerializer::deserialize(QSettings &settings)
{
    QPalette p;
    auto mg = QMetaEnum::fromType<QPalette::ColorGroup>();
    auto mr = QMetaEnum::fromType<QPalette::ColorRole>();
    foreach(QString groupKey, settings.childGroups())
    {
        auto group = (QPalette::ColorGroup)mg.keyToValue(groupKey.toUtf8().data());
        settings.beginGroup(groupKey);
        foreach(QString roleKey, settings.childKeys())
        {
            auto role = (QPalette::ColorRole)mr.keyToValue(roleKey.toUtf8().data());
            auto colors = settings.value(roleKey, "").toStringList();
            if(colors.length() == 4)
            {
                QColor color(colors[1].toInt(), colors[2].toInt(), colors[3].toInt(), colors[0].toInt());
                p.setColor(group, role, color);
            }
        }
        settings.endGroup();
    }
    return p;
}
