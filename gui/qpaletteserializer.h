#ifndef QPALETTESERIALIZER_H
#define QPALETTESERIALIZER_H

#include <QPalette>
#include <QFile>
#include <QTextStream>
#include <QIODevice>
#include <QVariant>
#include <QSettings>

namespace stereoscopic::gui
{
    class QPaletteSerializer
    {
        public:
            static void serialize(const QPalette &palette, QTextStream &stream);
            static void serialize(const QPalette &palette, QIODevice *device);
            static void serialize(const QPalette &palette, QString fileName);
            static QPalette deserialize(QString fileName);
            static QPalette deserialize(QSettings &settings);
    };
};
#endif // QPALETTESERIALIZER_H
