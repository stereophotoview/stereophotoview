#include "recentfolderslistmodel.h"
#include "settings.h"

using namespace stereoscopic::gui;

RecentFoldersListModel::RecentFoldersListModel(QObject *parent)
    : QAbstractItemModel(parent)
{
    m_list = Settings::instance().recentFolders();
}

QVariant RecentFoldersListModel::data(const QModelIndex &index, int) const
{
    if (index.row() < 0 || index.row() > m_list.count())
            return QVariant();
    if(index.row() >= 0 && index.row() < m_list.length())
        return m_list[index.row()];
    return QVariant();
}

int RecentFoldersListModel::rowCount(const QModelIndex& parent) const
{
    if(!parent.isValid())
        return m_list.length();
    return 0;
}

QModelIndex RecentFoldersListModel::index(int row, int column, const QModelIndex &) const
{
    return QAbstractItemModel::createIndex(row, column, row);
}

QModelIndex RecentFoldersListModel::parent(const QModelIndex &) const
{
    return QAbstractItemModel::createIndex(-1, -1, -1);
}

int RecentFoldersListModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QHash<int, QByteArray> RecentFoldersListModel::roleNames() const
{
    QHash<int, QByteArray> r;
    r.insert(PathRole, "Path");
    return r;
}

void RecentFoldersListModel::add(QString path)
{
    beginResetModel();
    m_list = Settings::instance().addRecentFolder(path);
    endResetModel();
}

void RecentFoldersListModel::remove(QString path)
{
    beginResetModel();
    m_list = Settings::instance().removeRecentFolder(path);
    endResetModel();
}

void RecentFoldersListModel::clear()
{
    beginResetModel();
    Settings::instance().clearRecentFolders();
    m_list.clear();
    endResetModel();
}
