#ifndef RECENTFOLDERSLISTMODEL_H
#define RECENTFOLDERSLISTMODEL_H

#include <QAbstractItemModel>

namespace stereoscopic::gui
{
    class RecentFoldersListModel : public QAbstractItemModel
    {
            Q_OBJECT
        public:
            explicit RecentFoldersListModel(QObject *parent = nullptr);
            enum Roles
            {
                PathRole = Qt::UserRole + 1,
            };
            Q_ENUM(Roles)
            QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
            int rowCount(const QModelIndex &) const override;
            QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
            QModelIndex parent(const QModelIndex &index) const override;
            int columnCount(const QModelIndex &parent = QModelIndex()) const override;
            QHash<int,QByteArray> roleNames() const override;
        public slots:
            void add(QString path);
            void remove(QString path);
            void clear();
        private:
            QStringList m_list;
    };
};
#endif // RECENTFOLDERSLISTMODEL_H
