#include "settings.h"

using namespace stereoscopic::gui;

Settings::Settings(QObject *parent) : QObject(parent)
{

}

QStringList Settings::recentFolders()
{
    return readStringList("RecentFolders", "Path");
}

void Settings::setRecentFolders(QStringList folders)
{
    writeStringList(folders, "RecentFolders", "Path");
    emit recentFoldersChanged(folders);
}

int Settings::recentFoldersLength()
{
    return settings_.value("Interface/RecentFoldersLength", 30).toInt();
}

void Settings::setRecentFoldersLength(int value)
{
    settings_.setValue("Interface/RecentFoldersLength", value);
    emit recentFoldersLengthChanged(value);
}

QStringList Settings::addRecentFolder(QString folder_path)
{
    QStringList folders = recentFolders();

#ifdef Q_OS_WIN
    QString folder_path_upper = folder_path.toUpper();
    for(int i = 0; i < folders.length(); i++)
    {
        if(folders[i].toUpper() == folder_path_upper)
        {
            folders.removeAt(i);
            i--;
        };
    }

#else
    folders.removeAll(folder_path);
#endif
    folders.insert(0, folder_path);
    int max_recent_folders = recentFoldersLength();
    while(folders.length() > max_recent_folders)
        folders.removeLast();

    setRecentFolders(folders);
    return folders;
}

QStringList Settings::removeRecentFolder(QString folderPath)
{
    QStringList folders = recentFolders();

#ifdef Q_OS_WIN
    QString folderPath_upper = folderPath.toUpper();
    for(int i = 0; i < folders.length(); i++)
    {
        if(folders[i].toUpper() == folderPath_upper)
        {
            folders.removeAt(i);
            i--;
        };
    }
#else
    folders.removeAll(folderPath);
#endif
    setRecentFolders(folders);
    return folders;
}

void Settings::clearRecentFolders()
{
    setRecentFolders(QStringList());
}

void Settings::copyAll(QString srcPath, QString dstPath)
{
    settings_.beginGroup(srcPath);
    QVariantMap map;
    foreach(QString key, settings_.allKeys())
        map.insert(key, settings_.value(key));
    settings_.endGroup();
    settings_.beginGroup(dstPath);
    foreach(QString key, map.keys())
        settings_.setValue(key, map[key]);
    settings_.endGroup();
}

void Settings::remove(QString path)
{
    settings_.remove(path);
}

QVariant Settings::value(QString key, const QVariant &defaultValue)
{
    return settings_.value(key, defaultValue);
}

void Settings::setValue(QString key, QVariant value)
{
    settings_.setValue(key, value);
}

QStringList Settings::readStringList(QString name, QString param)
{
    QStringList res;

    int size = settings_.beginReadArray(name);
    for(int i = 0; i < size; i++)
    {
        settings_.setArrayIndex(i);
        res.append(settings_.value(param).toString());
    }
    settings_.endArray();
    return res;
}

void Settings::writeStringList(QStringList list, QString name, QString param)
{
    settings_.beginWriteArray(name);
    settings_.remove("");
    for (int i = 0; i < list.size(); ++i) {
        settings_.setArrayIndex(i);
        settings_.setValue(param, list.at(i));
    }
    settings_.endArray();
}

StereoFormat *Settings::sourceFormat()
{
    m_sourceFormat = StereoFormat((StereoFormat::Layout)settings_.value("SourceFormat/Layout", StereoFormat::Monoscopic).toInt())
            .setLeftFirst(settings_.value("SourceFormat/LeftFirst", true).toBool());
    return &m_sourceFormat;
}

void Settings::setSourceFormat(const StereoFormat *format)
{
    m_sourceFormat = *format;
    settings_.setValue("SourceFormat/Layout", format->layout());
    settings_.setValue("SourceFormat/LeftFirst", format->leftFirst());
    emit sourceFormatChanged();
}

const StereoFormat Settings::renderFormat()
{
    StereoFormat::Layout layout = (StereoFormat::Layout)settings_.value("Interface/Layout", StereoFormat::RowInterlaced).toInt();
    bool leftFirst = settings_.value("Interface/LeftFirst", true).toBool();
    auto f = StereoFormat(layout).setLeftFirst(leftFirst);
    return f;
}

void Settings::setRenderFormat(const StereoFormat &format)
{
    settings_.setValue("Interface/Layout", format.layout());
    settings_.setValue("Interface/LeftFirst", format.leftFirst());
}

QString Settings::language()
{
    return settings_.value("Interface/Language", "").toString();
}

void Settings::setLanguage(QString language)
{
    settings_.setValue("Interface/Language", language);
    emit languageChanged(language);
}

QString Settings::colorTheme()
{
    return settings_.value("Interface/Theme", "").toString();
}

void Settings::setColorTheme(QString value)
{
    settings_.setValue("Interface/Theme", value);
    emit colorThemeChanged(value);
}

bool Settings::autoAlignParallax()
{
    return settings_.value("AutoAlign/Parallax", true).toBool();
}

void Settings::setAutoAlignParallax(bool value)
{
    settings_.setValue("AutoAlign/Parallax", value);
    emit autoAlignParallaxChanged(value);
}

bool Settings::autoAlignVOffset()
{
    return settings_.value("AutoAlign/VOffset", true).toBool();
}

void Settings::setAutoAlignVOffset(bool value)
{
    settings_.setValue("AutoAlign/VOffset", value);
    emit autoAlignVOffsetChanged(value);
}

bool Settings::autoAlignRotate()
{
    return settings_.value("AutoAlign/Rotate", false).toBool();
}

void Settings::setAutoAlignRotate(bool value)
{
    settings_.setValue("AutoAlign/Rotate", value);
    emit autoAlignRotateChanged(value);
}

int Settings::autoAlignQuality()
{
    return settings_.value("AutoAlign/Quality", 50).toInt();
}

void Settings::setAutoAlignQuality(int value)
{
    settings_.setValue("AutoAlign/Quality", value);
    emit autoAlignQualityChanged(value);
}

QStringList Settings::favorites()
{
    return readStringList("Favorites", "Path");
}

void Settings::setFavorites(QStringList favorites)
{
    writeStringList(favorites, "Favorites", "Path");
}

bool Settings::dontUseNativeDialog()
{
#ifdef Q_OS_WIN32
    bool def = false;
#else
    bool def = true;
#endif
    return settings_.value("Dialogs/dontUseNativeDialog", def).toBool();
}

void Settings::setDontUseNativeDialog(bool value)
{
    settings_.setValue("Dialogs/dontUseNativeDialog", value);
    emit dontUseNativeDialogChanged();
}
