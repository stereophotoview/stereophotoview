#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QSet>
#include <QSettings>
#include <QDebug>
#include <QFileInfo>
#include <QStringList>
#include "fileFormats/video/videooptions.h"

#include "stereoformat.h"

using namespace stereoscopic;
using namespace stereoscopic::fileFormats::video;

namespace stereoscopic::gui
{
    class Settings : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QStringList recentFolders READ recentFolders WRITE setRecentFolders NOTIFY recentFoldersChanged)
            Q_PROPERTY(int recentFoldersLength READ recentFoldersLength WRITE setRecentFoldersLength NOTIFY recentFoldersLengthChanged)
            Q_PROPERTY(QString language READ language WRITE setLanguage NOTIFY languageChanged)
            Q_PROPERTY(QStringList languages READ languages)
            Q_PROPERTY(QString colorTheme READ colorTheme WRITE setColorTheme NOTIFY colorThemeChanged)
            Q_PROPERTY(bool autoAlignParallax READ autoAlignParallax WRITE setAutoAlignParallax NOTIFY autoAlignParallaxChanged)
            Q_PROPERTY(bool autoAlignVOffset READ autoAlignVOffset WRITE setAutoAlignVOffset NOTIFY autoAlignVOffsetChanged)
            Q_PROPERTY(bool autoAlignRotate READ autoAlignRotate WRITE setAutoAlignRotate NOTIFY autoAlignRotateChanged)
            Q_PROPERTY(int autoAlignQuality READ autoAlignQuality WRITE setAutoAlignQuality NOTIFY autoAlignQualityChanged)
            Q_PROPERTY(StereoFormat* sourceFormat READ sourceFormat WRITE setSourceFormat NOTIFY sourceFormatChanged)
            Q_PROPERTY(QStringList favorites READ favorites WRITE setFavorites)
            Q_PROPERTY(bool dontUseNativeDialog READ dontUseNativeDialog WRITE setDontUseNativeDialog)

        public:
            explicit Settings(QObject *parent = nullptr);
            QStringList recentFolders();
            void setRecentFolders(QStringList);

            int recentFoldersLength();
            void setRecentFoldersLength(int);

            StereoFormat* sourceFormat();
            void setSourceFormat(const StereoFormat *);

            const StereoFormat renderFormat();
            void setRenderFormat(const StereoFormat &);

            bool sourceLeftFirst();
            void setSourceLeftFirst(bool);

            bool renderLeftFirst();
            void setRenderLeftFirst(bool);

            QString language();
            void setLanguage(QString language);

            QStringList languages(){ return languages_; }

            QString colorTheme();
            void setColorTheme(QString value);

            bool autoAlignParallax();
            void setAutoAlignParallax(bool value);

            bool autoAlignVOffset();
            void setAutoAlignVOffset(bool value);

            bool autoAlignRotate();
            void setAutoAlignRotate(bool value);

            int autoAlignQuality();
            void setAutoAlignQuality(int value);

            QStringList favorites();
            void setFavorites(QStringList);

            bool dontUseNativeDialog();
            void setDontUseNativeDialog(bool);

        signals:

            void recentFoldersChanged(QStringList);
            void recentFoldersLengthChanged(int);
            void languageChanged(QString);
            void colorThemeChanged(QString);
            void autoAlignParallaxChanged(bool);
            void autoAlignVOffsetChanged(bool);
            void autoAlignRotateChanged(bool);
            void autoAlignQualityChanged(int);
            void sourceFormatChanged();
            void dontUseNativeDialogChanged();

        public slots:

            QStringList addRecentFolder(QString folder_path);
            QStringList removeRecentFolder(QString folderPath);
            void clearRecentFolders();
            /**
             * @brief Копирует значения всех ключей из папки источника в папку назначения
             * @param srcPath Путь к исходной папке настроек
             * @param dstPath Путь к папке назначения
             */
            void copyAll(QString srcPath, QString dstPath);

            /**
             * @brief Удаляет ключ настройки
             * @param path Путь
             */
            void remove(QString path);

            QVariant value(QString key, const QVariant &defaultValue = QVariant());
            void setValue(QString key, QVariant value);

            static Settings& instance()
            {
                static Settings instance;
                return instance;
            }

        private:
            QSettings settings_;
            QStringList languages_ = { "ru", "en" };
            StereoFormat m_sourceFormat;
            QStringList readStringList(QString name, QString param);
            void writeStringList(QStringList list, QString name, QString param);
    };
};
#endif // SETTINGS_H
