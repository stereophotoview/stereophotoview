#include "standardpaths.h"

#include <QStandardPaths>

using namespace stereoscopic::gui;

StandardPaths::StandardPaths(QObject *parent) : QObject(parent)
{

}

QString StandardPaths::pictures()
{
    return QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).first();
}

QString StandardPaths::desktop()
{
    return QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).first();
}

QString StandardPaths::documents()
{
    return QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first();
}

QString StandardPaths::movies()
{
    return QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).first();
}

QString StandardPaths::home()
{
    return QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first();
}

QString StandardPaths::download()
{
    return QStandardPaths::standardLocations(QStandardPaths::DownloadLocation).first();
}
