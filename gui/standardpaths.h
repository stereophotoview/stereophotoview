#ifndef STANDARDPATHS_H
#define STANDARDPATHS_H

#include <QObject>
namespace stereoscopic::gui
{
    class StandardPaths : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QString pictures READ pictures NOTIFY picturesChanged)

        public:
            explicit StandardPaths(QObject *parent = nullptr);
            QString pictures();
            QString desktop();
            QString documents();
            QString movies();
            QString home();
            QString download();

        signals:
            void picturesChanged(QString);

        public slots:
    };
}
#endif // STANDARDPATHS_H
