#include "stereoimage.h"
#include <QDebug>
#include <QScreen>
#include <QQuickWindow>
#include <QImage>
#include <QPainter>
#include <QQuickItem>
#include <QDateTime>
#include <math.h>
#include <QtConcurrent/QtConcurrent>

#if DEBUG
#include <QElapsedTimer>
#endif

#include "imagehelper.h"
#include "stereocoders/coderfactory.h"
#include "../lib/mathhelper.h"

using namespace stereoscopic::coders;
using namespace stereoscopic::math;
using namespace stereoscopic::gui;

StereoImage::StereoImage(QQuickItem *parent)
    :QQuickPaintedItem(parent)
{
    m_renderFormat = Settings::instance().renderFormat();
    connect(this, SIGNAL(windowChanged(QQuickWindow*)), this, SLOT(handleWindowChanged(QQuickWindow*)));
    connect(this, SIGNAL(widthChanged()), this, SLOT(handleSizeChanged()));
    connect(this, SIGNAL(heightChanged()), this, SLOT(handleSizeChanged()));
    connect(this, SIGNAL(yChanged()), this, SLOT(handleYChanged()));
    connect(this, SIGNAL(xChanged()), this, SLOT(handleXChanged()));
    connect(&m_renderFormat, SIGNAL(layoutChanged(Layout)), this, SLOT(handleRenderFormatChanged()));
    connect(&m_renderFormat, SIGNAL(leftFirstChanged(bool)), this, SLOT(handleRenderFormatChanged()));
}

QRect StereoImage::viewRect()
{
    auto s = source();
    auto frameSize = s == nullptr ? QSize() : s->displayFrame()->size();
    QSize boundingSize((int)boundingRect().width(), (int)boundingRect().height());
    if(frameSize.isEmpty() || boundingSize.isEmpty())
        return QRect();
    QRect size = ImageHelper::getEnteredImageRect(frameSize, boundingSize);
    return size;
}

void StereoImage::handleWindowChanged(QQuickWindow *window)
{
    if(oldWindow)
        disconnect(oldWindow, SIGNAL(yChanged(int)), this, SLOT(handleYChanged()));
    if(window)
        connect(window, SIGNAL(yChanged(int)), this, SLOT(handleYChanged()));
    if(window)
        connect(window, SIGNAL(xChanged(int)), this, SLOT(handleXChanged()));
    oldWindow = window;
    updateGlobalY();
    updateGlobalX();
}

void StereoImage::handleYChanged()
{
    updateGlobalY();
    if(m_renderFormat.layout() == StereoFormat::RowInterlaced)
    {
        update();
    }
}

void StereoImage::handleXChanged()
{
    updateGlobalX();
    if(m_renderFormat.layout() == StereoFormat::ColumnInterlaced)
    {
        update();
    }
}

void StereoImage::handleSizeChanged()
{
    QSize size = boundingRect().size().toSize();
    if(size != oldSize_)
    {
        emit viewSizeChanged(size);
        emit viewRectChanged(viewRect());
        oldSize_ = size;
        clearViewSizeImageCache();
    }
}

void StereoImage::handleSourceChanged()
{
    if(!m_freeze)
    {
        clearViewSizeImageCache();
        clearLayoutImageCache();
        update();
    }
}

void StereoImage::handleRenderFormatChanged()
{
    Settings::instance().setRenderFormat(m_renderFormat);
    clearViewSizeImageCache();
    clearLayoutImageCache();
    update();
}

StereoFrame &StereoImage::viewSizeFrame()
{
    auto s = source();
    if(s)
    {
        if(s->isVideo())
            return *(s->displayFrame());
        if(m_viewSizeFrame_cache.isNull())
            m_viewSizeFrame_cache = s->displayFrame()->fitted(viewSize());
    }
    return m_viewSizeFrame_cache;
}

StereoFrame &StereoImage::viewFrame()
{
    if(equal(m_parallax, 0) && equal(m_vOffset, 0) && equal(m_leftAngle, 0) && equal(m_rightAngle, 0))
        return viewSizeFrame();
    if(m_viewFrame_cache.isNull())
    {
        m_viewFrame_cache = viewSizeFrame()
                .rotated(m_leftAngle, m_rightAngle)
                .aligned(QPointF(m_parallax, m_vOffset));
    }
    return m_viewFrame_cache;
}

void StereoImage::paint(QPainter *painter)
{
    if(m_freeze || !source()) return;
    QRect rect(m_globalX, m_globalY, (int)boundingRect().width(), (int)boundingRect().height());
#if DEBUG
    QElapsedTimer timer;
    timer.start();
#endif
    auto coder = CoderFactory::create(&m_renderFormat, &viewFrame());
    if(coder.get() != nullptr)
        coder.get()->draw(painter, rect);
#if DEBUG
    //qDebug() << "drow: " << timer.elapsed() << "ms";
#endif
}

void StereoImage::setLeftAngle(double value)
{
    if(fabs(value - m_leftAngle) > 0.00001)
    {
        clearLayoutImageCache();
        m_leftAngle = value;
        emit leftAngleChanged(m_leftAngle);
        update();
    }
}

void StereoImage::setRightAngle(double value)
{
    if(fabs(value - m_rightAngle) > 0.00001)
    {
        clearLayoutImageCache();
        m_rightAngle = value;
        emit rightAngleChanged(m_rightAngle);
        update();
    }
}

QRect StereoImage::translateToSource(QRect rect)
{
    auto coder = CoderFactory::create(&m_renderFormat, m_source->displayFrame());
    auto bs = boundingRect().size();
    QSize size((int)bs.width(), (int)bs.height());
    return coder.get()->translateToSource(rect, size);
}

void StereoImage::setParallax(double value)
{
    if(fabs(value - m_parallax) > 0.00001)
    {
        m_parallax = value;
        clearLayoutImageCache();
        emit parallaxChanged(value);
        update();
    }
}

void StereoImage::setVOffset(double value)
{
    if(fabs(m_vOffset - value) > 0.00001)
    {
        m_vOffset = value;
        clearLayoutImageCache();
        emit vOffsetChanged(value);
        update();
    }
}

void StereoImage::clearViewSizeImageCache()
{
    m_viewFrame_cache = StereoFrame();
    m_viewSizeFrame_cache = StereoFrame();
    clearLayoutImageCache();
}

void StereoImage::clearLayoutImageCache()
{
    m_viewFrame_cache = StereoFrame();
}

void StereoImage::updateGlobalY()
{
    m_globalY = (int)this->y();
    QQuickItem *item = this;
    while(item->parentItem())
    {
        item = item->parentItem();
        m_globalY += item->y();
    }
    m_globalY += item->window() ? item->window()->geometry().top() : 0;
}

void StereoImage::updateGlobalX()
{
    m_globalX = (int)this->x();
    QQuickItem *item = this;
    while(item->parentItem())
    {
        item = item->parentItem();
        m_globalX += item->x();
    }
    m_globalX += item->window() ? item->window()->geometry().left() : 0;
}
