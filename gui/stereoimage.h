#ifndef STEREOIMAGE_H
#define STEREOIMAGE_H

#include "stereoimagesource.h"

#include <QtQuick/QQuickPaintedItem>
#include <QQuickWindow>
#include "settings.h"

namespace stereoscopic::gui
{
    // Визуальный элемент для отображения стерео изображения
    class StereoImage : public QQuickPaintedItem
    {
            Q_OBJECT
            Q_PROPERTY(StereoImageSource* source READ source WRITE setSource NOTIFY sourceChanged)
            Q_PROPERTY(StereoFormat* renderFormat READ renderFormat NOTIFY renderFormatChanged)
            Q_PROPERTY(bool freeze READ freeze WRITE setFreeze NOTIFY freezeChanged)
            Q_PROPERTY(QSize viewSize READ viewSize NOTIFY viewSizeChanged)
            Q_PROPERTY(QRect viewRect READ viewRect NOTIFY viewRectChanged)
            Q_PROPERTY(double parallax READ parallax WRITE setParallax NOTIFY parallaxChanged)
            Q_PROPERTY(double vOffset READ vOffset WRITE setVOffset NOTIFY vOffsetChanged)
            Q_PROPERTY(double leftAngle READ leftAngle WRITE setLeftAngle NOTIFY leftAngleChanged)
            Q_PROPERTY(double rightAngle READ rightAngle WRITE setRightAngle NOTIFY rightAngleChanged)

        public:

            StereoImage(QQuickItem *parent = nullptr);

            StereoImageSource *source(){ return m_source; }

            void setSource(StereoImageSource *value)
            {
                if(m_source) disconnect(m_source, SIGNAL(changed()), this, SLOT(handleSourceChanged()));
                m_source = value;
                if(m_source) connect(m_source, SIGNAL(changed()), this, SLOT(handleSourceChanged()));
                handleSourceChanged();
                emit sourceChanged();
            }

            StereoFormat* renderFormat(){ return &m_renderFormat; }

            bool freeze(){ return m_freeze; }

            void setFreeze(bool value){
                m_freeze = value;
                clearViewSizeImageCache();
                clearLayoutImageCache();
                emit freezeChanged(value);
                if(!value)
                    emit viewSizeChanged(viewSize());
                update();
            }

            QSize viewSize(){
                return boundingRect().size().toSize();
            }

            // Прямоугольник с изображением в окне просмотра
            QRect viewRect();

            double parallax(){ return m_parallax; }
            void setParallax(double value);

            double vOffset(){ return m_vOffset; }
            void setVOffset(double value);

            void paint(QPainter *painter);

            double leftAngle(){ return m_leftAngle; }
            void setLeftAngle(double value);

            double rightAngle(){ return m_rightAngle; }
            void setRightAngle(double value);
            Q_INVOKABLE QRect translateToSource(QRect rect);

        public slots:
            void handleWindowChanged(QQuickWindow* window);
            void handleYChanged();
            void handleXChanged();
            void handleSizeChanged();
            void handleSourceChanged();

        private slots:
            void handleRenderFormatChanged();

        signals:
            void sourceChanged();
            void renderFormatChanged(StereoFormat*);
            void freezeChanged(bool);
            void viewSizeChanged(QSize);
            void viewRectChanged(QRect);
            void parallaxChanged(double value);
            void vOffsetChanged(double value);
            void leftAngleChanged(double);
            void rightAngleChanged(double);

        private:
            StereoImageSource *m_source = nullptr;
            StereoFormat m_renderFormat;
            QQuickWindow *oldWindow = nullptr;
            bool m_freeze = false;
            QSize oldSize_;
            double m_parallax = 0;
            double m_vOffset = 0;
            int m_globalY = 0;
            int m_globalX = 0;
            double m_leftAngle = 0;
            double m_rightAngle = 0;

            // Кадр, уменьшенный до размеров окна
            StereoFrame &viewSizeFrame();
            StereoFrame m_viewSizeFrame_cache;


            // Кадр, уменьшенный до размеров окна с применёнными горизонтальным и вертикальным сдвигами
            StereoFrame &viewFrame();
            StereoFrame m_viewFrame_cache;

            void clearViewSizeImageCache();
            void clearLayoutImageCache();
            void updateGlobalY();
            void updateGlobalX();

            QImage resImage;
    };
};
#endif // STEREOIMAGE_H
