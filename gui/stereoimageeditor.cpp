#include "stereoimageeditor.h"

#include "commands.h"

using namespace stereoscopic::gui;
using namespace stereoscopic::gui::commands;

void StereoImageEditor::crop(QRect rect, int borderWidth)
{
    if(!canModify()) return;
    push(new Crop(this, rect, borderWidth));
}

void StereoImageEditor::applyAlign(double parallax, double vOffset, double leftAngle, double rightAngle)
{
    if(!canModify()) return;
    push(new ApplyAlign(this, parallax, vOffset, leftAngle, rightAngle));
}

void StereoImageEditor::colorAutoAdjust(bool toRight, int type)
{
    if(!canModify()) return;
    push(new AutoColorAdjust(this, toRight, type));
}

void StereoImageEditor::setLeftSource(QImage *pixmap)
{
    if(!canModify()) return;
    push(new SetView(this, pixmap, nullptr));
}

void StereoImageEditor::setRightSource(QImage *pixmap)
{
    if(!canModify()) return;
    push(new SetView(this, nullptr, pixmap));
}

stereoscopic::gui::ExternalEditorWatcher *StereoImageEditor::createLeftExternalEditorWatcher(QString command, QString name)
{
    QImage image = source()->displayFrame()->leftFullView();
    auto w = createExternalEditorWatcher(&image, command, name);
    connect(w, SIGNAL(import(QImage*)), this, SLOT(setLeftSource(QImage*)));
    return w;
}

stereoscopic::gui::ExternalEditorWatcher *StereoImageEditor::createRightExternalEditorWatcher(QString command, QString name)
{
    QImage image = source()->displayFrame()->rightFullView();
    auto w = createExternalEditorWatcher(&image, command, name);
    connect(w, SIGNAL(import(QImage*)), this, SLOT(setRightSource(QImage*)));
    return w;
}

ExternalEditorWatcher *StereoImageEditor::createExternalEditorWatcher(QImage *pm, QString command, QString name)
{
    return new ExternalEditorWatcher(pm, command, name, this);
}

bool StereoImageEditor::canModify()
{
    return m_source && !m_source->displayFrame()->isNull();
}
