#ifndef MODEL_H
#define MODEL_H

#include "baseeditor.h"
#include "stereoimagesource.h"
#include "stereoimagesource.h"
#include "externaleditorwatcher.h"

using namespace stereoscopic;
namespace stereoscopic::gui
{
    class StereoImageEditor : public BaseEditor
    {
            Q_OBJECT
            Q_PROPERTY(StereoImageSource* source READ source WRITE setSource NOTIFY sourceChanged)

        public:
            StereoImageEditor(QObject *parent = nullptr)
                : BaseEditor(20, parent)
            {
            }

            StereoImageSource* source()
            {
                return m_source;
            }

            void setSource(StereoImageSource* source)
            {
                clear();
                m_source = source;
                emit sourceChanged(source);
            }

            void applySource(StereoImageSource* source)
            {
                m_source = source;
                emit sourceChanged(source);
            }

        public slots:
            void crop(QRect rect, int borderWidth);
            void applyAlign(double parallax, double vOffset, double leftAngle, double rightAngle);
            void colorAutoAdjust(bool toRight, int type);
            void setLeftSource(QImage *pixmap);
            void setRightSource(QImage *pixmap);

            stereoscopic::gui::ExternalEditorWatcher* createLeftExternalEditorWatcher(QString command, QString name);
            stereoscopic::gui::ExternalEditorWatcher* createRightExternalEditorWatcher(QString command, QString name);

        signals:
            void sourceChanged(StereoImageSource*);

        private:
            StereoImageSource* m_source = nullptr;

            ExternalEditorWatcher* createExternalEditorWatcher(QImage *pm, QString command, QString name);

        private slots:

        protected:
            virtual bool canModify();

    };
}
#endif // MODEL_H
