#ifndef STEREOIMAGEFORMAT_H
#define STEREOIMAGEFORMAT_H

#include <QObject>
#include <stereoimagesource.h>

class StereoImageFormat : public QObject
{
        Q_OBJECT
    public:
        explicit StereoImageFormat(QObject *parent = 0);

        virtual StereoImageSource* load() = 0;
        virtual void save(StereoImageSource*) = 0;

    signals:

    public slots:
};

#endif // STEREOIMAGEFORMAT_H
