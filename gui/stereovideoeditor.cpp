#include "stereovideoeditor.h"

using namespace stereoscopic::gui;
using namespace stereoscopic::gui::video_commands;

StereoVideoEditor::StereoVideoEditor(QObject *parent)
    : BaseEditor(1000, parent)
{
}

void StereoVideoEditor::crop(QRect rect, int borderWidthPercents)
{
    if(!canModify()) return;
    push(new Crop(m_source, rect, borderWidthPercents));
}

void StereoVideoEditor::applyAlign(double parallax, double vOffset, double leftAngle, double rightAngle)
{
    if(!canModify()) return;
    push(new ApplyAlign(m_source, QPointF(parallax, vOffset), leftAngle, rightAngle));
}

void StereoVideoEditor::addTimestamp()
{
    if(!canModify()) return;
    push(new AddTimestamp(m_source));
}

void StereoVideoEditor::removeTimestamp()
{
    if(!canModify()) return;
    push(new RemoveTimestamp(m_source));
}

void StereoVideoEditor::colorAutoAdjust(bool toRight, bool active)
{
    if(!canModify()) return;
    auto activeValue = toRight ? AlignParams::ColorAdjustment::LeftByRight : AlignParams::ColorAdjustment::RightByLeft;
    auto value = active ? activeValue : AlignParams::ColorAdjustment::None;
    push(new ApplyAlign(m_source, value));
}

bool StereoVideoEditor::canModify()
{
    return m_source != nullptr;
}
