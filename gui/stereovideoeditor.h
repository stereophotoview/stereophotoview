#ifndef STEREOVIDEOEDITOR_H
#define STEREOVIDEOEDITOR_H


#include "baseeditor.h"
#include "stereovideosource.h"
#include "videocommands.h"

namespace stereoscopic::gui
{
    class StereoVideoEditor : public BaseEditor
    {
            Q_OBJECT

            Q_PROPERTY(StereoVideoSource* source READ source WRITE setSource NOTIFY sourceChanged)

        public:
            explicit StereoVideoEditor(QObject *parent = nullptr);

            StereoVideoSource* source(){ return m_source; }
            void setSource(StereoVideoSource* value)
            {
                clear();
                m_source = value;
                emit sourceChanged(m_source);
            }

        public slots:
            void crop(QRect rect, int borderWidthPercents);
            void applyAlign(double parallax, double vOffset, double leftAngle, double rightAngle);
            void addTimestamp();
            void removeTimestamp();
            void colorAutoAdjust(bool toRight, bool active);

        signals:
            void sourceChanged(StereoVideoSource*);

        protected:
            virtual bool canModify();

        private:
            StereoVideoSource* m_source = nullptr;

    };
};
#endif // STEREOVIDEOEDITOR_H
