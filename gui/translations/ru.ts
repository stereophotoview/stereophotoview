<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="7"/>
        <source>About %1</source>
        <translation>О программе %1</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="29"/>
        <source>Version %1</source>
        <translation>Версия %1</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="31"/>
        <source>A viewer/editor for stereoscopic photo and video</source>
        <translation>Программа для просмотра и редактирования стереоскопических фотографий и видео</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="32"/>
        <source>Alexander Mamzikov</source>
        <translation>Александр Мамзиков</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="35"/>
        <source>StereoPhotoView is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>StereoPhotoView распространяется в надежде, что он будет полезен, но без каких-либо гарантий; даже без подразумеваемой гарантии коммерческой ценности или пригодности для определенной цели.</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="36"/>
        <source>See the %1 for more details.</source>
        <translation>Дополнительные сведения смотрите в %1.</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="39"/>
        <source>Used projects</source>
        <translation>Используемые проекты</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="47"/>
        <source>Website</source>
        <translation>Сайт программы</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="47"/>
        <source>/en/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="48"/>
        <source>Bug tracker</source>
        <translation>Учёт ошибок</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="59"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>BatchProcessingModel</name>
    <message>
        <location filename="../qml/BatchProcessingModel.qml" line="13"/>
        <source>All supported files</source>
        <translation>Все поддерживаемые файлы</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingModel.qml" line="14"/>
        <source>MPO files</source>
        <translation>Файлы MPO</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingModel.qml" line="15"/>
        <source>Stereo pairs</source>
        <translation>Стерео пары</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingModel.qml" line="16"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingModel.qml" line="47"/>
        <source>Skip</source>
        <translation>Пропустить</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingModel.qml" line="48"/>
        <source>Overwrite</source>
        <translation>Заменить</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingModel.qml" line="225"/>
        <location filename="../qml/BatchProcessingModel.qml" line="236"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
</context>
<context>
    <name>BatchProcessingWindow</name>
    <message>
        <source>Batch Processing</source>
        <translation type="vanished">Пакетная обработка</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="81"/>
        <source>Success</source>
        <translation>Успешно</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="83"/>
        <source>Load error</source>
        <translation>Ошибка загрузки</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="85"/>
        <source>Omitted, the file exists</source>
        <translation>Пропущено, файл существует</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="87"/>
        <source>Unknown destination format</source>
        <translation>Неизвестный формат назначения</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="144"/>
        <source>Sources</source>
        <translation>Источники</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="148"/>
        <source>Second sources</source>
        <translation>Вторые источники</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="161"/>
        <source>Presets</source>
        <translation>Предустановки</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="182"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="188"/>
        <source>Save as…</source>
        <translation>Сохранить как…</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="192"/>
        <source>%1 copy</source>
        <translation>Копия %1</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="197"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Source format</source>
        <translation type="vanished">Формат источника</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="213"/>
        <location filename="../qml/BatchProcessingWindow.qml" line="424"/>
        <source>Layout</source>
        <translation>Компоновка кадров</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="247"/>
        <source>Actions</source>
        <translation>Действия</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="229"/>
        <location filename="../qml/BatchProcessingWindow.qml" line="440"/>
        <source>Reverse</source>
        <translation>Обратный порядок</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="14"/>
        <source>Stereo Photo Batch Processing</source>
        <translation>Пакетная обработка стерео фотографий</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="116"/>
        <source>Source</source>
        <translation>Источник</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="122"/>
        <source>Second source</source>
        <translation>Второй источник</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="128"/>
        <source>Destination</source>
        <translation>Назначение</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="134"/>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="208"/>
        <source>Default source format</source>
        <translation>Формат источника по умолчанию</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="218"/>
        <location filename="../qml/BatchProcessingWindow.qml" line="233"/>
        <source>If the format cannot be detected automatically, this format is used</source>
        <translation>Если формат не определён  автоматически, используется этот</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="255"/>
        <source>Auto align</source>
        <translation>Авто выравнивание</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="269"/>
        <source>Horizontal</source>
        <translation>Горизонтально</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="279"/>
        <source>Vertical</source>
        <translation>Вертикально</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="289"/>
        <source>Rotate</source>
        <translation>Вращение</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="299"/>
        <source>Accuracy</source>
        <translation>Точность</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="318"/>
        <source>Area of estimation</source>
        <translation>Область оценки</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="341"/>
        <source>Drag to move, mouse wheel to resize</source>
        <translation>Перетащите для перемещения, крутите колесо мыши для изменения размера</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="366"/>
        <source>Auto Color Adjustment</source>
        <translation>Автоматическая регулировка цвета</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="382"/>
        <source>Adjust left view</source>
        <translation>Подстроить левый ракурс</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="393"/>
        <source>Adjust right view</source>
        <translation>Подстроить правый ракурс</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="403"/>
        <source>Save options</source>
        <translation>Параметры сохранения</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="408"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="415"/>
        <source>File type</source>
        <translation>Тип файла</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="453"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="482"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="499"/>
        <source>Keep aspect ratio</source>
        <translation>Сохранить соотношение сторон</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="499"/>
        <source>enabled</source>
        <translation>включено</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="515"/>
        <source>Height</source>
        <translation>Высота</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="526"/>
        <source>Jpeg quality</source>
        <translation>Качество JPEG</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="554"/>
        <source>Location</source>
        <translation>Расположение</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="561"/>
        <source>Folder</source>
        <translation>Папка</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="578"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="582"/>
        <source>File name pattern</source>
        <translation>Шаблон имени файла</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="587"/>
        <source>The following variables are available</source>
        <translation>Доступны следующие переменные</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="588"/>
        <source>source file name without extension</source>
        <translation>имя исходного файла без расширения</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="589"/>
        <source>second source file name without extension</source>
        <translation>имя второго исходного файла без расширения</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="590"/>
        <source>order number</source>
        <translation>порядковый номер</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="599"/>
        <source>Existing Files</source>
        <translation>Существующие файлы</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="676"/>
        <source>Start</source>
        <translation>Запустить</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="681"/>
        <source>Stop</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="686"/>
        <source>Resume</source>
        <translation>Возобновить</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="686"/>
        <source>Pause</source>
        <translation>Приостановить</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="691"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="645"/>
        <location filename="../qml/BatchProcessingWindow.qml" line="696"/>
        <location filename="../qml/BatchProcessingWindow.qml" line="733"/>
        <source>Add Sources</source>
        <translation>Добавить источники</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="665"/>
        <location filename="../qml/BatchProcessingWindow.qml" line="701"/>
        <location filename="../qml/BatchProcessingWindow.qml" line="744"/>
        <source>Add second sources</source>
        <translation>Добавить вторые источники</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="706"/>
        <source>Remove selected sources</source>
        <translation>Удалить выбранные источники</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="712"/>
        <source>Remove selected second sources</source>
        <translation>Удалить вторые выбранные источники</translation>
    </message>
    <message>
        <location filename="../qml/BatchProcessingWindow.qml" line="717"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
</context>
<context>
    <name>BitrateEditor</name>
    <message>
        <location filename="../qml/BitrateEditor.qml" line="63"/>
        <source>KiB/s</source>
        <translation>КиБ/c</translation>
    </message>
</context>
<context>
    <name>ConsoleArgumentsParser</name>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="31"/>
        <source>A viewer/editor for stereoscopic photo and video.</source>
        <translation>Программа для просмотра и редактирования стереоскопических фотографий и видео.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="32"/>
        <source>/en</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="35"/>
        <source>Input file.</source>
        <translation>Входной файл.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="36"/>
        <source>Optional second input file to load a separate stereo pair.</source>
        <translation>Необязательный второй входной файл для загрузки раздельной стерео-пары.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="40"/>
        <source>Run in batch mode.</source>
        <translation>Запустить в пакетном режиме.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="44"/>
        <source>Input stereo layout (see --layouts) - optinal.</source>
        <translation>Компоновка стерео-пары во входном файле (см. --layouts) - не обязательный.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="48"/>
        <source>Right-eye view first in the input file - optional.</source>
        <translation>Указывает, что во входном файле сначала идёт ракурс для правого глаза - не обязательно.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="52"/>
        <source>Display a list of layouts.</source>
        <translation>Отобразить список возможных компоновок стерео-пар.</translation>
    </message>
    <message>
        <location filename="../consoleargumentsparser.cpp" line="61"/>
        <source>Input layouts</source>
        <translation>Расположения, доступные для входных стерео-пар</translation>
    </message>
</context>
<context>
    <name>CropEditor</name>
    <message>
        <location filename="../qml/CropEditor.qml" line="46"/>
        <source>Crop</source>
        <translation>Кадрировать</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="48"/>
        <location filename="../qml/CropEditor.qml" line="57"/>
        <source>%1 [%2]</source>
        <translation>%1 [%2]</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="55"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="64"/>
        <source>Free</source>
        <translation>Свободное</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="65"/>
        <source>Album 4:3</source>
        <translation>Альбомное 4:3</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="66"/>
        <source>Album 3:2</source>
        <translation>Альбомное 3:2</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="67"/>
        <source>Album 2:1</source>
        <translation>Альбомное 2:1</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="68"/>
        <source>Album 14:9</source>
        <translation>Альбомное 14:9</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="69"/>
        <source>Album 16:9</source>
        <translation>Альбомное 16:9</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="70"/>
        <source>Album 16:10</source>
        <translation>Альбомное 16:10</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="72"/>
        <source>Square 1:1</source>
        <translation>Квадратное 1:1</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="74"/>
        <source>Portrait 3:4</source>
        <translation>Портретное 3:4</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="75"/>
        <source>Portrait 2:3</source>
        <translation>Портретное 2:3</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="76"/>
        <source>Portrait 1:2</source>
        <translation>Портретное 1:2</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="77"/>
        <source>Portrait 9:14</source>
        <translation>Портретное 9:14</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="78"/>
        <source>Portrait 9:16</source>
        <translation>Портретное 9:16</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="79"/>
        <source>Portrait 10:16</source>
        <translation>Портретное 10:16</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="84"/>
        <source>Aspect ratio</source>
        <translation>Соотношение сторон</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="98"/>
        <source>Border</source>
        <translation>Рамка</translation>
    </message>
    <message>
        <location filename="../qml/CropEditor.qml" line="112"/>
        <source>%</source>
        <translation>%</translation>
    </message>
</context>
<context>
    <name>ExternalEditorDialog</name>
    <message>
        <location filename="../qml/ExternalEditorDialog.qml" line="26"/>
        <source>Before:</source>
        <translation>До:</translation>
    </message>
    <message>
        <location filename="../qml/ExternalEditorDialog.qml" line="33"/>
        <source>After:</source>
        <translation>После:</translation>
    </message>
    <message>
        <location filename="../qml/ExternalEditorDialog.qml" line="77"/>
        <source>Import</source>
        <translation>Импортировать</translation>
    </message>
    <message>
        <location filename="../qml/ExternalEditorDialog.qml" line="81"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
</context>
<context>
    <name>FavoritesModel</name>
    <message>
        <source>Recent folders</source>
        <translation type="vanished">Последние папки</translation>
    </message>
</context>
<context>
    <name>FileInfoDialog</name>
    <message>
        <location filename="../qml/FileInfoDialog.qml" line="15"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../qml/FileInfoDialog.qml" line="39"/>
        <source>Left image</source>
        <translation>Левое изображение</translation>
    </message>
    <message>
        <location filename="../qml/FileInfoDialog.qml" line="46"/>
        <source>Right image</source>
        <translation>Правое изображение</translation>
    </message>
    <message>
        <location filename="../qml/FileInfoDialog.qml" line="120"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>FolderViewer</name>
    <message>
        <location filename="../qml/FolderViewer.qml" line="44"/>
        <source>Recent folders</source>
        <translation>Последние папки</translation>
    </message>
    <message>
        <location filename="../qml/FolderViewer.qml" line="128"/>
        <source>Forget this folder</source>
        <translation>Забыть эту папку</translation>
    </message>
    <message>
        <location filename="../qml/FolderViewer.qml" line="133"/>
        <source>Forget all</source>
        <translation>Забыть все</translation>
    </message>
    <message>
        <location filename="../qml/FolderViewer.qml" line="139"/>
        <source>Add to favorites</source>
        <translation>Добавить в избранное</translation>
    </message>
</context>
<context>
    <name>FoldersTree</name>
    <message>
        <location filename="../qml/FoldersTree.qml" line="52"/>
        <source>Favorites</source>
        <translation>Избранное</translation>
    </message>
    <message>
        <location filename="../qml/FoldersTree.qml" line="105"/>
        <source>Delete from favorites</source>
        <translation>Удалить из избранного</translation>
    </message>
    <message>
        <location filename="../qml/FoldersTree.qml" line="119"/>
        <source>File system</source>
        <translation>Файловая система</translation>
    </message>
    <message>
        <location filename="../qml/FoldersTree.qml" line="198"/>
        <source>Add to favorites</source>
        <translation>Добавить в избранное</translation>
    </message>
</context>
<context>
    <name>FullScreenWindow</name>
    <message>
        <source>Windowed mode</source>
        <translation type="vanished">Оконный режим</translation>
    </message>
</context>
<context>
    <name>ImageFileSettings</name>
    <message>
        <location filename="../qml/ImageFileSettings.qml" line="26"/>
        <source>Save image file</source>
        <comment>File dialog title</comment>
        <translation>Сохранить изображение</translation>
    </message>
    <message>
        <location filename="../qml/ImageFileSettings.qml" line="35"/>
        <source>File path</source>
        <translation>Путь к файлу</translation>
    </message>
    <message>
        <location filename="../qml/ImageFileSettings.qml" line="66"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
    <message>
        <location filename="../qml/ImageFileSettings.qml" line="93"/>
        <source>Video bitrate</source>
        <translation>Видео битрейт</translation>
    </message>
    <message>
        <location filename="../qml/ImageFileSettings.qml" line="106"/>
        <source>Audio bitrate</source>
        <translation>Аудио битрейт</translation>
    </message>
</context>
<context>
    <name>LayoutEditor</name>
    <message>
        <location filename="../qml/LayoutEditor.qml" line="151"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../qml/LayoutEditor.qml" line="133"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <location filename="../qml/LayoutEditor.qml" line="98"/>
        <location filename="../qml/LayoutEditor.qml" line="107"/>
        <source>Decrease [%1]</source>
        <translation>Уменьшить [%1]</translation>
    </message>
    <message>
        <location filename="../qml/LayoutEditor.qml" line="116"/>
        <location filename="../qml/LayoutEditor.qml" line="125"/>
        <source>Increase [%1]</source>
        <translation>Увеличить [%1]</translation>
    </message>
    <message>
        <location filename="../qml/LayoutEditor.qml" line="140"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../qml/LayoutEditor.qml" line="142"/>
        <location filename="../qml/LayoutEditor.qml" line="154"/>
        <source>%1 [%2]</source>
        <translation>%1 [%2]</translation>
    </message>
</context>
<context>
    <name>Model</name>
    <message>
        <location filename="../qml/Model.qml" line="16"/>
        <location filename="../qml/Model.qml" line="22"/>
        <location filename="../qml/Model.qml" line="27"/>
        <source>All supported files</source>
        <translation>Все поддерживаемые файлы</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="17"/>
        <location filename="../qml/Model.qml" line="23"/>
        <location filename="../qml/Model.qml" line="31"/>
        <source>MPO files</source>
        <translation>Файлы MPO</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="18"/>
        <location filename="../qml/Model.qml" line="24"/>
        <source>Stereo pairs</source>
        <translation>Стерео пары</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="19"/>
        <location filename="../qml/Model.qml" line="29"/>
        <source>Video files</source>
        <translation>Видео файлы</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="20"/>
        <location filename="../qml/Model.qml" line="25"/>
        <location filename="../qml/Model.qml" line="30"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="28"/>
        <location filename="../qml/Model.qml" line="33"/>
        <source>Images</source>
        <translation>Изображения</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="32"/>
        <source>JPS files</source>
        <translation>Файлы JPS</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="189"/>
        <source>Main</source>
        <translation>Главное</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="190"/>
        <source>Exif</source>
        <translation>Exif</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="191"/>
        <source>Original source type</source>
        <translation>Исходный формат источника</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="192"/>
        <source>File path</source>
        <translation>Путь к файлу</translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="386"/>
        <source>Open left image with </source>
        <translation>Открыть левый ракурс в </translation>
    </message>
    <message>
        <location filename="../qml/Model.qml" line="391"/>
        <source>Open right image with </source>
        <translation>Открыть правый ракурс в </translation>
    </message>
</context>
<context>
    <name>NewPresetWindow</name>
    <message>
        <location filename="../qml/NewPresetWindow.qml" line="20"/>
        <source>Save preset as…</source>
        <translation>Сохранить предустановки как…</translation>
    </message>
    <message>
        <location filename="../qml/NewPresetWindow.qml" line="32"/>
        <source>Name of the new Preset:</source>
        <translation>Название новых предустановок:</translation>
    </message>
    <message>
        <location filename="../qml/NewPresetWindow.qml" line="50"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../qml/NewPresetWindow.qml" line="60"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>NewVersionDialog</name>
    <message>
        <location filename="../qml/NewVersionDialog.qml" line="11"/>
        <source>Update Available</source>
        <translation>Доступно обновление</translation>
    </message>
    <message>
        <location filename="../qml/NewVersionDialog.qml" line="18"/>
        <source>/en/download.html</source>
        <translation>/download.html</translation>
    </message>
    <message>
        <location filename="../qml/NewVersionDialog.qml" line="49"/>
        <source>A new version of StereoPhotoView is now available for download.</source>
        <translation>Доступна новая версия программы &quot;Просмотр стереофотографий&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/NewVersionDialog.qml" line="55"/>
        <source>Current version</source>
        <translation>Текущая версия</translation>
    </message>
    <message>
        <location filename="../qml/NewVersionDialog.qml" line="57"/>
        <source>New version</source>
        <translation>Новая версия</translation>
    </message>
    <message>
        <location filename="../qml/NewVersionDialog.qml" line="63"/>
        <source>Skip this version</source>
        <translation>Пропустить эту версию</translation>
    </message>
    <message>
        <location filename="../qml/NewVersionDialog.qml" line="27"/>
        <source>Open Download Page</source>
        <translation>Открыть страницу загрузки</translation>
    </message>
    <message>
        <location filename="../qml/NewVersionDialog.qml" line="34"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>PlayerControl</name>
    <message>
        <location filename="../qml/PlayerControl.qml" line="34"/>
        <location filename="../qml/PlayerControl.qml" line="69"/>
        <source>Play</source>
        <translation>Воспроизведение</translation>
    </message>
</context>
<context>
    <name>PresetsControl</name>
    <message>
        <location filename="../qml/PresetsControl.qml" line="41"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Save as …</source>
        <translation type="vanished">Сохранить как…</translation>
    </message>
    <message>
        <location filename="../qml/PresetsControl.qml" line="57"/>
        <source>Save as…</source>
        <translation>Сохранить как…</translation>
    </message>
    <message>
        <location filename="../qml/PresetsControl.qml" line="60"/>
        <source>%1 copy</source>
        <translation>Копия %1</translation>
    </message>
    <message>
        <location filename="../qml/PresetsControl.qml" line="65"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>PresetsListModel</name>
    <message>
        <location filename="../presetslistmodel.cpp" line="324"/>
        <location filename="../presetslistmodel.cpp" line="332"/>
        <location filename="../presetslistmodel.cpp" line="340"/>
        <source>Default</source>
        <comment>Default preset name</comment>
        <translation>По умолчанию</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../presetslistmodel.h" line="143"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="144"/>
        <source>Auto lossless</source>
        <translation>Авто без потерь</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="145"/>
        <source>480p</source>
        <translation>480p</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="146"/>
        <source>576p</source>
        <translation>576p</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="147"/>
        <source>HD 720p</source>
        <translation>HD 720p</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="148"/>
        <source>Full HD 1080p</source>
        <translation>Full HD 1080p</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="149"/>
        <source>2K</source>
        <translation>2K</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="150"/>
        <source>4K UHD</source>
        <translation>4K UHD</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="151"/>
        <source>True 4K</source>
        <translation>True 4K</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="152"/>
        <source>8K UHD</source>
        <translation>8K UHD</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="153"/>
        <source>True 8K</source>
        <translation>True 8K</translation>
    </message>
    <message>
        <location filename="../presetslistmodel.h" line="154"/>
        <source>Manual</source>
        <translation>Вручную</translation>
    </message>
</context>
<context>
    <name>SaveImageWindow</name>
    <message>
        <location filename="../qml/SaveImageWindow.qml" line="41"/>
        <source>Preset</source>
        <translation>Предустановки</translation>
    </message>
    <message>
        <location filename="../qml/SaveImageWindow.qml" line="64"/>
        <source>JPEG quality:</source>
        <translation>Качество JPEG:</translation>
    </message>
    <message>
        <location filename="../qml/SaveImageWindow.qml" line="98"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../qml/SaveImageWindow.qml" line="108"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>SaveVideoWindow</name>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="10"/>
        <source>Save video as…</source>
        <translation>Сохранить видео как…</translation>
    </message>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="46"/>
        <source>Presets</source>
        <translation>Предустановки</translation>
    </message>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="64"/>
        <source>Stereo Settings</source>
        <translation>Настройки стерео</translation>
    </message>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="78"/>
        <source>Video Coding Settings</source>
        <translation>Настройки кодирования видео</translation>
    </message>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="111"/>
        <source>The output format &quot;%1&quot; is not yet supported.</source>
        <translation>Выходной формат &quot;%1&quot; пока не поддерживается.</translation>
    </message>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="138"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="156"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="101"/>
        <location filename="../qml/SaveVideoWindow.qml" line="110"/>
        <source>Saving video</source>
        <translation>Сохранение видео</translation>
    </message>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="102"/>
        <source>Can not save video to source file</source>
        <translation>Нельзя сохранить видео в исходный файл</translation>
    </message>
    <message>
        <location filename="../qml/SaveVideoWindow.qml" line="119"/>
        <source>Save video file</source>
        <comment>File dialog title</comment>
        <translation>Сохранить видео файл</translation>
    </message>
</context>
<context>
    <name>SettingsEditor</name>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="11"/>
        <location filename="../qml/SettingsEditor.qml" line="143"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="75"/>
        <source>Don&apos;t use native dialogs</source>
        <translation>Не использовать нативные диалоги</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="82"/>
        <source>Extended Auto Alignment</source>
        <translation>Расширенное авто-выравнивание</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="42"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="32"/>
        <source>User interface</source>
        <translation>Интерфейс пользователя</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="53"/>
        <source>Color theme:</source>
        <translation>Цветовая тема:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="64"/>
        <source>Number of recent folders:</source>
        <translation>Количество последних папок:</translation>
    </message>
    <message>
        <source>Show in full screen at startup</source>
        <translation type="vanished">Запускать в полноэкранном режиме</translation>
    </message>
    <message>
        <source>Check for new version at startup</source>
        <translation type="vanished">Проверять наличие новой версии при старте</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="90"/>
        <source>Accuracy:</source>
        <translation>Точность:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="110"/>
        <source>Parallax</source>
        <translation>Параллакс</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="115"/>
        <source>Vertical offset</source>
        <translation>Вертикальное смещение</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="120"/>
        <source>Rotate</source>
        <translation>Вращение</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="130"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="134"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="144"/>
        <source>The change will take effect the next time the application is started.</source>
        <translation>Изменения вступят в силу при следующем запуске приложения.</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="149"/>
        <source>System</source>
        <comment>Language</comment>
        <translation>Системный</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="150"/>
        <source>English</source>
        <comment>Language</comment>
        <translation>English (Английский)</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="151"/>
        <source>Русский (Russian)</source>
        <comment>Language</comment>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="156"/>
        <source>System</source>
        <comment>Color theme</comment>
        <translation>Системная</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="157"/>
        <source>Light</source>
        <comment>Color theme</comment>
        <translation>Светлая</translation>
    </message>
    <message>
        <location filename="../qml/SettingsEditor.qml" line="158"/>
        <source>Dark</source>
        <comment>Color theme</comment>
        <translation>Тёмная</translation>
    </message>
</context>
<context>
    <name>StereoOptions</name>
    <message>
        <source>Auto</source>
        <translation type="vanished">Авто</translation>
    </message>
    <message>
        <source>Auto max</source>
        <translation type="vanished">Авто макс</translation>
    </message>
    <message>
        <source>480p</source>
        <translation type="vanished">480p</translation>
    </message>
    <message>
        <source>576p</source>
        <translation type="vanished">576p</translation>
    </message>
    <message>
        <source>HD 720p</source>
        <translation type="vanished">HD 720p</translation>
    </message>
    <message>
        <source>Full HD 1080p</source>
        <translation type="vanished">Full HD 1080p</translation>
    </message>
    <message>
        <source>2K</source>
        <translation type="vanished">2K</translation>
    </message>
    <message>
        <source>4K UHD</source>
        <translation type="vanished">4K UHD</translation>
    </message>
    <message>
        <source>True 4K</source>
        <translation type="vanished">True 4K</translation>
    </message>
    <message>
        <source>8K UHD</source>
        <translation type="vanished">8K UHD</translation>
    </message>
    <message>
        <source>True 8K</source>
        <translation type="vanished">True 8K</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="vanished">Вручную</translation>
    </message>
    <message>
        <location filename="../qml/StereoOptions.qml" line="41"/>
        <source>Layout</source>
        <translation>Компоновка кадров</translation>
    </message>
    <message>
        <location filename="../qml/StereoOptions.qml" line="84"/>
        <source>Original (%1)</source>
        <translation>По умолчанию (%1)</translation>
    </message>
    <message>
        <location filename="../qml/StereoOptions.qml" line="108"/>
        <source>Reverse</source>
        <translation>Обратный порядок</translation>
    </message>
    <message>
        <location filename="../qml/StereoOptions.qml" line="124"/>
        <source>Resolution</source>
        <translation>Разрешение</translation>
    </message>
    <message>
        <location filename="../qml/StereoOptions.qml" line="145"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <location filename="../qml/StereoOptions.qml" line="158"/>
        <source>Keep aspect ratio</source>
        <translation>Сохранить соотношение сторон</translation>
    </message>
    <message>
        <location filename="../qml/StereoOptions.qml" line="158"/>
        <source>enabled</source>
        <translation>включено</translation>
    </message>
    <message>
        <location filename="../qml/StereoOptions.qml" line="170"/>
        <source>Height</source>
        <translation>Высота</translation>
    </message>
</context>
<context>
    <name>TaskQueueWindow</name>
    <message>
        <location filename="../qml/TaskQueueWindow.qml" line="10"/>
        <source>Active tasks: %1</source>
        <translation>Активные задачи: %1</translation>
    </message>
    <message>
        <location filename="../qml/TaskQueueWindow.qml" line="97"/>
        <source>Stop</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <location filename="../qml/TaskQueueWindow.qml" line="105"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>TimeShiftWindow</name>
    <message>
        <location filename="../qml/TimeShiftWindow.qml" line="7"/>
        <source>Time Shift</source>
        <translation>Сдвиг времени</translation>
    </message>
    <message>
        <location filename="../qml/TimeShiftWindow.qml" line="22"/>
        <source>Time shift of the second stream:</source>
        <translation>Сдвиг по времени второго потока:</translation>
    </message>
    <message>
        <location filename="../qml/TimeShiftWindow.qml" line="33"/>
        <source>seconds</source>
        <translation>секунд</translation>
    </message>
    <message>
        <source>frames</source>
        <translation type="vanished">кадров</translation>
    </message>
    <message>
        <location filename="../qml/TimeShiftWindow.qml" line="51"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../qml/TimeShiftWindow.qml" line="60"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
</context>
<context>
    <name>VerticalSliderEditor</name>
    <message>
        <location filename="../qml/VerticalSliderEditor.qml" line="31"/>
        <location filename="../qml/VerticalSliderEditor.qml" line="40"/>
        <source>Increase [%1]</source>
        <translation>Увеличить [%1]</translation>
    </message>
    <message>
        <location filename="../qml/VerticalSliderEditor.qml" line="49"/>
        <location filename="../qml/VerticalSliderEditor.qml" line="58"/>
        <source>Decrease [%1]</source>
        <translation>Уменьшить [%1]</translation>
    </message>
    <message>
        <location filename="../qml/VerticalSliderEditor.qml" line="66"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
</context>
<context>
    <name>VideoSettings</name>
    <message>
        <location filename="../qml/VideoSettings.qml" line="142"/>
        <source>Constant Rate Factor</source>
        <translation>Постоянный коэффициент (CRF)</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="71"/>
        <source>File format</source>
        <translation>Формат файла</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="98"/>
        <source>Video codec</source>
        <translation>Видео кодек</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="104"/>
        <source>Audio codec</source>
        <translation>Аудио кодек</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="110"/>
        <source>Stereo descriptor</source>
        <translation>Описание стерео-формата</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="113"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="113"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="121"/>
        <source>H264 Preset</source>
        <translation>Пресет H264</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="142"/>
        <source>Compression factor</source>
        <translation>Коэффициент сжатия</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="173"/>
        <source>High quality</source>
        <translation>Высокое качество</translation>
    </message>
    <message>
        <location filename="../qml/VideoSettings.qml" line="177"/>
        <source>Small size</source>
        <translation>Малый размер</translation>
    </message>
</context>
<context>
    <name>WorkSpace</name>
    <message>
        <location filename="../qml/WorkSpace.qml" line="61"/>
        <source>Save</source>
        <oldsource>&amp;Save</oldsource>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="142"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="45"/>
        <source>Open…</source>
        <translation>Открыть…</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="53"/>
        <source>Open separate files…</source>
        <translation>Открыть раздельные файлы…</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="80"/>
        <source>Save as copy...</source>
        <translation>Сохранить как копию…</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="94"/>
        <source>Save as copy…</source>
        <comment>Save as Video file action</comment>
        <translation>Сохранить как копию…</translation>
    </message>
    <message>
        <source>Save as JPS</source>
        <translation type="vanished">Сохранить как JPS</translation>
    </message>
    <message>
        <source>Save as MPO</source>
        <translation type="vanished">Сохранить как MPO</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="104"/>
        <source>Copy to…</source>
        <translation>Копировать в…</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="115"/>
        <source>Move to…</source>
        <translation>Переместить в…</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="124"/>
        <location filename="../qml/WorkSpace.qml" line="1076"/>
        <source>Move to Trash</source>
        <translation>Удалить в корзину</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="133"/>
        <source>Information…</source>
        <translation>Информация…</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="235"/>
        <source>Go to next frame</source>
        <translation>Следующий кадр</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="242"/>
        <source>Go to previous frame</source>
        <translation>Предыдущий кадр</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="249"/>
        <source>First</source>
        <translation>Первое</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="258"/>
        <source>Previous</source>
        <translation>Предыдущее</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="285"/>
        <source>Thumbnail bar</source>
        <translation>Панель миниатюр</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="301"/>
        <source>Use original</source>
        <translation>Исходный</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="447"/>
        <source>Time Shift</source>
        <translation>Сдвиг времени</translation>
    </message>
    <message>
        <source>frames</source>
        <translation type="vanished">кадров</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="485"/>
        <source>Auto Alignment</source>
        <translation>Авто-выравнивание</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="497"/>
        <source>Extended Auto Alignment</source>
        <translation>Расширенное авто-выравнивание</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="526"/>
        <source>Adjust Right View</source>
        <translatorcomment>Использовтаь левый ракурс в качетсве эталона</translatorcomment>
        <translation>Подстроить правый ракурс</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="511"/>
        <source>Adjust Left View</source>
        <translatorcomment>Использовать правый ракурс, как эталон</translatorcomment>
        <translation>Подстроить левый ракурс</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="447"/>
        <source>seconds</source>
        <translation>секунд</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="543"/>
        <source>Delete Timestamp</source>
        <translation>Удалить метку</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="543"/>
        <source>Add Timestamp</source>
        <translation>Добавить метку</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="556"/>
        <source>Alignment</source>
        <translation>Выравнивание</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="567"/>
        <source>Crop</source>
        <translation>Кадрировать</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="612"/>
        <source>Zoom to Fit</source>
        <translation>Вписать в окно</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="630"/>
        <source>Zoom In</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="641"/>
        <source>Zoom Out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="960"/>
        <source>Left angle rotation</source>
        <translation>Поворот левого ракурса</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="969"/>
        <source>Right angle rotation</source>
        <translation>Поворот правого ракурса</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="986"/>
        <source>Display format</source>
        <translation>Формат отображения</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="944"/>
        <source>Source format</source>
        <translation>Формат источника</translation>
    </message>
    <message>
        <source>First angle rotation</source>
        <translation type="vanished">Поворот первого ракурса</translation>
    </message>
    <message>
        <source>Second angle rotation</source>
        <translation type="vanished">Поворот второго ракурса</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1042"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1087"/>
        <source>Open file</source>
        <comment>File dialog title</comment>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1097"/>
        <source>Open left view</source>
        <comment>File dialog title</comment>
        <translation>Открыть левый ракурс</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1097"/>
        <source>Open right view</source>
        <comment>File dialog title</comment>
        <translation>Открыть правый ракурс</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1148"/>
        <source>Save image</source>
        <comment>File dialog title</comment>
        <translation>Сохранить изображение</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1303"/>
        <source>Saving video</source>
        <translation>Сохранение видео</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1304"/>
        <source>Can not save video to source file</source>
        <translation>Нельзя сохранить видео в исходный файл</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1347"/>
        <source>Copy to</source>
        <translation>Копировать в</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1349"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="vanished">Сохранение</translation>
    </message>
    <message>
        <source>Replace file &apos;%1&apos;?</source>
        <translation type="vanished">Заменить файл &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1052"/>
        <source>Saving changes</source>
        <translation>Сохранение изменений</translation>
    </message>
    <message>
        <source>Please save image manually (%1 -&gt; %2)</source>
        <translation type="vanished">Пожалуйста, сохраните изображение вручную  (%1 -&gt; %2)</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1019"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="80"/>
        <source>Save freeze frame as...</source>
        <translation>Сохранить стоп-кадр как…</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="70"/>
        <source>Save as...</source>
        <translation>Сохранить как…</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="223"/>
        <location filename="../qml/WorkSpace.qml" line="391"/>
        <source>Reverse</source>
        <translation>Обратный порядок</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="267"/>
        <source>Next</source>
        <translation>Следующее</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="276"/>
        <source>Last</source>
        <translation>Последнее</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1043"/>
        <source>Unable to load file:
%1</source>
        <translation>Невозможно загрузить файл:
%1</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">Сохранить как</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="589"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="598"/>
        <source>Redo</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="618"/>
        <source>Original Size</source>
        <translation>Реальный размер</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1012"/>
        <source>Auto Color Adjustment</source>
        <translation>Автоматическая регулировка цвета</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1053"/>
        <source>The current file was modified</source>
        <translation>Текущий файл был изменен</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1077"/>
        <source>Move file &apos;%1&apos; to trash?</source>
        <translation>Удалить файл &apos;%1&apos; в корзину?</translation>
    </message>
    <message>
        <source>Open left image</source>
        <comment>File dialog title</comment>
        <translation type="vanished">Открыть левое изображение</translation>
    </message>
    <message>
        <source>Open right image</source>
        <comment>File dialog title</comment>
        <translation type="vanished">Открыть правое изображение</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1224"/>
        <source>Save %1</source>
        <translation>Сохранить %1</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1356"/>
        <source>Move to</source>
        <translation>Переместить в</translation>
    </message>
    <message>
        <location filename="../qml/WorkSpace.qml" line="1357"/>
        <source>Move</source>
        <translation>Переместить</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="18"/>
        <source>modified</source>
        <comment>flag in the header</comment>
        <translation>изменён</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="80"/>
        <location filename="../qml/main.qml" line="299"/>
        <source>Left angle rotation</source>
        <translation>Поворот левого ракурса</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="89"/>
        <location filename="../qml/main.qml" line="310"/>
        <source>Right angle rotation</source>
        <translation>Поворот правого ракурса</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="177"/>
        <source>File</source>
        <oldsource>&amp;File</oldsource>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="471"/>
        <source>Window mode</source>
        <translation>Оконный режим</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="479"/>
        <source>Quit</source>
        <oldsource>&amp;Quit</oldsource>
        <translation>Выход</translation>
    </message>
    <message>
        <source>Source format</source>
        <translation type="vanished">Формат источника</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="281"/>
        <location filename="../qml/main.qml" line="320"/>
        <source>Source type</source>
        <translation>Формат источника</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="128"/>
        <location filename="../qml/main.qml" line="353"/>
        <location filename="../qml/main.qml" line="357"/>
        <source>Open left image with…</source>
        <translation>Открыть левый ракурс в…</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="133"/>
        <location filename="../qml/main.qml" line="363"/>
        <location filename="../qml/main.qml" line="367"/>
        <source>Open right image with…</source>
        <translation>Открыть правый ракурс в…</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="141"/>
        <source>Settings…</source>
        <translation>Настройки…</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="195"/>
        <source>View</source>
        <oldsource>&amp;View</oldsource>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="152"/>
        <source>Active tasks: %1</source>
        <translation>Активные задачи: %1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="43"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <source>&amp;Source format</source>
        <translation>Формат &amp;источника</translation>
    </message>
    <message>
        <source>First angle rotation</source>
        <translation type="vanished">Поворот первого ракурса</translation>
    </message>
    <message>
        <source>Second angle rotation</source>
        <translation type="vanished">Поворот второго ракурса</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="106"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="119"/>
        <location filename="../qml/main.qml" line="343"/>
        <source>Auto Color Adjustment</source>
        <translation>Автоматическая регулировка цвета</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="148"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="173"/>
        <source>Toolbars</source>
        <translation>Панели инструментов</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="183"/>
        <source>File operations</source>
        <translation>Файловые операции</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="201"/>
        <source>Go</source>
        <oldsource>&amp;Go</oldsource>
        <translation>Переход</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="226"/>
        <source>&amp;Play</source>
        <translation>В&amp;оспроизведение</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="234"/>
        <source>&amp;Go</source>
        <translation>П&amp;ереход</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="243"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="246"/>
        <source>Online help</source>
        <translation>Online справка</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="320"/>
        <location filename="../qml/main.qml" line="397"/>
        <source>Reverse</source>
        <translation>Обратный порядок</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="383"/>
        <location filename="../qml/main.qml" line="397"/>
        <source>Display format</source>
        <translation>Формат отображения</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="452"/>
        <source>Active tasks</source>
        <translation>Активные задачи</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="453"/>
        <source>There are %1 active tasks.
Do you want to stop them?</source>
        <translation>Имеется %1 активных задач.
Остановить их?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="471"/>
        <source>Full screen</source>
        <translation>Полный экран</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="488"/>
        <source>Batch processing</source>
        <translation>Пакетная обработка</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="189"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="17"/>
        <location filename="../qml/main.qml" line="18"/>
        <source>Stereo Photo View</source>
        <translation>Просмотр стерео фотографий</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Справка</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="249"/>
        <source>/en/help.html</source>
        <comment>Help url</comment>
        <translation>/help.html</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="253"/>
        <source>About %1…</source>
        <translation>О программе %1…</translation>
    </message>
</context>
<context>
    <name>stereoscopic::gui::ConsoleArgumentsParser</name>
    <message>
        <source>A viewer/editor for stereoscopic photo and video.</source>
        <translation type="vanished">Программа для просмотра и редактирования стереоскопических фотографий и видео.</translation>
    </message>
    <message>
        <source>/en</source>
        <translation type="vanished">/</translation>
    </message>
    <message>
        <source>Input file.</source>
        <translation type="vanished">Входной файл.</translation>
    </message>
    <message>
        <source>Optional second input file to load a separate stereo pair.</source>
        <translation type="vanished">Необязательный второй входной файл для загрузки раздельной стерео-пары.</translation>
    </message>
    <message>
        <source>Run in batch mode.</source>
        <translation type="vanished">Запустить в пакетном режиме.</translation>
    </message>
    <message>
        <source>Input stereo layout (see --layouts) - optinal.</source>
        <translation type="vanished">Компоновка кадров во входном файле (см. --layouts) - не обязательный.</translation>
    </message>
    <message>
        <source>Right-eye view first in the input file - optional.</source>
        <translation type="vanished">Указывает, что во входном файле сначала идёт ракурс для правого глаза - не обязательно.</translation>
    </message>
    <message>
        <source>Display a list of layouts.</source>
        <translation type="vanished">Отобразить список возможных компоновок кадров.</translation>
    </message>
    <message>
        <source>Input layouts</source>
        <translation type="vanished">Компановки кадров, доступные для входных стерео-пар</translation>
    </message>
</context>
<context>
    <name>stereoscopic::gui::FavoritesModel</name>
    <message>
        <source>Recent folders</source>
        <translation type="vanished">Последние папки</translation>
    </message>
</context>
<context>
    <name>stereoscopic::gui::PresetsListModel</name>
    <message>
        <source>Default</source>
        <comment>Default preset name</comment>
        <translation type="vanished">По умолчанию</translation>
    </message>
</context>
</TS>
