#include "videocommands.h"

using namespace stereoscopic::gui::video_commands;

VideoCommand::~VideoCommand()
{

}

void VideoCommand::redo()
{
    m_oldLayoutChart = m_source->layoutChart();
    execute();
}

void VideoCommand::undo()
{
    m_source->setLayoutChart(m_oldLayoutChart);
}

void ApplyAlign::execute()
{
    m_source->applyAlign(m_align);
}

void AddTimestamp::execute()
{
    m_source->addTimestamp();
}

void RemoveTimestamp::execute()
{
    m_source->removeTimestamp();
}

void Crop::execute()
{
    m_source->applyAlign(m_align);
}
