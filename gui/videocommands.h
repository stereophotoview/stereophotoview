#ifndef VIDEOCOMMANDS_H
#define VIDEOCOMMANDS_H

#include <QUndoCommand>
#include "stereovideosource.h"

#include <QDebug>

using namespace stereoscopic;

namespace stereoscopic::gui::video_commands {

    class VideoCommand : public QUndoCommand
    {
        public:
            explicit VideoCommand(StereoVideoSource* source, QUndoCommand *parent = nullptr)
                : QUndoCommand(parent)
            {
                m_source = source;
            }

            virtual ~VideoCommand() Q_DECL_OVERRIDE;

            void redo() Q_DECL_OVERRIDE;
            void undo() Q_DECL_OVERRIDE;
            virtual void execute() = 0;

        protected:
            StereoVideoSource* m_source;
            AlignChart m_oldLayoutChart;
    };

    class Crop : public VideoCommand
    {
        public:
            Crop(StereoVideoSource* source, QRect rect, double borderWidthPercents, QUndoCommand *parent = nullptr)
                : VideoCommand(source, parent)
            {
                m_align = AlignParams(rect, borderWidthPercents);
            }

            void execute() Q_DECL_OVERRIDE;
        private:
            AlignParams m_align;
    };

    class ApplyAlign : public VideoCommand
    {
        public:
            ApplyAlign(StereoVideoSource* source, QPointF offset, double leftAngle, double rightAngle, QUndoCommand *parent = nullptr)
                : VideoCommand(source, parent)
            {
                m_align = AlignParams(offset, leftAngle, rightAngle);
            }

            ApplyAlign(StereoVideoSource* source, AlignParams::ColorAdjustment colorsAdjustment, QUndoCommand *parent = nullptr)
                : VideoCommand(source, parent)
            {
                m_align = AlignParams(colorsAdjustment);
            }

            void execute() Q_DECL_OVERRIDE;
        private:
            AlignParams m_align;
    };

    class AddTimestamp : public VideoCommand
    {
        public:
            AddTimestamp(StereoVideoSource* source, QUndoCommand *parent = nullptr)
                : VideoCommand(source, parent)
            {

            }

            void execute() Q_DECL_OVERRIDE;
        private:

    };

    class RemoveTimestamp : public VideoCommand
    {
        public:
            RemoveTimestamp(StereoVideoSource* source, QUndoCommand *parent = nullptr)
                : VideoCommand(source, parent)
            {

            }

            void execute() Q_DECL_OVERRIDE;
    };
}

#endif // VIDEOCOMMANDS_H
