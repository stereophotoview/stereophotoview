#include "alignchart.h"
#include "mathhelper.h"
#include <QDebug>

using namespace stereoscopic;
using namespace stereoscopic::math;

AlignChart::AlignChart()
{
    m_framesize = QSize(0, 0);
    m_baseLayout.setCropRect(QRect(0,0,0,0));
}

void AlignChart::apply(double position, stereoscopic::AlignParams params)
{
    // Везде далее прибавляем переданные параметры к текущим, т.к. редактор парадаёт то, насколько нужно изменить текущие параметры

    // Применяем значение к базовому
    m_baseLayout = m_baseLayout.with(params);

    // Ищем точку на данный момент времени, и применяем к ней значения
    for (int i = 0; i < m_data.size(); i++)
    {
        double x = m_data[i].first;
        if(equal(x, position))
        {
            m_data[i].second = m_data[i].second.with(params);
            return;
        }
    }

    // Если не нашли точку на данный момент времени, применяем значение ко всем точкам
    for (int i = 0; i < m_data.size(); i++)
    {
        m_data[i].second = m_data[i].second.with(params);
    }
}

void AlignChart::removePoint(double position)
{
    if(equal(position, 0) || equal(position, m_duration))
        return;
    // Ищем и удаляем точку на данный момент времени
    for (int i = 0; i < m_data.size(); i++)
    {
        if(equal(m_data[i].first, position))
        {
            m_data.removeAt(i);
            break;
        }
    }

    // Если остались только крайние точки, удаляем их. Будем получать значение по m_baseLayout
    if(m_data.size() <= 2)
        m_data.clear();
}

AlignParams AlignChart::get(double position)
{
    double x1, x2, k;
    for (int i = 0; i < m_data.size() - 1; i++)
    {
        x1 = m_data[i].first;
        x2 = m_data[i + 1].first;

        // Находим точки, между которым находится искомая
        if(position >= x1 && position <= x2)
        {
            // Вычисляем пропорциональное выравнивание
            k = (position - x1) / (x2 - x1);
            AlignParams l1 = m_data[i].second;
            AlignParams l2 = m_data[i + 1].second;

            AlignParams params;
            params.setoffset(QPointF(scale(l1.offset().x(), l2.offset().x(), k),
                                     scale(l1.offset().y(), l2.offset().y(), k)));
            params.setLeftAngle(scale(l1.leftAngle(), l2.leftAngle(), k));
            params.setRightAngle(scale(l1.rightAngle(), l2.rightAngle(), k));
            params.setCropRect(QRect(
                                   scale(l1.cropRect().left(), l2.cropRect().left(), k),
                                   scale(l1.cropRect().top(), l2.cropRect().top(), k),
                                   scale(l1.cropRect().width(), l2.cropRect().width(), k),
                                   scale(l1.cropRect().height(), l2.cropRect().height(), k)
                                ));
            params.setBorderWidthPercents(scale(l1.borderWidthPercents(), l2.borderWidthPercents(), k));
            return params;
        }
    }
    // Если не нашли данных, возвращаем базовые значения
    return m_baseLayout;
}

void AlignChart::addPoint(double position)
{
    // Если точек пока нет, создаём граничные
    if(m_data.size() < 2)
    {
        m_data.clear();
        m_data.append(QPair<double, AlignParams>(0, m_baseLayout));
        m_data.append(QPair<double, AlignParams>(m_duration, m_baseLayout));
    }

    // Получаем текущие значения параметров выравнивания на данный момент времени
    AlignParams l = get(position);

    // Вставляем точку графика на своё место
    QPair<double, AlignParams> point(position, l);
    double x;
    for (int i = 0; i < m_data.size(); i++)
    {
        x = m_data[i].first;

        // Если точка уже есть - выходим
        if(equal(x, position))
            return;

        // Вставляем новую контрольную точку перед той, которая больше данной отметки времени
        if(position < x)
        {
            m_data.insert(i, point);
            return;
        }
    }

    // Если отметка времени больше всех существующих точек, добавляем контрольную точку вконец списка.
    m_data.append(point);
}

bool AlignChart::pointExists(double position)
{
    for (int i = 0; i < m_data.size(); i++)
    {
        if(equal(m_data[i].first, position))
            return  true;
    }
    return false;
}

QSize AlignChart::maxCropSize()
{
    QSize maxSize = m_baseLayout.cropRect().size();
    foreach(auto point, m_data)
    {
        auto cropRect = point.second.cropRect();
        if(maxSize.width() < cropRect.width())
            maxSize.setWidth(cropRect.width());
        if(maxSize.height() < cropRect.height())
            maxSize.setHeight(cropRect.height());
    }
    return maxSize;
}

void AlignChart::setFrameSize(QSize size)
{
    foreach(auto point, m_data)
    {
        if(point.second.cropRect().size() == m_framesize)
            point.second.setCropRect(QRect(QPoint(0,0), size));
    }
    if(m_baseLayout.cropRect().size() == m_framesize)
        m_baseLayout.setCropRect(QRect(QPoint(0,0), size));
    m_framesize = size;
}

void AlignChart::setDuration(double duration)
{
    m_duration = duration;
}

double AlignChart::scale(double a, double b, double k)
{
    return a + (b - a) * k;
}
