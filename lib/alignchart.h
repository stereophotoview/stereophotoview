#ifndef LAYOUTGRAPTH_H
#define LAYOUTGRAPTH_H

#include "alignparams.h"

namespace stereoscopic {
    /**
     * @brief Класс графика зависимости выравнивания ракурсов от времени в видео файле
     */
    class LIBSHARED_EXPORT AlignChart
    {
        public:
            /**
             * @brief Создаёт график зависимости выравнивания ракурсов от времени в видео файле
             */
            AlignChart();

            /**
             * @brief Применяет выравнивание на данный момент времени.
             *        Если есть точка на данную временную метку, значение применяется к ней.
             *        Если точка не найдена, значения применяются ко всем точкам.
             * @param position Отметка времени в секундах
             * @param layout Требуемое изменение параметров выравнивания на данный момент времени относительно текущих парамтеров
             */
            void apply(double position, AlignParams params);

            /**
             * @brief Удаляет точку графика
             * @param position Отметка времени в секундах
             */
            void removePoint(double position);

            /**
             * @brief Возвращает выравнивание ракурсов на данное время
             * @param position Отментка времени в секундах
             * @return Расположение ракурсов для данной отметки времени
             */
            AlignParams get(double position);

            /**
             * @brief Добавляет контрольную точку на данный момент времени c текущими параметрами
             * @param position Отметка времени в секундах
             */
            void addPoint(double position);

            /**
             * @brief Возвращает признак наличия точки в данный момент времени
             * @param position Момент времени в секундах
             * @return True, если есть точка
             */
            bool pointExists(double position);

            QSize maxCropSize();

            QList<QPair<double, AlignParams>> data() { return m_data; }
            AlignParams baseLayout(){ return m_baseLayout; }

            QSize frameSize(){ return m_framesize; }
            void setFrameSize(QSize size);

            double duration(){ return m_duration; }
            void setDuration(double duration);

        private:
            double m_duration = 0;                     ///< Продолжительность всего видео
            QSize m_framesize;                         ///< Размер кадра
            AlignParams m_baseLayout;                  ///< Параметры выравнивания для всего видео
            QList<QPair<double, AlignParams>> m_data;  ///< Отсортированный по значению параметра список точек графика

            /**
             * @brief Вычисляет промежуточное пропорциональное значения параметра между двумя точками.
             * @param a Значение парамтера в первой точке.
             * @param b Значение парамтера во второй точке.
             * @param k Коэффициент пропорции (0..1), показывающий, к какой точке рузультат должен быть ближе.
             * @return Промежуточное значение с учётом коэффициента.
             */
            double scale(double a, double b, double k);
    };
}
#endif // LAYOUTGRAPTH_H
