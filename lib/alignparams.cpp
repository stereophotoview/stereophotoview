#include "alignparams.h"
#include "mathhelper.h"

using namespace stereoscopic;

AlignParams::AlignParams(QObject *parent)
    : QObject(parent)
{

}

AlignParams::AlignParams(QPointF offset, double leftAngle, double rightAngle, QObject *parent) : QObject(parent)
{
    m_offset = offset;
    m_leftAngle = leftAngle;
    m_rightAngle = rightAngle;
}

AlignParams::AlignParams(QRect cropRect, double borderWidthPercents, QObject *parent) : QObject(parent)
{
    m_cropRect = cropRect;
    m_borderWidthPercents = borderWidthPercents;
}

AlignParams::AlignParams(AlignParams::ColorAdjustment colorAdjustment, QObject *parent): QObject(parent)
{
    m_colorAdjustment = colorAdjustment;
}

AlignParams::AlignParams(const AlignParams &src)
    : QObject(src.parent())
{
    m_offset = src.m_offset;
    m_leftAngle = src.m_leftAngle;
    m_rightAngle = src.m_rightAngle;
    m_cropRect = src.m_cropRect;
    m_borderWidthPercents = src.m_borderWidthPercents;
    m_colorAdjustment = src.m_colorAdjustment;
}

AlignParams AlignParams::operator=(const AlignParams &src)
{
    m_offset = src.m_offset;
    m_leftAngle = src.m_leftAngle;
    m_rightAngle = src.m_rightAngle;
    m_cropRect = src.m_cropRect;
    m_borderWidthPercents = src.m_borderWidthPercents;
    m_colorAdjustment = src.m_colorAdjustment;
    return *this;
}

AlignParams AlignParams::with(const AlignParams &other)
{
    AlignParams l = *this;
    AlignParams o = (AlignParams)other;
    l.setoffset(QPointF(l.offset().x() + o.offset().x(), l.offset().y() + o.offset().y()));
    l.setLeftAngle(l.leftAngle() + o.leftAngle());
    l.setRightAngle(l.rightAngle() + o.rightAngle());

    // We proceed from the fact that there is no action that simultaneously crops and changes the color correction
    if(!o.m_cropRect.isEmpty())
    {
        l.setCropRect(o.m_cropRect);
        l.setBorderWidthPercents(o.m_borderWidthPercents);
    }
    else
    {
        l.setColorAdjustment(o.colorAdjustment());
    }
    return l;
}
