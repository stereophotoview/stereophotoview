#ifndef LAYOUTPARAMS_H
#define LAYOUTPARAMS_H

#include <QObject>
#include <QPointF>
#include <QRect>

#include "lib_global.h"

namespace stereoscopic {
    /**
     * @brief Класс для представления параметров взаимного выравнивания ракурсов
     */
    class LIBSHARED_EXPORT AlignParams : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QPointF offset READ offset WRITE setoffset NOTIFY offsetChanged)
            Q_PROPERTY(double leftAngle READ leftAngle WRITE setLeftAngle NOTIFY leftAngleChanged)
            Q_PROPERTY(double rightAngle READ rightAngle WRITE setRightAngle NOTIFY rightAngleChanged)
            Q_PROPERTY(QRect cropRect READ cropRect WRITE setCropRect NOTIFY cropRectChanged)
            Q_PROPERTY(double borderWidthPercents READ borderWidthPercents WRITE setBorderWidthPercents NOTIFY borderWidthPercentsChanged)
            Q_PROPERTY(ColorAdjustment colorAdjustment READ colorAdjustment WRITE setColorAdjustment NOTIFY colorAdjustmentChanged)

        public:
            enum class ColorAdjustment { None = 0, LeftByRight = 1, RightByLeft = 2 };

            AlignParams(QObject *parent = nullptr);

            /**
             * @brief Создаёт параметры взаимного выравнивания ракурсов
             * @param offset Относительный сдвиг ракурсов (параллакс)
             * @param leftAngle Угол поворота левого ракурса
             * @param rightAngle Угол поворота правого ракурса
             * @param parent Родительский объект (опционально)
             */
            AlignParams(QPointF offset, double leftAngle = 0, double rightAngle = 0, QObject *parent = nullptr);

            /**
             * @brief Создаёт параметры видео файла
             * @param cropRect Прямоугольник кадрирования
             * @param borderWidthPercents Ширина рамки в процентах
             * @param parent Родительский объект (опционально)
             */
            AlignParams(QRect cropRect, double borderWidthPercents, QObject *parent = nullptr);

            /**
             * @brief Создаёт параметры видео файла
             * @param cropRect Прямоугольник кадрирования
             * @param borderWidthPercents Ширина рамки в процентах
             * @param parent Родительский объект (опционально)
             */
            AlignParams(ColorAdjustment colorAdjustment, QObject *parent = nullptr);

            AlignParams(const AlignParams &src);
            AlignParams operator=(const AlignParams &src);

            /// Возвращает относительный сдвиг ракурсов (параллакс)
            QPointF offset(){ return m_offset; }

            /// Устанавливает относительный сдвиг ракурсов
            void setoffset(QPointF value)
            {
                m_offset = value;
                emit offsetChanged();
            }

            /// Возврщает угол поворота левого изображения
            double leftAngle(){ return m_leftAngle; }

            /// Устанавливает угол поворота левого изображения
            void setLeftAngle(double value)
            {
                m_leftAngle = value;
                emit leftAngleChanged();
            }

            /// Возврщает угол поворота правого изображения
            double rightAngle(){ return m_rightAngle; }

            /// Устанавливает угол поворота правого изображения
            void setRightAngle(double value)
            {
                m_rightAngle = value;
                emit rightAngleChanged();
            }

            /// Возврщает прямоуголник кадрирования
            QRect cropRect(){ return m_cropRect; }

            /// Устанавливает прямоугольник кадрирования
            void setCropRect(QRect value)
            {
                m_cropRect = value;
                emit cropRectChanged();
            }

            /// Возврщает толщину рамки в процентах
            double borderWidthPercents(){ return m_borderWidthPercents; }

            /// Устанавливает толщину рамки в процентах
            void setBorderWidthPercents(double value)
            {
                m_borderWidthPercents = value;
                emit borderWidthPercentsChanged();
            }

            /**
             * @brief Возвращает сумму текущих параметров и других
             * @param other Другие параметры
             * @return Сумма
             */
            AlignParams with(const AlignParams &other);

            ColorAdjustment colorAdjustment() { return m_colorAdjustment; }
            void setColorAdjustment(ColorAdjustment value)
            {
                if(m_colorAdjustment != value)
                {
                    m_colorAdjustment = value;
                    emit colorAdjustmentChanged();
                }
            }

        signals:
            void offsetChanged();
            void leftAngleChanged();
            void rightAngleChanged();
            void cropRectChanged();
            void borderWidthPercentsChanged();
            void colorAdjustmentChanged();

        private:
            QPointF m_offset;
            double m_leftAngle = 0;
            double m_rightAngle = 0;
            QRect m_cropRect = QRect(0,0,0,0);
            double m_borderWidthPercents = 0;
            ColorAdjustment m_colorAdjustment = ColorAdjustment::None;
    };
}
#endif // LAYOUTPARAMS_H
