
#include <memory>
#if DEBUG
#include <QElapsedTimer>
#endif

#include "autoalign.h"
#include "imagehelper.h"
#include "mathhelper.h"
#include "vision/stereoframecv.h"
#include "vision/imagecv.h"

#ifdef CV_DEBUG
#include "opencv2/highgui.hpp"
#endif

using namespace std;
using namespace stereoscopic;
using namespace stereoscopic::vision;
using namespace stereoscopic::math;

bool AutoAlign::enabled()
{
#ifdef OPENCV
    return true;
#else
    return false;
#endif
}

QString AutoAlign::opencvVersion()
{
#ifdef OPENCV
    return CV_VERSION;
#else
    return "";
#endif
}

AutoAlign::AutoAlign(QObject *parent)
    : QObject(parent)
{
    connect(&w, &QFutureWatcher<QPointF>::finished, this, &AutoAlign::handleFinished);
}

AlignParams AutoAlign::calcOffset(StereoImageSource *image, InputAlignParams params)
{
    StereoFrame frame = createFrameFromImageByParams(image, params);
    StereoFrameCV frameCV(&frame);
    std::vector<DMatch> goodMatches = frameCV.matchingPoints();

    if(goodMatches.size() < 5)
    {
        qWarning() << "Too few matches:" << goodMatches.size();
        return AlignParams();
    }

    // Находим угол поворота между ракурсами
    double diff = params.doRotate ? findAngles(frameCV) : 0;
    double leftAngle = diff;
    double rightAngle = -diff;

    // Поворачиваем координаты точек относительно центра
    if(!equal(diff, 0))
    {
        rotate(frameCV, leftAngle, rightAngle);
    }

    // Находим смещение между ракурсами
    QPointF offset = findOffset(frameCV, params.doHorizontal, params.doVertical);
    if(!params.rect.isEmpty())
    {
        offset.setX(offset.x() * params.rect.width() / image->width());
        offset.setY(offset.y() * params.rect.height() / image->height());
    }
    return AlignParams(offset, leftAngle, rightAngle);
}

AlignParams AutoAlign::calcOffsetMT(StereoImageSource *image, InputAlignParams params)
{
    static QMutex mutex;
    mutex.lock();
    AlignParams p = calcOffset(image, params);
    mutex.unlock();
    return p;
}

void AutoAlign::startCalcOffset(QRect rect, bool doHorizontal, bool doVertical, bool doRotate, double quality)
{
    setRunning(true);
    w.setFuture(QtConcurrent::run(&calcOffsetMT, image(), InputAlignParams(rect, doHorizontal, doVertical, doRotate, quality)));
}

void AutoAlign::handleFinished()
{
    if(running())
    {
#if DEBUG
        qDebug() << "finished";
#endif
        p = w.result();
        emit offsetCalculated(&p);
        setRunning(false);
    }
}

void AutoAlign::setRunning(bool value)
{
    m_running = value;
    emit runningChanged(m_running);
}

StereoFrame AutoAlign::createFrameFromImageByParams(StereoImageSource *image, InputAlignParams params)
{
    if(!params.rect.isNull() && params.rect.isValid())
        return image->displayFrame()->cropped(params.rect).scaled(params.quality);
    return image->displayFrame()->scaled(params.quality);
}

AutoAlign::~AutoAlign()
{

}

#ifdef OPENCV
namespace stereoscopic
{
template<typename K, typename V>
class MatchIslands
{
    private:
        vector<vector<pair<K,V>>> valueIslands;
        bool (*m_func)(K p1, K p2);

    public:

        MatchIslands(bool (*func)(K p1, K p2))
        {
            m_func = func;
        }

        void add(K d,  V m)
        {
            pair<K,V> newPair;
            newPair.first = d;
            newPair.second = m;
            for(uint i = 0; i < valueIslands.size(); i++)
            {
                for(uint k = 0; k < valueIslands[i].size(); k++)
                {
                    pair<K, V> item = valueIslands[i][k];
                    if(m_func(item.first, d))
                    {
                        valueIslands[i].push_back(newPair);
                        return;
                    }
                }
            }
            vector<pair<K,V>> island;
            island.push_back(newPair);
            valueIslands.push_back(island);
        }
        vector<pair<K,V>> bigestIsland()
        {
            uint max = 0;
            uint index = 0;
            for(uint i=0; i< valueIslands.size(); i++)
            {
                uint s = (uint)valueIslands[i].size();
                if(s > max)
                {
                    max = s;
                    index = i;
                }
            }
            return valueIslands[index];
        }
};
}

Mat AutoAlign::createMat(QImage src, QSize size)
{
    src = src.convertToFormat(QImage::Format_Grayscale8);
    // Уменьшаем изображения для ускорения обработки
    src = ImageHelper::createThumbnail(&src, size);

    Mat mat(src.height(), src.width(), CV_8UC1);
    uint bytesPerLine = (uint)src.width();
    uchar* resData = mat.data;
    for(int y = 0; y < src.height(); y++)
    {
        uchar* srcLine = src.scanLine(y);
        uchar* resLine = resData + (uint)y * bytesPerLine;
        memcpy(resLine, srcLine, bytesPerLine);
    }
    return mat;
}

double AutoAlign::distance(Point2f p1, Point2f p2)
{
    int x = (int)(p2.x - p1.x);
    int y = (int)(p2.y - p1.y);
    return sqrt(x*x + y*y);
}

double AutoAlign::angle(Point2f p1, Point2f p2)
{
    int x = (int)(p2.x - p1.x);
    int y = (int)(p2.y - p1.y);
    return atan((double)y / x) * 180.0 / 3.14159265;
}

// Вращает ключевые точки, соответствующие goodMatches на заданный угол
void AutoAlign::rotate(StereoFrameCV frameCV, double leftAngle, double rightAngle)
{
    auto leftImage = frameCV.leftImage();
    auto rightImage = frameCV.rightImage();
    QSize imageSize = leftImage.size();
    Point2f center(imageSize.width() / 2, imageSize.height() / 2);

    double leftAngleR = leftAngle * 3.14159265 / 180.0;
    double rightAngleR = rightAngle * 3.14159265 / 180.0;
    foreach (DMatch m, frameCV.matchingPoints())
    {
        KeyPoint leftPoint = frameCV.getLeftKeyPoint(m);
        KeyPoint rightPoint = frameCV.getRightKeyPoint(m);
        leftPoint.pt = rotatePoint(leftPoint.pt, (float)leftAngleR, center);
        rightPoint.pt = rotatePoint(rightPoint.pt, (float)rightAngleR, center);
    }
}

Point2f AutoAlign::rotatePoint(Point2f p, float angle, Point2f center)
{
    float x1 = p.x - center.x;
    float y1 = p.y - center.y;
    float dist = sqrt(pow(x1, 2) + pow(y1, 2));
    float sina1 = x1 / dist;
    float an2 = asin(sina1) + angle;
    float sina2 = sin(an2);
    float x2 = sina2 * x1 / sina1;
    float y2 = sqrt(pow(dist, 2) - pow(x2, 2));
    Point2f p2;
    p2.x = center.x + x2;
    p2.y = center.y + y2;
    return p2;
}

// Находим необходимое смещение ракурсов по осям
QPointF AutoAlign::findOffset(StereoFrameCV frameCV, bool setParallax, bool setVOffset)
{
    QPointF offset;
    if(!setParallax && !setVOffset)
        return offset;
    MatchIslands<Point,DMatch> islands([](Point p1, Point p2){ return abs(p1.x - p2.x) < 10 && abs(p1.y - p2.y) < 10; });
    // Находим медианные значения в разнице координат по x и y
    int maxX = -1000000, minX = 10000000;
    int maxY = -1000000, minY = 10000000;
    foreach(DMatch m, frameCV.matchingPoints())
    {
        islands.add(frameCV.getDistanceBetweenMatchingPoints(m), m);
    }
    vector<pair<Point,DMatch>> matches = islands.bigestIsland();
    for( uint i = 0; i < matches.size(); i++)
    {
        Point d = matches[i].first;
        if(d.x < minX) minX = d.x;
        if(d.x > maxX) maxX = d.x;
        if(d.y < minY) minY = d.y;
        if(d.y > maxY) maxY = d.y;
    }

    double xMed = (double)((maxX + minX) / 2) / frameCV.size().width();
    double yMed = (double)((maxY + minY) / 2) / frameCV.size().height();

    if(setParallax)
        offset.setX(xMed);
    if(setVOffset)
        offset.setY(yMed);
    return offset;
}

double AutoAlign::findAngles(StereoFrameCV frame)
{
    // Эта функция работает некорректно, если геометрия ракурсов различается
    if(frame.matchingPoints().size() < 2)
        return 0;
    // Вычисляем угол поворота
    // Находим две точки максимально удалённые друг от друга
    double minDistance = 50;
    MatchIslands<double, pair<DMatch,DMatch>> islands([](double d1, double d2){ return abs(d1 - d2) < 2; });

    foreach(DMatch m1, frame.matchingPoints())
    {
        foreach(DMatch m2, frame.matchingPoints())
        {
            Point2f p1 = frame.leftImage().keyPoints()[(ulong)m1.queryIdx].pt;
            Point2f p2 = frame.rightImage().keyPoints()[(ulong)m2.trainIdx].pt;
            double d = distance(p1, p2);

            if(d > minDistance)
            {
                Point2f pl1 = frame.leftImage().keyPoints()[(ulong)m1.queryIdx].pt;
                Point2f pl2 = frame.leftImage().keyPoints()[(ulong)m2.queryIdx].pt;
                Point2f pr1 = frame.rightImage().keyPoints()[(ulong)m1.trainIdx].pt;
                Point2f pr2 = frame.rightImage().keyPoints()[(ulong)m2.trainIdx].pt;

                double al = angle(pl1, pl2);
                double ar = angle(pr1, pr2);
                double diff = (ar - al);
                pair<DMatch,DMatch> pair;
                pair.first = m1;
                pair.second = m2;
                islands.add(diff, pair);
            }
        }
    }

    auto matches = islands.bigestIsland();
    double sumDiff = 0;
    for(uint i = 0; i< matches.size(); i++)
    {
        auto match = matches[i];
        double diff = (double)match.first;
        sumDiff = sumDiff + diff;
    }
    double avgDiff = sumDiff / matches.size();
    if(abs(avgDiff) > 0.2 && abs(avgDiff) < 30)
        return avgDiff / 2;
    return 0;
}

#endif
