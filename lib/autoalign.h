#ifndef AUTOALIGN_H
#define AUTOALIGN_H

#include <QObject>

#include "stereoimagesource.h"
#include <QtConcurrent/QtConcurrent>
#include "alignparams.h"

#include "vision/stereoframecv.h"

using namespace stereoscopic::vision;

namespace stereoscopic {
    /**
    * @brief Структура входных данных для автоматического выравнивания взаимного расположения ракурсов
    */
    struct InputAlignParams
    {
        public:
            /// Прямоуголник для поиска ключевых точек
            QRect rect;
            /// режимы работы
            bool doHorizontal, doVertical, doRotate;
            /// Коэфициент масштабирования изображения перед обработкой. Используется, когда не задан rect
            double quality;

            /**
             * @brief Создаёт параметры для авто-выравнивания
             * @param rect Прямоуголник для поиска ключевых точек
             * @param doHorizontal Выполнять ли выравнвиание по горизонтали
             * @param doVertical Выполнять ли выравнивание по вертикали
             * @param doRotate Выполнять ли взаимное вращение ракурсов
             * @param quality Коэфициент масштабирования изображения перед обработкой. Используется, когда не задан rect
             */
            InputAlignParams(QRect rect, bool doHorizontal, bool doVertical, bool doRotate, double quality)
            {
                this->rect = rect;
                this->doHorizontal = doHorizontal;
                this->doVertical = doVertical;
                this->doRotate = doRotate;
                this->quality = quality;
            }
    };

    /**
    * @brief Класс для автоматического выравнивания взаимного расположения ракурсов
    */
    class LIBSHARED_EXPORT AutoAlign: public QObject
    {
            Q_OBJECT
            Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)
            Q_PROPERTY(StereoImageSource* image READ image WRITE setImage NOTIFY imageChanged)
            Q_PROPERTY(bool enabled READ enabled)
            Q_PROPERTY(QString opencvVersion READ opencvVersion)
        public:
            /// Возвращает признак того, что автовыравниваение поддерживается
            bool enabled();

            /// Возвращает признак того, что в данный момент идёт процесс
            bool running(){ return m_running; }

            /// Возвращает мсточник изображения
            StereoImageSource* image(){ return m_image; }

            /// Устанавливает источник изображения
            void setImage(StereoImageSource* value)
            {
                m_image = value;
                emit imageChanged(m_image);
                setRunning(false);
                w.cancel();
            }

            /// Вычисляет сдвиг синхронно
            static AlignParams calcOffset(StereoImageSource *image, InputAlignParams params);

            /// Возвращает версию используемой библиотеки OpenCV
            QString opencvVersion();

            explicit AutoAlign(QObject *parent = nullptr);
            ~AutoAlign();

        public slots:
            /// Запускает процесс асинхронного расчёта. Результат возвращается сигналом offsetCalculated.
            void startCalcOffset(QRect rect, bool doHorizontal, bool doVertical, bool doRotate, double quality);

        signals:
            /// Сигнал завершения процесса расчёта
            void offsetCalculated(AlignParams* result);
            /// Сигнал изменения признака идущего процесса вычисления
            void runningChanged(bool);
            /// Сигнал изменения источника изображения
            void imageChanged(StereoImageSource*);

        private:
            QFutureWatcher<AlignParams> w;
            StereoImageSource* m_image;
            bool m_running;
            AlignParams p;
            void setRunning(bool value);

            static StereoFrame createFrameFromImageByParams(StereoImageSource *image, InputAlignParams params);

            /// Вычисляет сдвиг синхронно с блокировкой мьютекса
            static AlignParams calcOffsetMT(StereoImageSource *image, InputAlignParams params);

#ifdef OPENCV
            static Mat createMat(QImage src, QSize size);
            static double distance(Point2f p1, Point2f p2);
            static double angle(Point2f p1, Point2f p2);
            static void rotate(StereoFrameCV frameCV, double leftAngle, double rightAngle);
            static Point2f rotatePoint(Point2f p, float angle, Point2f center);
            static QPointF findOffset(StereoFrameCV frameCV, bool setParallax, bool setVOffset);
            static double findAngles(StereoFrameCV frame);
#endif
        private slots:
            /// Обработчик завершения процесса расчёта
            void handleFinished();
    };
}
#endif // AUTOALIGN_H
