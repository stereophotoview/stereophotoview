#include "coloroperations.h"

#include <math.h>
#include <QDebug>
#include <QPainter>
#include <QPoint>
#include <QPen>
#include "colorsdebug.h"

using namespace stereoscopic::colors;

QImage ColorOperations::adjustImageColorsToReference(QImage source, QImage reference, int type, int pixelStep)
{
    ColorAsjustment adjustment(source, reference, type, pixelStep);
    return adjustment.execute();
}

// ColorAsjustment

ColorAsjustment::ColorAsjustment(QImage source, QImage reference, int type, int pixelStep)
{
    this->source = source;
    this->reference = reference;
    this->pixelStep = pixelStep;
    this->transformation = QSharedPointer<RgbTransformation>(createTransformation(type));
}

QImage ColorAsjustment::execute()
{
    QImage result = transformation->transformImage(source);
#ifdef CV_DEBUG
    ColorsDebug::showAdjustmentDebug(source, reference, result, transformation.get());
#endif
    return result;
}

RgbTransformation* ColorAsjustment::createTransformation(int type)
{
    switch (type) {
        case 1:
            return new RgbAverageTransformSmoothed(source, reference, pixelStep);
        case 2:
            return new RgbStandardDeviationTransformation(source, reference, pixelStep);
        default:
            throw "Not implemented";
    }
}
