#ifndef COLOROPERATIONS_H
#define COLOROPERATIONS_H

#include <QSharedPointer>
#include "lib_global.h"
#include <QImage>
#include <QVector>
#include <QPair>
#include "frgb.h"
#include "rgbtransformation.h"

namespace stereoscopic::colors {

    class LIBSHARED_EXPORT ColorOperations
    {
        public:            
            static QImage adjustImageColorsToReference(QImage source, QImage reference, int type, int pixelStep = 1);

        private:

    };

    class ColorAsjustment {
        public:
            ColorAsjustment(QImage source, QImage reference, int type, int pixelStep = 1);
            QImage execute();
        private:
            int pixelStep = 1;
            QImage source, reference;
            QSharedPointer<RgbTransformation> transformation;
            RgbTransformation *createTransformation(int type);
    };
};

#endif // COLOROPERATIONS_H
