#ifdef CV_DEBUG
#include "colorsdebug.h"
#include "../vision/imagecv.h"
#include <cmath>

using namespace stereoscopic::colors;
using namespace stereoscopic::vision;

void ColorsDebug::showAdjustmentDebug(QImage source, QImage reference, QImage result, RgbTransformation *transformation)
{
    showDebugImageHistograms(source, "Source Histograms");
    showDebugImageHistograms(reference, "Reference Histograms");
    showDebugCorrectionCurves(result, transformation);
}

void ColorsDebug::showDebugImageHistograms(QImage image, QString label)
{
    auto histogram = ColorsHelper::getHistogram(image);
    QImage histoImage(QSize(256, 256), QImage::Format_ARGB32);
    histoImage.fill(Qt::white);
    QPainter painter(&histoImage);
    drawGraph<unsigned long>(histogram, [](RgbULong rgb){ return rgb.red; }, &painter, histoImage.size(), Qt::red);
    drawGraph<unsigned long>(histogram, [](RgbULong rgb){ return rgb.green; }, &painter, histoImage.size(), Qt::green);
    drawGraph<unsigned long>(histogram, [](RgbULong rgb){ return rgb.blue; }, &painter, histoImage.size(), Qt::blue);
    ImageCV(histoImage).showDebug(label);
}

void ColorsDebug::showDebugCorrectionCurves(QImage result, RgbTransformation *transformation)
{
    QImage image(QSize(256, 256), QImage::Format_ARGB32);
    image.fill(Qt::white);
    QPainter painter(&image);

    auto histogram = ColorsHelper::getHistogram(result);
    drawGraph<unsigned long>(histogram, [](RgbULong rgb){ return rgb.red; }, &painter, image.size(), Qt::red);
    drawGraph<unsigned long>(histogram, [](RgbULong rgb){ return rgb.green; }, &painter, image.size(), Qt::green);
    drawGraph<unsigned long>(histogram, [](RgbULong rgb){ return rgb.blue; }, &painter, image.size(), Qt::blue);

    QVector<Rgb32> curve(256);
    for(int i = 0; i < 256; i++)
        curve[i] = transformation->transformPixel(Rgb32(i, i, i));

    drawGraph<uchar>(curve, [](Rgb32 rgb){ return rgb.red; }, &painter, image.size(), Qt::red);
    drawGraph<uchar>(curve, [](Rgb32 rgb){ return rgb.green; }, &painter, image.size(), Qt::green);
    drawGraph<uchar>(curve, [](Rgb32 rgb){ return rgb.blue; }, &painter, image.size(), Qt::blue);

    QVector<QPair<Rgb32, Rgb32>> correctionPoints = transformation->getKeyPoints();
    foreach(auto correctionPoint, correctionPoints)
    {
        drawCorrectionPoint(correctionPoint, [](Rgb32 rgb){ return rgb.red; }, &painter, image.size(), Qt::red);
        drawCorrectionPoint(correctionPoint, [](Rgb32 rgb){ return rgb.green; }, &painter, image.size(), Qt::green);
        drawCorrectionPoint(correctionPoint, [](Rgb32 rgb){ return rgb.blue; }, &painter, image.size(), Qt::blue);
    }
    ImageCV(image).showDebug("Result & Curves");
}

template<typename T>
void ColorsDebug::drawGraph(QVector<FRgb<T>> data, std::function<T(FRgb<T>)> extractValue, QPainter *painter, QSize size, QColor color)
{
    double max = 0;
    for(int i = 0; i < 256; i++)
    {
        double value = extractValue(data[i]);
        if(value > max)
            max = value;
    }
    QPoint oldPoint;

    painter->setPen(color);
    for(int i = 0; i < 256; i++)
    {
        QPoint point(
                    std::round((double)i / 256.0 * size.width()),
                    std::round((double)size.height() - extractValue(data[i]) / max * size.height()));
        if(i > 0)
            painter->drawLine(oldPoint, point);
        oldPoint = point;
    }
}

void ColorsDebug::drawCorrectionPoint(QPair<Rgb32, Rgb32> point, std::function<uchar(Rgb32)> extract, QPainter *painter, QSize size, QColor color)
{
    painter->setPen(color);
    painter->setBrush(color);
    QPoint p(extract(point.first), std::round((double)size.height() - extract(point.second)));
    painter->drawEllipse(p, 2, 2);
}
#endif
