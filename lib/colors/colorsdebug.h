#ifdef CV_DEBUG

#ifndef COLORSDEBUG_H
#define COLORSDEBUG_H

#include <QImage>
#include <QPainter>
#include "frgb.h"
#include "rgbtransformation.h"
#include <functional>

namespace stereoscopic::colors {
    class ColorsDebug
    {
        public:
            static void showAdjustmentDebug(QImage source, QImage reference, QImage result, RgbTransformation *transformation);
            static void showDebugImageHistograms(QImage image, QString label);
            static void showDebugCorrectionCurves(QImage resImage, RgbTransformation *transformation);
            template<typename T>
            static void drawGraph(QVector<FRgb<T>> data, std::function<T(FRgb<T>)> extract, QPainter *painter, QSize size, QColor color);
            static void drawCorrectionPoint(QPair<Rgb32, Rgb32> point, std::function<uchar(Rgb32)> extract, QPainter *painter, QSize size, QColor color);
    };
}

#endif // COLORSDEBUG_H
#endif // CV_DEBUG
