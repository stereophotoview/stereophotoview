#include "colorshelper.h"

#include <math.h>
#include <QElapsedTimer>

using namespace std;
using namespace stereoscopic::colors;

RgbInt ColorsHelper::getAverageRgb(QImage image, int pixelStep)
{
    unsigned long pixelCount = 0;
    RgbULong sum(0, 0, 0);

    auto height = image.height();
    auto width = image.width();

    uchar* sourceLine;
    int offset;
    FRgb<uchar> pixel;
    for(int y = 0; y < height; y += pixelStep)
    {
        sourceLine = image.scanLine(y);
        offset = 0;
        for(int x = 0; x < width; x += pixelStep)
        {
            pixel = FRgb<uchar>::readFromBGRA(sourceLine, offset);
            sum = sum + pixel;
            pixelCount++;
            offset += 4 * pixelStep;
        }
    }
    RgbULong avg = sum / pixelCount;
    return Rgb32(avg.red, avg.green, avg.blue);
}

RgbInt ColorsHelper::getStandardDeviation(QImage image, Rgb32 average, int pixelStep)
{
    int width = image.width();
    int height = image.height();

    RgbULong rowSumOfSquareDifferences;
    RgbULong sumOfRowsDeviations;
    uchar* sourceLine;
    int offset = 0;
    RgbULong diff;
    FRgb<uchar> pixel;
    for(int y = 0; y < height; y += pixelStep)
    {
        sourceLine = image.scanLine(y);
        offset = 0;
        for(int x = 0; x < width; x += pixelStep)
        {
            pixel = FRgb<uchar>::readFromBGRA(sourceLine, offset);
            diff = RgbULong(pixel) - average;
            rowSumOfSquareDifferences = rowSumOfSquareDifferences + (diff * diff);
            if(x >= width - pixelStep)
            {
                sumOfRowsDeviations = sumOfRowsDeviations + (rowSumOfSquareDifferences / width / pixelStep);
                rowSumOfSquareDifferences = RgbULong();
            }
            offset += 4 * pixelStep;
        }
    }
    auto dispersion = sumOfRowsDeviations / height / pixelStep;
    return RgbInt(sqrt(dispersion.red), sqrt(dispersion.green), sqrt(dispersion.blue));
}

QVector<RgbULong> ColorsHelper::getHistogram(QImage image)
{
    QVector<RgbULong> histogram(256);

    auto height = image.height();
    auto width = image.width();
    for(int y = 0; y < height; y++)
    {
        const uchar* sourceLine = image.scanLine(y);
        int offset = 0;
        for(int x = 0; x < width; x++)
        {
            FRgb<uchar> pixel = FRgb<uchar>::readFromBGRA(sourceLine, offset);
            histogram[(int)pixel.red].red++;
            histogram[(int)pixel.green].green++;
            histogram[(int)pixel.blue].blue++;
            offset += 4;
        }
    }
    return histogram;
}
