#ifndef COLORSHELPER_H
#define COLORSHELPER_H

#include "frgb.h"
#include <QVector>
#include <QImage>
#include <functional>
#include <utility>

namespace stereoscopic::colors {

    class ColorsHelper {
        public:
            static RgbInt getAverageRgb(QImage image, int pixelStep = 1);
            static RgbInt getStandardDeviation(QImage image, Rgb32 average, int pixelStep = 1);
            static QVector<RgbULong> getHistogram(QImage image);
    };
}
#endif // COLORSHELPER_H
