#include "frgb.h"

using namespace stereoscopic::colors;

template<typename T>
FRgb<T>::FRgb()
{

}

template<typename T>
FRgb<T>::FRgb(T red, T green, T blue)
{
    this->red = red;
    this->green = green;
    this->blue = blue;
}

template<typename T>
FRgb<T> FRgb<T>::readFromBGRA(const unsigned char *line, int offset)
{
    return FRgb(line[offset + 2], line[offset + 1], line[offset]);
}

template<typename T>
void FRgb<T>::writeToBGRA64(unsigned char *line, int offset)
{
    line[offset] = trancateToByte(blue);
    line[offset + 1] = trancateToByte(green);
    line[offset + 2] = trancateToByte(red);
    line[offset + 3] = 255;
}

template<typename T>
uchar FRgb<T>::redByte()
{
    return trancateToByte(red);
}

template<typename T>
uchar FRgb<T>::greenByte()
{
    return trancateToByte(green);
}

template<typename T>
uchar FRgb<T>::blueByte()
{
    return trancateToByte(blue);
}

template<typename T>
FRgb<T> FRgb<T>::getBalance()
{
    double sum = getSum();
    if(sum == 0)
        return FRgb(0,0,0);
    return FRgb(red / sum, green / sum, blue / sum);
}

template<typename T>
T FRgb<T>::getSum()
{
    return red + green + blue;
}

template<typename T>
FRgb<T> FRgb<T>::toSum(double sum)
{
    FRgb balance = getBalance();
    return FRgb<T>(sum * balance.red, sum * balance.green, sum * balance.blue);
}

template<typename T>
FRgb<T> FRgb<T>::operator+(const FRgb<T> other)
{
    int r = (int)red + other.red;
    int g = (int)green + other.green;
    int b = (int)blue + other.blue;
    auto res = FRgb<T>(r, g, b);
    return res;
}

template<typename T>
FRgb<T> FRgb<T>::operator-(const FRgb<T> other)
{
    return FRgb<T>(red - other.red,
                green - other.green,
                blue - other.blue);
}

template<typename T>
FRgb<T> FRgb<T>::operator/(const FRgb<T> other)
{
    return FRgb<T>(red / other.red,
                green / other.green,
                blue / other.blue);
}

template<typename T>
FRgb<T> FRgb<T>::operator/(int factor)
{
    if(factor == 0)
        return FRgb<T>(255, 255, 255);
    return FRgb<T>(red / factor,
                green / factor,
                blue / factor);
}

template<typename T>
uchar FRgb<T>::trancateToByte(T value)
{
    if(value < 0)
        return 0;
    if(value > 255)
        return 255;
    return value;
}


template<typename T>
FRgb<T> FRgb<T>::operator*(const FRgb<T> other)
{
    return FRgb(red * other.red,
                green * other.green,
                blue * other.blue);
}

template<typename T>
FRgb<T> FRgb<T>::operator*(const T factor)
{
    return FRgb(red * factor,
                green * factor,
                blue * factor);
}

namespace stereoscopic::colors {
    template struct FRgb<unsigned char>;
    template struct FRgb<int>;
    template struct FRgb<unsigned long>;
    template struct FRgb<long>;
    template struct FRgb<double>;
}

