#ifndef FRGB_H
#define FRGB_H

#include <QDebug>

namespace stereoscopic::colors {

    template<typename T>
    struct FRgb
    {
        public:
            T red = 0, green = 0, blue = 0;
            FRgb();
            FRgb(T red, T green, T blue);

            template<typename T2>
            FRgb(FRgb<T2> other)
            {
                red = other.red;
                green = other.green;
                blue = other.blue;
            }
            static inline FRgb readFromBGRA(const unsigned char *line, int offset);
            void inline writeToBGRA64(unsigned char* line, int offset);
            uchar inline redByte();
            uchar inline greenByte();
            uchar inline blueByte();

            /** Получить баланс цветовых компонентов (долю каждого компонента в сумме) */
            FRgb inline getBalance();
            T inline getSum();
            FRgb<T> inline toSum(double sum);

            FRgb<T> operator+(FRgb<T> const other);
            FRgb<T> operator-(FRgb<T> const other);
            FRgb<T> operator*(FRgb<T> const other);
            FRgb<T> operator*(T factor);
            FRgb<T> operator/(FRgb const other);
            FRgb<T> operator/(int factor);

            friend QDebug operator<<(QDebug debug, FRgb<T> rgb)
            {
                return debug << "Rgb(" << rgb.red << rgb.green << rgb.blue << ")";
            }
    private:
            uchar inline trancateToByte(T value);
    };

    typedef FRgb<int> RgbInt;
    typedef FRgb<unsigned long> RgbULong;
    typedef FRgb<uchar> Rgb32;
    typedef FRgb<double> RgbDouble;
}

#endif // FRGB_H
