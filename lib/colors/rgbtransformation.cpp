#include "rgbtransformation.h"
#include <math.h>
#include <QElapsedTimer>

using namespace stereoscopic::colors;

// RgbAverageTransformSmoothed

RgbAverageTransformSmoothed::RgbAverageTransformSmoothed(QImage source, QImage reference, int pixelStep)
{
    this->pixelStep = pixelStep;
    sourceAverage = ColorsHelper::getAverageRgb(source);
    referenceAverage = ColorsHelper::getAverageRgb(reference);
    prepareTransformationTable();
}

QImage RgbTransformation::transformImage(QImage sourceImage)
{
    uchar *sourceLine, *resLine;
    int offset;
    Rgb32 sourcePixel, resPixel;
    QImage resImage(sourceImage.width(), sourceImage.height(), QImage::Format_RGB32);
    for(int y = 0; y < sourceImage.height(); y++)
    {
        sourceLine = sourceImage.scanLine(y);
        resLine = resImage.scanLine(y);
        offset = 0;
        for(int x = 0; x < sourceImage.width(); x++)
        {
            sourcePixel = Rgb32::readFromBGRA(sourceLine, offset);
            resPixel = transformPixel(sourcePixel);
            resPixel.writeToBGRA64(resLine, offset);
            offset += 4;
        }
    }
    return resImage;
}

Rgb32 RgbTransformation::transformPixel(Rgb32 source)
{
    return Rgb32(transformationRedTable[(int)source.red],
                transformationGreenTable[(int)source.green],
                transformationBlueTable[(int)source.blue]);
}

void RgbTransformation::prepareTransformationTable()
{
    for(int i = 0; i < 256; i++)
    {
        Rgb32 source(i, i, i);
        auto res = RgbInt(transformComponent(source, [](const RgbInt &rgb){ return rgb.red; }),
                        transformComponent(source, [](const RgbInt &rgb){ return rgb.green; }),
                        transformComponent(source, [](const RgbInt &rgb){ return rgb.blue; }));
        transformationRedTable[i] = res.redByte();
        transformationGreenTable[i] = res.greenByte();
        transformationBlueTable[i] = res.blueByte();
    }
}

QVector<QPair<Rgb32, Rgb32>> RgbAverageTransformSmoothed::getKeyPoints()
{
    QVector<QPair<Rgb32, Rgb32>> res;
    res.append(QPair<Rgb32, Rgb32>(sourceAverage, referenceAverage));
    return res;
}

int RgbAverageTransformSmoothed::transformComponent(const Rgb32 &sourceRgb, std::function<int (const RgbInt &)> extractValue)
{
    // use quadratic interpolation
    auto source = extractValue(sourceRgb);
    auto referenceAvg = extractValue(referenceAverage);
    auto sourceAvg = extractValue(sourceAverage);
    double dres = referenceAvg * (source*(source - 255.0)) / (sourceAvg*(sourceAvg-255.0)) + 255.0 * (source*(source-sourceAvg))/(255.0*(255.0-sourceAvg));
    return std::round(dres);
}

// RgbStandardDeviationTransformation

RgbStandardDeviationTransformation::RgbStandardDeviationTransformation(QImage source, QImage reference, int pixelStep)
{
    this->pixelStep = pixelStep;
    QElapsedTimer timer;
    timer.start();
    sourceAverage = ColorsHelper::getAverageRgb(source, pixelStep);
    referenceAverage = ColorsHelper::getAverageRgb(reference, pixelStep);
    //qDebug() << "getAverageRgb" << timer.elapsed();
    timer.restart();
    auto sourceDeviation = ColorsHelper::getStandardDeviation(source, sourceAverage, pixelStep);
    auto referenceDeviation = ColorsHelper::getStandardDeviation(reference, referenceAverage, pixelStep);
    //qDebug() << "getStandardDeviation" << timer.elapsed();
    timer.restart();
    sourceMin = sourceAverage - sourceDeviation;
    sourceMax = sourceAverage + sourceDeviation;
    referenceMin = referenceAverage - referenceDeviation;
    referenceMax = referenceAverage + referenceDeviation;
    //qDebug() << "minMax" << timer.elapsed();
    timer.restart();
    prepareTransformationTable();
    //qDebug() << "prepareTransformationTable" << timer.elapsed();
}

int RgbStandardDeviationTransformation::transformComponent(const Rgb32 &sourcePixel, std::function<int (const RgbInt &)> extractValue)
{
    // Use linear interpolation

    double x = extractValue(sourcePixel);
    double x0, x1;
    double y0, y1;

    if(x > extractValue(sourceMax))
    {
        x0 = extractValue(sourceMax);
        y0 = extractValue(referenceMax);
        x1 = 255;
        y1 = 255;
    }
    else if(x > extractValue(sourceMin))
    {
        x0 = extractValue(sourceMin);
        y0 = extractValue(referenceMin);
        x1 = extractValue(sourceMax);
        y1 = extractValue(referenceMax);
    }
    else
    {
        x0 = 0;
        y0 = 0;
        x1 = extractValue(sourceMin);
        y1 = extractValue(referenceMin);
    }
    return std::round(y0 + ((x - x0) * (y1 - y0)) / (x1 - x0));
}

QVector<QPair<Rgb32, Rgb32>> RgbStandardDeviationTransformation::getKeyPoints()
{
    QVector<QPair<Rgb32, Rgb32>> points;
    points.append(QPair<Rgb32, Rgb32>(sourceMin, referenceMin));
    points.append(QPair<Rgb32, Rgb32>(sourceAverage, referenceAverage));
    points.append(QPair<Rgb32, Rgb32>(sourceMax, referenceMax));
    return points;
}
