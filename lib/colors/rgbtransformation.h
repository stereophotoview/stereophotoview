#ifndef RGBTRANSFORMATION_H
#define RGBTRANSFORMATION_H

#include <QVector>
#include <QPair>
#include <QImage>
#include "frgb.h"
#include "colorshelper.h"
#include <functional>

namespace stereoscopic::colors {
    class RgbTransformation {
        public:
            QImage transformImage(QImage sourceImage);
            inline Rgb32 transformPixel(Rgb32 source);
            virtual QVector<QPair<Rgb32, Rgb32>> getKeyPoints() = 0;
        protected:
            void prepareTransformationTable();
            inline virtual int transformComponent(const Rgb32 &source, std::function<int(const RgbInt &)> extractValue) = 0;
        private:
            uchar transformationRedTable[256];
            uchar transformationGreenTable[256];
            uchar transformationBlueTable[256];
    };

    class RgbAverageTransformSmoothed : public RgbTransformation {
        public:
            RgbAverageTransformSmoothed(QImage source, QImage reference, int pixelStep);
            virtual QVector<QPair<Rgb32, Rgb32>> getKeyPoints();
        protected:
            inline virtual int transformComponent(const Rgb32 &sourceRgb, std::function<int(const RgbInt &)> extractValue);
        private:
            int pixelStep = 1;
            Rgb32 referenceAverage, sourceAverage;
    };

    class RgbStandardDeviationTransformation: public RgbTransformation {
        public:
            RgbStandardDeviationTransformation(QImage source, QImage reference, int pixelStep = 1);
            virtual QVector<QPair<Rgb32, Rgb32>> getKeyPoints();
        protected:
            inline virtual int transformComponent(const Rgb32 &sourcePixel, std::function<int (const RgbInt &)> extractValue);
        private:
            int pixelStep = 1;
            RgbInt sourceAverage, sourceMin, sourceMax, referenceAverage, referenceMin, referenceMax;
    };
}

#endif // RGBTRANSFORMATION_H
