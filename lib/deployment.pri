include(../common_deployment.pri)

unix:!android {
    CONFIG(deploy) {
        #RESOURCES += $$PWD/../deploy.qrc
        PREFIX=$$OUT_PWD/../out/stereophotoview-$$APP_VERSION-$$QMAKE_HOST.arch-linux
    }
    isEmpty(PREFIX) {
        PREFIX = /usr
    }
    isEmpty(LIB) {
        LIB = lib
    }
    target.path = $$PREFIX/$$LIB
    INSTALLS += target
}

windows {
    target.path = $$BINPATH
    INSTALLS += target
    installer.depends = install
    QMAKE_EXTRA_TARGETS = install installer
}
