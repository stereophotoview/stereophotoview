#include "exifextension.h"

#include <QBuffer>
#include <QImage>
#include <QImage>
#include <QDateTime>
#include "imagehelper.h"

using namespace stereoscopic::fileFormats::jpeg;

ExifExtension::ExifExtension()
{

}

bool ExifExtension::load(QDataStream &baseStream)
{
    ushort fullLength;
    auto initPos = baseStream.device()->pos();
    baseStream >> fullLength;
    char formatId[9] = {0};
    baseStream.readRawData((char*)formatId, 6);
    if(strcmp(formatId, "Exif\0\0") != 0)
    {
        baseStream.device()->seek(initPos);
        return false;
    }
    ushort bufSize = fullLength - 2 - 6;
    char* buf = new char[bufSize];
    baseStream.readRawData(buf, bufSize);
    data_ = QByteArray(buf, bufSize);
    delete[] buf;
    return isLoaded_ = true;
}

void ExifExtension::save(QDataStream &baseStream)
{
    QBuffer buf;
    buf.open(QBuffer::ReadWrite);
    // Записываем данные в baseStream
    ushort length = (ushort)data_.length() + 2 + 6;
    baseStream << length;
    baseStream.writeRawData("Exif\0\0", 6);
    baseStream.writeRawData(data_.data(), data_.length());
}

QExifImageHeader &ExifExtension::header()
{
    QBuffer buf(&data_);
    buf.open(QBuffer::ReadOnly);
    QDataStream stream(&buf);
    header_.read(stream.device());
    return header_;
}

void ExifExtension::setHeader(QExifImageHeader *src, QImage *image)
{
    header_.clear();

    // Копируем значения
    setValues(src, src->imageTags());
    setValues(src, src->extendedTags());
    setValues(src, src->gpsTags());

    // Обновляем значения, связанные с параметрами изображения, которые могли измениться в процессе редактирования
    header_.setValue(QExifImageHeader::DateTime, QExifValue(QDateTime::currentDateTime().toString("yyyy:MM:dd hh:mm:ss")));
    header_.setValue(QExifImageHeader::ProcessingSoftware, QExifValue(QString("StereoPhotoView Ver") + APP_VERSION));
    header_.setValue(QExifImageHeader::ImageWidth, QExifValue(image->width()));
    header_.setValue(QExifImageHeader::ImageLength, QExifValue(image->height()));
    header_.setValue(QExifImageHeader::ColorSpace, QExifValue(1));
    header_.remove(QExifImageHeader::Orientation);
    header_.remove(QExifImageHeader::StripOffsets);

    // Устанавливаем уменьшенную копию изображения
    auto thumbnail = ImageHelper::createThumbnail(image, QSize(160, 160));
    header_.setThumbnail(thumbnail);
    data_.clear();
    QBuffer buf(&data_);
    buf.open(QBuffer::ReadWrite);
    QDataStream stream(&buf);
    header_.write(stream.device());
}

template<typename T>
void ExifExtension::setValues(QExifImageHeader *h, QList<T> tags)
{
    foreach(auto tag, tags)
        header_.setValue(tag, h->value(tag));
}
