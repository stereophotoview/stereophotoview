#ifndef EXIFEXTENSION_H
#define EXIFEXTENSION_H

#include <QImage>

#include "jpegmetadataextension.h"
#include "../jpeg/qexifimageheader.h"
#include "exifheaders.h"
#include "lib_global.h"

namespace stereoscopic::fileFormats::jpeg
{
    /**
     * @brief Класс для хранения, сериализации и десериализации Exif расширения
     */
    class LIBSHARED_EXPORT ExifExtension : public JpegMetadataExtension
    {
        public:
            ExifExtension();

            /// Возвращает призак того, что расширение было загружено
            virtual bool isLoaded(){ return isLoaded_; }

            /**
             * @brief Загружает расширение из потока данных
             * @param baseStream Поток данных
             * @return true, если загрузка произошла
             */
            virtual bool load(QDataStream &baseStream);

            /**
             * @brief Сохраняет расширение в поток данных
             * @param stream Поток данных
             */
            virtual void save(QDataStream &stream);

            /**
             * @brief Возвращает Exif заголовки
             * @return Exif заголовки
             */
            QExifImageHeader &header();

            /**
             * @brief Записывает некотрые EXIF заголовки в JPEG файл
             * @param src Exif заголовки
             * @param image Изображение JPEG
             */
            void setHeader(QExifImageHeader *src, QImage *image);

        private:
            QByteArray data_;
            QExifImageHeader header_;
            bool isLoaded_ = false;
            template<typename T>
            void setValues(QExifImageHeader *h, QList<T> tags);
    };
}
#endif // EXIFEXTENSION_H
