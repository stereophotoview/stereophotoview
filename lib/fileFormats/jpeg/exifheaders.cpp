#include "exifheaders.h"

using namespace stereoscopic::fileFormats::jpeg;

ExifHeaders::ExifHeaders(QObject *parent) : QObject(parent)
{

}

ExifHeaders::ExifHeaders(const QExifImageHeader &header) : ExifHeaders()
{
    header_ = new QExifImageHeader(header);
    fillAttributes();
}

QVariant ExifHeaders::attributes()
{
    return attributes_;
}

void ExifHeaders::fillAttributes()
{
    attrs_.clear();
    fillAttributesTags<QExifImageHeader::ImageTag>(header_->imageTags());
    fillAttributesTags<QExifImageHeader::ExifExtendedTag>(header_->extendedTags());
    fillAttributesTags<QExifImageHeader::GpsTag>(header_->gpsTags());
    attributes_ = QVariant::fromValue(attrs_);
}

template<typename T>
void ExifHeaders::fillAttributesTags(QList<T> tags)
{
    QList<T> tagsSorted(tags);
    std::sort(tagsSorted.begin(), tagsSorted.end());

    foreach(auto tag, tagsSorted)
    {
        QString name = ExifTagnames::instance().name(tag);
        QExifValue value = header_->value(tag);
        QExifValue::Type type = value.type();
        QStringList stringValues;
        switch (type) {
            case QExifValue::Byte:
                foreach(auto bv, value.toByteVector())
                    stringValues.append(QString::number(bv));
                break;
            case QExifValue::Undefined:
            case QExifValue::Ascii:
                stringValues.append(value.toString());
                break;
            case QExifValue::Short:
                foreach(auto shv, value.toShortVector())
                    stringValues.append(QString::number(shv));
                break;
            case QExifValue::Long:
                foreach(auto lv, value.toLongVector())
                    stringValues.append(QString::number(lv));
                break;
            case QExifValue::Rational:
                foreach(auto rv, value.toRationalVector())
                    stringValues.append(QString("%1/%2").arg(rv.first).arg(rv.second));
                break;
            case QExifValue::SignedLong:
                foreach(auto lv, value.toSignedLongVector())
                    stringValues.append(QString::number(lv));
                break;
            case QExifValue::SignedRational:
                foreach(auto rv, value.toSignedRationalVector())
                    stringValues.append(QString("%1/%2").arg(rv.first).arg(rv.second));
                break;
        }
        QString stringValue = stringValues.join(" ");
        if(name.length() > 0 && stringValue.length() > 0)
            attrs_.append(new AttributeValue(name, stringValue));
    }
}
