#ifndef EXIFHEADERS_H
#define EXIFHEADERS_H

#include <QObject>
#include <QHash>
#include <QList>
#include <QPair>

#include "qexifimageheader.h"
#include "lib_global.h"
namespace stereoscopic::fileFormats::jpeg {
    /**
     * @brief Класс для хранения отображаемых названий exif тегов
     */
    class LIBSHARED_EXPORT  ExifTagnames : public QObject
    {
            Q_OBJECT
        private:
            ExifTagnames(QObject* o = nullptr) : QObject(o) {}
            ExifTagnames( const ExifTagnames&);
            ExifTagnames& operator=( ExifTagnames& );

            const QHash<QExifImageHeader::ImageTag, QString> imageTagNames_ = {
                { QExifImageHeader::ImageWidth, tr("Image width") },
                { QExifImageHeader::ImageLength, tr("Image length") },
                { QExifImageHeader::BitsPerSample, tr("Bits per sample") },
                { QExifImageHeader::Compression, tr("Compression") },
                { QExifImageHeader::PhotometricInterpretation, tr("Photometric interpretation") },
                { QExifImageHeader::Orientation, tr("Orientation") },
                { QExifImageHeader::SamplesPerPixel, tr("Samples per pixel") },
                { QExifImageHeader::PlanarConfiguration, tr("Planar configuration") },
                { QExifImageHeader::YCbCrSubSampling, tr("YCbCr SubSampling") },
                { QExifImageHeader::XResolution, tr("X resolution") },
                { QExifImageHeader::YResolution, tr("Y resolution") },
                { QExifImageHeader::ResolutionUnit, tr("Resolution unit") },
                { QExifImageHeader::StripOffsets, tr("Strip offsets") },
                { QExifImageHeader::RowsPerStrip, tr("Rows per strip") },
                { QExifImageHeader::StripByteCounts, tr("Strip byte counts") },
                { QExifImageHeader::TransferFunction, tr("Transfer function") },
                { QExifImageHeader::WhitePoint, tr("White point") },
                { QExifImageHeader::PrimaryChromaciticies, tr("Primary chromaciticies") },
                { QExifImageHeader::YCbCrCoefficients, tr("YCbCr coefficients") },
                { QExifImageHeader::YCbCrPositioning, tr("YCbCr positioning") },
                { QExifImageHeader::ReferenceBlackWhite, tr("Reference black white") },
                { QExifImageHeader::DateTime, tr("Date time") },
                { QExifImageHeader::ImageDescription, tr("Image description") },
                { QExifImageHeader::Make, tr("Make") },
                { QExifImageHeader::Model, tr("Model") },
                { QExifImageHeader::Software, tr("Software") },
                { QExifImageHeader::ProcessingSoftware, tr("Processing software") },
                { QExifImageHeader::Artist, tr("Artist") },
                { QExifImageHeader::Copyright, tr("Copyright") },
            };

            const QHash<QExifImageHeader::ExifExtendedTag, QString> extendedTagNames_ = {
                { QExifImageHeader::ExifVersion, tr("Exif version") },
                { QExifImageHeader::FlashPixVersion, tr("FlashPix version") },
                { QExifImageHeader::ColorSpace, tr("ColorSpace") },
                { QExifImageHeader::ComponentsConfiguration, tr("Components configuration") },
                { QExifImageHeader::CompressedBitsPerPixel, tr("Compressed bits per pixel") },
                { QExifImageHeader::PixelXDimension, tr("Pixel X dimension") },
                { QExifImageHeader::PixelYDimension, tr("Pixel Y dimension") },
                { QExifImageHeader::MakerNote, tr("Maker note") },
                { QExifImageHeader::UserComment, tr("User comment") },
                { QExifImageHeader::RelatedSoundFile, tr("Related soundFile") },
                { QExifImageHeader::DateTimeOriginal, tr("Date time original") },
                { QExifImageHeader::DateTimeDigitized, tr("Date time digitized") },
                { QExifImageHeader::SubSecTime, tr("SubSec time") },
                { QExifImageHeader::SubSecTimeOriginal, tr("SubSec time original") },
                { QExifImageHeader::SubSecTimeDigitized, tr("SubSec time digitized") },
                { QExifImageHeader::ImageUniqueId, tr("Image uniqueId") },
                { QExifImageHeader::ExposureTime, tr("Exposure time") },
                { QExifImageHeader::FNumber, tr("F number") },
                { QExifImageHeader::ExposureProgram, tr("Exposure program") },
                { QExifImageHeader::SpectralSensitivity, tr("Spectral sensitivity") },
                { QExifImageHeader::ISOSpeedRatings, tr("ISO speed ratings") },
                { QExifImageHeader::Oecf, tr("Oecf") },
                { QExifImageHeader::ShutterSpeedValue, tr("Shutter dpeed value") },
                { QExifImageHeader::ApertureValue, tr("Aperture value") },
                { QExifImageHeader::BrightnessValue, tr("Brightness value") },
                { QExifImageHeader::ExposureBiasValue, tr("Exposure bias value") },
                { QExifImageHeader::MaxApertureValue, tr("Max aperture value") },
                { QExifImageHeader::SubjectDistance, tr("Subject distance") },
                { QExifImageHeader::MeteringMode, tr("Metering mode") },
                { QExifImageHeader::LightSource, tr("Light source") },
                { QExifImageHeader::Flash, tr("Flash") },
                { QExifImageHeader::FocalLength, tr("Focal length") },
                { QExifImageHeader::SubjectArea, tr("Subject area") },
                { QExifImageHeader::FlashEnergy, tr("Flash energy") },
                { QExifImageHeader::SpatialFrequencyResponse, tr("Spatial frequency response") },
                { QExifImageHeader::FocalPlaneXResolution, tr("Focal plane X resolution") },
                { QExifImageHeader::FocalPlaneYResolution, tr("Focal Plane Y resolution") },
                { QExifImageHeader::FocalPlaneResolutionUnit, tr("Focal plane resolutionUnit") },
                { QExifImageHeader::SubjectLocation, tr("Subject location") },
                { QExifImageHeader::ExposureIndex, tr("Exposure index") },
                { QExifImageHeader::SensingMethod, tr("Sensing method") },
                { QExifImageHeader::FileSource, tr("File source") },
                { QExifImageHeader::SceneType, tr("Scene type") },
                { QExifImageHeader::CfaPattern, tr("Cfa pattern") },
                { QExifImageHeader::CustomRendered, tr("Custom rendered") },
                { QExifImageHeader::ExposureMode, tr("Exposure mode") },
                { QExifImageHeader::WhiteBalance, tr("White balance") },
                { QExifImageHeader::DigitalZoomRatio, tr("Digital zoom ratio") },
                { QExifImageHeader::FocalLengthIn35mmFilm, tr("Focal kength in 35mm film") },
                { QExifImageHeader::SceneCaptureType, tr("Scene capture type") },
                { QExifImageHeader::GainControl, tr("Gain control") },
                { QExifImageHeader::Contrast, tr("Contrast") },
                { QExifImageHeader::Saturation, tr("Saturation") },
                { QExifImageHeader::Sharpness, tr("Sharpness") },
                { QExifImageHeader::DeviceSettingDescription, tr("Device setting description") },
                { QExifImageHeader::SubjectDistanceRange, tr("Subject distance range") },
            };

            const QHash<QExifImageHeader::GpsTag, QString> gpsTagNames_ = {
                { QExifImageHeader::GpsVersionId, tr("Gps version Id") },
                { QExifImageHeader::GpsLatitudeRef, tr("Gps latitude ref") },
                { QExifImageHeader::GpsLatitude, tr("Gps latitude") },
                { QExifImageHeader::GpsLongitudeRef, tr("Gps longitude ref") },
                { QExifImageHeader::GpsLongitude, tr("Gps longitude") },
                { QExifImageHeader::GpsAltitudeRef, tr("Gps altitude ref") },
                { QExifImageHeader::GpsAltitude, tr("Gps altitude") },
                { QExifImageHeader::GpsTimeStamp, tr("Gps TimeStamp") },
                { QExifImageHeader::GpsSatellites, tr("Gps satellites") },
                { QExifImageHeader::GpsStatus, tr("Gps status") },
                { QExifImageHeader::GpsMeasureMode, tr("Gps measure mode") },
                { QExifImageHeader::GpsDop, tr("Gps dop") },
                { QExifImageHeader::GpsSpeedRef, tr("Gps dpeed Ref") },
                { QExifImageHeader::GpsSpeed, tr("Gps speed") },
                { QExifImageHeader::GpsTrackRef, tr("Gps track ref") },
                { QExifImageHeader::GpsTrack, tr("Gps track") },
                { QExifImageHeader::GpsImageDirectionRef, tr("Gps image direction ref") },
                { QExifImageHeader::GpsImageDirection, tr("Gps image direction") },
                { QExifImageHeader::GpsMapDatum, tr("Gps map datum") },
                { QExifImageHeader::GpsDestLatitudeRef, tr("Gps dest latitude ref") },
                { QExifImageHeader::GpsDestLatitude, tr("Gps dest latitude") },
                { QExifImageHeader::GpsDestLongitudeRef, tr("Gps dest longitude ref") },
                { QExifImageHeader::GpsDestLongitude, tr("Gps dest longitude") },
                { QExifImageHeader::GpsDestBearingRef, tr("Gps dest bearing ref") },
                { QExifImageHeader::GpsDestBearing, tr("Gps dest bearing") },
                { QExifImageHeader::GpsDestDistanceRef, tr("Gps dest distance ref") },
                { QExifImageHeader::GpsDestDistance, tr("Gps dest distance") },
                { QExifImageHeader::GpsProcessingMethod, tr("Gps processing method") },
                { QExifImageHeader::GpsAreaInformation, tr("Gps area information") },
                { QExifImageHeader::GpsDateStamp, tr("Gps DateStamp") },
                { QExifImageHeader::GpsDifferential, tr("Gps differential") },
            };

        public:

            /**
             * @brief Возвращает наименование тега
             * @param tag Тег
             * @return Локализованное наименование тега
             */
            QString name(QExifImageHeader::ImageTag tag)
            {
                return imageTagNames_[tag];
            }

            /**
             * @brief Возвращает наименование тега
             * @param tag Тег
             * @return Локализованное наименование тега
             */
            QString name(QExifImageHeader::ExifExtendedTag tag)
            {
                return extendedTagNames_[tag];
            }

            /**
             * @brief Возвращает наименование тега
             * @param tag Тег
             * @return Локализованное наименование тега
             */
            QString name(QExifImageHeader::GpsTag tag)
            {
                return gpsTagNames_[tag];
            }

            /**
             * @brief Возвращает единственный экземпляр класса
             * @return единственный экземпляр класса
             */
            static ExifTagnames& instance()
            {
                static ExifTagnames  instance;
                return instance;
            }
    };

    /**
     * @brief Класс для представления пары тег-значение для отображения в интерфейсе пользователя
     */
    class LIBSHARED_EXPORT AttributeValue : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QString attribute READ attribute)
            Q_PROPERTY(QString value READ value)
        public:
            AttributeValue(QObject *parent = nullptr) : QObject(parent)
            {

            }

            AttributeValue(QString attribute, QString value) : QObject()
            {
                attribute_ = attribute;
                value_ = value;
            }

            AttributeValue(const AttributeValue &source) : QObject()
            {
                attribute_ = source.attribute_;
                value_ = source.value_;
            }

            /// Наименование тега
            QString attribute(){ return attribute_; }

            /// Значение тега
            QString value(){ return value_; }

            AttributeValue& operator=(const AttributeValue &source)
            {
                attribute_ = source.attribute_;
                value_ = source.value_;
                return *this;
            }

        private:
            QString attribute_, value_;

    };

    /**
     * @brief Класс для представления заголовков Exif в интерфейсе пользователя
     */
    class LIBSHARED_EXPORT  ExifHeaders : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QVariant attributes READ attributes)

        public:
            ExifHeaders(QObject *parent = nullptr);
            ExifHeaders(const QExifImageHeader&);

            /// Фозвращает список атрибутов пользователя
            QVariant attributes();

            ~ExifHeaders()
            {
                foreach(auto attr, attrs_)
                    delete attr;
                if(header_)
                    delete header_;
                attrs_.clear();
            }

            QExifImageHeader* header(){ return header_; }

        signals:

        public slots:

        private:
            QExifImageHeader* header_ = new QExifImageHeader();
            QList<QObject*> attrs_;
            QVariant attributes_;

            void fillAttributes();
            template<typename T>
            void fillAttributesTags(QList<T> tags);
    };

}

Q_DECLARE_METATYPE(stereoscopic::fileFormats::jpeg::AttributeValue)

#endif // EXIFHEADERS_H
