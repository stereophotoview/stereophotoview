#include "jpegmetadata.h"

// http://gvsoft.no-ip.org/exif/exif-explanation.html

#include <QDataStream>
#include <QDebug>
#include <QImage>
#include <imagehelper.h>

using namespace stereoscopic::fileFormats::jpeg;

bool JpegMetadata::loadFromData(QByteArray &data)
{
    parseError_ = "";
    ushort s;

    ushort len;

    QDataStream stream(data);
    stream >> s;
    if(s != SOI)
    {
        parseError_ = "SOI marker expected, but found: " + QString::number((uint)s, 16);
        return false;
    }
    while(!stream.atEnd())
    {
        stream >> s;

#ifdef TRACE
        auto pos1 = stream.device()->pos() - 2;
        qDebug()  << QString::number(pos1, 16) << ": marker: " << QString::number(s, 16);
#endif
        switch(s)
        {
            case EOI:
            case SOFO:
            case DHT:
            case SOS:
                return true;
            case SOI:
                return false;
            default:
                if((ushort)(s >> 8) == (ushort)0xFF)
                {
                    ushort v = s & (ushort)0x00FF;
                    if(s == CMT || ( v >= 0xE1 && v <= 0xEF ))
                    {
                        // Если это комментарий или сегмент E0 - EF, сохраняем в appData
                        loadExtension(s, stream);
                    }
                    else
                    {
                        // Иначе пропускаем
                        stream >> len;
                        stream.skipRawData(len - 2);
                    }
                }
                else
                {
                    // Следующий байт оказался не маркером
                    parseError_ = "bad marker " + QString::number((uint)s, 16);
                    return false;
                }
                break;
        }
    }

    return true;
}

void JpegMetadata::save(QBuffer &buf)
{
    QDataStream stream(&buf);
    stream << SOI;

    foreach(auto ext, appData_)
    {
        // Тег
        stream << ext.first;
        // Расширение
        ext.second->save(stream);
    }
}

JpegMetadata::~JpegMetadata()
{
    foreach(auto pair, appData_)
        delete pair.second;
    appData_.clear();
}

void JpegMetadata::appendExtension(ushort marker, JpegMetadataExtension *extension)
{
    appData_.append(QPair<ushort, JpegMetadataExtension*>(marker, extension));
}

QExifImageHeader& JpegMetadata::exifHeader()
{
    QList<ExifExtension*> exif = getExtensions<ExifExtension>(APP1);
    if(!exif.empty())
    {
        return exif.first()->header();
    }
    return emptyHeader_;
}

void JpegMetadata::setExifHeaders(QExifImageHeader *header, QImage* image)
{
    //todo теряются gps данные
    QList<ExifExtension*> exif = getExtensions<ExifExtension>(APP1);
    ExifExtension* ext = exif.empty() ? new ExifExtension() : exif.first();
    if(exif.empty())
        appendExtension(APP1, ext);
    ext->setHeader(header, image);
}

void JpegMetadata::loadExtension(ushort marker, QDataStream &stream)
{
    QList<JpegMetadataExtension *> list = createExtensionsByMarker(marker);
    foreach(JpegMetadataExtension *ext, list)
    {
        if(ext->load(stream))
            break;
    }
    JpegMetadataExtension *loadedExt = nullptr;
    foreach(JpegMetadataExtension *ext, list)
    {
        if(ext->isLoaded())
            loadedExt = ext;
        else
            delete ext;
    }
    appData_.append(QPair<ushort, JpegMetadataExtension*>(marker, loadedExt));
}
