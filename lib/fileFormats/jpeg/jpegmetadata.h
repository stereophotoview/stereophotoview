#ifndef JPEGMETADATA_H
#define JPEGMETADATA_H

#include <QDataStream>
#include <QString>
#include <QList>
#include <QBuffer>
#include <QMap>
#include <QPair>
#include <QHash>

#include "jpegmetadataextension.h"

#include "../mpo/mpoextension.h"
#include "../jps/stereoscopicdescriptor.h"
#include "rawjpegextension.h"
#include "exifextension.h"
#include "exifheaders.h"
#include "lib_global.h"

namespace stereoscopic::fileFormats::jpeg {

    const ushort
        SOI = 0xFFD8,   // Start of image marker
        EOI = 0xFFD9,   // End of image marker
        DQT = 0xFFDB,   // Таблица квантирования
        SOFO = 0xFFC0,  // Baseline DCT
        DHT = 0xFFC4,   // Таблица Хаффмана
        SOS = 0xFFDA,   // Start of Scan
        CMT = 0xFFFe,   // Comment
        APP1 = 0xFFE1,  // Application marker E1
        APP2 = 0xFFE2,  // Application marker E2
        APP3 = 0xFFE3;  // Application marker E3

    /**
     * @brief Класс для хранения, загрузки и сохранения метаданных JPEG файла
     */
    class LIBSHARED_EXPORT JpegMetadata
    {
        public:

            /// Возвращает последнюю ошибку разбора металанных
            QString parseError(){ return parseError_; }

            /**
             * @brief Загружет метаданные из массива байт
             * @param data Данные
             * @return True, если данные успешно разобраны
             */
            bool loadFromData(QByteArray &data);

            /**
             * @brief Сохраняет метаданные в буфер
             * @param buf
             */
            void save(QBuffer &buf);

            ~JpegMetadata();

            template<class T>
            /**
             * @brief Возвращает расширения с данным тегом
             * @param marker маркер тега
             * @return Расширения данного типа
             */
            QList<T*> getExtensions(ushort marker)
            {
                QList<T*> res;
                foreach(auto pair, appData_)
                {
                    if(pair.first == marker)
                    {
                        T* o = dynamic_cast<T*>(pair.second);
                        if(o) res.append(o);
                    }
                }
                return res;
            }

            /**
             * @brief Добавить расширение (секцию APP<N>)
             * @param marker Маркер расширения
             * @param extension Расширение
             */
            void appendExtension(ushort marker, JpegMetadataExtension* extension);

            /// Возвращает заголовки Exif метеданных
            QExifImageHeader &exifHeader();

            /**
             * @brief Устанавливает заголовки Exif метаданных
             * @param image
             */
            void setExifHeaders(QExifImageHeader *, QImage *image);

        private:
            QString parseError_;
            QList<QPair<ushort, JpegMetadataExtension*>> appData_;
            void loadExtension(ushort marker, QDataStream &stream);
            QExifImageHeader emptyHeader_;

            QList<JpegMetadataExtension*> createExtensionsByMarker(ushort marker)
            {
                QList<JpegMetadataExtension *> list;
                if(marker == APP1)
                    list.append((JpegMetadataExtension *)new ExifExtension());
                if(marker == APP2)
                    list.append((JpegMetadataExtension *)new stereoscopic::fileFormats::mpo::MpoExtension());
                if(marker == APP3)
                    list.append(new stereoscopic::fileFormats::jps::StereoscopicDescriptor());
                list.append((JpegMetadataExtension *)new RawJpegExtension());
                return list;
            }
    };
}

#endif // JPEGMETADATA_H
