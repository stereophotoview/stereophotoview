#ifndef JPEGMETADATAEXTENSION_H
#define JPEGMETADATAEXTENSION_H

#include <QDataStream>
#include <QByteArray>
#include "lib_global.h"

namespace stereoscopic::fileFormats::jpeg
{
    /**
     * @brief Базовый класс для представления расширения метаданных JPEG
     */
    class LIBSHARED_EXPORT JpegMetadataExtension
    {
        public:
            virtual ~JpegMetadataExtension();

            /// Возвращает признак того, что расширение было загружено
            virtual bool isLoaded() = 0;

            /**
             * @brief Загружает расширение метаданных из потока данных
             * @param baseStream Поток данных
             * @return true, если расширение было загружено
             */
            virtual bool load(QDataStream &baseStream) = 0;

            /**
             * @brief Сохраняет расширение метаданных JPEG в поток данных
             * @param stream Поток данных
             */
            virtual void save(QDataStream &stream) = 0;

    };
}
#endif // JPEGMETADATAEXTENSION_H
