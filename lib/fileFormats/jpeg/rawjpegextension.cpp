#include "rawjpegextension.h"

using namespace stereoscopic::fileFormats::jpeg;

RawJpegExtension::RawJpegExtension()
{

}

RawJpegExtension::RawJpegExtension(QByteArray data)
{
    data_ = data;
}

bool RawJpegExtension::load(QDataStream &stream)
{
    ushort length;
    stream >> length;
    ushort bufSize = length - 2;
    char* buf = new char[bufSize];
    stream.readRawData(buf, bufSize);
    data_ = QByteArray(buf, bufSize);
    isLoaded_ = true;
    delete[] buf;
    return true;
}

void RawJpegExtension::save(QDataStream &stream)
{
    ushort length = (ushort)data_.count();
    stream << length;
    uchar c;
    for(int i = 0; i < length; i++)
    {
        c = (uchar)data_[i];
        stream << c;
    }
}
