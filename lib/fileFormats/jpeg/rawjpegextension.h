#ifndef RAWJPEGEXTENSION_H
#define RAWJPEGEXTENSION_H

#include "jpegmetadataextension.h"
#include "lib_global.h"

namespace stereoscopic::fileFormats::jpeg
{
    /**
     * @brief Расширение для запоминания сегмента jpeg метеданных без разбора их внутренней структуры
     */
    class LIBSHARED_EXPORT RawJpegExtension : public JpegMetadataExtension
    {
        public:
            RawJpegExtension();
            RawJpegExtension(QByteArray data);

            /// Возвращает признак того, что расширение было загружено
            virtual bool isLoaded(){ return isLoaded_; }

            /**
             * @brief Загружает расширение метаданных из потока данных
             * @param baseStream Поток данных
             * @return true, если расширение было загружено
             */
            virtual bool load(QDataStream &baseStream);

            /**
             * @brief Сохраняет расширение метаданных JPEG в поток данных
             * @param stream Поток данных
             */
            virtual void save(QDataStream &stream);

            /// Сырые данные сегмента метаданных
            virtual QByteArray & data(){ return data_; }
        private:
            QByteArray data_;
            bool isLoaded_ = false;
    };
}
#endif // RAWJPEGEXTENSION_H
