#include "stereoscopicdescriptor.h"

using namespace stereoscopic::fileFormats::jps;

StereoscopicDescriptor::StereoscopicDescriptor()
{

}

bool StereoscopicDescriptor::load(QDataStream &baseStream)
{
    ushort fullLength;
    auto initPos = baseStream.device()->pos();
    baseStream >> fullLength;

    // check format identifier
    char formatId[9] = {0};
    baseStream.readRawData((char*)formatId, 8);
    if(strcmp(formatId, "_JPSJPS_") != 0)
    {
        baseStream.device()->seek(initPos);
        return false;
    }

    ushort bufSize = fullLength - 10;
    char* buf = new char[bufSize];
    baseStream.readRawData(buf, bufSize);
    QByteArray data(buf, bufSize);
    delete[] buf;

    QDataStream stream(data);

    ushort blockSize;
    stream >> blockSize;

    if(blockSize != 4)
    {
        baseStream.device()->seek(initPos);
        return false;
    }

    // MEDIA TYPE SPECIFIC VALUES
    uchar values[3];
    stream.readRawData((char*)values, 3);

    uchar s;
    stream >> s;

    mediaType_ = (MediaType)s;
    switch(mediaType_)
    {
        case MediaType::Monoscopic:
            monoValues_.display = (MonoDisplay)values[2];
            monoValues_.flags = (MiscFlags)values[1];
            break;
        case MediaType::Stereoscopic:
            stereoValues_.layout = (StereoLayout)values[2];
            stereoValues_.flags = (MiscFlags)values[1];
            stereoValues_.separation = values[0];
            break;
    }

    isLoaded_ = true;
    return true;
}

void StereoscopicDescriptor::save(QDataStream &stream)
{
    ushort fullLength = 16;
    ushort blockSize = 4;
    uchar mediaTypeByte = (uchar)mediaType_;
    uchar values[3] = {0};
    switch (mediaType_)
    {
        case MediaType::Monoscopic:
            values[2] = (uchar)monoValues_.display;
            values[1] = (uchar)monoValues_.flags;
            break;
        case MediaType::Stereoscopic:
            values[2] = (uchar)stereoValues_.layout;
            values[1] = (uchar)stereoValues_.flags;
            values[0] = stereoValues_.separation;
            break;
    }

    stream << fullLength;
    stream.writeRawData("_JPSJPS_", 8);
    stream << blockSize;
    stream.writeRawData((char*)values, 3);
    stream << mediaTypeByte;
}
