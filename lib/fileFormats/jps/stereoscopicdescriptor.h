#ifndef JPSEXTENSION_H
#define JPSEXTENSION_H

#include "../jpeg/jpegmetadataextension.h"

namespace stereoscopic::fileFormats::jps
{
    /**
     * @brief Варианты типов содержимого
     */
    enum class LIBSHARED_EXPORT MediaType : uchar
    {
        /// Моноскопическое изображение
        Monoscopic = 0,
        /// Стереоскопическое изображение
        Stereoscopic = 1
    };

    /**
     * @brief Режим отображения
     */
    enum class LIBSHARED_EXPORT MonoDisplay : uchar
    {
        /// Оба ракурса
        Both = 0,
        /// Левый ракурс
        Left = 1,
        /// Правый ракурс
        Right = 2
    };

    /**
     * @brief Варианты расположения ракурсов на стерео кадре
     */
    enum class LIBSHARED_EXPORT StereoLayout : uchar
    {
        /// Чередование строк
        Interlaced = 1,
        /// Бок-о-бок
        SideBySide = 2,
        /// Сверху-снизу
        OverUnder = 3,
        /// Анаглиф
        Anaglyph = 4
    };

    /**
     * @brief Различные флаги
     */
    enum class LIBSHARED_EXPORT MiscFlags: uchar
    {
        /// Используется половинная высота ракурсов
        HalfHeight = 1 << 0,
        /// Используется половинная ширина ракурсов
        HalfWidth = 1 << 1,
        /// Левый ракурс идёт первым
        LeftFirst = 1 << 2,
    };

    inline MiscFlags operator|(MiscFlags a, MiscFlags b)
    {return static_cast<MiscFlags>(static_cast<uchar>(a) | static_cast<uchar>(b));}

    /**
     * @brief Вспомагательный класс для работы с перечислениями
     */
    class LIBSHARED_EXPORT Enum
    {
        public:
            /**
             * @brief Определяет наличие флага
             * @param flags Значение, в котором необходимо проверить наличие флага flag
             * @param flag Флаг, наличие которого нужно проверить в flags
             * @return true, если flag есть в значении flags
             */
            static bool hasFlag(MiscFlags flags, MiscFlags flag)
            {
                return ((uchar)flags & (uchar)flag) != 0;
            }
    };

    /// Cпецифичные значения для моноскопического содержимого
    struct MonoSpecificValues
    {
        public:
            /// Отображение
            MonoDisplay display = MonoDisplay::Both;

            /// Флаги
            MiscFlags flags = (MiscFlags)(0);
    };

    /// Cпецифичные значения для стереоскопического содержимого
    struct StereoSpecificValues
    {
        public:
            /// Расположение ракурсов
            StereoLayout layout = StereoLayout::Interlaced;
            /// Флаги
            MiscFlags flags = (MiscFlags)0;
            /// Расстояние между ракурсвами на кадре в пикселах
            uchar separation = 0;
    };

    /**
     * @brief Класс для загрузки и сохранения сегмента метаданных - стерео-дескриптора (JPS App3 extension)
     * @ref http://paulbourke.net/dataformats/stereoimage/spec.pdf
     */
    class LIBSHARED_EXPORT StereoscopicDescriptor : public stereoscopic::fileFormats::jpeg::JpegMetadataExtension
    {
        public:
            StereoscopicDescriptor();

            /// Возвращает признак того, что расширение было загружено
            virtual bool isLoaded(){ return isLoaded_; }

            /**
             * @brief Загружает расширение метаданных из потока данных
             * @param baseStream Поток данных
             * @return true, если расширение было загружено
             */
            virtual bool load(QDataStream &stream);

            /**
             * @brief Сохраняет расширение метаданных JPEG в поток данных
             * @param stream Поток данных
             */
            virtual void save(QDataStream &stream);

            /// Возвращает тип медиа (стерео или моно)
            MediaType mediaType(){ return mediaType_; }
            /// Устанавливает тип медиа (стерео или моно)
            void setMediaType(MediaType mediaType){ mediaType_ = mediaType; }

            /// Возвращает парамтеры моноскопического содержимого
            MonoSpecificValues monoValues(){ return monoValues_; }
            /// Устанавливает парамтеры моноскопического содержимого
            void setMonoValues(MonoSpecificValues monoValues){ monoValues_ = monoValues; }

            /// Возвращает парамтеры стереоскопического содержимого
            StereoSpecificValues &stereoValues(){ return stereoValues_; }
            /// Устанавливает парамтеры стереоскопического содержимого
            void setStereoValues(StereoSpecificValues stereoValues){ stereoValues_ = stereoValues; }

        private:
            bool isLoaded_ = false;
            MediaType mediaType_ = MediaType::Monoscopic;
            MonoSpecificValues monoValues_;
            StereoSpecificValues stereoValues_;
    };

}

#endif // JPSEXTENSION_H
