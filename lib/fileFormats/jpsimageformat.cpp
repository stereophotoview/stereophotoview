#include <QFile>
#include <QDebug>
#include "jpsimageformat.h"
#include "jpeg/jpegmetadata.h"
#include "stereocoders/coderfactory.h"
#include "../imagehelper.h"

using namespace stereoscopic;
using namespace stereoscopic::coders;
using namespace stereoscopic::fileFormats;
using namespace stereoscopic::fileFormats::jps;
using namespace stereoscopic::fileFormats::jpeg;

JpsImageFormat::JpsImageFormat(QObject *parent) : SingleImageFormat(parent)
{
    extensions_.append("jps");
    extensions_.append("jpeg");
    extensions_.append("jpg");
}

StereoImageSource *JpsImageFormat::load(QString fileName, StereoFormat *defFormat)
{
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
    {
        qWarning() << "can not open file: " << file.errorString();
        return nullptr;
    }
    QByteArray data = file.readAll();

    file.close();
    JpegMetadata metadata;
    metadata.loadFromData(data);

    // Читаем изображение
    QImage sourceImage;
    if(!sourceImage.loadFromData((data)))
    {
        qWarning() << "can not load image";
        return nullptr;
    }
    sourceImage = sourceImage.convertToFormat(QImage::Format_RGB32);
    sourceImage = ImageHelper::applyOrientation(sourceImage, metadata.exifHeader().value(QExifImageHeader::Orientation).toShort());

    StereoscopicDescriptor* sourceStereoDescr = getStereoscopicDescriptor(metadata);
    StereoFormat format;
    if(sourceStereoDescr)
    {
        format = getStereoFormat(*sourceStereoDescr);
        format.setIsLoaded(true);
    }
    else
    {
        QFileInfo f(fileName);
        QString suf = f.suffix().toLower();
        if(suf == "jps")
        {
            format.setLayout(StereoFormat::Horizontal).setLeftFirst(false);
        }
        else
        {
            format.setLayout(StereoFormat::Monoscopic);
        }
    }
    auto target = new StereoImageSource(StereoFrame(sourceImage, format), defFormat);
    target->setLeftExifHeaders(metadata.exifHeader());
    target->setRightExifHeaders(metadata.exifHeader());
    return target;
}

void JpsImageFormat::save(StereoImageSource *source, QString fileName, int quality)
{
    StereoFormat format = source->displayFrame()->format();
    save(source, fileName, quality, &format);
}

void JpsImageFormat::save(StereoImageSource * source, QString fileName, int quality, StereoFormat *format)
{
    save(source, fileName, quality, format, QSize());
}

void JpsImageFormat::save(StereoImageSource *source, QString fileName, int quality, StereoFormat *format, QSize targetSize)
{
    if(!source)
    {
        qWarning() << "source is not defined";
        return;
    }
    StereoscopicDescriptor* sd = getStereoscopicDescriptorFromFormat(format);
    QBuffer pixmapBuf, metadataBuf;

    auto coder = CoderFactory::create(format, source->displayFrame());
    QImage respixmap = coder.get()->createImage(targetSize);

    // Сохраняем метаданные в буфер
    JpegMetadata metadata;
    if(sd != nullptr)
        metadata.appendExtension(APP3, sd);
    metadata.setExifHeaders(source->leftExifHeaders()->header(), &respixmap);
    metadataBuf.open(QBuffer::WriteOnly);
    metadata.save(metadataBuf);
    metadataBuf.close();

    // Сохранем изображение в буфер
    pixmapBuf.open(QBuffer::WriteOnly);
    respixmap.save(&pixmapBuf, "JPEG", quality);
    pixmapBuf.close();

    // Записываем всё в файл...
    QFile file(fileName);
    if(!file.open(QIODevice::Truncate|QIODevice::WriteOnly))
    {
        qWarning() << "can not open file: " << file.errorString();
        return;
    }

    // Метаданные изображения
    file.write(metadataBuf.data());

    // Изображение без начального маркера
    file.write(pixmapBuf.data().data() + 2, pixmapBuf.data().count() - 2);
    file.close();
}

StereoscopicDescriptor *JpsImageFormat::getStereoscopicDescriptor(JpegMetadata &d)
{
    auto extensions = d.getExtensions<StereoscopicDescriptor>(APP3);
    if(!extensions.empty())
        return extensions.first();
    return nullptr;
}

StereoscopicDescriptor *JpsImageFormat::getStereoscopicDescriptorFromFormat(StereoFormat *format)
{
    if(format->layout() == StereoFormat::ColumnInterlaced || format->layout() == StereoFormat::AnaglyphYB)
        return nullptr;
    StereoscopicDescriptor* sd = new StereoscopicDescriptor();
    sd->setMediaType(format->layout() == StereoFormat::Monoscopic ? MediaType::Monoscopic : MediaType::Stereoscopic);
    switch (format->layout())
    {
        case StereoFormat::AnamorphHorizontal:
            sd->stereoValues().flags = MiscFlags::HalfWidth;
#ifdef __clang__
            [[clang::fallthrough]];
#endif
        case StereoFormat::Horizontal:
            sd->stereoValues().layout = StereoLayout::SideBySide;
            break;
        case StereoFormat::AnamorphVertical:
            sd->stereoValues().flags = MiscFlags::HalfHeight;
#ifdef __clang__
            [[clang::fallthrough]];
#endif
        case StereoFormat::Vertical:
            sd->stereoValues().layout = StereoLayout::OverUnder;
            break;
        case StereoFormat::RowInterlaced:
            sd->stereoValues().layout = StereoLayout::Interlaced;
            break;
        case StereoFormat::AnaglyphRC:
            sd->stereoValues().layout = StereoLayout::Anaglyph;
            break;
        default:
            break;
    }
    if(format->leftFirst())
        sd->stereoValues().flags = sd->stereoValues().flags | MiscFlags::LeftFirst;
    return sd;
}

StereoFormat JpsImageFormat::getStereoFormat(StereoscopicDescriptor &descr)
{
    StereoFormat format;
    MiscFlags flags;
    if(descr.mediaType() == MediaType::Monoscopic)
    {
        format.setLayout(StereoFormat::Monoscopic);
        flags = descr.monoValues().flags;
    }
    else
    {
        flags = descr.stereoValues().flags;
        StereoFormat::Layout layout = StereoFormat::Monoscopic;
        switch (descr.stereoValues().layout) {
            case StereoLayout::Interlaced:
                layout = StereoFormat::RowInterlaced;
                break;
            case StereoLayout::SideBySide:
                if(Enum::hasFlag(flags, MiscFlags::HalfWidth))
                    layout = StereoFormat::AnamorphHorizontal;
                else
                    layout = StereoFormat::Horizontal;
                break;
            case StereoLayout::OverUnder:
                if(Enum::hasFlag(flags, MiscFlags::HalfHeight))
                    layout = StereoFormat::AnamorphVertical;
                else
                    layout = StereoFormat::Vertical;
                break;
            case StereoLayout::Anaglyph:
                layout = StereoFormat::AnaglyphRC;
                break;
        }
        format.setLayout(layout);
    }
    return format.setLeftFirst(Enum::hasFlag(flags,MiscFlags::LeftFirst));
}
