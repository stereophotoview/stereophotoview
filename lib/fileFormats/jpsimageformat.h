#ifndef JPSIMAGEFORMAT_H
#define JPSIMAGEFORMAT_H

#include <QImage>
#include <QFileInfo>

#include "singleimageformat.h"
#include "stereoimagesource.h"
#include "jps/stereoscopicdescriptor.h"
#include "jpeg/jpegmetadata.h"
#include "lib_global.h"

namespace stereoscopic::fileFormats
{
    /**
     * @brief Класс, предоставляющий поддержку формата JPS
     */
    class LIBSHARED_EXPORT  JpsImageFormat : public SingleImageFormat
    {
            Q_OBJECT
            Q_PROPERTY(QStringList extensions READ extensions)

        public:
            explicit JpsImageFormat(QObject *parent = nullptr);

        public slots:
            /**
             * @brief Загружает стерео изображение из файла JPS
             * @param fileName Путь к файлу
             * @param defFormat Стерео формат по умолчанию
             * @return Загруженное изображение - источник для отборажения
             */
            stereoscopic::StereoImageSource* load(QString fileName, StereoFormat* defFormat);

            /**
             * @brief Сохраняет стерео изображение в файл, используя исходный стерео формат и размер кадра
             * @param source Исходное изображение
             * @param fileName Путь к файлу
             * @param quality Качество сохранения Jpeg
             */
            void save(StereoImageSource * source, QString fileName, int quality);

            /**
             * @brief Сохраняет стерео изображение в файл, используя исходный размер кадра
             * @param source Исходное изображение
             * @param fileName Путь к файлу
             * @param quality Качество сохранения Jpeg
             * @param format Стерео формат
             */
            void save(StereoImageSource * source, QString fileName, int quality, StereoFormat* format);

            /**
             * @brief Сохраняет стерео изображение в файл
             * @param source Исходное изображение
             * @param fileName Путь к файлу
             * @param quality Качество сохранения Jpeg
             * @param format Стерео формат
             * @param fullFrameSize Размер кадра
             */
            void save(StereoImageSource * source, QString fileName, int quality, StereoFormat* format, QSize fullFrameSize);

        private:
            StereoscopicDescriptor* getStereoscopicDescriptor(JpegMetadata &d);
            StereoscopicDescriptor *getStereoscopicDescriptorFromFormat(StereoFormat *format);
            StereoFormat getStereoFormat(StereoscopicDescriptor &descr);

    };
}
#endif // JPSIMAGEFORMAT_H
