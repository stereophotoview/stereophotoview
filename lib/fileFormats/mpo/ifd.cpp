#include "ifd.h"

#include <QDebug>

using namespace stereoscopic::fileFormats::mpo;

void MpIfd::load(QDataStream &stream, uint &nextOffset)
{
    ushort count;
    stream >> count;

    ushort id;
    QList<BaseTag*> tags;
    QList<TagId> tagIds;
    for(int i = 0; i < count; i++)
    {
        BaseTag * tag;
        stream >> id;
        switch((TagId)id)
        {
            case TagId::MPFVersion:
                tag = new MPFormatVersionTag();
                break;

            case TagId::MPEntry:
                tag = new MPEntryTag();
                break;

            // LONG
            case TagId::NumberOfImages:
            case TagId::TotalFrames:
            case TagId::MPIndividualNum:
            case TagId::PanOrientation:
            case TagId::BaseViewpointNum:
                tag = new LongTag();
                break;

            // RATIONAL
            case TagId::PanOverlap_H:
            case TagId::PanOverlap_V:
            case TagId::BaselineLength:
                tag = new RationalTag();
                break;

            // SRATIONAL
            case TagId::ConvergenceAngle:
            case TagId::VerticalDivergence:
            case TagId::AxisDistance_X:
            case TagId::AxisDistance_Y:
            case TagId::AxisDistance_Z:
            case TagId::YawAngle:
            case TagId::PitchAngle:
            case TagId::RollAngle:
                tag = new SRationalTag();
                break;

            // UIDList
            case TagId::ImageUIDList:
                tag = new BytesTag(33);
                break;

            default:
                qWarning() << "unknown tag id " + QString::number(id, 16);
                tag = new RawTag();
                break;
        }
        if(tag)
        {
            tag->load(stream);
            tags.append(tag);
            tagIds.append((TagId)id);
        }
    }

    stream >> nextOffset;

    // Загружаем значения тегов
    for(int i = 0; i < tags.count(); i++)
    {
        auto tagId = tagIds[i];
        BaseTag* tag = tags[i];
        tag->loadValue(stream, this);
        tags_.insert(tagId, tag);
    }

}

// Сохраняет IFD в поток
void MpIfd::save(QDataStream &stream, bool writeOffset)
{
    auto tagIds = tags_.keys();
    stream << (ushort)tagIds.count();
    foreach(TagId id, tagIds)
    {
        auto tag = tags_[id];
        stream << (ushort)id;
        tag->save(stream);
    }
    auto nextIfdAddr = stream.device()->pos();
    uint zero = 0;
    // Смещение к IFD Attributes. Потом перезапишем.
    stream << zero;
    foreach(TagId id, tagIds)
    {
        auto tag = tags_[id];
        tag->saveValue(stream, this);
    }
    if(writeOffset)
    {
        // Возвращаемся и перезаписываем смещение к IFD Attributes.
        auto offset = stream.device()->pos();
        stream.device()->seek(nextIfdAddr);
        stream << (uint)offset;
        stream.device()->seek(offset);
    }
}

MpIfd::~MpIfd()
{
    foreach(BaseTag* tag, tags_)
        delete tag;
    tags_.clear();
}

void BaseTag::load(QDataStream &stream)
{
    stream.skipRawData(10);
}

void BaseTag::loadValue(QDataStream &, MpIfd *)
{

}

void BaseTag::save(QDataStream &stream)
{
    uchar zero = 0;
    for(int i = 0; i < 10; i++)
        stream << zero;
}

void BaseTag::saveValue(QDataStream&, MpIfd*)
{
    // Ничего не делаем
}

void MPFormatVersionTag::load(QDataStream &stream)
{
    stream.skipRawData(6);
    stream.readRawData(formatVersion_, 4);
}

void MPFormatVersionTag::save(QDataStream &stream)
{
    uchar zero = 0;
    for(int i=0; i < 6; i++)
        stream << zero;
    stream.writeRawData(formatVersion_, 4);
}

QString MPFormatVersionTag::toString()
{
    return QString(QByteArray(formatVersion_, 4));
}

template<typename T>
void BaseValueTag<T>::load(QDataStream &stream)
{
    stream.skipRawData(10 - sizeof(T));
    stream >> value_;
}

template<typename T>
void BaseValueTag<T>::save(QDataStream &stream)
{
    uchar zero = 0;
    int l = 10 - sizeof(T);
    for(int i=0; i < l; i++)
        stream << zero;
    stream << value_;
}

template<typename T>
QString BaseValueTag<T>::toString()
{
    return QString("%1").arg(value_);
}

void BytesTag::loadValue(QDataStream &stream, MpIfd *ifd)
{
    char* buf = new char[length_];
    for(uint i = 0; i< ifd->numberOfImages(); i++)
    {
        stream.readRawData(buf, length_);
        data_.append(new QByteArray(buf, length_));
    }
    delete[] buf;
}

void BytesTag::saveValue(QDataStream &stream, MpIfd*)
{
    foreach(auto array, data_)
    {
        stream.writeRawData(array->data(), data_.length());
    }
}

QString BytesTag::toString()
{
    QString res;
    foreach(auto array, data_)
    {
        if(res.length() > 0)
            res = res + ", ";
        res = res + "[" + QString(array->toHex()) + "]";
    };
    return res;
}

void MPEntryTag::save(QDataStream &stream)
{
    // Если заполнить просто нулями, телевизор LG не распознаёт стерео фото
    uchar data[] = { 0x07, 0x00, 0x20, 0x00, 0x00, 0x00, 0x32, 0x00, 0x00, 0x00 };
    for(int i=0; i< 10; i++)
        stream << data[i];
}

void MPEntryTag::loadValue(QDataStream &stream, MpIfd *ifd)
{
    for(uint i = 0; i< ifd->numberOfImages(); i++)
    {
        ImageEntry entry;
        stream.readRawData(entry.attribute, 4);
        stream >> entry.size;
        stream >> entry.offset;
        stream >> entry.dep1;
        stream >> entry.dep2;
        entries_.append(entry);
    }
}

void MPEntryTag::saveValue(QDataStream &stream, MpIfd *ifd)
{
    for(int i = 0; i < (int)ifd->numberOfImages(); i++)
    {
        ImageEntry entry = entries_[i];
        stream.writeRawData(entry.attribute, 4);
        stream << entry.size;
        stream << entry.offset;
        stream << entry.dep1;
        stream << entry.dep2;
    }
}

QString MPEntryTag::toString()
{
    QString res;
    foreach(ImageEntry entry, entries_)
    {
        if(res.length() > 0)
            res = res + ",";
        res = res + "{"
                + "attr:" + QString(QByteArray(entry.attribute, 4).toHex())
                + ", offset:" + QString::number(entry.offset)
                + ", size:" + QString::number(entry.size)
                + ", dep1:" + QString::number(entry.dep1)
                + ", dep2:" + QString::number(entry.dep2)
                + "}";
    }
    return res;
}

void RawTag::load(QDataStream &stream)
{
    stream.readRawData(data_, 10);
}

void RawTag::save(QDataStream &stream)
{
    stream.writeRawData(data_, 10);
}

template<typename T>
void BaseRationalTag<T>::load(QDataStream &stream)
{
    stream.skipRawData(10 - sizeof(T) * 2);
    stream >> value_.numerator;
    stream >> value_.denominator;
}

template<typename T>
void BaseRationalTag<T>::save(QDataStream &stream)
{
    int l = 10 - sizeof(T) * 2;
    uchar zero = 0;
    for(int i = 0; i < l; i++)
        stream << zero;
    stream << value_.numerator;
    stream << value_.denominator;
}

template<typename T>
QString BaseRationalTag<T>::toString()
{
    return QString("%1/%2").arg(value_.numerator).arg(value_.denominator);
}


LongTag::LongTag()
{

}

SRationalTag::SRationalTag()
{

}

RationalTag::RationalTag()
{

}
