/**
 * This classes represents the IFD Index and attributes of MPO.
 * @ref http://www.cipa.jp/std/documents/e/DC-007_E.pdf
 * @ref ../../docs/MPO.txt
*/

#ifndef IFD
#define IFD

#include <QDataStream>
#include <QString>
#include <QList>
#include <QMap>
#include <QStringList>
#include <QString>

#include "lib_global.h"

namespace stereoscopic::fileFormats::mpo {

    /**
     * @brief Идентификаторы тегов
     */
    enum class LIBSHARED_EXPORT TagId : ushort
    {
        /// MP Format Version Number
        MPFVersion = 0xB000,
        /// Number of Images
        NumberOfImages = 0xB001,
        /// Информация об изображениях: тип, размер, смещение
        MPEntry = 0xB002,
        /// Список индивидуальных идентификаторов изображений
        ImageUIDList = 0xB003,
        /// Total Number of Captured Frames
        TotalFrames = 0xB004,
        /// Номер текущего изображения
        MPIndividualNum = 0xB101,
        /// Panorama Scanning Orientation
        PanOrientation = 0xB201,
        /// Panorama Horizontal Overlap
        PanOverlap_H  = 0xB202,
        /// Panorama Vertical Overlap
        PanOverlap_V = 0xB203,
        /// Номер точки зрения, относительно которой вычисляются углы и расстояния текущего изображения
        BaseViewpointNum = 0xB204,
        /// Угол отклонения относительно базовой точки зрения
        ConvergenceAngle = 0xB205,
        /// Расстояние от базовой точки зрения
        BaselineLength = 0xB206,
        /// Divergence Angle
        VerticalDivergence = 0xB207,
        /// Horizontal Axis Distance
        AxisDistance_X = 0xB208,
        /// Vertical Axis Distance
        AxisDistance_Y = 0xB209,
        /// Collimation Axis Distance
        AxisDistance_Z = 0xB20A,
        /// Yaw Angle
        YawAngle = 0xB20B,
        /// Pitch Angle
        PitchAngle = 0xB20C,
        /// Roll Angle
        RollAngle = 0xB20D,
    };

    class LIBSHARED_EXPORT MpIfd;

    /**
     * @brief Базовый класс для тегов
     */
    class LIBSHARED_EXPORT BaseTag
    {
        public:
            BaseTag(){}
            /// Загружает тег из потока данных
            virtual void load(QDataStream &stream);

            /// Загружает значение тега из потока
            virtual void loadValue(QDataStream &stream, MpIfd *ifd);

            /// Сохраняет тег в поток данных
            virtual void save(QDataStream &stream);

            /// Сохраняет значение тега в поток
            virtual void saveValue(QDataStream &stream, MpIfd *ifd);

            /// Строковое представление
            virtual QString toString(){ return QString(); }

            virtual ~BaseTag(){}
    };

    /**
     * @brief Тег, представляющий сырые неразобранные данные
     */
    class LIBSHARED_EXPORT RawTag : public BaseTag
    {
        public:
            /// Загружает тег из потока данных
            void load(QDataStream &stream);

            /// Сохраняет тег в поток данных
            void save(QDataStream &stream);
        private:
            char data_[10];
    };

    /**
     * @brief Тег с номером версии формата
     */
    class LIBSHARED_EXPORT MPFormatVersionTag : public BaseTag
    {
        public:
            /// Номер версии формата
            QByteArray formatVersion()
            {
                return QByteArray(formatVersion_, 4);
            }
            void load(QDataStream &stream);
            void save(QDataStream &stream);
            QString toString();
        private:
            /// Поддерживаемая версия формата
            char formatVersion_[4] = {'0', '1', '0', '0'};
    };

    template<typename T>
    /**
     * @brief Шаблонный класс для тега со значением типа, заданного парамтером шаблона
     */
    class LIBSHARED_EXPORT BaseValueTag : public BaseTag
    {
        public:
            /// Значение тега
            T value(){ return value_; }
            void setValue(T value){ value_ = value; }
            void load(QDataStream &stream);
            void save(QDataStream &stream);
            QString toString();
        private:
            T value_ = 0;
    }   ;

    /**
     * @brief Тег со значениям типа uint
     */
    class LIBSHARED_EXPORT LongTag : public BaseValueTag<uint>
    {
        public:
            LongTag();
    };

    template<typename T>
    /**
     * @brief Класс для представления рациональной дроби
     * Тип числителя и знаменателя задаются параметром шаблона
     */
    struct RationalNumber
    {
        /// Числитель
        T numerator;

        /// Знаменатель
        T denominator;

        /// Вычисленное значение
        double value(){ return (double) numerator / (double) denominator; }

        RationalNumber(){ numerator = 0; denominator = 1; }
        RationalNumber(T numerator, T denominator){ this->numerator = numerator; this->denominator = denominator; }
    };

    template<typename T>
    /**
     * @brief Тег со значением в виде рациональной дроби
     * Тип числителя и знаменателя задаётся параметром шаблона
     */
    class LIBSHARED_EXPORT BaseRationalTag : public BaseTag
    {
        public:
            /// Значение тега в виде рациональной дроби
            RationalNumber<T> value(){ return value_; }
            void setValue(RationalNumber<T> value) { value_ = value; }
            void load(QDataStream &stream);
            void save(QDataStream &stream);
            QString toString();
        private:
            RationalNumber<T> value_;

    };

    /**
     * @brief Тег, представляющий рациональная дробь из без-знаковых чисел
     */
    class LIBSHARED_EXPORT RationalTag : public BaseRationalTag<uint>
    {
        public:
            RationalTag();
    };

    /**
     * @brief Тег, представляющий рациональная дробь из целых чисел со знаком
     */
    class LIBSHARED_EXPORT SRationalTag : public BaseRationalTag<int>
    {
        public:
            SRationalTag();
    };

    /**
     * @brief Тег с массивом байт указанного размера на каждое изображение в области значений
     */
    class LIBSHARED_EXPORT BytesTag : public BaseTag
    {
        public:
            QList<QByteArray*> &data(){ return data_; }
            int length(){ return length_; }

            BytesTag(int length)
            {
                this->length_ = length;
            }

            ~BytesTag()
            {
                foreach(QByteArray* d, data_)
                    delete d;
                data_.clear();
            }

            void loadValue(QDataStream &stream, MpIfd *ifd);
            void saveValue(QDataStream &stream, MpIfd *ifd);
            QString toString();
        private:
            int length_ = 0;
            QList<QByteArray*> data_;
    };

    /**
     * @brief Информация об одном индивидуальном изображении
     */
    struct ImageEntry
    {
        public:
            /// Атрибут изображения
            char attribute[4] = {0};

            /// Размер изображения
            uint size = 0;

            /// Сдвиг данных
            uint offset = 0;

            /// Зависимое изображение 1
            ushort dep1 = 0;

            /// Зависимое изображение 2
            ushort dep2 = 0;

            ImageEntry(){}

            /**
             * @brief Создаёт информацию об одном индивидуальном изображении
             * @param attribute Атрибут изображения
             * @param size Размер изображения
             * @param offset Сдвиг данных
             * @param dep1 Зависимое изображение 1
             * @param dep2 Зависимое изображение 2
             */
            ImageEntry(char* attribute, uint size, uint offset, ushort dep1 = 0, ushort dep2 = 0)
            {
                memcpy(this->attribute, attribute, 4);
                this->size = size;
                this->offset = offset;
                this->dep1 = dep1;
                this->dep2 = dep2;
            }
    };

    /**
     * @brief Тег с информацией об индивидуальных изображениях в области значений
     */
    class LIBSHARED_EXPORT MPEntryTag : public BaseTag
    {
        public:
            /// Записи информаций об индивидуальных изображениях
            QList<ImageEntry> & entries(){ return entries_; }

            /// Сохраняет тег в поток данных
            void save(QDataStream &stream);

            /// Загружает значение тега
            void loadValue(QDataStream &stream, MpIfd *ifd);

            /// Сохраняет значение тега
            void saveValue(QDataStream &stream, MpIfd *ifd);

            QString toString();
        private:
            QList<ImageEntry> entries_;
    };

    /**
     * @brief Класс для представления, загрузки и сохранения метеданных MPO файла
     */
    class LIBSHARED_EXPORT MpIfd
    {
        public:
            MpIfd(){}

            /// Словарь всех тегов
            QMap<TagId, BaseTag*> & tags(){ return tags_; }

            /// Возвращает версию формата
            QByteArray Version()
            {
                auto tag = getTag<MPFormatVersionTag>(TagId::MPFVersion);
                return tag->formatVersion();
            }
            /// Устанавливает значение версии формата по умолчанию
            void setDefaultVersion()
            {
                createTag<MPFormatVersionTag>(TagId::MPFVersion);
            }

            /// Возвращает количество изображений
            uint numberOfImages()
            {
                return getLongTagValue(TagId::NumberOfImages);
            }
            /// Устанавливает количество изображений
            void setNumberOfImages(uint numberOfImages)
            {
                setLongTagValue(TagId::NumberOfImages, numberOfImages);
            }

            /// Возвращает информацию об индивидуальных изображениях
            QList<ImageEntry>* imageEntries()
            {
                auto tag = createTag<MPEntryTag>(TagId::MPEntry);
                return &tag->entries();
            }

            /// Возвращает количество индивидуальных изображений
            uint individualNum()
            {
                return getLongTagValue(TagId::MPIndividualNum);
            }
            /// Устанавливает количество индивидуальных изображений
            void setIndividualNum(uint individualNum)
            {
                setLongTagValue(TagId::MPIndividualNum, individualNum);
            }

            /// Возвращает количество базовых точек зрения
            uint baseViewpointNum()
            {
                return getLongTagValue(TagId::BaseViewpointNum);
            }
            /// Устанавливает количество базовых точек зрения
            void setBaseViewpointNum(uint baseViewpointNum)
            {
                setLongTagValue(TagId::BaseViewpointNum, baseViewpointNum);
            }

            /// Возвращает длинну базовой линии
            RationalNumber<uint> baselineLength()
            {
                return getRationalTagValue(TagId::BaselineLength);
            }
            /// Устанавливает длинну базовой линии
            void setBaselineLength(RationalNumber<uint> baselineLength)
            {
                setRationalTagValue(TagId::BaselineLength, baselineLength);
            }

            /// Возвращает угол отклонения относительно базовой точки зрения
            RationalNumber<int> convergenceAngle()
            {
                return getSRationalTagValue(TagId::ConvergenceAngle);
            }
            /// Устонавливает угол отклонения относительно базовой точки зрения
            void setConvergenceAngle(RationalNumber<int> convergenceAngle)
            {
                setSRationalTagValue(TagId::ConvergenceAngle, convergenceAngle);
            }

            /**
             * @brief Загружает метаданные из потока данных
             * @param stream Поток данных
             * @param[out] nextOffset Сдвиг до следующего IFD
             */
            void load(QDataStream &stream, uint &nextOffset);

            /**
             * @brief Сохраняет метаданные в поток данных
             * @param stream Поток данных
             * @param writeOffset Признак того, что нужно записать смещение к ifd атрибутам
             */
            void save(QDataStream &stream, bool writeOffset);

            ~MpIfd();
        private:
            MpIfd(MpIfd &){}

            QMap<TagId, BaseTag*> tags_;

            uint getLongTagValue(TagId id)
            {
               auto tag = getTag<LongTag>(id);
               return tag ? tag->value() : 0;
            }

            void setLongTagValue(TagId id, uint value)
            {
               auto tag = createTag<LongTag>(id);
               tag->setValue(value);
            }

            RationalNumber<uint> getRationalTagValue(TagId id)
            {
               auto tag = getTag<RationalTag>(id);
               return tag ? tag->value() : RationalNumber<uint>();
            }

            void setRationalTagValue(TagId id, RationalNumber<uint> value)
            {
               auto tag = createTag<RationalTag>(id);
               tag->setValue(value);
            }

            RationalNumber<int> getSRationalTagValue(TagId id)
            {
               auto tag = getTag<SRationalTag>(id);
               return tag ? tag->value() : RationalNumber<int>();
            }
            void setSRationalTagValue(TagId id, RationalNumber<int> value)
            {
               auto tag = createTag<SRationalTag>(id);
               tag->setValue(value);
            }


            template<typename T>
            T* getTag(TagId id)
            {
               if(!tags_.contains(id))
                   return nullptr;
               return dynamic_cast<T*>(tags_[id]);
            }

            template<typename T>
            T* createTag(TagId id)
            {
               T * tag = nullptr;
               if(!tags_.contains(id))
                   tags_[id] = tag = new T();
               else
                   tag = dynamic_cast<T*>(tags_[id]);
               return tag;
            }
    };
}

#endif // IFD

