#include <QBuffer>

#include "mpoextension.h"

using namespace stereoscopic::fileFormats::mpo;

// Загрузка расширения MPO
bool MpoExtension::load(QDataStream &baseStream)
{
    ushort length;
    auto initPos = baseStream.device()->pos();
    baseStream >> length;

    // check format identifier
    char formatId[4];
    baseStream.readRawData((char*)formatId, 4);
    if(strcmp(formatId, "MPF") != 0)
    {
        baseStream.device()->seek(initPos);
        return false;
    }

    // Начало заголовка. Все смещения считаются от этого положения
    startPos_ = baseStream.device()->pos();

    // read data to parse
    ushort bufSize = length - 6;
    char* buf = new char[bufSize];
    baseStream.readRawData(buf, bufSize);
    QByteArray data(buf, bufSize);
    delete[] buf;
    QDataStream stream(data);

    // header...
    uint iv;
    // endian
    stream >> iv;
    if(iv == MP_LITTLE_ENDIAN)
        stream.setByteOrder(QDataStream::LittleEndian);
    else if(iv == MP_BIG_ENDIAN)
        stream.setByteOrder(QDataStream::BigEndian);
    else
    {
        baseStream.device()->seek(initPos);
        return false;
    }

    uint offsetToIFD;
    //offset to ifd
    stream >> offsetToIFD;

    if(offsetToIFD)
    {
        if(ifdAttributes_)
            delete ifdAttributes_;
        ifdAttributes_ = new MpIfd();
        offsetToIFD = loadIfd(data, stream.byteOrder(), ifdAttributes_, offsetToIFD);
    }

    if(offsetToIFD > 0)
    {
        if(ifdIndex_)
        {
            delete ifdIndex_;
            ifdIndex_ = nullptr;
        }
        ifdIndex_ = ifdAttributes_;
        ifdAttributes_ = new MpIfd();
        loadIfd(data, stream.byteOrder(), ifdAttributes_, offsetToIFD);
    }
    isLoaded_ = true;
    return true;
}

// Загрузка структура IFD
uint MpoExtension::loadIfd(QByteArray &data, QDataStream::ByteOrder byteOrder, MpIfd *ifd, uint offset)
{
    QDataStream ifdStream(data);
    ifdStream.setByteOrder(byteOrder);
    ifdStream.skipRawData((int)offset);
    uint newOffset;
    ifd->load(ifdStream, newOffset);
    return newOffset;
}

// Сохранение расширения MPO
void MpoExtension::save(QDataStream &baseStream)
{
    // Сохраняем анные в отдельный буфер
    QBuffer buf;
    buf.open(QBuffer::ReadWrite);
    QDataStream stream(&buf);
    stream << MP_LITTLE_ENDIAN;
    stream.setByteOrder(QDataStream::LittleEndian);
    stream << (uint)8; // offset to ifd
    if(ifdIndex_)
        ifdIndex_->save(stream, true);
    if(ifdAttributes_)
        ifdAttributes_->save(stream, false);
    // Записываем длину и содержимое отдельного буфера в baseStream
    QByteArray data = buf.data();
    ushort length = data.count() + 2 + 4;
    baseStream << length;
    baseStream.writeRawData("MPF", 4);
    startPos_ = baseStream.device()->pos();
    baseStream.writeRawData(data.data(), data.count());
}

