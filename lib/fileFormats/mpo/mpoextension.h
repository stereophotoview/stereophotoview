#ifndef MPOEXTENSION_H
#define MPOEXTENSION_H

#include "ifd.h"
#include "../jpeg/jpegmetadataextension.h"

namespace stereoscopic::fileFormats::mpo
{
    const uint
        MP_LITTLE_ENDIAN = 0x49492A00,
        MP_BIG_ENDIAN = 0x4d4d002A;

    /**
     * @brief Класс для представления, загрузки и сохранения MPO расширения в APP2 сегменте
     * @ref http://www.cipa.jp/std/documents/e/DC-007_E.pdf
     */
    class LIBSHARED_EXPORT MpoExtension : public stereoscopic::fileFormats::jpeg::JpegMetadataExtension
    {
        public:
            /// Возвращщает MP Index IFD
            MpIfd* ifdIndex(){ return ifdIndex_; }
            /// Устанавливает MP Index IFD
            void setIfdIndex(MpIfd* ifdIndex)
            {
                if(ifdIndex_)
                    delete ifdIndex_;
                ifdIndex_ = ifdIndex;
            }
            /// Возвращает атрибуты IFD
            MpIfd* ifdAttributes(){ return ifdAttributes_; }
            /// Устанавливает атрибуты IFD
            void setIfdAttributes(MpIfd* ifdAttributes)
            {
                if(ifdAttributes_)
                    delete ifdAttributes_;
                ifdAttributes_ = ifdAttributes;
            }

            /// Возвращает начало заголовка
            long startPos(){ return startPos_; }

            /// Возвращает признак того, что расширение было загружено из потока данных
            virtual bool isLoaded(){ return isLoaded_; }

            /// Загружает расширение из потока данных
            virtual bool load(QDataStream &baseStream);

            /// Сохраняет расширение в поток данных
            virtual void save(QDataStream &stream);

            virtual ~MpoExtension()
            {
                if(ifdIndex_)
                    delete ifdIndex_;
                if(ifdAttributes_)
                    delete ifdAttributes_;
            }

        private:
            long startPos_;
            MpIfd* ifdIndex_= 0;
            MpIfd* ifdAttributes_ = 0;
            bool isLoaded_ = false;
            uint loadIfd(QByteArray &data, QDataStream::ByteOrder byteOrder, MpIfd *ifd, uint offset);
    };

}

#endif // MPOEXTENSION_H
