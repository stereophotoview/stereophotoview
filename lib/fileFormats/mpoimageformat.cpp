#include "mpoimageformat.h"
#include <QUrl>
#include <QFile>
#include <QDebug>
#include <QBuffer>
#include <QByteArrayMatcher>

#include "jpeg/jpegmetadata.h"
#include "singleimageformat.h"
#include "stereoframe.h"
#include "imagehelper.h"

using namespace stereoscopic;
using namespace stereoscopic::fileFormats;
using namespace stereoscopic::fileFormats::mpo;

MpoImageFormat::MpoImageFormat(QObject *parent): SingleImageFormat(parent)
{
    extensions_.append("mpo");
}

StereoImageSource *MpoImageFormat::load(QString fileName, StereoFormat* defUserFormat)
{
    StereoFormat format(StereoFormat::Separate);
    format.setIsLoaded(true);
    auto target = new StereoImageSource(format, defUserFormat);

    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
    {
        qWarning() << "can not open file: " << file.errorString();
        return nullptr;
    }
    QByteArray data = file.readAll();

    file.close();

    int secondImageOffset = 0;

    // Читаем метаданные первого изображения
    JpegMetadata leftMetadata, rightMetadata;

    // Загружаем и анализируем метаданные левого изображения
    // Получаем из них адрес правого изображения
    if(!leftMetadata.loadFromData(data) || !checkLeftMetadata(leftMetadata, secondImageOffset))
    {
        error(target, fileName, "is not correct mpo file");
        // Пытаемся найти второе изображение
        secondImageOffset = searchSecondImageOffset(data);
    }

    // Читаем левое изображение
    QImage leftImage;
    if(!leftImage.loadFromData((data)))
        return error(target, fileName, "can not load first image");
    leftImage = leftImage.convertToFormat(QImage::Format_RGB32);

    // Получаем данные, содержащие второе изображение
    QByteArray rightData = data.right(data.length() - secondImageOffset);

    // Читаем метаданные второго изображения
    if(!rightMetadata.loadFromData(rightData))
        qWarning() << "error load right metadata: " << rightMetadata.parseError();

    // Читаем второе изображение
    QImage rightImage;
    if(!rightImage.loadFromData(rightData))
        return error(target, fileName, "can not load second image");
    rightImage = rightImage.convertToFormat(QImage::Format_RGB32);
    leftImage = ImageHelper::applyOrientation(leftImage, leftMetadata.exifHeader().value(QExifImageHeader::Orientation).toShort());
    rightImage = ImageHelper::applyOrientation(rightImage, rightMetadata.exifHeader().value(QExifImageHeader::Orientation).toShort());
    target->setOriginalFrame(StereoFrame(leftImage, rightImage));
    target->setLeftExifHeaders(leftMetadata.exifHeader());
    target->setRightExifHeaders(rightMetadata.exifHeader());

#ifdef TRACE
    qDebug() << "Left metadata:";
    debugOutputMetadata(leftMetadata);
    qDebug() << "Right metadata:";
    debugOutputMetadata(rightMetadata);
#endif
    return target;
}

void MpoImageFormat::save(StereoImageSource *source, QString fileName, bool leftFirst, QSize targetSize, int quality)
{
    if(!source)
    {
        qWarning() << "source is not defined";
        return;
    }

    QBuffer leftViewBuf, rightViewBuf, leftMetadataBuf, rightMetadataBuf;
    JpegMetadata leftMetadata, rightMetadata;

    StereoFrame* frame = source->displayFrame();

    QImage leftView = leftFirst ? frame->leftFullView() : frame->rightFullView();
    QImage rightView = leftFirst ? frame->rightFullView() : frame->leftFullView();

    if(!targetSize.isEmpty())
    {
        leftView = leftView.scaled(ImageHelper::getEnteredImageRect(leftView.size(), targetSize).size());
        rightView = rightView.scaled(ImageHelper::getEnteredImageRect(rightView.size(), targetSize).size());
    }

    ExifHeaders* leftExif = leftFirst ? source->leftExifHeaders() : source->rightExifHeaders();
    ExifHeaders* rightExif = leftFirst ? source->rightExifHeaders() : source->leftExifHeaders();

    // Сохранем левое изображение в буфер
    leftViewBuf.open(QBuffer::WriteOnly);
    leftView.save(&leftViewBuf, "JPEG", quality);
    leftViewBuf.close();

    // Сохранем правое изображение в буфер
    rightViewBuf.open(QBuffer::WriteOnly);
    rightView.save(&rightViewBuf, "JPEG", quality);
    rightViewBuf.close();

    // Заполняем предварительные метаданные для первого изображения
    fillLeftMetadata(leftMetadata, 0, 0, 0);
    leftMetadata.setExifHeaders(leftExif->header(), &leftView);

    // Заполняем метаданные для второго изображения
    fillRightMetadata(rightMetadata);
    rightMetadata.setExifHeaders(rightExif->header(), &rightView);

    // Сохраняем предварительные метаданные для первого изображения в буфер
    leftMetadataBuf.open(QBuffer::WriteOnly);
    leftMetadata.save(leftMetadataBuf);
    leftMetadataBuf.close();

    // Сохраняем метаданные для второго изображения в буфер
    rightMetadataBuf.open(QBuffer::WriteOnly);
    rightMetadata.save(rightMetadataBuf);
    rightMetadataBuf.close();

    // Вычисляем размеры и смещения изображений в файле
    uint leftSize = (uint)leftMetadataBuf.data().count() + (uint)leftViewBuf.data().count() - 2;
    uint rightSize = (uint)rightMetadataBuf.data().count() + (uint)rightViewBuf.data().count() - 2;
    uint rightOffset = leftSize - (uint)getMpoExtension(leftMetadata)->startPos();

    // Заполняем окончательные метаданные для первого изображения
    fillLeftMetadata(leftMetadata, leftSize, rightSize, rightOffset);

    // Сохраняем окончательные метаданные для первого изображения в буфер
    leftMetadataBuf.open(QBuffer::WriteOnly|QBuffer::Truncate);
    leftMetadata.save(leftMetadataBuf);
    leftMetadataBuf.close();

    // Записываем всё в файл...
    QFile file(fileName);
    if(!file.open(QIODevice::Truncate|QIODevice::WriteOnly))
    {
        qWarning() << "can not open file: " << file.errorString();
        return;
    }

    // Метаданные первого изображения
    file.write(leftMetadataBuf.data());

    // Первое изображение без начального маркера
    file.write(leftViewBuf.data().data() + 2, leftViewBuf.data().count() - 2);

    // Метаданные второго изображения
    file.write(rightMetadataBuf.data());

    // Второе изображение без начального маркера
    file.write(rightViewBuf.data().data() + 2, rightViewBuf.data().count() - 2);

    file.close();
}

void MpoImageFormat::save(StereoImageSource *source, QString fileName, int quality)
{
    save(source, fileName, true, QSize(), quality);
}

StereoImageSource* MpoImageFormat::error(StereoImageSource *target, QString fileName, QString error)
{
    target->setOriginalFrame(StereoFrame());
    qWarning() << QString("'%1': %2").arg(fileName).arg(error);
    return (StereoImageSource*)nullptr;
}

MpoExtension *MpoImageFormat::getMpoExtension(JpegMetadata &d)
{
    MpoExtension* mpoExtension;
    auto extensions = d.getExtensions<MpoExtension>(APP2);
    if(extensions.empty())
    {
        mpoExtension = new MpoExtension();
        d.appendExtension(APP2, (JpegMetadataExtension*)mpoExtension);
    }
    else
        mpoExtension = extensions.first();
    return mpoExtension;
}

void MpoImageFormat::fillLeftMetadata(JpegMetadata &d, uint size1, uint size2, uint offset2)
{
    stereoscopic::fileFormats::mpo::MpoExtension* mpoExtension = getMpoExtension(d);
    MpIfd* index = new MpIfd();
    index->setDefaultVersion();
    index->setNumberOfImages(2);
    QList<ImageEntry>* entries = index->imageEntries();
    entries->clear();
    char imageAttr1[4] = { 0x02, 0x00, 0x02, 0x20 };
    char imageAttr2[4] = { 0x02, 0x00, 0x02, 0x00 };
    entries->append(ImageEntry(imageAttr1, size1, 0));
    entries->append(ImageEntry(imageAttr2, size2, offset2));

    mpoExtension->setIfdIndex(index);

    MpIfd* attr = new MpIfd();
    attr->setIndividualNum(1);
    attr->setBaseViewpointNum(1);
    attr->setConvergenceAngle(RationalNumber<int>(1, 136));
    attr->setBaselineLength(RationalNumber<uint>(1, 144));
    mpoExtension->setIfdAttributes(attr);
}

void MpoImageFormat::fillRightMetadata(JpegMetadata &d)
{
    stereoscopic::fileFormats::mpo::MpoExtension* mpoExtension = getMpoExtension(d);

    MpIfd* attr = new MpIfd();
    attr->setDefaultVersion();
    attr->setIndividualNum(2);
    attr->setBaseViewpointNum(1);
    attr->setConvergenceAngle(RationalNumber<int>(1, 74));
    attr->setBaselineLength(RationalNumber<uint>(1, 82));

    mpoExtension->setIfdAttributes(attr);
}

bool MpoImageFormat::checkLeftMetadata(JpegMetadata& d, int& secondImageOffset)
{
    MpoExtension* ext = getMpoExtension(d);
    if(!ext)
        return false;
    MpIfd* index = ext->ifdIndex();
    if(!index)
        return false;
    if(index->numberOfImages() != 2)
        return false;
    QList<ImageEntry>* entries = index->imageEntries();
    if(!entries || entries->count() != 2)
        return false;
    // Вычисляем адрес второго изображения
    secondImageOffset = (int)entries->at(1).offset + (int)ext->startPos();
    return true;
}

int MpoImageFormat::searchSecondImageOffset(QByteArray &data)
{
    char search[] = { (char)0xFF, (char)0xD8 };
    int startIndex = (int)(data.length() * 0.4);
    QByteArrayMatcher matcher(QByteArray(search, 2));
    int res = matcher.indexIn(data, startIndex);
    if(res == -1)
        res = 0;
    return res;
}

#ifdef TRACE

void MpoImageFormat::debugOutputMetadata(JpegMetadata &d)
{
    auto mpoExtensions = d.getExtensions<MpoExtension>(APP3);
    if(mpoExtensions.length() < 1)
        return;
    auto mpoExtension = mpoExtensions.first();
    if(mpoExtension->ifdIndex())
    {
        qDebug() << "Index:";
        debugOutputIfd(mpoExtension->ifdIndex());
    };
    if(mpoExtension->ifdAttributes())
    {
        qDebug() << "Attributes:";
        debugOutputIfd(mpoExtension->ifdAttributes());
    }
}

void MpoImageFormat::debugOutputIfd(MpIfd *ifd)
{
    for(auto e : ifd->tags().toStdMap())
    {
        qDebug() << QString::number((int)e.first, 16) << e.second->toString();
    }
}

#endif
