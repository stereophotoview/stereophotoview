#ifndef MPOIMAGEFORMAT_H
#define MPOIMAGEFORMAT_H

#include <QObject>
#include <QString>
#include "stereoimagesource.h"
#include "singleimageformat.h"
#include "jpeg/jpegmetadata.h"
#include "mpo/mpoextension.h"
#include "mpo/ifd.h"

using namespace stereoscopic::fileFormats::mpo;

namespace stereoscopic::fileFormats
{
    using namespace stereoscopic::fileFormats::jpeg;

    /**
     * @brief Класс, предоставляющий поддержку формата MPO
     * @ref ../docs/MPO.txt
     */
    class LIBSHARED_EXPORT  MpoImageFormat : public SingleImageFormat
    {
            Q_OBJECT
            Q_PROPERTY(QStringList extensions READ extensions)
        public:
            explicit MpoImageFormat(QObject *parent = nullptr);

        public slots:
            /**
             * @brief Загружает стереоскопическое изображение из MPO файла
             * @param fileName Путь к файлу
             * @param defUserFormat Формат файла по умолчанию
             * @return Источник стереоскопического изображения
             */
            stereoscopic::StereoImageSource* load(QString fileName, StereoFormat* defUserFormat);

            /**
             * @brief Сохраняет стереоскопическое изображение в MPO файл
             * @param source Исходное стереоскопическое изображение
             * @param fileName Путь к файлу
             * @param leftFirst Поместить левый ракурс перед правым
             * @param targetSize Целевой размер ракурсов
             * @param quality Качество JPEG
             */
            void save(StereoImageSource * source, QString fileName, bool leftFirst, QSize targetSize, int quality);

            /**
             * @brief Сохраняет стереоскопическое изображение в MPO файл
             * @param source Исходное стереоскопическое изображение
             * @param fileName Путь к файлу
             * @param quality Качество JPEG
             */
            void save(StereoImageSource * source, QString fileName, int quality);

        private:
            StereoImageSource* error(StereoImageSource *target, QString fileName, QString error);
            stereoscopic::fileFormats::mpo::MpoExtension* getMpoExtension(JpegMetadata &d);
            void fillLeftMetadata(JpegMetadata &d, uint size1, uint size2, uint offset2);
            void fillRightMetadata(JpegMetadata &d);
            bool checkLeftMetadata(JpegMetadata & d, int& secondImageOffset);
            int searchSecondImageOffset(QByteArray &data);
#ifdef TRACE
            void debugOutputMetadata(JpegMetadata &d);
            void debugOutputIfd(MpIfd *ifd);
#endif
    };

}

#endif // MPOIMAGEFORMAT_H
