#include "separateimageformat.h"
#include "qdebug.h"
#include "qfile.h"
#include "../stereoimagesource.h"
#include "../imagehelper.h"
#include <QFileInfo>

using namespace stereoscopic::fileFormats;
using namespace stereoscopic::fileFormats::jpeg;

SeparateImageFormat::SeparateImageFormat(QObject *parent) : QObject(parent)
{
    extensions_.append("jpg");
    extensions_.append("jpeg");
    extensions_.append("jps");
}

void SeparateImageFormat::saveLeft(StereoImageSource *source, QString fileName, int quality)
{
    if(!source)
    {
        qWarning() << "source is not defined";
        return;
    }
    QImage pixmap = source->displayFrame()->leftFullView();
    ExifHeaders* metadata = source->leftExifHeaders();
    saveImage(fileName, &pixmap, metadata, quality);
}

void SeparateImageFormat::saveRight(StereoImageSource *source, QString fileName, int quality)
{
    if(!source)
    {
        qWarning() << "source is not defined";
        return;
    }
    QImage pixmap = source->displayFrame()->rightFullView();
    ExifHeaders* metadata = source->rightExifHeaders();
    saveImage(fileName, &pixmap, metadata, quality);
}

bool SeparateImageFormat::checkFileName(QString fileName)
{
    auto filePaths = fileName.split("\n");
    if(filePaths.length() != 2)
        return false;
    QFileInfo f(filePaths.first());
    QString fileExt = f.suffix().toLower();
    return extensions_.contains(fileExt);
}

StereoImageSource *SeparateImageFormat::load(QString fileName, StereoFormat* defUserFormat)
{
    auto filePaths = fileName.split("\n");

    JpegMetadata leftMetadata, rightMetadata;
    QImage leftImage, rightImage;

    if(loadView(filePaths[0], leftMetadata, leftImage) && loadView(filePaths[1], rightMetadata, rightImage))
    {
        auto target = new StereoImageSource(StereoFrame(leftImage, rightImage), defUserFormat);
        target->setLeftExifHeaders(leftMetadata.exifHeader());
        target->setRightExifHeaders(rightMetadata.exifHeader());
        return target;
    }
    else
    {
        return nullptr;
    }
}

void SeparateImageFormat::saveImage(QString fileName, QImage *pixmap, ExifHeaders *exif, int quality)
{
    JpegMetadata metadata;
    metadata.setExifHeaders(exif->header(), pixmap);

    QBuffer pixmapBuf, metadataBuf;

    // Сохраняем метаданные в буфер
    metadataBuf.open(QBuffer::WriteOnly);
    metadata.save(metadataBuf);
    metadataBuf.close();

    // Сохранем изображение в буфер
    pixmapBuf.open(QBuffer::WriteOnly);
    pixmap->save(&pixmapBuf, "JPEG", quality);
    pixmapBuf.close();

    // Записываем всё в файл...
    QFile file(fileName);
    if(!file.open(QIODevice::Truncate|QIODevice::WriteOnly))
    {
        qWarning() << "can not open file: " << file.errorString();
        return;
    }

    // Метаданные изображения
    file.write(metadataBuf.data());

    // Изображение без начального маркера
    file.write(pixmapBuf.data().data() + 2, pixmapBuf.data().count() - 2);
    file.close();

}

bool SeparateImageFormat::loadView(QString fileName, JpegMetadata &metadata, QImage &image)
{
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
    {
        qWarning() << "can not open file: " << file.errorString();
        return false;
    }
    QByteArray data = file.readAll();
    file.close();
    metadata.loadFromData(data);
    if(!image.loadFromData((data)))
    {
        qWarning() << "can not load pixmap";
        return false;
    }
    image = ImageHelper::applyOrientation(image, metadata.exifHeader().value(QExifImageHeader::Orientation).toShort());
    return true;
}
