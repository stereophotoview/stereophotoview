#ifndef SEPARATEFORMAT_H
#define SEPARATEFORMAT_H

#include <QObject>
#include <QList>
#include "../stereoimagesource.h"
#include "jpeg/jpegmetadata.h"

using namespace stereoscopic;
using namespace stereoscopic::fileFormats::jpeg;

namespace stereoscopic::fileFormats {
    /**
     * @brief Класс, предоставляющий поддержку формата отдельных файлов
     */
    class LIBSHARED_EXPORT SeparateImageFormat : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QStringList extensions READ extensions)

        public:
            explicit SeparateImageFormat(QObject *parent = nullptr);
            QStringList extensions(){ return extensions_; }

        signals:

        public slots:
            void saveLeft(StereoImageSource * source, QString fileName, int quality);
            void saveRight(StereoImageSource * source, QString fileName, int quality);
            bool checkFileName(QString fileName);

            stereoscopic::StereoImageSource* load(QString fileName, StereoFormat *defUserFormat);

        private:
            void saveImage(QString fileName, QImage *pixmap, ExifHeaders* exif, int quality);
            bool loadView(QString fileName, JpegMetadata& metadata, QImage & image);
            QStringList extensions_;

    };
}
#endif // SEPARATEFORMAT_H
