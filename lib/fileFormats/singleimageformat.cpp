#include "singleimageformat.h"

#include <QFileInfo>

using namespace stereoscopic::fileFormats;

bool SingleImageFormat::checkFileName(QString fileName)
{
    QFileInfo f(fileName);
    QString fileExt = f.suffix().toLower();
    return extensions_.contains(fileExt);
}
