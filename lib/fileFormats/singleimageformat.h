#ifndef SINGLEFILEFORMAT_H
#define SINGLEFILEFORMAT_H

#include <QObject>

#include "stereoimagesource.h"
namespace stereoscopic::fileFormats {

    /**
     * @brief Базовый класс для добавления поддержки формата файла
     */
    class LIBSHARED_EXPORT SingleImageFormat : public QObject
    {
            Q_OBJECT
        public:
            explicit SingleImageFormat(QObject *parent = nullptr) : QObject(parent){ }

            /// Возвращает список расширений файлов данного формата
            QStringList extensions(){ return extensions_; }

        public slots:
            bool checkFileName(QString fileName);

        protected:
            QStringList extensions_;
    };
}
#endif // SINGLEFILEFORMAT_H
