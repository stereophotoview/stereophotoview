#include "demux.h"

using namespace stereoscopic::fileFormats::video;

Demux::~Demux()
{
    close();
    foreach(InStream* stream, m_streams)
        delete stream;
}

QList<AVInputFormat *> Demux::supportedFormats()
{
    QList<AVInputFormat*> res;
    av_register_all();
#if !DEBUG
    av_log_set_level(AV_LOG_FATAL);
#endif
    AVInputFormat * iformat = av_iformat_next(nullptr);
    while(iformat != nullptr)
    {
        res.append(iformat);
        iformat = av_iformat_next(iformat);
    }
    return res;
}

double Demux::duration()
{
    if(formatContext == nullptr)
        return 0;
    return (double)((int64_t)formatContext->duration / AV_TIME_BASE);
}

ReadResult::Result Demux::readPacket(InPacket &resultPacket)
{
    if(!formatContext)
        return lastResult = ReadResult::ERR;
    for(;;)
    {
        AVPacket* packet = av_packet_alloc();
        int res = av_read_frame(formatContext, packet);
        if(res >= 0)
        {
            // Если нашли поток в streams, возвращаем пакет
            foreach (InStream* stream, m_streams)
            {
                if(packet->stream_index == stream->index())
                {
                    resultPacket = InPacket(stream, packet);
                    return ReadResult::parseResult(res);
                }
            }
            av_packet_unref(packet);
            av_packet_free(&packet);
            // Если потока нет в streams, переходим к чтению следующего пакета
            continue;
        }
        // Если неудачное чтение - выходим
        return lastResult = ReadResult::parseResult(res);
    }
}

const Metadata &Demux::metadata()
{
    if(m_metadata.isNull())
        m_metadata = QSharedPointer<Metadata>(new Metadata(formatContext ? formatContext->metadata : nullptr));
    return *(m_metadata.data());
}

int Demux::open(QString fileName)
{
    av_register_all();
#if !DEBUG
    av_log_set_level(AV_LOG_FATAL);
#endif
    int error = avformat_open_input(&formatContext, fileName.toUtf8().data(), nullptr, nullptr);
    if (error < 0)
    {
        qWarning() << "ffmpeg: Unable to open input file" << fileName;
        return error;
    }
    error = avformat_find_stream_info(formatContext, nullptr);
    if (error < 0)
    {
        qWarning() << "ffmpeg: Unable to find stream info" << fileName;
        return error;
    }

    uint videoStreamNumber = 0;
    for (uint i = 0; i < formatContext->nb_streams; ++i)
    {
        if (formatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            InVideoStream* stream = new InVideoStream();
            if(!stream->open(formatContext, (int)i))
            {
                delete stream;
                continue;
            }
            if(stream->frameRate() > 60)
            {
                stream->close();
                delete stream;
                continue;
            }
            m_videoStreams.append(stream);
            m_streams.append(stream);

            error = stream->error();
            if(error < 0)
            {
                qWarning() << "unable to open stream " << i << ": " << error;
                return error;
            }
            videoStreamNumber++;
        }
    }

    int audioStreamIndex = av_find_best_stream(formatContext, AVMEDIA_TYPE_AUDIO, -1, -1, nullptr, 0);
    if(audioStreamIndex >= 0)
    {
        m_bestAudioStream = new InAudioStream();
        m_streams.append(m_bestAudioStream);
        m_bestAudioStream->open(formatContext, audioStreamIndex);
        error = m_bestAudioStream->error();
        if(error < 0)
        {
            qWarning() << "unable to open stream " << audioStreamIndex << ": " << error;
            return error;
        }
    }
    else
    {
        qWarning() << "ffmpeg: Unable to find audio stream";
        //return -2;
    }

    return error;
}

void Demux::close()
{
    foreach(InStream *stream, m_streams)
        stream->close();
    avformat_close_input(&formatContext);
    formatContext = nullptr;
}

void Demux::seek(double time)
{
    for(int i = 0; i < streams().length(); i++)
    {
        auto stream = streams()[i];
        auto timeStamp = stream->frameNumByTime(time);
        bool res = av_seek_frame(formatContext, stream->index(), timeStamp, AVSEEK_FLAG_ANY) >= 0;
        lastResult = res ? ReadResult::OK : ReadResult::ERR;
    }
}

InVideoFrame *Demux::seekToKeyFrame(InVideoStream *stream)
{
    InVideoFrame *videoFrame = nullptr;
    // Ищем ключевой кадр среди 1000 пакетов
    for(int i = 0; i < 1000; i++)
    {
        InPacket packet;
        InFrame* frame = nullptr;
        if(readPacket(packet) == ReadResult::OK)
        {
            if(packet.stream() == stream)
            {
                if(packet.decode(frame) == ReadResult::OK)
                {
                    videoFrame = dynamic_cast<InVideoFrame*>(frame);
                    if(videoFrame != nullptr && videoFrame->isKey())
                        break;
                    delete frame;
                    frame = nullptr;
                    videoFrame = nullptr;
                }
            }
        }
    }
    return videoFrame;
}

