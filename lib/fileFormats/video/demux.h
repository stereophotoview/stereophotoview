#ifndef DEMUX_H
#define DEMUX_H


#include <QFile>
#include <QList>
#include <QStandardPaths>
#include <QThread>

#include "instream.h"
#include "invideostream.h"
#include "inaudiostream.h"
#include "inpacket.h"
#include "readresult.h"
#include "metadata.h"
#include <QSharedPointer>

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Класс - демультиплексор видео файла
     */
    class LIBSHARED_EXPORT Demux
    {
        public:
            explicit Demux() { }
            ~Demux();

            /// Возвращает список поддерживаемых форматов
            static QList<AVInputFormat*> supportedFormats();

            /// Возвращает список потоков
            QList<InStream*> &streams() { return m_streams; }

            /// Возвращает список видео потоков
            QList<InVideoStream*> &videoStreams() { return m_videoStreams; }

            /// Возвращает наиболее подходящий аудио поток
            InAudioStream* bestAudioStream(){ return m_bestAudioStream; }

            /// Возвращает продолжительность в секундах
            double duration();

            /// Открывает видео файл
            int open(QString fileName);

            /// Закрывает видео файл
            void close();

            /// Возвращает признак того, что поток закончился
            bool eos(){ return lastResult == ReadResult::EOS; }

            /// Возвращает признак того, что произошла ошибка
            bool error() { return lastResult == ReadResult::ERR; }

            /**
             * @brief Перематывает на данный момент времени
             */
            void seek(double time);

            /**
             * @brief Ищет ключевой кадр данного потока в указанном моменте времени
             * @param stream Поток
             * @return Кадр
             */
            InVideoFrame* seekToKeyFrame(InVideoStream* stream);

            /**
             * @brief Читает следующий пакет
             * @param[out] packet Пакет
             * @return Результат чтения
             */
            ReadResult::Result readPacket(InPacket &packet);

            /// Возвращает значение метаданных потока по ключу
            const Metadata& metadata();
        private:
            QList<InStream*> m_streams;
            QList<InVideoStream*> m_videoStreams;
            InAudioStream* m_bestAudioStream = nullptr;
            AVFormatContext* formatContext = nullptr;
            ReadResult::Result lastResult = ReadResult::OK;
            QSharedPointer<Metadata> m_metadata;
    };
}
#endif // DEMUX_H
