#include "ffmpeghelper.h"

using namespace stereoscopic;
using namespace stereoscopic::fileFormats::video;

FFMpegHelper::FFMpegHelper()
{

}

QString FFMpegHelper::errorString(int code)
{
    char* str = new char[AV_ERROR_MAX_STRING_SIZE]{0};
    av_make_error_string(str, AV_ERROR_MAX_STRING_SIZE, code);
    return QString(str);
}

int FFMpegHelper::bytesPerSample(AVSampleFormat fmt)
{
    switch (fmt) {
        case AV_SAMPLE_FMT_U8:
        case AV_SAMPLE_FMT_U8P:
            return 1;
        case AV_SAMPLE_FMT_S16:
        case AV_SAMPLE_FMT_S16P:
            return 2;
        case AV_SAMPLE_FMT_S32:
        case AV_SAMPLE_FMT_S32P:
        case AV_SAMPLE_FMT_FLTP:
        case AV_SAMPLE_FMT_FLT:
            return 4;
        case AV_SAMPLE_FMT_DBL:
        case AV_SAMPLE_FMT_DBLP:
            return 8;
        default:
            return -1;
    }
}

int FFMpegHelper::stereo_mode(StereoFormat *f)
{
    switch (f->layout()) {
        case StereoFormat::RowInterlaced:
            return f->leftFirst() ? 7 : 6;
        case StereoFormat::ColumnInterlaced:
            return f->leftFirst() ? 9 : 8;
        case StereoFormat::AnamorphHorizontal:
            return f->leftFirst() ? 1 : 11;
        case StereoFormat::AnamorphVertical:
            return f->leftFirst() ? 3 : 2;
        case StereoFormat::AnaglyphRC:
            return 10;
        default:
            return 0;
    }
}

StereoFormat FFMpegHelper::stereo_format(QString stereoMode)
{
    if(stereoMode == "left_right")
        return StereoFormat(StereoFormat::AnamorphHorizontal);
    if(stereoMode == "bottom_top")
        return StereoFormat(StereoFormat::AnamorphVertical).setLeftFirst(false);
    if(stereoMode == "top_bottom")
        return StereoFormat(StereoFormat::AnamorphVertical);
    if(stereoMode == "row_interleaved_rl")
        return StereoFormat(StereoFormat::RowInterlaced).setLeftFirst(false);
    if(stereoMode == "row_interleaved_lr")
        return StereoFormat(StereoFormat::RowInterlaced);
    if(stereoMode == "col_interleaved_rl")
        return StereoFormat(StereoFormat::ColumnInterlaced).setLeftFirst(false);
    if(stereoMode == "col_interleaved_lr")
        return StereoFormat(StereoFormat::ColumnInterlaced);
    if(stereoMode == "anaglyph_cyan_red")
        return StereoFormat(StereoFormat::AnaglyphRC).setLeftFirst(false);
    if(stereoMode == "right_left")
        return StereoFormat(StereoFormat::AnamorphHorizontal).setLeftFirst(false);
    return StereoFormat();
}

int FFMpegHelper::frame_packing(StereoFormat *f)
{
    switch (f->layout()) {
        case StereoFormat::RowInterlaced:
            return 2;
        case StereoFormat::ColumnInterlaced:
            return 1;
        case StereoFormat::AnamorphHorizontal:
            return 3;
        case StereoFormat::AnamorphVertical:
            return 4;
        default:
            return -1;
    }
}

StereoFormat::Layout FFMpegHelper::layout(AVStereo3DType type)
{
    switch (type) {
        case AV_STEREO3D_2D:
            return StereoFormat::Monoscopic;
        case AV_STEREO3D_SIDEBYSIDE:
            return StereoFormat::AnamorphHorizontal;
        case AV_STEREO3D_TOPBOTTOM:
            return StereoFormat::AnamorphVertical;
        case AV_STEREO3D_LINES:
            return StereoFormat::RowInterlaced;
        case AV_STEREO3D_COLUMNS:
            return StereoFormat::ColumnInterlaced;
        default:
            return (StereoFormat::Layout)-1;
    }
}
