#ifndef FFMPEGHELPER_H
#define FFMPEGHELPER_H


#include <QString>
#include "stereoformat.h"

#include "stereoformat.h"

extern "C"
{
#include <libavformat/avformat.h>
#include <libavutil/stereo3d.h>
}

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Вспомагательный класс для работы с библиотекой ffmpeg
     */
    class LIBSHARED_EXPORT FFMpegHelper
    {
        public:
            FFMpegHelper();
            /**
             * @brief Возвращает тектовое описание ошибки по её коду
             * @param Код ошибки ffmpeg
             * @return Текст ошибки
             */
            static QString errorString(int code);

            /**
             * @brief Возвращает колиечтсво байт на один семпл по формату семпла
             * @param fmt
             * @return
             */
            static int bytesPerSample(AVSampleFormat fmt);

            /**
             * @brief Возвращает стерео режим для метаданных
             * @param Стерео формат
             * @return Стандартное числовое представление стерео формата
             */
            static int stereo_mode(StereoFormat *f);

            /**
             * @brief Возвращает стерео формат по строке из метаданных
             * @param stereoMode
             * @return
             */
            static StereoFormat stereo_format(QString stereoMode);

            /**
             * @brief Возвращает тип расположения ракурсов на кадре для метаданных
             * @param Стерео формат
             * @return Число, представляющее тип расположения ракурсов на кадре для метаданных
             */
            static int frame_packing(StereoFormat *f);

            /**
             * @brief Возвращает расположение ракурсов по метаданным
             * @param Тип стерео из метаданных
             * @return Стерео формат
             */
            static StereoFormat::Layout layout(AVStereo3DType type);
    };
}

#endif // FFMPEGHELPER_H
