#include "inaudioframe.h"

using namespace stereoscopic::fileFormats::video;

InAudioFrame::InAudioFrame()
    : InFrame(nullptr, 0, 0)
{

}

InAudioFrame::InAudioFrame(InStream* stream, double timeBase, double pts, QByteArray data)
    : InFrame(stream, timeBase, pts)
{
    m_data = data;
}
