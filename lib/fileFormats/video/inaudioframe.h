#ifndef AUDIOFRAME_H
#define AUDIOFRAME_H

#include "inframe.h"

#include <QByteArray>
#include "lib_global.h"

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Класс кадра входного аудио потока
     */
    class LIBSHARED_EXPORT InAudioFrame : public InFrame
    {
        public:
            /**
             * @brief InAudioFrame
             */
            InAudioFrame();

            /**
             * @brief Создаёт кадр входного аудио потока
             * @param stream Входной поток
             * @param timeBase Единица измерения времени для pts
             * @param framePts Количество кадров в секунду
             * @param pts Время представления кадра в едницах timebase
             * @param data Данные кадра
             */
            InAudioFrame(InStream *stream, double timeBase, double pts, QByteArray data);

            /**
             * @brief Возвращает данные кадра
             * @return Массив байт содержимого кадра
             */
            QByteArray data() { return m_data; }

            ~InAudioFrame()
            {

            }
        private:
            QByteArray m_data;
    };
}

#endif // AUDIOFRAME_H
