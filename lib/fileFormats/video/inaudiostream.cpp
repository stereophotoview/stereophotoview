#include "inaudiostream.h"

using namespace stereoscopic::fileFormats::video;

bool InAudioStream::sampleFmtIsPlain()
{
    AVSampleFormat fmt = m_codecContext->sample_fmt;
    switch (fmt) {
        case AV_SAMPLE_FMT_U8P:
        case AV_SAMPLE_FMT_S16P:
        case AV_SAMPLE_FMT_S32P:
        case AV_SAMPLE_FMT_FLTP:
        case AV_SAMPLE_FMT_DBLP:
            return true;
        default:
            return false;
    }
}

QAudioFormat::SampleType InAudioStream::sampleType()
{
    AVSampleFormat fmt = m_codecContext->sample_fmt;
    switch (fmt) {
        case AV_SAMPLE_FMT_U8:
        case AV_SAMPLE_FMT_U8P:
            return QAudioFormat::UnSignedInt;
        case AV_SAMPLE_FMT_S16:
        case AV_SAMPLE_FMT_S32:
        case AV_SAMPLE_FMT_S16P:
        case AV_SAMPLE_FMT_S32P:
            return QAudioFormat::SignedInt;
        case AV_SAMPLE_FMT_FLT:
        case AV_SAMPLE_FMT_DBL:
        case AV_SAMPLE_FMT_FLTP:
        case AV_SAMPLE_FMT_DBLP:
            return QAudioFormat::Float;
        default:
            return QAudioFormat::Unknown;
    }
}

QAudioFormat InAudioStream::qAudioFormat()
{
    QAudioFormat format;
    format.setSampleRate(sampleRate());
    format.setChannelCount(channels());
    format.setSampleSize(sampleBytes() * 8);
    format.setCodec("audio/pcm");
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(sampleType());
    return format;
}

int InAudioStream::decodePacket(AVPacket *packet, InFrame *&frame)
{
    int res = decodePacketRaw(packet);
    if(res == 0)
    {
        QByteArray res;
        int frameSize = this->frameSize();
        int channels = m_frame->channels;
        if(sampleFmtIsPlain())
        {
            int _sampleBytes = sampleBytes();
            int dataSize = frameSize != 0 ? _sampleBytes * frameSize : m_frame->linesize[0];
            int outputBufferLen = dataSize * channels;
            char* buf = new char[outputBufferLen];
            int offset = 0;
            for(int i = 0; i < dataSize; i = i + _sampleBytes)
            {
                for(int channel = 0; channel < channels; channel++)
                {
                    for(int byte = 0; byte < _sampleBytes; byte++ )
                    {
                        buf[offset++] = m_frame->data[channel][i + byte];
                    }
                }
            }
            res = QByteArray(buf, outputBufferLen);
            delete[] buf;
        }
        else
        {
            int dataSize = frameSize != 0 ? sampleBytes() * frameSize * channels: m_frame->linesize[0];
            res = QByteArray((char*)m_frame->data[0], dataSize);
        }
        frame = new InAudioFrame(this, av_q2d(m_codecContext->time_base), getPts(packet), res);
    }
    return res;
}

void InAudioStream::close()
{
    InStream::close();
}

int InAudioStream::sampleBytes()
{
    return FFMpegHelper::bytesPerSample(m_codecContext->sample_fmt);
}
