#ifndef AUDIOSTREAM_H
#define AUDIOSTREAM_H

#include "instream.h"
#include "inaudioframe.h"
#include "ffmpeghelper.h"
#include <QByteArray>
#include <QBuffer>
#include <QAudioFormat>

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Класс входного аудио потока
     */
    class LIBSHARED_EXPORT InAudioStream : public InStream
    {
        public:
            explicit InAudioStream() : InStream() { }

            /**
             * @brief Закрывает поток
             */
            virtual void close();

            /**
             * @brief Возвращает количество байтов в семпле для данного потока
             * @return
             */
            int sampleBytes();

            /**
             * @brief Возвращает признак того, что используется плоский формат семплов
             * @return true, если используется плоский формат семплов
             */
            bool sampleFmtIsPlain();
            /// Возвращает тип семплов
            QAudioFormat::SampleType sampleType();

            /// Возвращает скорость отображения семплов в секнду
            int sampleRate() { return m_codecContext->sample_rate; }

            /// Возвращает размер семпла в байтах
            int frameSize() { return m_codecContext->frame_size; }

            /// Возвращает количество аудио каналов в данном потоке
            int channels() { return m_codecContext->channels; }

            /// Возвращает тип расположения аудио каналов
            ulong channelLayout() { return m_codecContext->channel_layout; }

            /// Возвращает формат семплов
            AVSampleFormat SampleFmt(){ return m_codecContext->sample_fmt; }

            /// Возвращает аудио формат
            QAudioFormat qAudioFormat();

            /// Всегда возвращает false
            bool isVideo() { return false; }

        protected:

            virtual int decodePacket(AVPacket* packet, InFrame *&frame);

        signals:
            void decoded(InAudioStream* sender, QByteArray bytes, double pts);

    };
}


#endif // AUDIOSTREAM_H
