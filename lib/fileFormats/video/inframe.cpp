#include "inframe.h"

using namespace stereoscopic::fileFormats::video;

InFrame::InFrame(InStream *stream, double timeBase, double pts)
{
    m_timeBase = timeBase;
    m_pts = pts;
    m_stream = stream;
}

InFrame::InFrame(InFrame &src)
{
    m_timeBase = src.m_timeBase;
    m_pts = src.m_pts;
    m_stream = src.m_stream;
}
