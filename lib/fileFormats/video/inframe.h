#ifndef FRAME_H
#define FRAME_H

#include "lib_global.h"

namespace stereoscopic::fileFormats::video
{
    // Предопределяем класс входного потока
    class InStream;

    /**
     * @brief Базовый класс для кадра входного потока
     */
    class LIBSHARED_EXPORT InFrame
    {
        public:
            /**
             * @brief Конструктор
             * @param stream Входной поток
             * @param timeBase Единица измерения времени для pts
             * @param num Оригинальное значение pts из ffmpeg
             * @param pts Время отображения в единицах timeBase
             */
            InFrame(InStream *stream, double timeBase, double pts);
            InFrame(InFrame &src);
            virtual ~InFrame() { }

            /// Возвращает время отображения
            double ptsInSeconds(){ return m_pts; }

            /// Поток, к которому относится кадр
            InStream* stream(){ return m_stream; }

            /// Признак того, что кадр пустой
            bool isNull() { return m_timeBase == 0; }

        protected:
            double m_pts = 0, m_timeBase = 0;
            InStream* m_stream = 0;
    };
}
#endif // FRAME_H
