#include "inpacket.h"

using namespace stereoscopic::fileFormats::video;

InPacket::InPacket(InStream *stream, AVPacket *packet)
{
    m_stream = stream;
    m_packet = std::shared_ptr<AVPacket>(packet, &InPacket::packetDeleter);
}

ReadResult::Result InPacket::decode(InFrame *&frame)
{
    AVPacket *packet = m_packet.get();
    if(m_stream != NULL)
    {
        int res = m_stream->decodePacket(packet, frame);
        return ReadResult::parseResult(res);
    }
    return ReadResult::OK;
}

ReadResult::Result InPacket::decodeRaw(AVFrame *&frame)
{
    frame = nullptr;
    AVPacket *packet = m_packet.get();
    if(m_stream != nullptr)
    {
        int res = m_stream->decodePacketRaw(packet);
        frame = m_stream->avframe();
        return ReadResult::parseResult(res);
    }
    return ReadResult::OK;
}

void InPacket::packetDeleter(AVPacket *packet)
{
    av_packet_unref(packet);
    av_packet_free(&packet);
}
