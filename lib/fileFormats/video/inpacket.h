#ifndef PACKET_H
#define PACKET_H

#include "instream.h"
#include "readresult.h"
#include "inframe.h"

#include <memory>
#include <QDebug>

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Базовый класс для пакета входных данных
     * Это обёртка над AVPacket
     */
    class LIBSHARED_EXPORT InPacket
    {
        public:
            InPacket() { }

            /**
             * @brief Конструктор
             * @param m_stream Входной поток
             * @param m_packet Пакет ffmpeg
             */
            InPacket(InStream* m_stream, AVPacket *m_packet);

            /**
             * @brief Декодирует пакет
             * @param frame[out] Декодированный кадр, если это завершающий пакет кадра
             * @return Результат декодирования
             */
            ReadResult::Result decode(InFrame *&frame);

            /**
             * @brief Декодирует пакет, не оборачивая результирующий кадр в обёртку InFrame
             * @param frame[out] Декодированный кадр, если это завершающий пакет кадра
             * @return Результат декодирования
             */
            ReadResult::Result decodeRaw(AVFrame *&frame);

            /// Возвращает поток, которому принадлежит данный кадр
            InStream* stream() { return m_stream; }

            /// Пакет ffmpeg
            AVPacket* avPacket() { return m_packet.get(); }

            /// Возвращает время отображения пакета в секундах
            double pts()
            {
                AVPacket* p = m_packet.get();
                return m_stream != nullptr && p != nullptr ? m_stream->getPts(p) : 0;
            }

        private:
            InStream* m_stream = nullptr;
            std::shared_ptr<AVPacket> m_packet;
            double oldPts = 0;
            static void packetDeleter(AVPacket *packet);

    };
}
#endif // PACKET_H
