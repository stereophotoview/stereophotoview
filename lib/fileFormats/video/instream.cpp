#include "instream.h"

using namespace stereoscopic::fileFormats::video;

bool InStream::open(AVFormatContext *format_context, int streamIndex)
{
    close();
    m_index = streamIndex;
    m_avStream = format_context->streams[streamIndex];
    m_codecParameters = m_avStream->codecpar;
    m_codec = avcodec_find_decoder(m_codecParameters->codec_id);
    m_codecContext = avcodec_alloc_context3(m_codec);
    m_error = avcodec_parameters_to_context(m_codecContext, m_codecParameters);
    if(m_error < 0)
        return false;
    m_error = avcodec_open2(m_codecContext, m_codec, NULL);
    if (m_error < 0)
    {
        qWarning() << "ffmpeg: Unable to open codec: " << m_error;
        return false;
    }
    m_frame = av_frame_alloc();
    return true;
}

void InStream::close()
{
    if(m_codecContext)
    {
        avcodec_close(m_codecContext);
        avcodec_free_context(&m_codecContext);
    }
    if(m_frame)
    {
        av_frame_free(&m_frame);
        m_frame = nullptr;
    }
}

int InStream::decodePacketRaw(AVPacket *packet)
{
    int res = avcodec_send_packet(m_codecContext, packet);
    if(res == 0)
    {
        res = avcodec_receive_frame(m_codecContext, m_frame);
    }
    return res;
}

double InStream::getPts(AVFrame* frame)
{
    return av_q2d(m_codecContext->time_base) * (double)frame->pts;
}

double InStream::getPts(AVPacket* packet)
{
    auto res = packet->dts;
    if (res == AV_NOPTS_VALUE) {
        res = packet->pts;
    }
    if (res == AV_NOPTS_VALUE) {
        res = oldPts;
    }
    oldPts = res;
    return av_q2d(m_avStream->time_base) * res;
}

const Metadata& InStream::metadata()
{
    if(m_metadata.isNull())
        m_metadata = QSharedPointer<Metadata>(new Metadata(m_avStream->metadata));
    return *(m_metadata.data());
}
