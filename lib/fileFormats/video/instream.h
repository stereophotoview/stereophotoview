#ifndef STREAM_H
#define STREAM_H

#include <QThread>
#include <QDebug>
#include <QMutex>
#include <QWaitCondition>
#include <QQueue>
#include <QMap>
#include <QSharedPointer>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

#include "inframe.h"
#include "metadata.h"

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Базовый класс входящего мультимедиа потока
     */
    class LIBSHARED_EXPORT InStream
    {
        public:
            explicit InStream(){ }

            virtual ~InStream()
            {
                close();
            }
            /**
             * @brief Открывает поток для чтения
             * @param format_context
             * @param streamIndex
             * @return
             */
            virtual bool open(AVFormatContext* format_context, int streamIndex);

            /**
             * @brief Закрывает поток
             */
            virtual void close();

            /**
             * @brief Номер потока в файле
             * @return
             */
            int index(){ return m_index; }

            /**
             * @brief Возвращает последнюю произошедшую ошибку
             * @return
             */
            int error(){ return m_error; }

            /// Возвращает бит в секунду
            int bitrate() { return m_codecContext->bit_rate; }

            /// Возвращает время начала потока в секундах
            double startTime() { return timeBase() * m_avStream->start_time; }

            /// Возвращает продолжительность потока в секундах
            double duration() { return m_avStream->duration > 0 ? timeBase() * m_avStream->duration : 0; }

            /// Проверяет, активен ли поток в указанной момент времени в секундах
            bool isActiveTo(double pts) { return pts >= startTime() && (duration() <= 0 || pts < duration()); }

            /// Возвращает номер кадра по времени в секундах
            ulong frameNumByTime(double time){ return round(time / timeBase()); }

            /// Возвращает номер кадра по времени в секундах
            double timeByFrameNum(ulong frameNum){ return timeBase() * frameNum; }

            /// Единица измерения времени (длительность кадра)
            double timeBase(){ return av_q2d(m_avStream->time_base); }

            /// Единица измерения времени в кодеке
            double codecTimeBase(){ return av_q2d(m_codecContext->time_base); }

            /**
             * @brief Деодирует пакет
             * @param packet Пакет
             * @param frame[out] Декодированный кадр
             * @return Код ошибки ffmpeg
             */
            virtual int decodePacket(AVPacket* packet, InFrame *&frame) = 0;

            /**
             * @brief Декодирует пакет не оборачивая результат
             * @param packet Пакет
             * @return
             */
            int decodePacketRaw(AVPacket* packet);

            /**
             * @brief Возвращает время отображения кадра
             * @param frame Кадр
             * @return время отображения в секундах
             */
            double getPts(AVFrame *frame);

            /**
             * @brief Возвращает время отображения пакета
             * @param packet Пакет
             * @return время отображения в секундах
             */
            double getPts(AVPacket *packet);

            /// Абстрактный метод для возврата признака того, является ли поток видео потоком
            virtual bool isVideo() = 0;

            /// Возвращает указатель на последний декодированный ffmpeg кадр
            AVFrame* avframe(){ return m_frame; }

            /// Возвращает значение метаданных потока по ключу
            const Metadata& metadata();
        protected:
            int m_index = -1;
            AVStream* m_avStream = nullptr;
            AVCodecParameters* m_codecParameters = nullptr;
            AVCodecContext* m_codecContext = nullptr;

            AVCodec* m_codec = nullptr;
            AVFrame* m_frame = nullptr;
            int m_error = 0;

        private:
            long oldPts = 0;
            QSharedPointer<Metadata> m_metadata;
    };
}
#endif // STREAM_H
