#include "invideoframe.h"

using namespace stereoscopic;
using namespace stereoscopic::fileFormats::video;

InVideoFrame::InVideoFrame(InStream *stream, bool isKey, StereoFormat::Layout layout, double timeBase, double pts, QImage image)
    : InFrame(stream, timeBase, pts)
{
    m_isKey = isKey;
    m_image = image;
    m_layout = layout;
}

InVideoFrame::InVideoFrame(InVideoFrame &src)
    : InFrame(src)
{
    m_isKey = src.m_isKey;
    m_image = src.m_image;
    m_layout = src.m_layout;
}

InVideoFrame::InVideoFrame(QImage image, double timeBase, double pts)
    : InFrame(NULL, timeBase, pts)
{
    m_image = image;
}

InVideoFrame::~InVideoFrame()
{

}
