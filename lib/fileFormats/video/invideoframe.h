#ifndef VIDEOFRAME_H
#define VIDEOFRAME_H

#include "inframe.h"
#include "stereoformat.h"
#include <QImage>

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Класс входного видео кадра
     */
    class LIBSHARED_EXPORT InVideoFrame : public InFrame
    {
        public:
            /**
             * @brief Конструктор
             * @param stream Поток
             * @param isKey Признак того, что это ключевой кадр
             * @param layout Расположение ракурсов
             * @param timeBase Единица измерения времени
             * @param avPts Время отображения кадра в единицах измерения ffmpeg
             * @param pts Время отображения кадра
             * @param image Изображение кадра
             */
            InVideoFrame(InStream *stream, bool isKey, StereoFormat::Layout layout, double timeBase, double pts, QImage image);

            /// @brief Конструктор копирования
            InVideoFrame(InVideoFrame &src);

            /**
             * @brief Конструктор
             * @param image Изображение кадра
             * @param timeBase Единица измерения времени
             * @param avPts Время отображения кадра в единицах измерения ffmpeg
             * @param pts Время отображения кадра
             */
            InVideoFrame(QImage image, double timeBase, double pts);
            ~InVideoFrame();

            /// Возвращает признак ключевого кадра
            bool isKey() { return m_isKey; }

            /// Возвращает изображение кадра
            QImage image() { return m_image; }

            /// Возвращает расопложение ракурсов на кадре
            StereoFormat::Layout layout(){ return m_layout; }

        private:
            bool m_isKey = false;
            QImage m_image;
            StereoFormat::Layout m_layout = StereoFormat::Monoscopic;
    };
}
#endif // VIDEOFRAME_H
