#include "invideostream.h"

#include "ffmpeghelper.h"

using namespace stereoscopic::fileFormats::video;

bool InVideoStream::open(AVFormatContext *format_context, int streamIndex)
{
    if(InStream::open(format_context, streamIndex))
    {
        if(m_codecContext->pix_fmt == AV_PIX_FMT_NONE)
            return false;
        m_srcframeSize = getDisplayFrameSize();
        setDstFrameSize(m_srcframeSize);
        return true;
    }
    return false;
}

void InVideoStream::close()
{
    InStream::close();
    if(m_convertContext != NULL)
        sws_freeContext(m_convertContext);
    m_convertContext = NULL;
    m_frame = NULL;
}

void InVideoStream::setDstFrameSize(QSize size)
{
    m_dstframeSize = size;
    if(m_convertContext != NULL)
        sws_freeContext(m_convertContext);
    m_convertContext = sws_getContext(m_codecContext->width, m_codecContext->height,
                                      m_codecContext->pix_fmt,
                                      m_dstframeSize.width(), m_dstframeSize.height(),
                                      AV_PIX_FMT_RGB32, SWS_BICUBIC,
                                      NULL, NULL, NULL);
    if (m_convertContext == NULL)
    {
        qWarning() << "ffmpeg: Cannot initialize the conversion context";
        m_error = -1;
    }
}

int InVideoStream::decodePacket(AVPacket *packet, InFrame *&frame)
{
    int res = decodePacketRaw(packet);
    if(res == 0)
    {
        AVStereo3DType stereoType = (AVStereo3DType)-1;
        AVFrameSideData* sideData = av_frame_get_side_data(m_frame, AV_FRAME_DATA_STEREO3D);
        if(sideData)
        {
            AVStereo3D* stereo3D = (AVStereo3D* )sideData->data;
            if(stereo3D)
                stereoType = stereo3D->type;
        }
        bool isKey = m_frame->key_frame > 0;
        QImage image(dstFrameSize(), QImage::Format_RGB32);
        image.fill(0);
        int stride = image.bytesPerLine();
        uint8_t* data = image.bits();

        sws_scale(m_convertContext, m_frame->data, m_frame->linesize, 0, m_frame->height, &data, &stride);
        //qDebug() << "video" << pts;
        frame = new InVideoFrame(this, isKey, FFMpegHelper::layout(stereoType), av_q2d(m_codecContext->time_base), getPts(packet), image);
    }
    return res;
}

QSize InVideoStream::getDisplayFrameSize()
{
    const QSize srcSize = QSize(m_codecParameters->width, m_codecParameters->height);
    if (m_avStream->sample_aspect_ratio.num)
    {
        const AVRational sar = m_avStream->sample_aspect_ratio;
        if(sar.num > sar.den)
            return QSize(srcSize.width() * sar.num / sar.den, srcSize.height());
        else
            return QSize(srcSize.width(), srcSize.height() * sar.den / sar.num);
    }
    else {
        return srcSize;
    }
}
