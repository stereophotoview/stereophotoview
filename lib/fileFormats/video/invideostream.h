#ifndef VIDEOSTREAM_H
#define VIDEOSTREAM_H

#include "instream.h"
#include "invideoframe.h"
#include "stereoformat.h"
#include <QImage>


namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Класс входного видео потока
     */
    class LIBSHARED_EXPORT InVideoStream : public InStream
    {
        public:
            explicit InVideoStream() : InStream() { }

            /**
             * @brief Открывает поток для стения
             * @param format_context Контекст
             * @param streamIndex Номер потока
             * @return true, если поток успешно открыт
             */
            virtual bool open(AVFormatContext *format_context, int streamIndex);

            /// Закрывает поток
            virtual void close();

            /// Возвращает исходный размер кадра
            QSize srcFrameSize(){ return m_srcframeSize; }

            /// Возвращает требуемый размер кадра
            QSize dstFrameSize(){ return m_dstframeSize; }

            /// Устанавливает требуемый размер кадра
            void setDstFrameSize(QSize size);

            /// Возвращает количество кадров в секунду
            double frameRate() { return av_q2d(m_avStream->r_frame_rate); }

            // Возвращает SwsContext
            SwsContext* convertContext(){ return m_convertContext; }

            /// Всегда true
            bool isVideo() { return true; }
        signals:
            void currentFrameChanged(InVideoStream* sender, QImage* image, double pts);

        protected:
            virtual int decodePacket(AVPacket* packet, InFrame *&frame);

        private:
            struct SwsContext* m_convertContext = nullptr;
            QSize m_srcframeSize, m_dstframeSize;
            QSize getDisplayFrameSize();
    };
}
#endif // VIDEOSTREAM_H
