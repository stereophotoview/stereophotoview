#include "metadata.h"

using namespace stereoscopic::fileFormats::video;

Metadata::Metadata() : QMap<QString, QString>()
{

}

Metadata::Metadata(const AVDictionary *dictionary): QMap<QString, QString>()
{
    if(dictionary != nullptr)
    {
        AVDictionaryEntry* entry = nullptr;
        do {
            entry = av_dict_get(dictionary, "", entry, AV_DICT_IGNORE_SUFFIX);
            if(entry)
                (*this)[entry->key] = entry->value;
        } while(entry);
    }
}
