#ifndef METADATA_H
#define METADATA_H

#include <QMap>
#include <QString>

extern "C"
{
#include <libavutil/dict.h>
}

namespace stereoscopic::fileFormats::video
{

    class Metadata: public QMap<QString, QString>
    {
        public:
            Metadata();
            Metadata(const AVDictionary *dictionary);
    };
}
#endif // METADATA_H
