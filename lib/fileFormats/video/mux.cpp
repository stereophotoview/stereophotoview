#include "mux.h"

#include "ffmpeghelper.h"

using namespace stereoscopic::fileFormats::video;

QList<AVCodecID> Mux::notSupportedCodecs = { AV_CODEC_ID_VORBIS, AV_CODEC_ID_AAC };
QStringList Mux::supportedFormatNames = {"mp4", "mov", "matroska", "avi", "flv"};

Mux::Mux()
{

}

Mux::~Mux()
{
    close();
    foreach (OutputVideoStream* s, m_videoStreams)
        delete s;
    m_videoStreams.clear();

    foreach (OutputAudioStream* s, m_audioStreams)
        delete s;
    m_audioStreams.clear();

    if(m_formatContext)
    {
        avformat_close_input(&m_formatContext);
    }
}

QList<AVOutputFormat*> Mux::supportedFormats()
{
    QMap<QString, AVOutputFormat*> all;
    av_register_all();
#if !DEBUG
    av_log_set_level(AV_LOG_FATAL);
#endif

    AVOutputFormat * oformat = av_oformat_next(nullptr);
    while(oformat != nullptr)
    {
        // matroska при сохранении выдаёт ошибку
        if(oformat->video_codec != AV_CODEC_ID_NONE && oformat->audio_codec != AV_CODEC_ID_NONE)
            all[oformat->name] = oformat;
        oformat = av_oformat_next(oformat);
    }
    QList<AVOutputFormat*> res;
    foreach(QString name, supportedFormatNames)
    {
        auto f = all[name];
        if(f != nullptr) res.append(f);
    }
    return res;
}

QList<AVCodec *> Mux::supportedVideoCodecs()
{
    return supportedCodecs(AVMEDIA_TYPE_VIDEO);
}

QList<AVCodec *> Mux::supportedAudioCodecs()
{
    return supportedCodecs(AVMEDIA_TYPE_AUDIO);
}

AVCodecID Mux::defaultVideoCodec(AVOutputFormat *f)
{
    return f->video_codec;
}

AVCodecID Mux::defaultAudioCodec(AVOutputFormat *f)
{
    AVCodecID codecId = f->audio_codec;
    if(notSupportedCodecs.contains(codecId))
        codecId = AV_CODEC_ID_AC3;
    return codecId;
}

QString Mux::codecName(AVCodecID codecId)
{
    return avcodec_get_name(codecId);
}

QList<AVCodec *> Mux::supportedCodecs(AVMediaType type)
{
    QList<AVCodec*> res;
    AVCodec* codec = av_codec_next(nullptr);
    while(codec != nullptr)
    {
        if(codec->type == type && avcodec_find_encoder(codec->id) != nullptr && !notSupportedCodecs.contains(codec->id))
            res.append(codec);
        codec = av_codec_next(codec);
    }
    return res;
}

bool Mux::open(QString fileName, AVOutputFormat* format)
{
    m_fileName = fileName;
    av_register_all();
#if !DEBUG
    av_log_set_level(AV_LOG_FATAL);
#endif
    int error = avformat_alloc_output_context2(&m_formatContext, format, nullptr, fileName.toUtf8().data());
    if (!m_formatContext)
    {
        qWarning() << "ffmpeg: Unable to open output file" << fileName << ":" << error;
        return false;
    }
    return true;
}

bool Mux::writeHeader()
{
    if(m_error >= 0)
        m_error = avio_open(&m_formatContext->pb, m_fileName.toUtf8().data(), AVIO_FLAG_WRITE);
    if (m_error >= 0)
        m_error = avformat_write_header(m_formatContext, nullptr);
#if DEBUG
    av_dump_format(m_formatContext, 0, m_fileName.toUtf8().data(), 1);
#endif
    return m_opened = m_error >= 0;
}

bool Mux::close()
{
    if(m_error < 0)
        return false;
    if(m_opened)
    {
        m_error = av_write_trailer(m_formatContext);
        avformat_flush(m_formatContext);
        m_opened = false;
    }
    return m_error >= 0;
}

OutputVideoStream *Mux::addVideoStream(AVCodecID codecId)
{
    AVCodec *codec = avcodec_find_encoder(codecId);
    OutputVideoStream* stream = new OutputVideoStream(m_formatContext, codec);
    m_videoStreams.append(stream);
    return stream;
}

OutputAudioStream *Mux::addAudioStream(AVCodecID codecId)
{
    AVCodec *codec = avcodec_find_encoder(codecId);
    OutputAudioStream* stream = new OutputAudioStream(m_formatContext, codec);
    m_audioStreams.append(stream);
    return stream;
}

int Mux::error()
{
    return m_error;
}

QString Mux::errorString()
{
    return FFMpegHelper::errorString(m_error);
}
