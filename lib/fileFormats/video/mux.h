#ifndef MUX_H
#define MUX_H

#include <QString>
#include <QList>
#include <QDebug>

extern "C"
{
#include <libavformat/avformat.h>
}
// https://ffmpeg.org/doxygen/2.0/doc_2examples_2muxing_8c-example.html

#include "outputstream.h"
#include "outputvideostream.h"
#include "outputaudiostream.h"
#include "lib_global.h"

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Класс мультиплексора потоков
     * @example savevideotask.cpp
     */
    class LIBSHARED_EXPORT  Mux
    {
        public:
            Mux();
            ~Mux();
            /// Возвращает список поддерживаемых форматов файлов
            static QList<AVOutputFormat*> supportedFormats();

            /// Возвращает список поддерживаемых видео кодеков
            static QList<AVCodec*> supportedVideoCodecs();

            /// Возвращает список поддерживаемых аудио кодеков
            static QList<AVCodec*> supportedAudioCodecs();

            /// Возвращает видео кодек по умолчанию
            static AVCodecID defaultVideoCodec(AVOutputFormat*);

            /// Возвращает аудио кодек по умолчанию
            static AVCodecID defaultAudioCodec(AVOutputFormat*);

            /// Возвращает наименование кодека по идентфикатору
            static QString codecName(AVCodecID codecId);

            /**
             * @brief Открывает файл для записи
             * @param fileName Путь к файлу
             * @param format Формат файла
             * @return true в случае успешного открытия
             */
            bool open(QString fileName, AVOutputFormat *format = NULL);

            /// Записывает заголовок файла
            bool writeHeader();

            /// Закрывает файл
            bool close();

            /// Добавляет видео поток
            OutputVideoStream* addVideoStream(AVCodecID codecId);

            /// Добавляет аудио поток
            OutputAudioStream* addAudioStream(AVCodecID codecId);

            /// Возвращает видео потоки
            QList<OutputVideoStream*> videoStreams(){ return m_videoStreams; }

            /// Фозвращает последнюю ошибку ffmpeg
            int error();

            /// Возвращает текст последней ошибки
            QString errorString();

            // Возвращает список поддерживаемых видео кодеков
            QList<AVCodecID> videoCodecSupported();

        private:
            QString m_fileName;
            AVFormatContext *m_formatContext = NULL;
            static QList<AVCodec*> supportedCodecs(AVMediaType type);
            QList<OutputVideoStream*> m_videoStreams;
            QList<OutputAudioStream*> m_audioStreams;
            int m_error = 0;
            bool m_opened = false;
            static QList<AVCodecID> notSupportedCodecs;
            static QStringList supportedFormatNames;
    };
}

#endif // MUX_H
