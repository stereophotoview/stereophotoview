#include "outputaudiostream.h"

using namespace stereoscopic::fileFormats::video;


OutputAudioStream::OutputAudioStream(AVFormatContext *context, AVCodec *codec)
    : OutputStream(context, codec)
{
    m_codecContext->sample_fmt = defaultSampleFmt(codec);
    m_codecContext->bit_rate = 64000;
    m_codecContext->sample_rate = 44100;
    if (codec->supported_samplerates) {
        m_codecContext->sample_rate = codec->supported_samplerates[0];
        for (int i = 0; codec->supported_samplerates[i]; i++) {
            if (codec->supported_samplerates[i] == 44100)
                m_codecContext->sample_rate = 44100;
        }
    }
    m_codecContext->channels = av_get_channel_layout_nb_channels(m_codecContext->channel_layout);
    m_codecContext->channel_layout = AV_CH_LAYOUT_STEREO;
    if (codec->channel_layouts) {
        m_codecContext->channel_layout = codec->channel_layouts[0];
        for (int i = 0; codec->channel_layouts[i]; i++) {
            if (codec->channel_layouts[i] == AV_CH_LAYOUT_STEREO)
                m_codecContext->channel_layout = AV_CH_LAYOUT_STEREO;
        }
    }
    m_codecContext->channels = av_get_channel_layout_nb_channels(m_codecContext->channel_layout);

    m_codecContext->time_base = AVRational{ 1, m_codecContext->sample_rate };
}

OutputAudioStream::~OutputAudioStream()
{
    if(m_dstOffset > 0)
    {
        int nb_samplesToCopy = m_frame->nb_samples - m_dstOffset;
        for(int i = 0; i< m_frame->channels; i++)
            for(int k=0; k< m_frame->linesize[i]; k++)
                m_frame->data[i][k] = 0;
        copyTmpToFrame(nb_samplesToCopy);
    }
    if(m_tmpData != NULL)
        av_freep(m_tmpData);
}

AVSampleFormat OutputAudioStream::sampleFmt()
{
    return m_codecContext->sample_fmt;
}

void OutputAudioStream::setSampleFmt(AVSampleFormat value)
{
    for(int i = 0 ; m_codec->sample_fmts[i]; i++)
    {
        if(m_codec->sample_fmts[i] == value)
            m_codecContext->sample_fmt = value;
    }
}

int OutputAudioStream::sampleRate()
{
    return m_codecContext->sample_rate;
}

void OutputAudioStream::setSampleRate(int value)
{
    if(m_codec->supported_samplerates)
    {
        for (int i = 0; m_codec->supported_samplerates[i]; i++)
        {
            if (m_codec->supported_samplerates[i] == value)
                m_codecContext->sample_rate = value;
        }
    }
    else
    {
        m_codecContext->sample_rate = value;
    }
    m_codecContext->time_base = AVRational{ 1, m_codecContext->sample_rate };
}

int OutputAudioStream::channels()
{
    return m_codecContext->channels;
}

void OutputAudioStream::setChannels(int value)
{
    m_codecContext->channels = value;
}

int OutputAudioStream::channelLayout()
{
    return m_codecContext->channel_layout;
}

void OutputAudioStream::setChannelLayout(uint64_t value)
{
    if(m_codec->channel_layouts)
    {
        for (int i = 0; m_codec->channel_layouts[i]; i++)
        {
            if(m_codec->channel_layouts[i] == value)
            {
                m_codecContext->channel_layout = value;
                m_codecContext->channels = av_get_channel_layout_nb_channels(value);
            }
        }
    }
    else
    {
        m_codecContext->channel_layout = value;
        m_codecContext->channels = av_get_channel_layout_nb_channels(value);
    }
}

int OutputAudioStream::inSampleRate()
{
    return m_inSampleRate;
}

void OutputAudioStream::setInSampleRate(int value)
{
    m_inSampleRate = value;
}

AVSampleFormat OutputAudioStream::inSampleFmt()
{
    return m_inSampleFmt;
}

void OutputAudioStream::setInSampleFmt(AVSampleFormat value)
{
    m_inSampleFmt= value;
}

int OutputAudioStream::inChannels()
{
    return m_inChannels;
}

void OutputAudioStream::setInChannels(int value)
{
    m_inChannels = value;
}

int OutputAudioStream::inChannelLayout()
{
    return m_inChannelLayout;
}

void OutputAudioStream::setInChannelLayout(int value)
{
    m_inChannelLayout = value;
}

void OutputAudioStream::setInputCoderTimeBase(double timeBase)
{
    m_inCoderTimeBase = av_d2q(timeBase, 100);
}

void OutputAudioStream::setInputStreamTimeBase(double timeBase)
{
    m_inStreamTimeBase = av_d2q(timeBase, 100);
}

bool OutputAudioStream::encode(AVFrame *frame)
{
    if(m_error == AVERROR(EAGAIN))
        m_error = 0;

    if(m_error < 0)
        return false;

    prepareFrame();
    prepareTmpData(frame);

    if(m_error < 0)
        return false;

    int nb_samples = frame->nb_samples;
    // Преобразуем семплы во временный массив
    m_error = swr_convert(m_swrContext, m_tmpData, m_codecContext->frame_size, (const uint8_t **)frame->data, nb_samples);
    if(m_error < 0)
        return false;

    m_tmpOffset = 0;
    // Копируем временный массив в выходной кадр
    while(nb_samples > 0)
    {
        int nb_samplesToCopy = nb_samples;
        if(nb_samplesToCopy > m_frame->nb_samples - m_dstOffset)
            nb_samplesToCopy = m_frame->nb_samples - m_dstOffset;
        if(!copyTmpToFrame(nb_samplesToCopy))
            return false;
        nb_samples -= nb_samplesToCopy;
    }
    return true;
}

AVSampleFormat OutputAudioStream::defaultSampleFmt(AVCodec* codec)
{
    return codec->sample_fmts ? codec->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;
}

void OutputAudioStream::prepareFrame()
{
    if(m_frame != NULL)
        return;
    m_frame = createFrame();
    if(m_error < 0) return;

    m_swrContext = swr_alloc();
    av_opt_set_int(m_swrContext, "in_channel_count", m_inChannels, 0);
    av_opt_set_int(m_swrContext, "in_channel_layout", m_inChannelLayout, 0);
    av_opt_set_int(m_swrContext, "in_sample_rate", m_inSampleRate, 0);
    av_opt_set_sample_fmt(m_swrContext, "in_sample_fmt", m_inSampleFmt, 0);
    av_opt_set_int(m_swrContext, "out_channel_count", m_codecContext->channels, 0);
    av_opt_set_int(m_swrContext, "out_channel_layout", m_codecContext->channel_layout, 0);
    av_opt_set_int(m_swrContext, "out_sample_rate", m_codecContext->sample_rate, 0);
    av_opt_set_sample_fmt(m_swrContext, "out_sample_fmt", m_codecContext->sample_fmt, 0);

    m_error = swr_init(m_swrContext);
    if(m_error) return;
    if(m_error)
        return;

}

void OutputAudioStream::prepareTmpData(AVFrame* inFrame)
{
    int count = m_frame->nb_samples;
    if(count < inFrame->nb_samples)
        count = inFrame->nb_samples;
    if(m_tmpDataLength >= count)
        return;
    m_tmpDataLength = count;
    if(m_tmpData != NULL)
        av_freep(m_tmpData);
    int linesize;
    m_error = av_samples_alloc_array_and_samples(&m_tmpData, &linesize, channels(), count, sampleFmt(), 0);
    if(m_error < 0) return;
}

bool OutputAudioStream::copyTmpToFrame(int nb_samplesToCopy)
{
    if(av_samples_copy(m_frame->data, m_tmpData, m_dstOffset, m_tmpOffset, nb_samplesToCopy, channels(), m_codecContext->sample_fmt) < 0)
        return false;
    m_dstOffset += nb_samplesToCopy;
    m_tmpOffset += nb_samplesToCopy;
    if(m_dstOffset >= m_frame->nb_samples)
    {
        m_dstOffset = 0;
#ifdef DEBUG
    //qDebug() << "audio" << m_frame->pts << av_q2d(m_codecContext->time_base) * m_frame->pts;
#endif
        if(!sendFrame())
            return false;
        m_frame->pts += m_frame->nb_samples;
    }
    return true;
}

AVFrame *OutputAudioStream::createFrame()
{
    AVFrame* frame = av_frame_alloc();
    frame->format = sampleFmt();
    frame->channels = channels();
    frame->channel_layout = channelLayout();
    frame->sample_rate = sampleRate();
    frame->nb_samples = m_codecContext->frame_size;
    frame->pts = 0;
    m_error = av_frame_get_buffer(frame, 0);
    if(m_error < 0) return NULL;
    return frame;
}
