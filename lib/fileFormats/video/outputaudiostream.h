#ifndef AUDIOOUTPUTSTREAM_H
#define AUDIOOUTPUTSTREAM_H

#include "outputstream.h"
#include "lib_global.h"

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Класс выходного аудио потока
     */
    class LIBSHARED_EXPORT OutputAudioStream : public OutputStream
    {
        public:
            OutputAudioStream(AVFormatContext *context, AVCodec* codec);

            ~OutputAudioStream();

            AVSampleFormat sampleFmt();
            void setSampleFmt(AVSampleFormat value);

            int sampleRate();
            void setSampleRate(int value);

            int channels();
            void setChannels(int value);

            int channelLayout();
            void setChannelLayout(uint64_t value);

            int inSampleRate();
            void setInSampleRate(int value);

            AVSampleFormat inSampleFmt();
            void setInSampleFmt(AVSampleFormat value);

            int inChannels();
            void setInChannels(int value);

            int inChannelLayout();
            void setInChannelLayout(int value);

            void setInputCoderTimeBase(double timeBase);

            void setInputStreamTimeBase(double timeBase);

            bool encode(AVFrame* frame);

            static AVSampleFormat defaultSampleFmt(AVCodec *codec);

        private:
            SwrContext* m_swrContext = NULL;
            int m_inSampleRate = -1;
            int m_inChannels = -1;
            int m_inChannelLayout = -1;
            int m_samples_count = 0;
            uint8_t ** m_tmpData = NULL;
            int m_dstOffset = 0, m_tmpOffset = 0, m_tmpDataLength = 0;
            AVSampleFormat m_inSampleFmt = AV_SAMPLE_FMT_NONE;
            AVRational m_inCoderTimeBase, m_inStreamTimeBase;

            void prepareFrame();
            void prepareTmpData(AVFrame *inFrame);
            bool copyTmpToFrame(int nb_samplesToCopy);
            AVFrame* createFrame();

    };

}
#endif // AUDIOOUTPUTSTREAM_H
