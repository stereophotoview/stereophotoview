#include "outputstream.h"

#include "ffmpeghelper.h"

using namespace stereoscopic::fileFormats::video;

OutputStream::OutputStream(AVFormatContext *context, AVCodec* codec)
{
    m_codec = codec;
    m_context = context;

    m_avstream = avformat_new_stream(m_context, m_codec);
    if (!m_avstream)
    {
        qWarning() << "Could not allocate stream";
    }
    m_avstream->id = m_context->nb_streams - 1;
    m_codecContext = avcodec_alloc_context3(m_codec);
    m_codecContext->codec_id = codec->id;
    if(!m_codecContext)
    {
        qWarning() << "Could not allocate stream";
    }
    m_isValid = true;
}

OutputStream::~OutputStream()
{
    if(m_codecContext != NULL)
    {
        avcodec_free_context(&m_codecContext);
    }
    if(m_avstream != NULL)
    {
        //av_free(m_avstream);
        m_avstream = NULL;
    }
}

bool OutputStream::open()
{
    if(m_error >= 0)
        m_error = avcodec_open2(m_codecContext, m_codec, NULL);
    if(m_error >= 0)
        m_error = avcodec_parameters_from_context(m_avstream->codecpar, m_codecContext);
    return m_error >= 0;
}

int OutputStream::bitrate()
{
    return m_codecContext->bit_rate;
}

void OutputStream::setBitrate(int value)
{
    m_codecContext->bit_rate = value;
}

int OutputStream::error()
{
    return m_error;
}

QString OutputStream::errorString()
{
    return FFMpegHelper::errorString(m_error);
}

bool OutputStream::setCodecOpt(QString name, QString value)
{
    m_error = av_opt_set(m_codecContext->priv_data, name.toUtf8().data(), value.toUtf8().data(), 0);
    return m_error >= 0;
}

void OutputStream::setMetadata(QString name, QString value)
{
    av_dict_set(&m_avstream->metadata, name.toUtf8().data(), value.toUtf8().data(), 0);
}

bool OutputStream::sendFrame()
{
    m_error = avcodec_send_frame(m_codecContext, m_frame);
    if(m_error < 0) return false;

    AVPacket pkt;
    av_init_packet(&pkt);
    m_error = avcodec_receive_packet(m_codecContext, &pkt);
    if(m_error == AVERROR(EAGAIN))
    {
        m_error = 0;
        av_packet_unref(&pkt);
        return true; // Нужно повторить отправку данных
    }

    if(m_error < 0)
    {
        av_packet_unref(&pkt);
        return false;
    }
    pkt.stream_index = m_avstream->index;
    if(m_frame->pts != AV_NOPTS_VALUE)
    {
        av_packet_rescale_ts(&pkt, m_codecContext->time_base, m_avstream->time_base);
#if DEBUG
        //qDebug() << "pkt: " << pkt.pts << pkt.dts;
#endif
    }
    m_error = av_interleaved_write_frame(m_context, &pkt);

    av_packet_unref(&pkt);

    return m_error >= 0;
}
