#ifndef OUTPUTSTREAM_H
#define OUTPUTSTREAM_H

#include <QList>
#include <QDebug>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/opt.h>
}

#include "lib_global.h"

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Базовый класс выходного потока
     */
    class LIBSHARED_EXPORT OutputStream
    {
        public:
            OutputStream(AVFormatContext *context, AVCodec *codec);
            virtual ~OutputStream();

            bool isValid(){ return m_isValid; }
            bool open();

            int bitrate();
            void setBitrate(int value);

            int error();
            QString errorString();

            bool setCodecOpt(QString name, QString value);

            void setMetadata(QString name, QString value);

        protected:
            AVFormatContext *m_context = NULL;
            AVStream *m_avstream = NULL;
            AVCodec* m_codec = NULL;
            AVCodecContext* m_codecContext = NULL;
            AVFrame* m_frame = NULL;
            int m_error = 0;
            bool m_isValid = false;
            int m_pts = 0;

            bool sendFrame();
    };

}
#endif // OUTPUTSTREAM_H
