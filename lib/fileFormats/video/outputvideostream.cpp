#include "outputvideostream.h"

using namespace stereoscopic::fileFormats::video;


OutputVideoStream::OutputVideoStream(AVFormatContext *context, AVCodec *codec)
    : OutputStream(context, codec)
{
    m_codecContext->pix_fmt = AV_PIX_FMT_YUV420P;
    m_codecContext->gop_size = 12;
    if (codec->id == AV_CODEC_ID_MPEG1VIDEO)
        m_codecContext->mb_decision = 2;
    if (context->flags & AVFMT_GLOBALHEADER)
        m_codecContext->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
}

OutputVideoStream::~OutputVideoStream()
{
    if(m_convertContext != NULL)
        sws_freeContext(m_convertContext);
    if(m_frame != NULL)
        av_free(m_frame);
    m_convertContext = NULL;
    m_frame = NULL;
}

bool OutputVideoStream::encode(QImage image)
{
    if(m_error < 0) return false;

    int stride = image.bytesPerLine();
    uint8_t* data = image.bits();
    if(!prepareFrame(image.size()))
        return false;
    m_error = sws_scale(m_convertContext, &data, &stride, 0, image.height(), m_frame->data, m_frame->linesize);
    if(m_error < 0) return false;
    m_frame->pts++;
#ifdef DEBUG
    //qDebug() << "video" << m_frame->pts << av_q2d(m_codecContext->time_base) * m_frame->pts;
#endif
    return sendFrame();
}

QSize OutputVideoStream::frameSize()
{
    return QSize(m_codecContext->width, m_codecContext->height);
}

double OutputVideoStream::frameRate()
{
    return (double)1 / av_q2d(m_codecContext->time_base);
}

void OutputVideoStream::setFramerate(double fps)
{
    AVRational time_base = av_d2q(1 / fps, 100);
    m_codecContext->time_base = time_base;
    m_avstream->codec->time_base = time_base;
}

void OutputVideoStream::setFrameSize(QSize size)
{
    m_codecContext->width = size.width();
    m_codecContext->height = size.height();
}

int OutputVideoStream::gopSize()
{
    return m_codecContext->gop_size;
}

void OutputVideoStream::setGopSize(int value)
{
    m_codecContext->gop_size = value;
}

AVPixelFormat OutputVideoStream::pixelFormat()
{
    return m_codecContext->pix_fmt;
}

void OutputVideoStream::setPixelFormat(AVPixelFormat value)
{
    m_codecContext->pix_fmt = value;
}

void OutputVideoStream::setCrf(int value)
{
    setCodecOpt("crf", QString::number(value));
}

void OutputVideoStream::setPreset(QString preset)
{
    setCodecOpt("preset", preset);
}

bool OutputVideoStream::prepareFrame(QSize size)
{
    if(cSize != size)
    {
        if(m_convertContext != NULL)
            sws_freeContext(m_convertContext);
        cSize = size;
        m_convertContext = sws_getContext(cSize.width(), cSize.height(),
                                          AV_PIX_FMT_RGB32,
                                          m_codecContext->width, m_codecContext->height,
                                          m_codecContext->pix_fmt, SWS_BICUBIC,
                                          NULL, NULL, NULL);
        m_frame = av_frame_alloc();
        m_frame->format = m_codecContext->pix_fmt;
        m_frame->width  = m_codecContext->width;
        m_frame->height = m_codecContext->height;
        m_frame->pts = -1;
        int ret = av_frame_get_buffer(m_frame, 32);
        if (ret < 0)
        {
            qDebug() << "Could not allocate frame data.";
            return false;
        }
    }
    return true;
}
