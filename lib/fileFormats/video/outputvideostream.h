#ifndef VIDEOOUTPUTSTREAM_H
#define VIDEOOUTPUTSTREAM_H

#include "outputstream.h"
#include <QImage>
#include "lib_global.h"

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Класс выходного видео потока
     */
    class LIBSHARED_EXPORT OutputVideoStream : public OutputStream
    {
        public:
            OutputVideoStream(AVFormatContext *context, AVCodec* codec);
            virtual ~OutputVideoStream();

            double frameRate();
            void setFramerate(double fps);

            QSize frameSize();
            void setFrameSize(QSize size);

            int gopSize();
            void setGopSize(int value);

            AVPixelFormat pixelFormat();
            void setPixelFormat(AVPixelFormat value);

            void setCrf(int value);
            void setPreset(QString preset);

            bool encode(QImage image);

        private:
            struct SwsContext* m_convertContext = NULL;
            QSize cSize;
            bool prepareFrame(QSize size);

    };
}
#endif // VIDEOOUTPUTSTREAM_H
