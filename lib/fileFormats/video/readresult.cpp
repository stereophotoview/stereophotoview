#include "readresult.h"

using namespace stereoscopic::fileFormats::video;

ReadResult::ReadResult()
{

}

ReadResult::Result ReadResult::parseResult(int res)
{
    if(res == 0)
        return Result::OK;
    else if(res == AVERROR(EAGAIN))
        return Result::RAGAIN;
    else if(res == AVERROR_EOF)
        return Result::EOS;
    else if(res == AVERROR(EINVAL))
        return Result::ERR;
    else
    {
        qDebug() << "Unknown ffmpeg result" << res;
        return Result::ERR;
    }
}
