#ifndef READRESULT_H
#define READRESULT_H

extern "C"
{
#include <libavformat/avformat.h>
}

#include <QDebug>
#include "lib_global.h"

namespace stereoscopic::fileFormats::video
{
    /**
     * @brief Результат чтения кадра
     */
    class LIBSHARED_EXPORT ReadResult
    {
        public:
            /**
             * @brief Варианты результата
             */
            enum Result {
                /// Кадр прочитан
                OK,
                /// Достигнут конец потока
                EOS,
                /// Кадр прочитан не до конца, нужно снова вызвать чтение
                RAGAIN,
                /// Во время чтения произошла ошибка
                ERR
            };

            ReadResult();

            /// Преобразует результат чтение ffmpeg в один из вариантов результата
            static Result parseResult(int res);
    };
}
#endif // READRESULT_H
