#include "savevideotask.h"
#include "urlconv.h"
#include "demux.h"
#include "fileFormats/video/ffmpeghelper.h"
#include "stereocoders/coderfactory.h"
#include "mathhelper.h"
#include "stereovideodecoder.h"

using namespace stereoscopic::fileFormats::video;
using namespace stereoscopic::coders;
using namespace stereoscopic::math;

SaveVideoTask::SaveVideoTask(QObject *parent) : QObject(parent)
{

}

bool SaveVideoTask::execute()
{
    setIsActive(true);
    UrlConv url;
    if(url.equals(m_dstFilePath, m_srcFilePath))
    {
        return finish(false, tr("The destination file must not be equal to the source"));
    }

    StereoVideoDecoder decoder;

    if(!decoder.open(m_srcFilePath))
        return finish(false, getDoderErrorText(decoder.error()));
    decoder.setTimeShiftInSeconds(m_timeShift);

    Mux mux;
    if(!mux.open(m_dstFilePath))
        return finish(false, tr("Can not open \"%1\"").arg(m_dstFilePath) + ": " + mux.errorString());

    if(m_srcFormat.layout() == StereoFormat::Separate && decoder.videoStream2() == nullptr)
        return finish(false, tr("Bad input format"));

    QSize targetSize = m_options->frameSize();
    if(targetSize.width() <= 0 || targetSize.height() <= 0)
    {
        StereoFrame inFrame;
        if(m_srcFormat.layout() == StereoFormat::Separate)
            inFrame = StereoFrame(QImage(decoder.videoFrameSize(), QImage::Format_Mono), QImage(decoder.videoFrameSize(), QImage::Format_Mono));
        else
            inFrame = StereoFrame(QImage(decoder.videoFrameSize(), QImage::Format_Mono), m_srcFormat);
        targetSize = m_options->stereoFormat()->fullFrameSize(inFrame.fullLeftSize(), inFrame.fullRightSize());
    }

    StereoVideoCoder coder(&mux, m_options);
    if(!coder.open(m_options->videoCodec(), decoder.videoFrameRate(), targetSize, decoder.audioStream()))
        return finish(false, getCoderErrorText(coder.error(), coder.errorString()));

    double duration = decoder.duration();

    while(decoder.decodeFrame())
    {
        if(!decoder.stereoVideoFrame().isNull())
        {
            AlignParams l = m_layout.get(decoder.position());
            StereoFrame frameToWrite = decoder.stereoVideoFrame().asFormat(m_srcFormat);
            if(l.colorAdjustment() == AlignParams::ColorAdjustment::RightByLeft)
                frameToWrite = frameToWrite.adjustColorRightByLeft(m_colorAdjustPixelStep);
            if(l.colorAdjustment() == AlignParams::ColorAdjustment::LeftByRight)
                frameToWrite = frameToWrite.adjustColorLeftByRight(m_colorAdjustPixelStep);
            frameToWrite = frameToWrite
                    .rotated(l.leftAngle(), l.rightAngle())
                    .aligned(l.offset())
                    .cropped(l.cropRect())
                    .withFeatheredBorder(l.borderWidthPercents());
            coder.writeStereoVideoFrame(frameToWrite);
        }
        if(decoder.audioRawFrame() != nullptr)
            coder.writeAudioFrame(decoder.audioRawFrame());

        if(coder.error() != StereoVideoCoder::ErrorCode::NoError)
            return finish(false, getCoderErrorText(coder.error(), coder.errorString()));

        setProgress((int)(decoder.position() / duration * 100));
        if(m_stop)
            break;
    }
    coder.close();

    if(decoder.error() != StereoVideoDecoder::ErrorCode::NoError
            && decoder.error() != StereoVideoDecoder::ErrorCode::EOS)
        return finish(false, getDoderErrorText(decoder.error()));

    return finish(true, m_stop ? tr("Stopped") : tr("Success"));
}

void SaveVideoTask::stop()
{
    m_stop = true;
}

void SaveVideoTask::setProgress(int value)
{
    if(m_progress < value)
    {
        m_progress = value;
        emit progressChanged(m_progress);
    }
}

bool SaveVideoTask::finish(bool result, QString str)
{
    setResult(result);
    setResultStr(str);
    setIsActive(false);
    setIsFinished(true);
    return result;
}

void SaveVideoTask::setIsActive(bool value)
{
    m_isActive = value;
    emit isActiveChanged(m_isActive);
}

void SaveVideoTask::setIsFinished(bool value)
{
    m_isFinished = value;
    emit finished();
}

QString SaveVideoTask::getDoderErrorText(StereoVideoDecoder::ErrorCode errorCode)
{
    switch (errorCode) {
        case StereoVideoDecoder::ErrorCode::EOS:
        case StereoVideoDecoder::ErrorCode::NoError:
            return "";
        case StereoVideoDecoder::ErrorCode::VideoStreamsNotFound:
            return tr("Input video streams not found");
        case StereoVideoDecoder::ErrorCode::CanNotOpenFile:
            return tr("Can not open input file");
        case StereoVideoDecoder::ErrorCode::ReadError:
            return tr("Unknown decoder error");
    }
    return tr("Unknown decoder error");
}

QString SaveVideoTask::getCoderErrorText(StereoVideoCoder::ErrorCode errorCode, QString errorString)
{
    QString errorText = getCoderErrorCodeText(errorCode);
    if(errorString.length() > 0)
        errorText += ":" + errorString;
    return errorText;
}

QString SaveVideoTask::getCoderErrorCodeText(StereoVideoCoder::ErrorCode errorCode)
{
    switch (errorCode) {
        case StereoVideoCoder::ErrorCode::NoError:
            return "";
        case StereoVideoCoder::ErrorCode::EncodeAudio:
            return tr("Can not write audio frame");
        case StereoVideoCoder::ErrorCode::CreateVideoStream:
            return tr("Can not create video stream");
        case StereoVideoCoder::ErrorCode::OpenVideoStream:
            return tr("Can not open output video stream");
        case StereoVideoCoder::ErrorCode::OpenAudioStream:
            return tr("Can not open output audio stream");
        case StereoVideoCoder::ErrorCode::WriteHeader:
            return tr("Can not write header");
        case StereoVideoCoder::ErrorCode::WriteVideoFrame:
            return tr("Can not write video frame");
    }
    return "";
}
