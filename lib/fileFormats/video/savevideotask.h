#ifndef SAVEVIDEOTASK_H
#define SAVEVIDEOTASK_H

#include <QObject>
#include "videooptions.h"
#include "stereoformat.h"
#include "../../alignchart.h"
#include "stereoframe.h"
#include "stereovideocoder.h"
#include "stereovideodecoder.h"

namespace stereoscopic::fileFormats::video {
    /**
     * @brief Класс задачи асинхронного сохранения видео файла
     */
    class LIBSHARED_EXPORT  SaveVideoTask : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QString srcFilePath READ srcFilePath WRITE setSrcFilePath NOTIFY srcFilePathChanged)
            Q_PROPERTY(QString dstFilePath READ dstFilePath WRITE setDstFilePath NOTIFY dstFilePathChanged)
            Q_PROPERTY(VideoOptions* options READ options WRITE setOptions NOTIFY optionsChanged)
            Q_PROPERTY(StereoFormat srcFormat READ srcFormat WRITE setSrcFormat NOTIFY srcFormatChanged)
            Q_PROPERTY(int timeShift READ timeShift WRITE setTimeShift NOTIFY timeShiftChanged)
            Q_PROPERTY(QString resultStr READ resultStr WRITE setResultStr NOTIFY resultStrChanged)
            Q_PROPERTY(bool result READ result WRITE setResult NOTIFY resultChanged)
            Q_PROPERTY(int progress READ progress NOTIFY progressChanged)
            Q_PROPERTY(bool isActive READ isActive WRITE setIsActive NOTIFY isActiveChanged)
            Q_PROPERTY(bool isFinished READ isFinished WRITE setIsFinished NOTIFY finished)

        public:
            explicit SaveVideoTask(QObject *parent = nullptr);

            QString srcFilePath(){ return m_srcFilePath; }
            void setSrcFilePath(QString value)
            {
                m_srcFilePath = value;
                emit srcFilePathChanged(m_srcFilePath);
            }

            QString dstFilePath(){ return m_dstFilePath; }
            void setDstFilePath(QString value)
            {
                m_dstFilePath = value;
                emit dstFilePathChanged(m_dstFilePath);
            }

            VideoOptions* options(){ return m_options; }
            void setOptions(VideoOptions* value)
            {
                m_options = new VideoOptions(value, this);
                emit optionsChanged(m_options);
            }

            void setLayout(AlignChart layout)
            {
                m_layout = layout;
            }

            StereoFormat srcFormat(){ return m_srcFormat; }
            void setSrcFormat(StereoFormat value)
            {
                m_srcFormat = value;
                emit srcFormatChanged(m_srcFormat);
            }

            double timeShift(){ return m_timeShift; }
            void setTimeShift(double value)
            {
                m_timeShift = value;
                emit timeShiftChanged();
            }

            QString resultStr(){ return m_resultStr; }
            void setResultStr(QString value)
            {
                m_resultStr = value;
                emit resultStrChanged(m_resultStr);
            }

            bool result(){ return m_result; }
            void setResult(bool value)
            {
                m_result = value;
                emit resultChanged(m_result);
            }

            int progress(){ return m_progress; }

            bool isActive(){ return m_isActive; }

            bool isFinished(){ return m_isFinished; }

        public slots:
            bool execute();
            void stop();

        signals:
            void srcFilePathChanged(QString);
            void dstFilePathChanged(QString);
            void optionsChanged(VideoOptions*);
            void srcFormatChanged(StereoFormat);
            void timeShiftChanged();
            void resultStrChanged(QString);
            void progressChanged(int);
            void finished();
            void resultChanged(bool);
            void isActiveChanged(bool);

        private:
            QString m_srcFilePath;
            QString m_dstFilePath;
            VideoOptions* m_options;
            AlignChart m_layout;
            StereoFormat m_srcFormat;
            double m_timeShift = 0;
            QString m_resultStr = tr("Waiting…");
            int m_progress = 0;
            bool m_isActive = false;
            bool m_isFinished = false;
            bool m_stop = false;
            const int m_colorAdjustPixelStep = 2;

            void setProgress(int value);
            bool m_result;
            bool finish(bool result, QString str);
            void setIsActive(bool value);
            void setIsFinished(bool  value);
            QString getDoderErrorText(StereoVideoDecoder::ErrorCode errorCode);
            QString getCoderErrorText(StereoVideoCoder::ErrorCode errorCode, QString errorString);
            QString getCoderErrorCodeText(StereoVideoCoder::ErrorCode errorCode);
    };
}
#endif // SAVEVIDEOTASK_H
