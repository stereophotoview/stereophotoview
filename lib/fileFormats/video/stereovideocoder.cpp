#include "stereovideocoder.h"
#include "../../stereocoders/coderfactory.h"

using namespace stereoscopic::fileFormats::video;
using namespace stereoscopic::coders;

StereoVideoCoder::StereoVideoCoder(Mux *mux, VideoOptions* options)
{
    this->mux = mux;
    this->options = options;
}

bool StereoVideoCoder::open(AVCodecID videoCodec, double frameRate, QSize targetSize, InAudioStream *inAudioStream)
{
    this->targetSize = targetSize;
    videoStream = mux->addVideoStream(videoCodec);
    if(videoStream == nullptr)
        return setError(ErrorCode::CreateVideoStream);

    if(targetSize.width() % 2 != 0)
        targetSize.setWidth(targetSize.width() + 1);
    if(targetSize.height() % 2 != 0)
        targetSize.setHeight(targetSize.height() + 1);

    videoStream->setFrameSize(targetSize);
    videoStream->setFramerate(frameRate);
    videoStream->setMetadata("stereo_mode", QString::number(FFMpegHelper::stereo_mode(options->stereoFormat())));

    if(options->videoCodecID() == VideoOptions::CodecH264)
    {
        videoStream->setCrf(options->crf());
        videoStream->setPreset(options->preset());

        int framePacking = FFMpegHelper::frame_packing(options->stereoFormat());
        if(framePacking > 0)
            videoStream->setCodecOpt("x264opts", "frame-packing=" + QString::number(framePacking));
    }
    else
    {
        long rawBitrate = targetSize.width() * targetSize.height() * 24 * (int)videoStream->frameRate();
        long targetBitrate = (long)(100 - options->crf()) * rawBitrate / 100;
        videoStream->setBitrate((int)targetBitrate);
    }

    if(!videoStream->open())
        return setError(ErrorCode::OpenVideoStream);

    if(inAudioStream != nullptr)
    {
        audioStream = mux->addAudioStream(options->audioCodec());
        audioStream->setChannelLayout(inAudioStream->channelLayout());
        audioStream->setChannels(inAudioStream->channels());
        audioStream->setSampleRate(inAudioStream->sampleRate());

        audioStream->setInChannelLayout((int)inAudioStream->channelLayout());
        audioStream->setInChannels(inAudioStream->channels());
        audioStream->setInSampleFmt(inAudioStream->SampleFmt());
        audioStream->setInSampleRate(inAudioStream->sampleRate());

        //channels / 2 * sampleRate * (v !== 0 ? v : bytesPerSample)* 8
        int bytesPerSample = options->audioBytesPerSample();
        if(bytesPerSample <= 0)
            bytesPerSample = FFMpegHelper::bytesPerSample(inAudioStream->SampleFmt());
        long rawBitrate = audioStream->channels() / 2 * audioStream->sampleRate() * bytesPerSample * 8;
        long targetBitrate = (long)(100 - options->crf()) * rawBitrate / 100;
        audioStream->setBitrate((int)targetBitrate);

        if(!audioStream->open())
            return setError(ErrorCode::OpenAudioStream);
        audioStream->setInputCoderTimeBase(inAudioStream->codecTimeBase());
        audioStream->setInputStreamTimeBase(inAudioStream->timeBase());
    }

    if(!mux->writeHeader())
    {
        m_muxError = mux->errorString();
        return setError(ErrorCode::WriteHeader);
    }
    return true;
}

StereoVideoCoder::ErrorCode StereoVideoCoder::error()
{
    return m_error;
}

bool StereoVideoCoder::writeStereoVideoFrame(stereoscopic::StereoFrame frame)
{
    auto coder = CoderFactory::create(options->stereoFormat(), &frame);
    return writeImage(coder.get()->createImage(targetSize));
}

bool StereoVideoCoder::writeImage(QImage image)
{
    if(!videoStream->encode(image))
    {
        m_errorString = videoStream->errorString();
        return setError(ErrorCode::WriteVideoFrame);
    }
    return true;
}

void StereoVideoCoder::writeAudioFrame(AVFrame* frame)
{
    if(!audioStream->encode(frame))
    {
        m_error = ErrorCode::EncodeAudio;
        m_errorString = audioStream->errorString();
    }
}

void StereoVideoCoder::close()
{
    mux->close();
}

bool StereoVideoCoder::setError(StereoVideoCoder::ErrorCode error)
{
    m_error = error;
    return error == ErrorCode::NoError;
}
