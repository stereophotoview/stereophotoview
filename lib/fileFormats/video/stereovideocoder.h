#ifndef STEREOVIDEOCODER_H
#define STEREOVIDEOCODER_H

#include "mux.h"
#include "inaudioframe.h"
#include "../../stereoframe.h"
#include "videooptions.h"
#include "inaudiostream.h"

namespace stereoscopic::fileFormats::video{
    class LIBSHARED_EXPORT StereoVideoCoder
    {
        public:
            enum ErrorCode {
                NoError,
                EncodeAudio,
                CreateVideoStream,
                OpenVideoStream,
                OpenAudioStream,
                WriteHeader,
                WriteVideoFrame,
            };
            StereoVideoCoder(Mux *mux, VideoOptions *options);
            bool open(AVCodecID videoCodec, double frameRate, QSize frameSize, InAudioStream* inAudioStream);
            ErrorCode error();
            QString errorString() { return m_errorString; }
            bool writeStereoVideoFrame(StereoFrame frame);
            void writeAudioFrame(AVFrame *frame);
            void close();
        private:
            Mux *mux;
            VideoOptions* options;
            QSize targetSize;
            OutputVideoStream* videoStream = nullptr;
            OutputAudioStream* audioStream = nullptr;
            ErrorCode m_error = ErrorCode::NoError;
            QString m_muxError;
            QString m_errorString;

            bool writeImage(QImage image);
            bool setError(ErrorCode error);
    };
}
#endif // STEREOVIDEOCODER_H
