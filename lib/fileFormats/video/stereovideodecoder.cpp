#include "stereovideodecoder.h"

#include <QList>
#include "mathhelper.h"

using namespace stereoscopic::fileFormats::video;
using namespace stereoscopic::math;

StereoVideoDecoder::StereoVideoDecoder()
{

}

bool StereoVideoDecoder::open(QString fileName)
{
    m_fileName = fileName;
    auto fileNames = m_fileName.split("\n");
    if(fileNames.length() == 1)
    {
        if(m_demux.open(m_fileName) != 0)
            return setError(ErrorCode::CanNotOpenFile);
        m_audioStream = m_demux.bestAudioStream();

        QList<InVideoStream*> inVideoStreams = m_demux.videoStreams();

        if(inVideoStreams.length() < 1)
            return setError(ErrorCode::VideoStreamsNotFound);

        m_videoStream1 = inVideoStreams[0];

        if(inVideoStreams.length() > 1)
        {
            // Если два видео потока, открываем второй поток во втором демультиплексоре для возможности сдвига времени
            if(m_demux2.open(m_fileName) != 0)
                return setError(ErrorCode::CanNotOpenFile);
            m_videoStream2 = m_demux2.videoStreams()[1];
            foreach(InStream* s, inVideoStreams)
                m_inVideoFrames[s] = QSharedPointer<InVideoFrame>();
        }
    }
    else if(fileNames.length() == 2)
    {
        if(m_demux.open(fileNames[0]) != 0 || m_demux2.open(fileNames[1]) != 0)
            return setError(ErrorCode::CanNotOpenFile);
        if(m_demux.videoStreams().length() <= 0 || m_demux2.videoStreams().length() <= 0)
            return setError(ErrorCode::VideoStreamsNotFound);
        if(m_demux.videoStreams()[0]->frameRate() != m_demux2.videoStreams()[0]->frameRate())
            return setError(ErrorCode::VideoStreamsNotFound);
        m_audioStream = m_demux.bestAudioStream();
        m_videoStream1 = m_demux.videoStreams()[0];
        m_videoStream2 = m_demux2.videoStreams()[0];
        m_inVideoFrames[m_videoStream1] = QSharedPointer<InVideoFrame>();
        m_inVideoFrames[m_videoStream2] = QSharedPointer<InVideoFrame>();
    }
    return setError(ErrorCode::NoError);
}

double StereoVideoDecoder::duration()
{
    if(m_videoStream2 == nullptr)
        return m_videoStream1->duration();
    return fmax(m_videoStream1->duration(), m_videoStream2->duration()) - abs(m_timeShiftInSeconds);
}

QSize StereoVideoDecoder::videoFrameSize()
{
    return m_videoStream1->srcFrameSize();
}

double StereoVideoDecoder::videoFrameRate()
{
    return m_videoStream1->frameRate();
}

bool StereoVideoDecoder::decodeFrame()
{
    if(!m_videoFrame.isNull())
        m_videoFrame = StereoFrame();

    m_audioRawFrame = nullptr; // Память очищается в demux
    m_audioFrame.clear();

    m_error = ErrorCode::NoError;

    for(;;)
    {
        switch (readFrame(m_demux)) {
            case ReadFrameResult::VideoFrameReaded:
                if(m_videoStream2 != nullptr)
                {
                    // Читаем второй кадр из второго демультиплексора, если нужно
                    while(m_inVideoFrames[m_videoStream2].isNull()
                          || m_inVideoFrames[m_videoStream2]->ptsInSeconds() < m_inVideoFrames[m_videoStream1]->ptsInSeconds() + m_timeShiftInSeconds)
                    {

                        switch(readFrame(m_demux2))
                        {
                            case ReadFrameResult::VideoFrameReaded:
#ifdef TRACE
                                if(!m_inVideoFrames[m_videoStream1].isNull() && !m_inVideoFrames[m_videoStream2].isNull())
                                    qDebug() << "decoded" << m_inVideoFrames[m_videoStream1]->num() << m_inVideoFrames[m_videoStream2]->num();
#endif

                                if(!m_inVideoFrames[m_videoStream1].isNull() && !m_inVideoFrames[m_videoStream2].isNull()
                                        && !equal(m_inVideoFrames[m_videoStream1]->ptsInSeconds(), m_lastVideoFramePts)
                                        && isgreaterequal(m_inVideoFrames[m_videoStream2]->ptsInSeconds(), m_inVideoFrames[m_videoStream1]->ptsInSeconds() + m_timeShiftInSeconds))
                                {
                                    m_videoFrame = StereoFrame(m_inVideoFrames[m_videoStream1]->image(), m_inVideoFrames[m_videoStream2]->image());
                                    m_position = fmin(m_inVideoFrames[m_videoStream1]->ptsInSeconds(),m_inVideoFrames[m_videoStream2]->ptsInSeconds());
                                    m_lastVideoFramePts = m_inVideoFrames[m_videoStream1]->ptsInSeconds();
                                    return true;
                                }
                            case ReadFrameResult::AudioFrameReaded:
                                continue;
                            case ReadFrameResult::Error:
                                return false;
                        }
                    }

                }
                else if(!m_inVideoFrames[m_videoStream1].isNull())
                {
                    auto videoFrame = m_inVideoFrames[m_videoStream1];
                    if(!equal(videoFrame->ptsInSeconds(), m_lastVideoFramePts))
                    {
                        m_videoFrame = StereoFrame(videoFrame->image(), getVideoFrameStereoFormat(videoFrame->layout()));
                        m_position = videoFrame->ptsInSeconds();
                        m_lastVideoFramePts = videoFrame->ptsInSeconds();
                        return true;
                    }
                }
            case ReadFrameResult::AudioFrameReaded:
                return true;
            case ReadFrameResult::Error:
                return false;
        }
    }
    return false;
}

StereoVideoDecoder::ReadFrameResult StereoVideoDecoder::readFrame(Demux &demux)
{
    InPacket packet;
    ReadResult::Result readRes;
    for(;;)
    {
        readRes = demux.readPacket(packet);
        switch (readRes)
        {
            case ReadResult::EOS:
                setError(ErrorCode::EOS);
                return ReadFrameResult::Error;
            case ReadResult::ERR:
                setError(ErrorCode::ReadError);
                return ReadFrameResult::Error;
            case ReadResult::RAGAIN:
                continue;
            case ReadResult::OK:
                if(packet.stream()->isVideo())
                {
                    InFrame* frame = nullptr;
                    if(packet.decode(frame) == ReadResult::OK)
                    {
                        auto videoFrame = QSharedPointer<InVideoFrame>(dynamic_cast<InVideoFrame*>(frame));
                        InStream* s = videoFrame->stream();
                        m_inVideoFrames[s] = videoFrame;
                        return ReadFrameResult::VideoFrameReaded;
                    }
                }

                if(packet.stream() == m_audioStream)
                {
                    InFrame* frame = nullptr;
                    if(packet.decode(frame) == ReadResult::OK)
                    {
                        m_audioFrame = QSharedPointer<InAudioFrame>(dynamic_cast<InAudioFrame*>(frame));
                        m_audioRawFrame = audioStream()->avframe();
                        return ReadFrameResult::AudioFrameReaded;
                    }
                }
        }
    }
}

StereoVideoDecoder::ErrorCode StereoVideoDecoder::error()
{
    return m_error;
}

void StereoVideoDecoder::setPosition(double targetPosition)
{
    if(targetPosition < 0) targetPosition = 0;
    m_demux.seek(m_timeShiftInSeconds < 0 ? targetPosition + abs(m_timeShiftInSeconds) : targetPosition);
    m_inVideoFrames[m_videoStream1].clear();

    if(m_videoStream2 != nullptr)
    {
        m_demux2.seek(m_timeShiftInSeconds < 0 ? targetPosition : targetPosition + m_timeShiftInSeconds);
        m_inVideoFrames[m_videoStream2].clear();
    }
    m_position = targetPosition;
}

int StereoVideoDecoder::timeShiftInSeconds()
{
    return m_timeShiftInSeconds;
}

void StereoVideoDecoder::setTimeShiftInSeconds(double timeShift)
{
    m_timeShiftInSeconds = timeShift;
}

StereoFormat StereoVideoDecoder::getVideoFrameStereoFormat(StereoFormat::Layout videoFrameLayout)
{
    // Правильный videoFrameLayout бывает не на всех кадрах, на других - Default. Если получили Default, нужно использовать предыдущее значение
    if(videoFrameLayout != StereoFormat::Default && videoFrameLayout != currentStereoFormat.layout())
        currentStereoFormat = StereoFormat(videoFrameLayout).setIsLoaded(true);
    return currentStereoFormat;
}

bool StereoVideoDecoder::setError(ErrorCode error)
{
    m_error = error;
    return error == ErrorCode::NoError;
}
