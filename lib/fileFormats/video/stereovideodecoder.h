#ifndef STEREOVIDEODECODER_H
#define STEREOVIDEODECODER_H

#include <QObject>
#include <QMap>
#include <QSharedPointer>
#include "demux.h"
#include "../../stereoframe.h"
#include "../../stereoformat.h"
#include "instream.h"
#include "invideostream.h"
#include "invideoframe.h"

using namespace stereoscopic;

namespace stereoscopic::fileFormats::video
{
    class LIBSHARED_EXPORT StereoVideoDecoder
    {
        public:
            enum class ErrorCode {
                CanNotOpenFile,
                NoError,
                VideoStreamsNotFound,
                EOS,
                ReadError
            };

            StereoVideoDecoder();

            Demux *demux() { return &m_demux; }
            Demux *demux2() { return &m_demux2; }
            QString fileName() { return m_fileName; }

            bool open(QString fileName);

            InVideoStream* &videoStream1(){ return m_videoStream1; }
            InVideoStream* &videoStream2(){ return m_videoStream2; }
            double duration();

            QSize videoFrameSize();
            double videoFrameRate();

            /**
             * @brief Декодирует кадр. Декодированные кадры можно поулчить из stereoVideoFrame, audioFrame, или audioRawFrame.
             * @return True, если кадр декодирован, и можно попытаться декодировать следующий. False, если процесс закончен.
             */
            bool decodeFrame();

            ErrorCode error();
            StereoFrame stereoVideoFrame() { return m_videoFrame; }
            AVFrame* audioRawFrame() { return m_audioRawFrame; }
            QSharedPointer<InAudioFrame> audioFrame() { return m_audioFrame; }
            InAudioStream* audioStream() { return m_audioStream; }
            double position() { return m_position; }
            void setPosition(double position);
            int timeShiftInSeconds();
            void setTimeShiftInSeconds(double timeShift);

        private:
            enum class ReadFrameResult {
                Error = 0,
                VideoFrameReaded = 1,
                AudioFrameReaded = 2
            };
            QString m_fileName;
            Demux m_demux; //!< Основной демультиплексор
            Demux m_demux2; //!< Демультиплексор для второго видео потока, если он есть.
            InVideoStream *m_videoStream1 = nullptr; //!< Основной видео поток
            InVideoStream *m_videoStream2 = nullptr; //!< Второй видео поток, если он есть
            InAudioStream *m_audioStream = nullptr; //!< Аудио поток
            QMap<InStream*, QSharedPointer<InVideoFrame>> m_inVideoFrames; //!< Прочитанные видео кадры
            ErrorCode m_error = ErrorCode::NoError; //!< Ошибка декодирования

            StereoFrame m_videoFrame; //!< Последний декодированный стерео видео кадр
            AVFrame* m_audioRawFrame; //!< Последний декодированный аудио кадр
            QSharedPointer<InAudioFrame> m_audioFrame;
            double m_position = -1;
            double m_lastVideoFramePts = -1;
            StereoFormat currentStereoFormat;
            double m_timeShiftInSeconds = 0; //!< Сдвиг времени второго видео потока относительно первого

            ReadFrameResult readFrame(Demux &demux);
            StereoFormat getVideoFrameStereoFormat(StereoFormat::Layout videoFrameLayout);
            bool setError(ErrorCode error);
    };
}

#endif // STEREOVIDEODECODER_H
