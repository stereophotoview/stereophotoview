#include "taskqueue.h"

using namespace stereoscopic::fileFormats::video;

TaskQueue::TaskQueue(QObject *parent) : QObject(parent)
{
    updateAllTasks();
}

TaskQueue::~TaskQueue()
{
}

int TaskQueue::finishedCount()
{
    return m_finishedTasks.length();
}

int TaskQueue::activeCount()
{
    return m_tasks.length() + (m_activeTask ? 1 : 0);
}

void TaskQueue::add(SaveVideoTask *task)
{
    task->setParent(this);
    m_tasks.append(task);
    updateAllTasks();
    runNextTask();
    emit taskAdded();
}

void TaskQueue::activeTaskFinished()
{
    if(m_activeTask)
    {
        m_finishedTasks.append(m_activeTask);
        disconnect(&m_watcher, &QFutureWatcher<void>::finished, this, &TaskQueue::activeTaskFinished);
        if(m_stop)
        {
            m_stop = false;
            foreach(auto task, m_tasks)
                delete task;
            m_tasks.clear();
            if(m_activeTask)
                delete m_activeTask;
            m_activeTask = nullptr;
            emit stopped();
        }
        else
            runNextTask();
    }
}

void TaskQueue::removeAt(int index)
{
    SaveVideoTask* task = nullptr;
    if(index < m_finishedTasks.length())
    {
        task = m_finishedTasks[index];
        m_finishedTasks.removeAt(index);
    }
    else
    {
        int index2 = index - m_finishedTasks.length() - (m_activeTask ? 1 : 0);
        if(index2 >= 0 && index2 < m_tasks.length())
        {
            task = m_tasks[index2];
            m_tasks.removeAt(index2);
        }
    }
    if(task != nullptr)
        delete task;
    updateAllTasks();
}

void TaskQueue::stop()
{
    if(m_activeTask)
        m_activeTask->stop();
    m_stop = true;
}

void TaskQueue::runNextTask()
{
    if(m_activeTask == nullptr || m_activeTask->isFinished())
    {
        if(m_tasks.length() > 0)
        {
            m_activeTask = m_tasks.dequeue();
            auto f = QtConcurrent::run(m_activeTask, &SaveVideoTask::execute);
            m_watcher.setFuture(f);
            connect(&m_watcher, &QFutureWatcher<void>::finished, this, &TaskQueue::activeTaskFinished);
        }
        else
        {
            m_activeTask = nullptr;
        }
        updateAllTasks();
        emit currentIndexChanged(currentIndex());
    }
    emit finishedCountChanged(finishedCount());
    emit activeCountChanged(activeCount());
}

void TaskQueue::setAllTasks(QVariant value)
{
    m_allTasks = value;
    emit allTasksChanged(m_allTasks);
    emit currentIndexChanged(currentIndex());
}

void TaskQueue::updateAllTasks()
{
    QList<QObject*> res;
    foreach(auto task, m_finishedTasks)
    {
        res.append(task);
    }
    if(m_activeTask)
        res.append(m_activeTask);
    foreach(auto task, m_tasks)
    {
        res.append(task);
    }

    setAllTasks(QVariant::fromValue(res));
}

QVariant TaskQueue::qVariant(SaveVideoTask* task)
{
    QJsonObject obj;
    obj["result"] = task->result();
    obj["dstFilePath"] = task->dstFilePath();
    obj["srcFilePath"] = task->srcFilePath();
    return QVariant(obj);
}
