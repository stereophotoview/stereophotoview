#ifndef TASKQUEUE_H
#define TASKQUEUE_H

#include <QObject>
#include "savevideotask.h"
#include <QQueue>
#include <QtConcurrent/QtConcurrent>
#include "lib_global.h"

namespace stereoscopic::fileFormats::video {
    /**
     * @brief Класс для выполнения задач сохранения видео файлов по очереди
     */
    class LIBSHARED_EXPORT  TaskQueue : public QObject
    {
            Q_OBJECT

            Q_PROPERTY(QVariant allTasks READ allTasks NOTIFY allTasksChanged)
            Q_PROPERTY(int currentIndex READ currentIndex NOTIFY currentIndexChanged)
            Q_PROPERTY(int finishedCount READ finishedCount NOTIFY finishedCountChanged)
            Q_PROPERTY(int activeCount READ activeCount NOTIFY activeCountChanged)

        public:
            explicit TaskQueue(QObject *parent = nullptr);
            ~TaskQueue();

            QVariant allTasks(){ return m_allTasks; }
            int currentIndex(){ return m_finishedTasks.length() - (m_activeTask == NULL ? 1 : 0); }

            int finishedCount();
            int activeCount();

        signals:
            void allTasksChanged(QVariant);
            void currentIndexChanged(int);
            void taskAdded();
            void finishedCountChanged(int);
            void activeCountChanged(int);
            void stopped();

        public slots:
            void add(SaveVideoTask* task);
            void removeAt(int index);
            void stop();
            static TaskQueue* instance()
            {
                static TaskQueue instance;
                return &instance;
            }

        private slots:
            void activeTaskFinished();

        private:
            QQueue<SaveVideoTask*> m_tasks;
            QList<SaveVideoTask*> m_finishedTasks;
            SaveVideoTask* m_activeTask = nullptr;
            QFutureWatcher<void> m_watcher;
            QVariant m_allTasks;
            bool m_stop = false;

            void runNextTask();
            void setAllTasks(QVariant value);
            void updateAllTasks();
            QVariant qVariant(SaveVideoTask *task);

    };
}
#endif // TASKQUEUE_H
