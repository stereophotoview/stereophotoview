#include "videooptions.h"

#include <QFileInfo>

using namespace stereoscopic::fileFormats::video;

VideoOptions::VideoOptions(VideoOptions *src, QObject *parent) : QObject(parent)
{
    if(src != nullptr)
    {
        m_frameSize = src->m_frameSize;
        m_stereoFormat = src->m_stereoFormat;
        m_audioCodecId = src->m_audioCodecId;
        m_videoCodecId = src->m_videoCodecId;
        m_crf = src->m_crf;
        m_preset = src->m_preset;
        m_currentFormatindex = src->m_currentFormatindex;
    }
}

AVOutputFormat *VideoOptions::outputFormat()
{
    auto supported = Mux::supportedFormats();
    if(m_currentFormatindex > -1 && m_currentFormatindex < supported.length() )
        return supported[m_currentFormatindex];
    return nullptr;
}

QVariant VideoOptions::supportedFormats()
{
    QList<QVariant> res;
    foreach(AVOutputFormat* format, Mux::supportedFormats())
    {
        QJsonObject obj;
        obj["name"] = format->name;
        obj["long_name"] = format->long_name;
        obj["extensions"] = format->extensions;
        res.append(QVariant(obj));
    }
    return QVariant(res);
}

QString VideoOptions::currentFormatName()
{
    auto f = outputFormat();
    return f != nullptr ? f->name : QString();
}

void VideoOptions::setCurrentFormatName(QString value)
{
    auto f = findFormatByName(value);
    setCurrentFormatIndex(Mux::supportedFormats().indexOf(f));
}

QStringList VideoOptions::nameFilters()
{
    QStringList res;
    auto f = outputFormat();
    if(f != nullptr)
    {
        QStringList filters;
        foreach(QString ext, QString(f->extensions).split(","))
            filters.append(QString("*." + ext));
        res.append(QString(f->long_name).replace("/", ",") + " (" + filters.join("; ") + ")");
    }
    return res;
}

int VideoOptions::currentFormatIndex()
{
    return m_currentFormatindex;
}

void VideoOptions::setCurrentFormatIndex(int index)
{
    if(index != m_currentFormatindex && index > -1 && index < Mux::supportedFormats().length())
    {
        m_currentFormatindex = index;
        auto f = outputFormat();
        m_videoCodecId = f  != nullptr ? Mux::defaultVideoCodec(f) : AV_CODEC_ID_NONE;
        m_audioCodecId = f  != nullptr ? Mux::defaultAudioCodec(f) : AV_CODEC_ID_NONE;
        emit currentFormatChanged();
    }
}

int VideoOptions::audioBytesPerSample()
{
    if(m_audioCodecId == AV_CODEC_ID_NONE)
        return 0;
    AVCodec *codec = avcodec_find_encoder(m_audioCodecId);
    return FFMpegHelper::bytesPerSample(OutputAudioStream::defaultSampleFmt(codec));
}

QString VideoOptions::videoCodecName()
{
    AVCodec *codec = avcodec_find_encoder(m_videoCodecId);
    if(codec == nullptr)
        return QString();
    return QString(codec->name);
}

QString VideoOptions::audioCodecName()
{
    AVCodec *codec = avcodec_find_encoder(m_audioCodecId);
    if(codec == nullptr)
        return QString();
    return QString(codec->name);
}

VideoOptions::CoDec VideoOptions::videoCodecID()
{
    switch (m_videoCodecId) {
        case AV_CODEC_ID_H264:
            return CodecH264;
        case AV_CODEC_ID_MPEG4:
            return CodecMPEG4;
        case AV_CODEC_ID_FLV1:
            return CodecFLV;
        default:
            return CodecNone;
    }
}

QString VideoOptions::changeSuffix(QString filePath, QString formatName)
{
    foreach(AVOutputFormat* format, Mux::supportedFormats())
    {
        if(format->name == formatName)
        {
            int lastIndex = filePath.lastIndexOf('.');
            if(lastIndex > -1)
                filePath = filePath.remove(lastIndex, filePath.length() - lastIndex);
            QString ext(format->extensions);
            return filePath + "." + ext.split(',')[0];
        }
    }
    return filePath;
}

int VideoOptions::findFormatByFilePath(QString filePath)
{
    QFileInfo info(filePath);
    QString suf = info.suffix();
    int i = 0;
    foreach(AVOutputFormat* format, Mux::supportedFormats())
    {
        QString e(format->extensions);
        foreach (QString ext, e.split(','))
            if(ext.toUpper() == suf.toUpper())
                return i;
        i++;
    }
    return -1;
}

AVOutputFormat *VideoOptions::findFormatByName(QString name)
{
    foreach(AVOutputFormat* format, Mux::supportedFormats())
    {
        if(format->name == name)
            return format;
    }
    return nullptr;
}

int VideoOptions::findFormatIndexByName(QString name)
{
    int i = 0;
    foreach(AVOutputFormat* format, Mux::supportedFormats())
    {
        if(format->name == name)
            return i;
        i++;
    }
    return -1;
}
