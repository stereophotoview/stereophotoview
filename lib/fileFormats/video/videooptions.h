#ifndef VIDEOOPTIONS_H
#define VIDEOOPTIONS_H

#include <QObject>

extern "C"
{
#include <libavformat/avformat.h>
}

#include "stereoformat.h"
#include "mux.h"
#include "ffmpeghelper.h"
#include <memory>
#include <QVariant>
#include <QJsonObject>

namespace stereoscopic::fileFormats::video {

    /**
     * @brief Класс, представляющий параметры для сохранения видео файла
     */
    class LIBSHARED_EXPORT VideoOptions : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QString currentFormatName READ currentFormatName WRITE setCurrentFormatName NOTIFY currentFormatChanged)
            Q_PROPERTY(QStringList nameFilters READ nameFilters NOTIFY currentFormatChanged)
            Q_PROPERTY(int currentFormatIndex READ currentFormatIndex WRITE setCurrentFormatIndex NOTIFY currentFormatChanged)
            Q_PROPERTY(int crf READ crf WRITE setCrf NOTIFY crfChanged)
            Q_PROPERTY(QString preset READ preset WRITE setPreset NOTIFY presetChanged)
            Q_PROPERTY(QSize frameSize READ frameSize WRITE setFrameSize NOTIFY frameSizeChanged)
            Q_PROPERTY(StereoFormat* stereoFormat READ stereoFormat)
            Q_PROPERTY(QVariant supportedFormats READ supportedFormats)
            Q_PROPERTY(int audioBytesPerSample READ audioBytesPerSample NOTIFY audioBytesPerSampleChanged)
            Q_PROPERTY(CoDec videoCodecID READ videoCodecID NOTIFY currentFormatChanged)
            Q_PROPERTY(QString videoCodecName READ videoCodecName NOTIFY currentFormatChanged)
            Q_PROPERTY(QString audioCodecName READ audioCodecName NOTIFY currentFormatChanged)

        public:

            enum CoDec {
                CodecNone, CodecH264, CodecMPEG4, CodecFLV
            };
            Q_ENUMS(CoDec)

            explicit VideoOptions(VideoOptions* src = nullptr, QObject *parent = nullptr);

            AVOutputFormat* outputFormat();

            int crf(){ return m_crf; }
            void setCrf(int value)
            {
                m_crf = value;
                emit crfChanged();
            }

            QString preset(){ return m_preset; }
            void setPreset(QString value)
            {
                m_preset = value;
                emit presetChanged();
            }

            QSize frameSize(){ return m_frameSize; }
            void setFrameSize(QSize value)
            {
                m_frameSize = value;
                emit frameSizeChanged();
            }

            StereoFormat* stereoFormat(){ return &m_stereoFormat; }

            QVariant supportedFormats();

            QString currentFormatName();
            void setCurrentFormatName(QString value);

            /// Возвращает фильтры текущего формата для окна сохранения
            QStringList nameFilters();

            int currentFormatIndex();
            void setCurrentFormatIndex(int index);
            int audioBytesPerSample();

            AVCodecID videoCodec() { return m_videoCodecId; }
            AVCodecID audioCodec() { return m_audioCodecId; }

            QString videoCodecName();
            QString audioCodecName();

            CoDec videoCodecID();

        public slots:

            QString changeSuffix(QString filePath, QString formatName);
            int findFormatByFilePath(QString filePath);

        signals:
            void outputFormatChanged();
            void frameSizeChanged();
            void stereoFormatChanged();
            void currentFormatChanged();
            void nameFiltersChanged();
            void audioBytesPerSampleChanged();
            void crfChanged();
            void presetChanged();

        private:
            QSize m_frameSize;
            StereoFormat m_stereoFormat;
            AVCodecID
                m_audioCodecId = AV_CODEC_ID_NONE,
                m_videoCodecId = AV_CODEC_ID_NONE;
            int m_crf = 23;
            QString m_preset = "medium";
            int m_currentFormatindex = -1;

            AVOutputFormat* findFormatByName(QString name);
            int findFormatIndexByName(QString name);
    };
}
#endif // VIDEOOPTIONS_H
