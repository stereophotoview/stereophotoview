#include "videoformat.h"
#include "video/taskqueue.h"

using namespace stereoscopic;
using namespace stereoscopic::fileFormats;
using namespace stereoscopic::fileFormats::video;

VideoFormat::VideoFormat(QObject *parent)
    : SingleImageFormat(parent)
{
    foreach (AVInputFormat* f, Demux::supportedFormats())
    {
        QString ext(f->extensions);
        if(ext.length() > 0)
        {
            foreach(QString s, ext.split(','))
            {
                if(s != "txt" && s != "mpo" && !extensions_.contains(s))
                    extensions_.append(s);
            }
        }
    }
    extensions_.sort();
}

StereoImageSource *VideoFormat::load(QString fileName, StereoFormat* format)
{
    return load(fileName, format, true);
}

StereoImageSource *VideoFormat::load(QString fileName, StereoFormat* format, bool play)
{
    auto s = new StereoVideoSource(fileName, format, play);
    if(s->error() >= 0)
        return s;
    delete s;
    return nullptr;
}

void VideoFormat::save(StereoVideoSource *source, QString fileName, VideoOptions *options)
{
    TaskQueue::instance()->add(createSaveTask(source, fileName, options));
}

SaveVideoTask *VideoFormat::createSaveTask(StereoVideoSource *source, QString fileName, VideoOptions *options)
{
    SaveVideoTask* task = new SaveVideoTask();
    task->setSrcFilePath(source->fileName());
    task->setDstFilePath(fileName);
    task->setOptions(options);
    task->setLayout(source->layoutChart());
    task->setSrcFormat(*(source->activeFormat()));
    task->setTimeShift(source->timeShift());
    return task;
}
