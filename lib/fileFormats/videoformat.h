#ifndef VIDEOFORMAT_H
#define VIDEOFORMAT_H

#include "singleimageformat.h"
#include "stereoformat.h"
#include "stereovideosource.h"
#include "video/instream.h"
#include "video/videooptions.h"
#include "video/savevideotask.h"

namespace stereoscopic::fileFormats
{
    /**
     * @brief Класс, предоставляющий поддержку формата видео файлов
     */
    class LIBSHARED_EXPORT  VideoFormat : public SingleImageFormat
    {
            Q_OBJECT
            Q_PROPERTY(QStringList extensions READ extensions)

        public:
            VideoFormat(QObject *parent = nullptr);

        public slots:
            /**
             * @brief Загружает видео файл
             * @param fileName Путь к файлу
             * @param format Стерео формат
             * @param play Начать проигрывание сразу после загрузки
             * @return Источник стерео для отображения
             */
            stereoscopic::StereoImageSource* load(QString fileName, StereoFormat* format, bool play);

            /**
             * @brief Загружает видео файл
             * @param fileName Путь к файлу
             * @param format Стерео формат
             * @return Источник стерео для отображения
             */
            stereoscopic::StereoImageSource* load(QString fileName, StereoFormat* format);

            /**
             * @brief Сохраняет видео файл
             * @param source Источник исходного видео
             * @param fileName Путь к файлу назначения
             * @param options Параметры видео
             */
            void save(StereoVideoSource *source, QString fileName, VideoOptions* options);

            /**
             * @brief Создаёт задачу для асинхронного сохранения видео файла
             * @param source Источник исходного видео
             * @param fileName Путь к файлу назначения
             * @param options Параметры видео
             * @return Задача для асинхронного сохранения видео файла
             */
            stereoscopic::fileFormats::video::SaveVideoTask* createSaveTask(StereoVideoSource *source, QString fileName, VideoOptions* options);
    };
}
#endif // VIDEOFORMAT_H
