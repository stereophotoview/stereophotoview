#include "imagehelper.h"
#include <math.h>

#include <QPainter>
#include <QDebug>
#include <math.h>

using namespace stereoscopic;

QRect ImageHelper::getEnteredImageRect(QSize imageSize, QSize frame)
{
    double imageAspectRatio = (double)imageSize.width() / imageSize.height();
    double frameAspectRatio = (double)frame.width() / frame.height();

    if (imageAspectRatio > frameAspectRatio)
    {
        int height = (int)((double)frame.width() / imageAspectRatio);
        return QRect(0, (frame.height() - height) / 2, frame.width(), height);
    }
    else
    {
        int width = (int)((double)frame.height() * imageAspectRatio);
        return QRect((frame.width() - width) / 2, 0, width, frame.height());
    }
}

QImage ImageHelper::createThumbnail(QImage *src, const QSize &size)
{
    if(src->width() < size.width() && src->height() < size.height())
        return src->copy();
    QRect rect = getEnteredImageRect(src->size(), size);
    return src->scaled(rect.size());
}

QImage ImageHelper::rotated(QImage src, double angle)
{
    if(fabs(0 - angle) <= 0.0001)
        return src;
    QMatrix matr;
    matr = matr.rotate(angle);
    QTransform trans(matr);

    QImage res(src.size(), QImage::Format_RGB32);
    res.fill(Qt::black);
    QPainter painter(&res);
    painter.translate(QPoint(src.size().width() / 2, src.size().height() / 2));
    painter.rotate(angle);
    painter.drawImage(QPoint(-src.size().width() / 2, -src.size().height() / 2), src);
    return res;
}

void drawTopBorder(QPainter &painter, int borderWidth, QRect rect)
{
    QRect lineRect(rect.topLeft(), QSize(rect.width(), borderWidth));
    QLinearGradient gradient(QPoint(0, lineRect.top()), QPoint(0, lineRect.bottom()));
    gradient.setColorAt(0, QColor(0, 0, 0, 255));
    gradient.setColorAt(1, QColor(0, 0, 0, 0));
    painter.fillRect(lineRect, gradient);
}

void drawBottomBorder(QPainter &painter, int borderWidth, QRect rect)
{
    QRect lineRect(QPoint(rect.left(), rect.bottom() - borderWidth), rect.bottomRight());
    QLinearGradient gradient(QPoint(0, lineRect.bottom()), QPoint(0, lineRect.top()));
    gradient.setColorAt(0, QColor(0, 0, 0, 255));
    gradient.setColorAt(1, QColor(0, 0, 0, 0));
    painter.fillRect(lineRect, gradient);
}

void drawLeftBorder(QPainter &painter, int borderWidth, QRect rect)
{
    QRect lineRect(rect.topLeft(), QSize(borderWidth, rect.height()));
    QLinearGradient gradient(QPoint(rect.left(), 0), QPoint(rect.left() + borderWidth, 0));
    gradient.setColorAt(0, QColor(0, 0, 0, 255));
    gradient.setColorAt(1, QColor(0, 0, 0, 0));
    painter.fillRect(lineRect, gradient);
}

void drawRightBorder(QPainter &painter, int borderWidth, QRect rect)
{
    QRect lineRect(QPoint(rect.right() - borderWidth, rect.top()), QSize(borderWidth, rect.height()));
    QLinearGradient gradient(QPoint(rect.right(), 0), QPoint(rect.right() - borderWidth, 0));
    gradient.setColorAt(0, QColor(0, 0, 0, 255));
    gradient.setColorAt(1, QColor(0, 0, 0, 0));
    painter.fillRect(lineRect, gradient);
}

QImage ImageHelper::withFeatheredBorder(QImage src, int borderWidthPercents, QRect rect, QColor outerColor)
{
    QPainter painter(&src);
    if(rect.isValid())
    {
        painter.fillRect(QRect(QPoint(0, 0), QPoint(src.width(), rect.top()-1)), outerColor); // Сверху
        painter.fillRect(QRect(QPoint(0, rect.top()), QPoint(rect.left()-1, src.height())), outerColor); // Слева
        painter.fillRect(QRect(QPoint(rect.right(), rect.top()), QPoint(src.width(), src.height())), outerColor); // Справа
        painter.fillRect(QRect(QPoint(rect.left(), rect.bottom()+1), QPoint(rect.right()-1, src.height())), outerColor); // Снизу
    }
    else
    {
        rect = QRect(QPoint(0,0), src.size());
    }
    const int borderWidth = fmin(rect.width(), rect.height()) * borderWidthPercents / 100;
    if(borderWidth > 0)
    {
        drawTopBorder(painter, borderWidth, rect);
        drawRightBorder(painter, borderWidth, rect);
        drawBottomBorder(painter, borderWidth, rect);
        drawLeftBorder(painter, borderWidth, rect);
    }
    return src;
}

QSize ImageHelper::fitToArea(QSize size, int targetArea)
{
    double h1 = sqrt((double)targetArea * size.height() / size.width());
    double w1 = (double)targetArea / h1;
    return QSize((int)w1, (int)h1);
}

QImage ImageHelper::createThumbnail(QImage &src, int targetArea)
{
    if(src.width() * src.height() <= targetArea)
        return src;
    QSize targetSize = fitToArea(src.size(), targetArea);
    return createThumbnail(&src, targetSize);
}

QImage ImageHelper::applyOrientation(QImage &src, ushort orientatin)
{
    QTransform t;
    switch (orientatin)
    {
    case 2:
        // Mirror horizontal
        return src.mirrored(true, false);
    case 3:
        // Rotate 180
        return src.transformed(t.rotate(180));
    case 4:
        // Mirror vertical
        return src.mirrored(false, true);
    case 5:
        // Mirror horizontal and rotate 270 CW
        return src.mirrored(true, false).transformed(t.rotate(270));
    case 6:
        // Rotate 90 CW
        return src.transformed(t.rotate(90));
    case 7:
        // Mirror horizontal and rotate 90 CW
        return src.mirrored(true, false).transformed(t.rotate(90));
    case 8:
        // Rotate 270 CW
        return src.transformed(t.rotate(270));
    default:
        // 1 - Horizontal (normal)
        return src;
    }
}

QRect ImageHelper::translateToSource(QRect rect, QSize frameSize, QSize imageSize)
{
    rect = QRect(QPoint(0,0), frameSize).intersected(rect);
    double cw =  (double)imageSize.width() / (double)frameSize.width();
    double ch = (double)imageSize.height() / (double)frameSize.height();
    QRect res((int)(rect.x() * cw),
              (int)(rect.y() * ch),
              (int)(rect.width() * cw),
              (int)(rect.height() * ch));
    return res;
}

ImageHelper::ImageHelper()
{

}
