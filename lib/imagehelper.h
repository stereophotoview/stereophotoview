#ifndef PIXMAPHELPER_H
#define PIXMAPHELPER_H

#include <QRect>
#include <QSize>

#include <QImage>

#include "fileFormats/jps/stereoscopicdescriptor.h"
#include "lib_global.h"

using namespace stereoscopic::fileFormats::jps;
namespace stereoscopic {
    /**
     * @briefВспомагательный класс для обработки изображений
     */
    class LIBSHARED_EXPORT  ImageHelper
    {
        public:
            /// Возвращает прямоугольник изображения imageSize, вписанного в прямоугольник frame, с сохранением соотношения сторон
            static QRect getEnteredImageRect(QSize imageSize, QSize frame);

            /// Создаёт изображение предпросмотра для исходного изображения, вписанного в указанные размеры
            static QImage createThumbnail(QImage *, const QSize &);

            /**
             * @brief Возвращает повёрнутое на указанный угол изображение
             * @param src Исходное изображение
             * @param angle Угол поворота
             * @return Повёрнутое изображение
             */
            static QImage rotated(QImage src, double angle);

            /**
             * @brief Возвращает изображение с наложеной сглаженной рамкой
             * @param src Исходное изображение
             * @param borderWidth Ширина рамки
             * @param rect Прямоугольник рамки, если рамка накладывается на часть кадра
             * @param outerColor Цвет заливки пространства покруг рамки
             */
            static QImage withFeatheredBorder(QImage src, int borderWidthPercents, QRect rect = QRect(), QColor outerColor = QColor());

            /**
             * @brief Вписывает размер изображения size в указанное количество пикселей (площадь)
             * @param size Размер исходного изображения
             * @param targetArea Целевая площадь
             * @return Размер изображения, вписанный в целевую площадь
             */
            static QSize fitToArea(QSize size, int targetArea);

            /**
             * @brief Создаёт изображения предпросмотра с указанным количеством пикселей (площадь)
             * @param src Исходное изображение
             * @param targetArea Целевая площадь значка предпросмотра
             * @return Уменьшенное до целевой площади изображение
             */
            static QImage createThumbnail(QImage &src, int targetArea);

            /**
             * @brief Применяет ориантацию из exif
             * @param src Исходное изображение
             * @param orientatin Значение exif тега orientation
             * @return Преобразованное изображение
             */
            static QImage applyOrientation(QImage &src, ushort orientatin);

            /**
              @brief Возвращает прямоугольник, преобразованный из координат отображения в координаты исходного снимка.
              @param frameSize Размер кадра, в который вписывается изображение
              @param imageSize Размер исходного изображения
              @param rect Прямоугольник, координаты которого нужно преобразовать
              @return Преобразованный прямоугольник
             */
            static QRect translateToSource(QRect rect, QSize frameSize, QSize imageSize);

        private:
            ImageHelper();
    };
}
#endif // PIXMAPHELPER_H
