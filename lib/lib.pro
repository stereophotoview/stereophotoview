#-------------------------------------------------
#
# Project created by QtCreator 2017-11-10T14:29:43
#
#-------------------------------------------------

QT += widgets multimedia qml

QT -= gui

TARGET = stereophotoview
TEMPLATE = lib
#CONFIG += plugin

DEFINES += LIB_LIBRARY

include(../common.pri)

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    alignchart.cpp \
    alignparams.cpp \
    autoalign.cpp \
    colors/coloroperations.cpp \
    colors/colorsdebug.cpp \
    colors/colorshelper.cpp \
    colors/frgb.cpp \
    colors/rgbtransformation.cpp \
    fileFormats/video/metadata.cpp \
    fileFormats/video/stereovideocoder.cpp \
    fileFormats/video/stereovideodecoder.cpp \
    vision/imagecv.cpp \
    fileFormats/mpo/ifd.cpp \
    fileFormats/singleimageformat.cpp \
    fileFormats/mpo/mpoextension.cpp \
    fileFormats/jpeg/jpegmetadataextension.cpp \
    fileFormats/jpeg/jpegmetadata.cpp \
    fileFormats/jpeg/rawjpegextension.cpp \
    fileFormats/jps/stereoscopicdescriptor.cpp \
    fileFormats/separateimageformat.cpp \
    fileFormats/jpeg/qexifimageheader.cpp \
    fileFormats/jpeg/exifextension.cpp \
    fileFormats/jpeg/exifheaders.cpp \
    fileFormats/video/demux.cpp \
    fileFormats/video/readresult.cpp \
    fileFormats/video/mux.cpp \
    fileFormats/video/outputstream.cpp \
    fileFormats/video/outputaudiostream.cpp \
    fileFormats/video/outputvideostream.cpp \
    fileFormats/video/ffmpeghelper.cpp \
    fileFormats/video/videooptions.cpp \
    fileFormats/video/inaudioframe.cpp \
    fileFormats/video/inaudiostream.cpp \
    fileFormats/video/inframe.cpp \
    fileFormats/video/inpacket.cpp \
    fileFormats/video/instream.cpp \
    fileFormats/video/invideoframe.cpp \
    fileFormats/video/invideostream.cpp \
    fileFormats/video/savevideotask.cpp \
    fileFormats/video/taskqueue.cpp \
    vision/stereoframecv.cpp \
    stereoimagesource.cpp \
    stereoformat.cpp \
    stereoframe.cpp \
    stereodecoders/stereodecoder.cpp \
    stereodecoders/rowinterlaceddecoder.cpp \
    stereodecoders/separatedecoder.cpp \
    stereodecoders/pairdecoder.cpp \
    stereodecoders/decoderfactory.cpp \
    stereodecoders/colinterlaceddecoder.cpp \
    stereodecoders/anaglyphdecoder.cpp \
    stereodecoders/anaglyphybdecoder.cpp \
    stereovideosource.cpp \
    stereocoders/stereocoder.cpp \
    stereocoders/anaglyphcoder.cpp \
    stereocoders/anaglyphybcoder.cpp \
    stereocoders/rowinterlacedcoder.cpp \
    stereocoders/colinterlacedcoder.cpp \
    stereocoders/paircoder.cpp \
    stereocoders/coderfactory.cpp \
    imagehelper.cpp \
    mediaplayer/mediaplayer.cpp \
    mediaplayer/decoderthread.cpp \
    mediaplayer/framesheduler.cpp \
    urlconv.cpp \
    stereolib.cpp \
    fileFormats/jpsimageformat.cpp \
    fileFormats/mpoimageformat.cpp \
    fileFormats/videoformat.cpp \
    mathhelper.cpp


HEADERS += \
    alignchart.h \
    alignparams.h \
    autoalign.h \
    colors/coloroperations.h \
    colors/colorsdebug.h \
    colors/colorshelper.h \
    colors/frgb.h \
    colors/rgbtransformation.h \
    fileFormats/video/metadata.h \
    fileFormats/video/stereovideocoder.h \
    fileFormats/video/stereovideodecoder.h \
    vision/imagecv.h \
    lib_global.h \
    fileFormats/mpo/ifd.h \
    fileFormats/singleimageformat.h \
    fileFormats/mpo/mpoextension.h \
    fileFormats/jpeg/jpegmetadataextension.h \
    fileFormats/jpeg/jpegmetadata.h \
    fileFormats/jpeg/rawjpegextension.h \
    fileFormats/jps/stereoscopicdescriptor.h \
    fileFormats/separateimageformat.h \
    fileFormats/jpeg/qexifimageheader.h \
    fileFormats/jpeg/exifextension.h \
    fileFormats/jpeg/exifheaders.h \
    fileFormats/video/demux.h \
    fileFormats/video/readresult.h \
    fileFormats/video/mux.h \
    fileFormats/video/outputstream.h \
    fileFormats/video/outputaudiostream.h \
    fileFormats/video/outputvideostream.h \
    fileFormats/video/ffmpeghelper.h \
    fileFormats/video/videooptions.h \
    fileFormats/video/inaudioframe.h \
    fileFormats/video/inaudiostream.h \
    fileFormats/video/inframe.h \
    fileFormats/video/inpacket.h \
    fileFormats/video/instream.h \
    fileFormats/video/invideoframe.h \
    fileFormats/video/invideostream.h \
    fileFormats/video/savevideotask.h \
    fileFormats/video/taskqueue.h \
    stereoformat.h \
    vision/stereoframecv.h \
    stereoimagesource.h \
    stereoframe.h \
    stereodecoders/stereodecoder.h \
    stereodecoders/rowinterlaceddecoder.h \
    stereodecoders/separatedecoder.h \
    stereodecoders/pairdecoder.h \
    stereodecoders/decoderfactory.h \
    stereodecoders/colinterlaceddecoder.h \
    stereodecoders/anaglyphdecoder.h \
    stereodecoders/anaglyphybdecoder.h \
    stereovideosource.h \
    stereocoders/stereocoder.h \
    stereocoders/anaglyphcoder.h \
    stereocoders/anaglyphybcoder.h \
    stereocoders/rowinterlacedcoder.h \
    stereocoders/colinterlacedcoder.h \
    stereocoders/paircoder.h \
    stereocoders/coderfactory.h \
    imagehelper.h \
    mediaplayer/mediaframe.h \
    mediaplayer/mediaplayer.h \
    mediaplayer/decoderthread.h \
    mediaplayer/framesheduler.h \
    urlconv.h \
    stereolib.h \
    fileFormats/jpsimageformat.h \
    fileFormats/mpoimageformat.h \
    fileFormats/videoformat.h \
    mathhelper.h


DISTFILES += \
    docs/MPO.txt

# Подключаем сторонние библиотеки
include(../external_libs.pri)

# Распространение
include(deployment.pri)

# Локализация
include(translations.prf)
