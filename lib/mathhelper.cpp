#include "mathhelper.h"
#include <math.h>
#include <limits>

using namespace std;

bool stereoscopic::math::equal(double a, double b)
{
    return abs(a - b) < numeric_limits<double>::min();
}
