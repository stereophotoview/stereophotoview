#ifndef MATHHELPER_H
#define MATHHELPER_H

#include <lib_global.h>

/**
 * @brief Вспомагательные математические функции
 */
namespace stereoscopic::math {
    /**
     * @brief Проверяет два числа с плавающий точкой на равенство
     * @param Первое число
     * @param Второе число
     * @return true, если числа равны
     */
    LIBSHARED_EXPORT bool equal(double a, double b);
}
#endif // MATHHELPER_H
