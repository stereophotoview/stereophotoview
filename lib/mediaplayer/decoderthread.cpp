#include "decoderthread.h"

using namespace stereoscopic::mediaplayer;

DecoderThread::DecoderThread(StereoVideoDecoder *decoder)
{
    m_decoder = decoder;
}

DecoderThread::~DecoderThread()
{
    requestInterruption();
    wait();
}

StereoFrame DecoderThread::currentStereoVideoFrame()
{
    QMutexLocker locker(&mutex);
    return m_decoder->stereoVideoFrame();
}

double DecoderThread::position()
{
    QMutexLocker locker(&mutex);
    return m_decoder->position();
}

void DecoderThread::setPosition(double position)
{
    QMutexLocker locker(&mutex);
    m_decoder->setPosition(position);
}

StereoFrame DecoderThread::readVideoFrameAt(double position)
{
    QMutexLocker locker(&mutex);
    m_decoder->setPosition(position - 1);
    StereoFrame videoFrame;
    while(m_decoder->decodeFrame() && m_decoder->position() < position) {
        if(!m_decoder->stereoVideoFrame().isNull())
            videoFrame = m_decoder->stereoVideoFrame();
    }
    return videoFrame;
}

void DecoderThread::run()
{
    for(;;)
    {
        mutex.lock();
        if(isInterruptionRequested() || !m_decoder->decodeFrame())
        {
            mutex.unlock();
            break;
        }
        auto videoFrame = m_decoder->stereoVideoFrame();
        auto audioFrame = m_decoder->audioFrame();
        auto position = m_decoder->position();
        mutex.unlock();

        if(!videoFrame.isNull())
            emit mediaFrameDecoded(MediaFrame(videoFrame, position));
        if(audioFrame != nullptr)
            emit mediaFrameDecoded(MediaFrame(audioFrame));
    }
}
