#ifndef DECODERTHREAD_H
#define DECODERTHREAD_H

#include <QThread>
#include <QMutexLocker>
#include <QQueue>
#include <memory>

#include "fileFormats/video/stereovideodecoder.h"
#include "fileFormats/video/demux.h"
#include "fileFormats/video/instream.h"
#include "fileFormats/video/inpacket.h"
#include "stereoframe.h"
#include "mediaframe.h"

using namespace std;
using namespace stereoscopic;
using namespace stereoscopic::fileFormats::video;

namespace stereoscopic::mediaplayer
{
    /**
     * @brief Класс для выполнения декодирования мультимедиа потока в отдельном потоке выполнения
     */
    class LIBSHARED_EXPORT DecoderThread : public QThread
    {
            Q_OBJECT
        public:
            DecoderThread(StereoVideoDecoder *decoder);
            ~DecoderThread();
            QString fileName(){ return m_decoder->fileName(); }
            StereoVideoDecoder* decoder() { return m_decoder; }
            StereoFrame currentStereoVideoFrame();
            double position();
            void setPosition(double position);
            StereoFrame readVideoFrameAt(double position);

        signals:
            void mediaFrameDecoded(stereoscopic::mediaplayer::MediaFrame frame);

        protected:
            void run() Q_DECL_OVERRIDE;

        private:
            StereoVideoDecoder *m_decoder = nullptr;
            bool m_stopped = false;
            bool m_isSuspended = false;
            QMutex mutex;
    };

}
#endif // DECODERTHREAD_H
