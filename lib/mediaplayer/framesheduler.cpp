#include "framesheduler.h"

using namespace stereoscopic::mediaplayer;

FrameSheduler::FrameSheduler()
    : QThread(nullptr)
{
}

FrameSheduler::~FrameSheduler()
{
    stop();
    wait();
}

void FrameSheduler::putMediaFrame(MediaFrame frame)
{
    QMutexLocker locker(&mutex);
    if(isInterruptionRequested() || m_stop)
        return;
    if(frameBuffer.length() >= m_frameBufferSize)
        whileFullQueue.wait(&mutex);

    for(int i = 0; i < frameBuffer.length(); i++)
    {
        if(frameBuffer[i].pts > frame.pts)
        {
            frameBuffer.insert(i, frame);
            return;
        }
    }
    frameBuffer.append(frame);
    whileEmptyQueue.wakeAll();
}

void FrameSheduler::checkFrame()
{
    QMutexLocker locker(&mutex);
    if(m_stop)
        return;

    if(frameBuffer.empty())
        whileEmptyQueue.wait(&mutex);

    if(m_stop || isInterruptionRequested() || frameBuffer.length() == 0)
        return;

    double clock = playTime();
    double minFramePts = frameBuffer[0].pts;

    static ulong remaining = (minFramePts - clock) * 1000000;
    if(remaining < intervalMks)
    {
        mutex.unlock();
        usleep((ulong)remaining);
        mutex.lock();
    }
    clock = playTime();
    if(clock <= 0 || clock >= minFramePts - 0.00001)
    {
        QList<MediaFrame> curFrames;
        while(frameBuffer.length() > 0 && frameBuffer[0].pts < clock + 0.00001)
        {
            MediaFrame frame = frameBuffer.dequeue();
            // skip passed video frames
            if(!frame.audioFrame.isNull() || m_skippedVideoFrames > 10 || frame.pts >= clock - 0.01)
            {
                if(frame.audioFrame.isNull())
                    m_skippedVideoFrames = 0;
                curFrames.append(frame);
            }
        }
        if(m_startTime.isNull())
            m_startTime = QDateTime::currentDateTime().addMSecs((int)(-minFramePts * 1000));

        if(!curFrames.empty())
            emit timeToShowFrames(curFrames);

        if(frameBuffer.length() < m_frameBufferSize)
            whileFullQueue.wakeAll();
    }
}

void FrameSheduler::run()
{
    // We give the decoder time to fill the frame buffer
    sleep(1);
    if(suspendedPos == std::numeric_limits<double>::max())
        m_startTime = QDateTime::currentDateTime();
    while(!isInterruptionRequested() && !m_stop)
    {
        uiMutex.lock();
        if(playTime() >= suspendedPos && !m_stop)
            whileSuspend.wait(&uiMutex);
        uiMutex.unlock();
        checkFrame();
        usleep(intervalMks);
    }
    whileFullQueue.wakeAll();
    whileEmptyQueue.wakeAll();
}

bool FrameSheduler::streamIndexLessThen(const MediaFrame &f1, const MediaFrame &f2)
{
    return f1.streamIndex < f2.streamIndex;
}

double FrameSheduler::playTime()
{
    if(m_startTime.isNull())
        return 0;
    return (double)m_startTime.msecsTo(QDateTime::currentDateTime()) / 1000;
}

void FrameSheduler::stop()
{
    QMutexLocker locker(&mutex);

    m_stop = true;
    requestInterruption();

    frameBuffer.clear();
    whileFullQueue.wakeAll();
    whileEmptyQueue.wakeAll();
    whileSuspend.wakeAll();
}

void FrameSheduler::suspend()
{
    QMutexLocker locker(&uiMutex);
    suspendedPos = playTime();
}

void FrameSheduler::resume()
{
    QMutexLocker locker(&mutex);
    m_startTime = suspendedPos < std::numeric_limits<double>::max()
            ? QDateTime::currentDateTime().addMSecs(-suspendedPos * 1000)
            : QDateTime::currentDateTime();
    suspendedPos = std::numeric_limits<double>::max();
    whileSuspend.wakeAll();
}

void FrameSheduler::setPosition(double position)
{
    QMutexLocker locker(&mutex);
    frameBuffer.clear();
    whileFullQueue.wakeAll();
    whileEmptyQueue.wakeAll();

    if(suspendedPos != std::numeric_limits<double>::max())
        suspendedPos = position;
    m_startTime = QDateTime::currentDateTime().addMSecs(-position * 1000);
    emit startTimeChanged(m_startTime);
}
