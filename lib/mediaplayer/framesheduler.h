#ifndef FRAMESHEDULER_H
#define FRAMESHEDULER_H

#include <limits>
#include <QMutex>
#include <QWaitCondition>
#include <QObject>
#include <QQueue>
#include <memory>
#include <QTimer>
#include <QDateTime>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QMutexLocker>

#include "fileFormats/video/inaudioframe.h"
#include "fileFormats/video/instream.h"
#include "stereoframe.h"
#include "mediaframe.h"

using namespace std;
using namespace stereoscopic::fileFormats::video;

namespace stereoscopic::mediaplayer
{
    /**
     * @brief Класс планировщика отображения кадров в нужное время
     */
    class LIBSHARED_EXPORT FrameSheduler : public QThread
    {
            Q_OBJECT
            Q_PROPERTY(QDateTime startTime READ startTime NOTIFY startTimeChanged)

        public:
            FrameSheduler();
            ~FrameSheduler();
            QDateTime startTime(){ return m_startTime; }
            double playTime();

            void stop();
            void suspend();
            void resume();
            void setPosition(double position);

        signals:
            void startTimeChanged(QDateTime);
            void timeToShowFrames(QList<stereoscopic::mediaplayer::MediaFrame> frame);

        public slots:
            void putMediaFrame(stereoscopic::mediaplayer::MediaFrame frame);

        private slots:
            void checkFrame();

        protected:
            void run();

        private:
            const ulong intervalMks = 100;
            QDateTime m_startTime;
            QMutex mutex;
            QQueue<MediaFrame> frameBuffer;
            const int m_frameBufferSize = 100;
            QWaitCondition whileFullQueue;
            QWaitCondition whileEmptyQueue;
            QMutex uiMutex;
            double suspendedPos = std::numeric_limits<double>::max();
            QWaitCondition whileSuspend;
            bool m_stop = false;
            int m_skippedVideoFrames = 0;

            static bool streamIndexLessThen(const MediaFrame &f1, const MediaFrame& f2);
    };
}

#endif // FRAMESHEDULER_H
