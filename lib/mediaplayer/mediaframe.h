#ifndef MEDIAFRAME_H
#define MEDIAFRAME_H

#include <QSharedPointer>
#include "stereoframe.h"
#include "fileFormats/video/inaudioframe.h"

using namespace stereoscopic;
using namespace stereoscopic::fileFormats::video;

namespace stereoscopic::mediaplayer
{
    class MediaFrame {
        public:
            MediaFrame(StereoFrame videoFrame, double pts){
                this->videoframe = videoFrame;
                this->pts = pts;
            }
            MediaFrame(QSharedPointer<InAudioFrame> audioFrame)
            {
                this->audioFrame = audioFrame;
                this->pts = audioFrame->ptsInSeconds();
            }
            int streamIndex = 1;
            double pts = 0;
            StereoFrame videoframe;
            QSharedPointer<InAudioFrame> audioFrame;
    };
}

#endif // MEDIAFRAME_H
