#include "mediaplayer.h"

using namespace stereoscopic::mediaplayer;

MediaPlayer::MediaPlayer(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QList<QImage>>("ImageList");
}

MediaPlayer::~MediaPlayer()
{
    killTimer(timerId);
    setSource(nullptr);
}

void MediaPlayer::setSource(StereoVideoDecoder *decoder)
{
    initSource(decoder);
    emit sourceChanged();
}

double MediaPlayer::duration()
{
    if(decoderThread.isNull() || decoderThread->decoder() == nullptr)
        return 0;
    return decoderThread->decoder()->duration();
}

double MediaPlayer::position()
{
    double d = duration();
    return d > m_clock ? m_clock : d;
}

void MediaPlayer::setPosition(double position)
{
    if(decoderThread != nullptr)
    {
        clearAudioBuf();

        StereoFrame videoFrame = decoderThread->readVideoFrameAt(position);
        if(!videoFrame.isNull())
            emit videoFrameChanged(videoFrame, position);

        decoderThread->setPosition(position);
        sheduler->setPosition(position);
        m_clock = position;
        emit positionChanged(m_clock);
        emit framePositionChanged();
    }
}

void MediaPlayer::setIsPlaying(bool value)
{
    m_isPlaying = value;
    if(sheduler == nullptr)
        return;
    if(m_isPlaying)
    {
        sheduler->resume();
        if(m_audio != nullptr)
            m_audio->resume();
    }
    else
    {
        if(m_audio != nullptr)
            m_audio->suspend();
        sheduler->suspend();
    }
    emit isPlayingChanged(m_isPlaying);
}

void MediaPlayer::setTimeShift(double value)
{
     m_timeShift = value;
     decoderThread->decoder()->setTimeShiftInSeconds(value);
     setPosition(position());
     emit durationChanged();
}

ulong MediaPlayer::framePosition()
{
    auto d = decoderThread->decoder();
    if(d != nullptr && d->videoStream1() != nullptr)
        return round(m_clock * d->videoStream1()->frameRate());
    return -1;
}

void MediaPlayer::setFramePosition(ulong frameNum)
{
    if(frameNum >= 0) {
        auto d  = decoderThread->decoder();
        if(d != nullptr && d->videoStream1() != nullptr)
        {
            auto pos = (double)frameNum / d->videoStream1()->frameRate();
            if(pos <= duration())
                setPosition(pos);
        }
    }
}

void MediaPlayer::initSource(StereoVideoDecoder *decoder)
{
    if(decoderThread != nullptr && sheduler != nullptr)
    {
        disconnect(decoderThread.data(), &DecoderThread::mediaFrameDecoded, sheduler.data(), &FrameSheduler::putMediaFrame);
        disconnect(sheduler.data(), &FrameSheduler::timeToShowFrames, this, &MediaPlayer::showFrames);
        sheduler->stop();
        sheduler.clear();
        decoderThread.clear();
    }

    if(m_audio != nullptr)
    {
        m_audio->stop();
        delete m_audio;
        m_audio = nullptr;
    }

    if(decoder != nullptr)
    {
        decoderThread = QSharedPointer<DecoderThread>(new DecoderThread(decoder));
        sheduler = QSharedPointer<FrameSheduler>(new FrameSheduler());
        connect(decoderThread.data(), &DecoderThread::mediaFrameDecoded, sheduler.data(), &FrameSheduler::putMediaFrame, Qt::ConnectionType::DirectConnection);
        connect(sheduler.data(), &FrameSheduler::timeToShowFrames, this, &MediaPlayer::showFrames, Qt::ConnectionType::DirectConnection);

        if(auto audioStream = decoderThread->decoder()->demux()->bestAudioStream())
        {
            QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
            if(!info.isNull())
            {
                QAudioFormat format = audioStream->qAudioFormat();
                //qDebug() << info.deviceName() << format;

                if (!info.isFormatSupported(format)) {
                    qWarning() << "raw audio format not supported by backend, cannot play audio.";
                    format = info.nearestFormat(format);
                }
                m_audio = new QAudioOutput(info, format);
                m_audioBuf = m_audio->start();
                emit volumeChanged(m_audio->volume());
            }
        }
        sheduler->start();
        decoderThread->start();
        if(!m_isPlaying)
            sheduler->suspend();
    }
    emit durationChanged();
}

void MediaPlayer::clearAudioBuf()
{
    audioBufferMutex.lock();
    m_audioBytes.clear();
    audioBufferMutex.unlock();
}

void MediaPlayer::showFrames(QList<MediaFrame> frames)
{
    if(!isPlaying())
        return;

    foreach(MediaFrame frame, frames)
    {
        m_clock = frame.pts;
        if(!frame.videoframe.isNull())
            emit videoFrameChanged(frame.videoframe, frame.pts);

        if(!frame.audioFrame.isNull())
        {
            auto audioFrame = dynamic_cast<InAudioFrame*>(frame.audioFrame.data());
            if(audioFrame != nullptr)
            {
                audioBufferMutex.lock();
                m_audioBytes.append(audioFrame->data());
                audioBufferMutex.unlock();
                writeAudioBuf();
                m_clock = frame.pts;
            }
        }
    }

    emit positionChanged(m_clock);
    emit framePositionChanged();
}

void MediaPlayer::writeAudioBuf()
{
    audioBufferMutex.lock();
    if(m_audioBytes.length() > 0 && m_audioBuf != nullptr)
    {
        //int length = m_audioBytes.length();
        //qDebug() << "audioFrame" << "clock:" << clock << "audioPts: " << audioPts << "length:" << audioBytes.length() << audio->state();
        int writenBytes = (int)m_audioBuf->write(m_audioBytes);
        if(writenBytes > 0)
        {
            //qDebug() << "audio write" << writenBytes << "from" << length;
            m_audioBytes.remove(0, writenBytes);
        }
    }
    audioBufferMutex.unlock();
}

void MediaPlayer::timerEvent(QTimerEvent *event)
{
    if(event->timerId() == timerId)
        writeAudioBuf();
}
