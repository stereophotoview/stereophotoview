#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

#include <QObject>
#include <QAudio>
#include <QAudioOutput>
#include <QAudioFormat>
#include <QAudioDeviceInfo>
#include <QMap>
#include <QDateTime>
#include <memory>

#include "fileFormats/video/readresult.h"
#include "stereoframe.h"
#include "stereoformat.h"
#include "decoderthread.h"
#include "framesheduler.h"
#include "lib_global.h"
#include <../lib/fileFormats/video/stereovideodecoder.h>

#include <QtConcurrent/QtConcurrent>

using namespace std;
using namespace stereoscopic::fileFormats::video;

namespace stereoscopic::mediaplayer
{
    /**
     * @brief Класс стереоскопического мультимедиа проигрывателя
     */
    class LIBSHARED_EXPORT MediaPlayer : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QString fileName READ fileName NOTIFY sourceChanged)
            Q_PROPERTY(double duration READ duration NOTIFY durationChanged)
            Q_PROPERTY(double position READ position WRITE setPosition NOTIFY positionChanged)
            Q_PROPERTY(double framePosition READ framePosition WRITE setFramePosition NOTIFY framePositionChanged)
            Q_PROPERTY(double volume READ volume WRITE setVolume NOTIFY volumeChanged)
            Q_PROPERTY(bool isPlaying READ isPlaying WRITE setIsPlaying NOTIFY isPlayingChanged)
        public:
            explicit MediaPlayer(QObject *parent = nullptr);
            ~MediaPlayer();

            QString fileName() { return decoderThread ? decoderThread->fileName() : nullptr; }

            void setSource(StereoVideoDecoder* decoder);

            double duration();

            double position();
            void setPosition(double position);

            double volume() { return m_audio != nullptr ? m_audio->volume() : 0; }
            void setVolume(double volume)
            {
                if(m_audio != nullptr)
                    m_audio->setVolume(volume);
                emit volumeChanged(volume);
            }
            bool isPlaying(){ return m_isPlaying; }
            void setIsPlaying(bool value);

            int framesShift() { return m_timeShift; }

            void setTimeShift(double value);

            ulong framePosition();
            void setFramePosition(ulong frameNum);

        signals:
            void sourceChanged();
            void videoFrameChanged(stereoscopic::StereoFrame frame, double pts);
            void durationChanged();
            void positionChanged(double);
            void framePositionChanged();
            void volumeChanged(double);
            void isPlayingChanged(bool);
            void sourceFormatChanged();

        private slots:
            void showFrames(QList<stereoscopic::mediaplayer::MediaFrame> frames);
            void writeAudioBuf();

        protected:
            void timerEvent(QTimerEvent *event);

        private:
            StereoFormat* m_sourceFormat = nullptr;
            int timerId = -1;
            double m_clock = 0;
            QSharedPointer<FrameSheduler> sheduler;
            QByteArray m_audioBytes;
            QSharedPointer<DecoderThread> decoderThread;
            bool m_isPlaying = true;
            QAudioOutput* m_audio = nullptr;
            QIODevice* m_audioBuf = nullptr;
            QMutex audioBufferMutex;
            double m_timeShift = 0;

            void initSource(StereoVideoDecoder *decoder);
            void clearAudioBuf();
    };
}

#endif // MEDIAPLAYER_H
