#include "anaglyphcoder.h"

#include "imagehelper.h"

using namespace stereoscopic::coders;

void AnaglyphCoder::draw(QPainter *painter, QRect boundingRect)
{
    // Размер всего экрана
    QSize boundingSize(boundingRect.width(), boundingRect.height());
    // Прямоугольник, ограничивающий изображение на полном кадре
    QRect leftViewRect = ImageHelper::getEnteredImageRect(leftFullSize(), boundingSize);
    // Прямоугольник, ограничивающий изображение на полном кадре
    QRect rightViewRect = ImageHelper::getEnteredImageRect(rightFullSize(), boundingSize);
    if(leftViewRect.size() != rightViewRect.size())
        return;

    QImage left = leftViewSource().scaled(leftViewRect.size());
    QImage right = rightViewSource().scaled(rightViewRect.size());

    int resWidth = left.width();
    int resHeight = left.height();
    QImage res(resWidth, resHeight, QImage::Format_RGB32);
    for(int y = 0; y < resHeight; y++)
    {
        uchar* resLine = res.scanLine(y);
        uchar* leftLine = left.scanLine(y);
        uchar* rightLine = right.scanLine(y);
        // Копируем правое изображение
        memcpy(resLine, rightLine, (size_t)res.bytesPerLine());
        int offset = 0;
        for(int x = 0; x < resWidth; x++)
        {
            // Берём красный канал с левого изображения
            resLine[offset + 2] = leftLine[offset + 2];// BGRA
            offset += 4;
        }
    }
    painter->drawImage(leftViewRect, res);
}

QRect AnaglyphCoder::translateToSource(QRect rect, QSize size)
{
    QRect enteredRect = ImageHelper::getEnteredImageRect(m_frame->size(), size);
    rect.translate(-enteredRect.topLeft());
    return ImageHelper::translateToSource(rect, enteredRect.size(), m_frame->size());
}
