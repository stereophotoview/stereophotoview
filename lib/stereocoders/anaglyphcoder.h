#ifndef ANAGLYPHCODER_H
#define ANAGLYPHCODER_H

#include "stereocoder.h"

namespace stereoscopic::coders {
    /**
    * @brief Класс для кодирования стерео кадра изображения в красно-синий анаглиф
    */
    class LIBSHARED_EXPORT AnaglyphCoder : public StereoCoder
    {
        public:
            AnaglyphCoder(StereoFrame* frame, StereoFormat* format)
                : StereoCoder(frame, format)
            {

            }

            void draw(QPainter *painter, QRect boundingRect);
            QRect translateToSource(QRect rect, QSize size);
    };
}
#endif // ANAGLYPHCODER_H
