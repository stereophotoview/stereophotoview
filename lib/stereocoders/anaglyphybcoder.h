#ifndef ANAGLYPHYBCODER_H
#define ANAGLYPHYBCODER_H

#include "stereocoder.h"
namespace stereoscopic::coders {
    /**
 * @brief Класс для кодирования стерео кадра изображения в жёлто-голубой анаглиф
 */
    class LIBSHARED_EXPORT AnaglyphYBCoder : public StereoCoder
    {
        public:
            AnaglyphYBCoder(StereoFrame* frame, StereoFormat* format)
                : StereoCoder(frame, format)
            {

            }

            void draw(QPainter *painter, QRect boundingRect);
            QRect translateToSource(QRect rect, QSize size);
    };
}
#endif // ANAGLYPHYBCODER_H
