#include "coderfactory.h"

#include "paircoder.h"
#include "rowinterlacedcoder.h"
#include "colinterlacedcoder.h"
#include "anaglyphcoder.h"
#include "anaglyphybcoder.h"

using namespace stereoscopic;
using namespace stereoscopic::coders;

std::shared_ptr<StereoCoder> CoderFactory::create(StereoFormat *format, StereoFrame *frame)
{
    return std::shared_ptr<StereoCoder>(createImpl(format, frame));
}

StereoCoder *CoderFactory::createImpl(StereoFormat *format, StereoFrame *frame)
{
    switch (format->layout())
    {
        case StereoFormat::Monoscopic:
        case StereoFormat::Horizontal:
        case StereoFormat::AnamorphHorizontal:
        case StereoFormat::Vertical:
        case StereoFormat::AnamorphVertical:
            return new PairCoder(frame, format);
        case StereoFormat::RowInterlaced:
            return new RowInterlacedCoder(frame, format);
        case StereoFormat::ColumnInterlaced:
            return new ColInterlacedCoder(frame, format);
        case StereoFormat::AnaglyphRC:
            return new AnaglyphCoder(frame, format);
        case StereoFormat::AnaglyphYB:
            return new AnaglyphYBCoder(frame, format);
        case StereoFormat::Separate:
        case StereoFormat::Default:
            return nullptr;
    }
    return nullptr;
}
