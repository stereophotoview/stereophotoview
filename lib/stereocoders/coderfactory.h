#ifndef CODERFACTORY_H
#define CODERFACTORY_H

#include "stereocoder.h"
#include "stereoformat.h"
#include "stereoframe.h"
#include <memory>

namespace stereoscopic::coders {
    /**
 * @brief Класс фабрики стерео кодеров
 */
    class LIBSHARED_EXPORT CoderFactory
    {
        public:
            /**
         * @brief Создаёт кодер требуемого стерео формата для указанного кадра
         * @param format Стерео  формат
         * @param frame Исходный стерео кадр
         * @return Умный указатель на созданный стерео кодер
         */
            static std::shared_ptr<StereoCoder> create(StereoFormat* format, StereoFrame* frame);
        private:
            static StereoCoder* createImpl(StereoFormat* format, StereoFrame* frame);
    };
}
#endif // CODERFACTORY_H
