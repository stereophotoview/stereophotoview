#include "colinterlacedcoder.h"

#include "imagehelper.h"

using namespace stereoscopic::coders;

void ColInterlacedCoder::draw(QPainter *painter, QRect boundingRect)
{
    QSize boundingSize = QSize(boundingRect.width(), boundingRect.height());

    QImage leftImage = leftViewSource();
    // Прямоугольник, ограничивающий левое изображение на полном кадре
    QRect leftViewRect = ImageHelper::getEnteredImageRect(leftFullSize(), boundingSize);
    // Масштабируем изображения до половинного размера кадра
    QSize leftHalfSize(leftViewRect.width() / 2, leftViewRect.height());
    QImage scaledLeft = leftImage.scaled(leftHalfSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    QImage rightImage = rightViewSource();
    // Прямоугольник, ограничивающий правое изображение на полном кадре
    QRect rightViewRect = ImageHelper::getEnteredImageRect(rightFullSize(), boundingSize);
    // Масштабируем изображения до половинного размера кадра
    QSize rightHalfSize(rightViewRect.width() / 2, rightViewRect.height());
    QImage scaledRight = rightImage.scaled(rightHalfSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    bool even = (leftViewRect.left() + boundingRect.x()) % 2 == 0 ? false : true;
    drawImageColumnInterlaced(painter, leftViewRect, scaledLeft, even);
    drawImageColumnInterlaced(painter, rightViewRect, scaledRight, !even);
}

QRect ColInterlacedCoder::translateToSource(QRect rect, QSize size)
{
    QRect enteredRect = ImageHelper::getEnteredImageRect(m_frame->size(), size);
    rect.translate(-enteredRect.topLeft());
    return ImageHelper::translateToSource(rect, enteredRect.size(), m_frame->size());
}

void ColInterlacedCoder::drawImageColumnInterlaced(QPainter *painter, QRect &viewRect, QImage &pixmap, bool even)
{
    // even == true: рисовать в чётных строках, начиная со второй
    int rowIndex = viewRect.left() + (even ? 1 : 0);
    for(int i = 0; i < pixmap.width(); i++)
    {
        //painter->drawImage(rowIndex, viewRect.top(), 1, viewRect.height(), pixmap, i, 0, 1, pixmap.height());
        painter->drawImage(rowIndex, viewRect.top(), pixmap, i, 0, 1, pixmap.height());
        rowIndex += 2;
    }
}
