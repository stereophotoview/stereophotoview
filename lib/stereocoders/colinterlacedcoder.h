#ifndef COLINTERLACEDCODER_H
#define COLINTERLACEDCODER_H

#include "stereocoder.h"

namespace stereoscopic::coders {
    /**
 * @brief Класс для кодирования стерео кадра изображения в формат с чередованием столбцов
 */
    class LIBSHARED_EXPORT ColInterlacedCoder : public StereoCoder
    {
        public:
            ColInterlacedCoder(StereoFrame* frame, StereoFormat* format)
                : StereoCoder(frame, format)
            {

            }

            void draw(QPainter *painter, QRect boundingRect);
            QRect translateToSource(QRect rect, QSize size);

        private:

            void drawImageColumnInterlaced(QPainter *painter, QRect &viewRect, QImage &pixmap, bool even);
    };
}
#endif // COLINTERLACEDCODER_H
