#include "paircoder.h"

#include "imagehelper.h"

using namespace stereoscopic::coders;

void PairCoder::draw(QPainter *painter, QRect boundingRect)
{
    QPair<QRect, QRect> rects = getViewrects(boundingRect.size());
    painter->setRenderHint(QPainter::SmoothPixmapTransform, true);
    if(!rects.first.isNull())
    {
        QImage image = leftViewSource();
        painter->drawImage(rects.first, image);
    }
    if(!rects.second.isNull())
    {
        QImage image = rightViewSource();
        painter->drawImage(rects.second, image);
    }
}

QRect PairCoder::translateToSource(QRect rect, QSize size)
{
    QPair<QRect, QRect> rects = getViewrects(size);
    QRect r1 = rects.first.intersected(rect).translated(-rects.first.topLeft());
    QRect r2 = rects.second.intersected(rect).translated(-rects.second.topLeft());
    if(r1.isValid() && r1.width() * r1.height() >= r2.width() * r2.height())
        return ImageHelper::translateToSource(r1, rects.first.size(), leftFullSize());
    if(r2.isValid())
        return ImageHelper::translateToSource(r2, rects.second.size(), rightFullSize());
    return QRect(0, 0, -1, -1);
}

QPair<QRect, QRect> PairCoder::getViewrects(QSize frameSize)
{
    QRect rect1, rect2;
    QSize leftSize = m_format->codingSize(leftFullSize());
    QSize rightSize = m_format->codingSize(rightFullSize());
    QSize halfFrame;
    switch (m_format->layout())
    {
        case StereoFormat::AnaglyphRC:
        case StereoFormat::AnaglyphYB:
        case StereoFormat::RowInterlaced:
        case StereoFormat::ColumnInterlaced:
        case StereoFormat::Monoscopic:
            return QPair<QRect, QRect>(ImageHelper::getEnteredImageRect(leftSize, frameSize), QRect());
        case StereoFormat::Separate:
            return QPair<QRect, QRect>(QRect(), QRect());
        case StereoFormat::Horizontal:
        case StereoFormat::AnamorphHorizontal:
        {
            int width = (frameSize.width() - m_format->separation()) / 2;
            halfFrame = QSize(width, frameSize.height());
            break;
        }
        case StereoFormat::Vertical:
        case StereoFormat::AnamorphVertical:
        {
            int height = (frameSize.height() - m_format->separation()) / 2;
            halfFrame = QSize(frameSize.width(), height);
            break;
        }
    };
    rect1 = ImageHelper::getEnteredImageRect(leftSize, halfFrame);
    rect2 = ImageHelper::getEnteredImageRect(rightSize, halfFrame);
    rect2.moveTopLeft(QPoint(frameSize.width() - halfFrame.width() + rect2.left(),
                             frameSize.height() - halfFrame.height() + rect2.top()));

    return QPair<QRect, QRect>(rect1, rect2);
}
