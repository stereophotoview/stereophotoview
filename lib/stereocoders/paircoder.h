#ifndef PAIRCODER_H
#define PAIRCODER_H

#include "stereocoder.h"

#include <QPair>
#include <QRect>

namespace stereoscopic::coders {
    /**
 * @brief Класс для кодирования стерео кадра изображения в формат с положением ракурсов рядом на одном изображении
 * Тип расположения ракурсов определеятеся стерео форматом, переданном в конструкторе
 */
    class LIBSHARED_EXPORT PairCoder : public StereoCoder
    {
        public:
            PairCoder(StereoFrame* frame, StereoFormat* format)
                : StereoCoder(frame, format)
            {

            }

            void draw(QPainter *painter, QRect boundingRect);
            QRect translateToSource(QRect rect, QSize size);

        private:
            QPair<QRect,QRect> getViewrects(QSize frameSize);
    };
}
#endif // PAIRCODER_H
