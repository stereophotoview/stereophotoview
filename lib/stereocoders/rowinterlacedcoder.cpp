#include "rowinterlacedcoder.h"

#include "imagehelper.h"

using namespace stereoscopic::coders;

void RowInterlacedCoder::draw(QPainter *painter, QRect boundingRect)
{
    QSize boundingSize = QSize(boundingRect.width(), boundingRect.height());

    QImage leftImage = leftViewSource();
    // Прямоугольник, ограничивающий левое изображение на полном кадре
    QRect leftViewRect = ImageHelper::getEnteredImageRect(leftFullSize(), boundingSize);
    // Масштабируем изображения до половинного размера кадра
    QSize leftHalfSize(leftViewRect.width(), leftViewRect.height() / 2);
    QImage scaledLeft = leftImage.scaled(leftHalfSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    QImage rightImage = rightViewSource();
    // Прямоугольник, ограничивающий правое изображение на полном кадре
    QRect rightViewRect = ImageHelper::getEnteredImageRect(rightFullSize(), boundingSize);
    // Масштабируем изображения до половинного размера кадра
    QSize rightHalfSize(rightViewRect.width(), rightViewRect.height() / 2);
    QImage scaledRight = rightImage.scaled(rightHalfSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    bool even = (leftViewRect.top() + boundingRect.y()) % 2 == 0 ? false : true;
    drawImageRowInterlaced(painter, leftViewRect, scaledLeft, even);
    drawImageRowInterlaced(painter, rightViewRect, scaledRight, !even);
}

QRect RowInterlacedCoder::translateToSource(QRect rect, QSize size)
{
    QRect enteredRect = ImageHelper::getEnteredImageRect(m_frame->size(), size);
    rect.translate(-enteredRect.topLeft());
    return ImageHelper::translateToSource(rect, enteredRect.size(), m_frame->size());
}

void RowInterlacedCoder::drawImageRowInterlaced(QPainter *painter, QRect &viewRect, QImage &pixmap, bool even)
{
    // even == true: рисовать в чётных строках, начиная со второй
    int lineIndex = viewRect.top() + (even ? 1 : 0);
    for(int i = 0; i < pixmap.height(); i++)
    {
        painter->drawImage(viewRect.left(), lineIndex, pixmap, 0, i, pixmap.width(), 1);
        lineIndex += 2;
    }
}
