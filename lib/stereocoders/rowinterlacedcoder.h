#ifndef ROWINTERLACEDCODER_H
#define ROWINTERLACEDCODER_H

#include "stereocoder.h"
namespace stereoscopic::coders {
    /**
 * @brief Класс для кодирования стерео кадра изображения в формат с чередованием строк
 */
    class LIBSHARED_EXPORT RowInterlacedCoder : public StereoCoder
    {
        public:
            RowInterlacedCoder(StereoFrame* frame, StereoFormat* format)
                : StereoCoder(frame, format)
            {

            }

            void draw(QPainter *painter, QRect boundingRect);
            QRect translateToSource(QRect rect, QSize size);

        private:
            void drawImageRowInterlaced(QPainter *painter, QRect &viewRect, QImage &pixmap, bool even);
    };
}
#endif // ROWINTERLACEDCODER_H
