#include "stereocoder.h"

#include "stereodecoders/decoderfactory.h"

using namespace stereoscopic::coders;

StereoCoder::~StereoCoder()
{

}

QImage StereoCoder::createImage(QSize targetSize)
{
    if(targetSize.isEmpty())
    {
        targetSize = m_format->fullFrameSize(m_frame->fullLeftSize(), m_frame->fullRightSize());
    }
    QImage res(targetSize, QImage::Format_RGB32);
    res.fill(Qt::black);
    QPainter painter(&res);
    draw(&painter, QRect(QPoint(0, 0), targetSize));
    return res;
}

QImage StereoCoder::leftViewSource()
{
    auto decoderPtr = m_frame->createDecoder();
    StereoDecoder* decoder = decoderPtr.get();
    return m_format->leftFirst() ? decoder->leftViewSource() : decoder->rightViewSource();
}

QImage StereoCoder::rightViewSource()
{
    auto decoderPtr = m_frame->createDecoder();
    StereoDecoder* decoder = decoderPtr.get();
    return !m_format->leftFirst() ? decoder->leftViewSource() : decoder->rightViewSource();
}

QSize StereoCoder::leftFullSize()
{
    auto decoderPtr = m_frame->createDecoder();
    StereoDecoder* decoder = decoderPtr.get();
    return m_format->leftFirst() ? decoder->leftFullSize() : decoder->rightFullSize();
}

QSize StereoCoder::rightFullSize()
{
    auto decoderPtr = m_frame->createDecoder();
    StereoDecoder* decoder = decoderPtr.get();
    return !m_format->leftFirst() ? decoder->leftFullSize() : decoder->rightFullSize();
}
