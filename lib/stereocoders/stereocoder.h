#ifndef STEREOCODER_H
#define STEREOCODER_H

#include "stereoframe.h"
#include <QPainter>
#include <QRect>
#include <QSize>

namespace stereoscopic::coders {

    /**
 * @brief Базовый класс для кодирования стерео кадра
 */
    class LIBSHARED_EXPORT StereoCoder
    {
        public:
            /**
         @brief Создаёт кодер
         @param[in] frame исходный кадр
         @param[in] format формат кодирования
         */
            StereoCoder(StereoFrame* frame, StereoFormat* format)
            {
                m_frame = frame;
                m_format = format;
            }

            virtual ~StereoCoder();

            /**
         @brief Рисует кадр
         @param[in] painter рисователь
         @param[in] boundingRect прямоугольная область экрана, на которой должен быть нарисован кадр.
         */
            virtual void draw(QPainter *painter, QRect boundingRect) = 0;

            /**
         @brief Кодирует кдар
         @param targetSize Целевой размер изображения (опционально)
         */
            QImage createImage(QSize targetSize = QSize());

            /**
         @brief Переводит прямоугольник из координат отображаемого кадра в координаты исходного изображения
         @param rect Прямоугольник
         @param boundingSize Отображаемый размер кадра
         */
            virtual QRect translateToSource(QRect rect, QSize boundingSize) = 0;

        protected:
            StereoFrame* m_frame = nullptr;
            StereoFormat* m_format;

            QImage leftViewSource();
            QImage rightViewSource();
            QSize leftFullSize();
            QSize rightFullSize();
    };
}
#endif // STEREOCODER_H
