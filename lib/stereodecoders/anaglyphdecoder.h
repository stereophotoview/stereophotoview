#ifndef ANAGLYPHDECODER_H
#define ANAGLYPHDECODER_H

#include "stereodecoder.h"

namespace stereoscopic::decoders {
    /**
     * @brief Класс для декодирования стерео кадра изображения из формата анаглиф
     */
    class LIBSHARED_EXPORT AnaglyphDecoder : public StereoDecoder
    {
        public:
            AnaglyphDecoder(StereoFormat* format, QPointF offset, QImage* source1, QImage* source2)
                : StereoDecoder(format, offset, source1, source2)
            {
            }

        protected:
            QSize fullSize(bool left);
            QImage viewSource(bool left);
    };
}
#endif // ANAGLYPHDECODER_H
