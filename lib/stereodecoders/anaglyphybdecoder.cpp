#include "anaglyphybdecoder.h"

using namespace stereoscopic::decoders;

QSize AnaglyphYBDecoder::fullSize(bool)
{
    return viewRectWithOffset(m_source1->size(), m_offset).size();
}

QImage AnaglyphYBDecoder::viewSource(bool left)
{
    QImage source = left ? leftViewWithOffset(m_source1) : rightViewWithOffset(m_source1);
    QImage res(source.width(), source.height(), QImage::Format_RGB32);

    for(int y = 0; y < source.height(); y++)
    {
        uchar* resLine = res.scanLine(y);
        uchar* srcLine = source.scanLine(y);
        int offset = 0;
        for(int x = 0; x < source.width(); x++)
        {
            // Левое - только синий канал
            // Правое - только красный и зелёный
            // BGRA
            resLine[offset] = 0;
            resLine[offset + 1] = 0;
            resLine[offset + 2] = 0;
            resLine[offset + 3] = 255;
            if(left)
            {
                resLine[offset] = srcLine[offset];
            }
            else
            {
                resLine[offset + 1] = srcLine[offset + 1];
                resLine[offset + 2] = srcLine[offset + 2];
            }
            offset += 4;
        }
    }
    return res;
}
