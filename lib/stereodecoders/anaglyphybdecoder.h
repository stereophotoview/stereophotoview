#ifndef ANAGLYPHYBDECODER_H
#define ANAGLYPHYBDECODER_H

#include "stereodecoder.h"

namespace stereoscopic::decoders {
    /**
     * @brief Класс для декодирования стерео кадра изображения из жёлто-голубого анаглифа
     */
    class LIBSHARED_EXPORT AnaglyphYBDecoder : public StereoDecoder
    {
        public:
            AnaglyphYBDecoder(StereoFormat* format, QPointF offset, QImage* source1, QImage* source2)
                : StereoDecoder(format, offset, source1, source2)
            {
            }

        protected:
            QSize fullSize(bool left);
            QImage viewSource(bool left);
    };
}
#endif // ANAGLYPHYBDECODER_H
