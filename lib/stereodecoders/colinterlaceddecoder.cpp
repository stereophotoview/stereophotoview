#include "colinterlaceddecoder.h"

using namespace stereoscopic::decoders;

QSize ColInterlacedDecoder::fullSize(bool)
{
    return viewRectWithOffset(m_source1->size(), m_offset).size();
}

QImage ColInterlacedDecoder::viewSource(bool left)
{
    QImage source = left ? leftViewWithOffset(m_source1) : rightViewWithOffset(m_source1);
    QImage res(source.width() / 2, source.height(), source.format());
    int bytesPerPixel = res.depth() / 8;
    int offset = left ? 0 : bytesPerPixel;
    for(int y = 0; y < res.height(); y++)
    {
        uchar* srcPoint = source.scanLine(y) + offset;
        uchar* resPoint = res.scanLine(y);
        for(int x = 0; x < res.width(); x++)
        {
            memcpy(resPoint, srcPoint, bytesPerPixel);
            srcPoint += bytesPerPixel * 2;
            resPoint += bytesPerPixel;
        }
    }
    return res;
}
