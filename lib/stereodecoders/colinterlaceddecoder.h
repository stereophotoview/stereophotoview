#ifndef COLINTERLACEDDECODER_H
#define COLINTERLACEDDECODER_H

#include "stereodecoder.h"
namespace stereoscopic::decoders {
    /**
     * @brief Класс для декодирования стерео кадра изображения из формата с чередованием столбцов
     */
    class LIBSHARED_EXPORT ColInterlacedDecoder : public StereoDecoder
    {
        public:
            ColInterlacedDecoder(StereoFormat* format, QPointF offset, QImage* source1, QImage* source2)
                : StereoDecoder(format, offset, source1, source2)
            {
            }

        protected:
            QSize fullSize(bool left);
            QImage viewSource(bool left);
    };
}
#endif // COLINTERLACEDDECODER_H
