#include "decoderfactory.h"

#include "stereodecoders/pairdecoder.h"
#include "stereodecoders/separatedecoder.h"
#include "stereodecoders/rowinterlaceddecoder.h"
#include "stereodecoders/colinterlaceddecoder.h"
#include "stereodecoders/anaglyphdecoder.h"
#include "stereodecoders/anaglyphybdecoder.h"

using namespace stereoscopic;
using namespace stereoscopic::decoders;

std::shared_ptr<StereoDecoder> DecoderFactory::create(StereoFormat *format, QImage *source1, QImage *source2, QPointF offset)
{
    return std::shared_ptr<StereoDecoder>(createImpl(format, source1, source2, offset));
}

StereoDecoder* DecoderFactory::createImpl(StereoFormat *format, QImage *source1, QImage *source2, QPointF offset)
{
    switch (format->layout())
    {
        case StereoFormat::Monoscopic:
        case StereoFormat::Horizontal:
        case StereoFormat::AnamorphHorizontal:
        case StereoFormat::Vertical:
        case StereoFormat::AnamorphVertical:
            return new PairDecoder(format, offset, source1, source2);
        case StereoFormat::RowInterlaced:
            return new RowInterlacedDecoder(format, offset, source1, source2);
        case StereoFormat::ColumnInterlaced:
            return new ColInterlacedDecoder(format, offset, source1, source2);
        case StereoFormat::AnaglyphRC:
            return new AnaglyphDecoder(format, offset, source1, source2);
        case StereoFormat::AnaglyphYB:
            return new AnaglyphYBDecoder(format, offset, source1, source2);
        case StereoFormat::Separate:
            return new SeparateDecoder(format, offset, source1, source2);
        default:
            return new PairDecoder(format, offset, source1, source2);
    }
}
