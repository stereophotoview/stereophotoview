#ifndef DECODERFACTORY_H
#define DECODERFACTORY_H

#include "stereodecoders/stereodecoder.h"
#include "stereoformat.h"
#include <QImage>
#include <memory>
namespace stereoscopic::decoders {
    /**
     * @brief Класс фабрики дектодеров стерео изображения
     */
    class LIBSHARED_EXPORT DecoderFactory
    {
        public:
            /**
         * @brief Создаёт декодер заданного формата из заданного источника
         * @param format Стерео формат источника
         * @param source1 Первый источник
         * @param source2 Второй источник (опционально)
         * @param offset Относительный сдвиг ракурсов (параллакс), который нужно сразу применить
         * @return Умный указатель на декодер
         */
            static std::shared_ptr<StereoDecoder> create(StereoFormat* format, QImage* source1, QImage* source2, QPointF offset);
        private:
            static StereoDecoder* createImpl(StereoFormat* format, QImage* source1, QImage* source2, QPointF offset);
    };
}
#endif // DECODERFACTORY_H
