#include "pairdecoder.h"

#include "imagehelper.h"

using namespace stereoscopic::decoders;

QSize PairDecoder::fullSize(bool left)
{
    QPair<QRect, QRect> rects = getViewSourceRects();
    if(left)
        return m_format->decodingSize(rects.first.size());
    else
        return m_format->decodingSize(rects.second.size());
}

QPair<QRect, QRect> PairDecoder::getViewSourceRects()
{
    QRect rect1, rect2;
    switch (m_format->layout())
    {
        case StereoFormat::AnaglyphRC:
        case StereoFormat::AnaglyphYB:
        case StereoFormat::RowInterlaced:
        case StereoFormat::ColumnInterlaced:
        case StereoFormat::Monoscopic:
        {
            rect2 = rect1 = QRect(QPoint(0, 0), m_source1->size());
            break;
        }
        case StereoFormat::Horizontal:
        case StereoFormat::AnamorphHorizontal:
        {
            int width = (m_source1->width() - m_format->separation()) / 2;
            rect1 = QRect(0, 0, width, m_source1->height());
            rect2 = QRect(width + m_format->separation(), 0, width, m_source1->height());
            break;
        }
        case StereoFormat::Vertical:
        case StereoFormat::AnamorphVertical:
        {
            int height = (m_source1->height() - m_format->separation()) / 2;
            rect1 = QRect(0, 0, m_source1->width(), height);
            rect2 = QRect(0, height + m_format->separation(), m_source1->width(), height);
            break;
        }
        case StereoFormat::Separate:
            break;
    };
    rect1 = applyOffset(rect1, m_offset, m_format->leftFirst());
    rect2 = applyOffset(rect2, m_offset, !m_format->leftFirst());
    return QPair<QRect, QRect>(rect1, rect2);
}

QImage PairDecoder::viewSource(bool left)
{
    QPair<QRect, QRect> rects = getViewSourceRects();
    return ImageHelper::rotated(
                m_source1->copy(left ? rects.first : rects.second),
                getRotationAngle(left ? m_format->leftRotation() : m_format->rightRotation()));
}

QRect PairDecoder::applyOffset(QRect rect, QPointF offset, bool isLeft)
{
    if(!isLeft)
        offset = QPointF(-offset.x(), -offset.y());
    QRect innerRect = viewRectWithOffset(rect.size(), offset);
    return QRect(rect.x() + innerRect.x(), rect.y() + innerRect.y(), innerRect.width(), innerRect.height());
}
