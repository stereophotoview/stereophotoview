#ifndef PAIRDECODER_H
#define PAIRDECODER_H

#include "stereodecoder.h"
namespace stereoscopic::decoders {
    /**
     * @brief Класс для декодирования стерео кадра  формате с расположением ракурсов рядом на одном изображении
     */
    class LIBSHARED_EXPORT PairDecoder : public StereoDecoder
    {
        public:
            PairDecoder(StereoFormat* format, QPointF offset, QImage* source1, QImage* source2)
                : StereoDecoder(format, offset, source1, source2)
            {

            }
        protected:
            QSize fullSize(bool left);
            QPair<QRect, QRect> getViewSourceRects();
            QImage viewSource(bool left);
            QRect applyOffset(QRect rect, QPointF offset, bool isLeft);
    };
}
#endif // PAIRDECODER_H
