#include "rowinterlaceddecoder.h"
using namespace stereoscopic::decoders;
QSize RowInterlacedDecoder::fullSize(bool)
{
    return viewRectWithOffset(m_source1->size(), m_offset).size();
}

QImage RowInterlacedDecoder::viewSource(bool left)
{
    QImage source = left ? leftViewWithOffset(m_source1) : rightViewWithOffset(m_source1);
    QImage res(source.width(), source.height() / 2, source.format());
    int k = 0;
    for(int i = left ? 0 : 1; i < source.height(); i += 2)
    {
        if(k < res.height())
        {
            uchar* srcLine = source.scanLine(i);
            uchar* resLine = res.scanLine(k);
            memcpy(resLine, srcLine, (size_t)res.bytesPerLine());
            k++;
        }
    }
    return res;
}
