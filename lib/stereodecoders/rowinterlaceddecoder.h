#ifndef ROWINTERLACEDDECODER_H
#define ROWINTERLACEDDECODER_H

#include "stereodecoder.h"
namespace stereoscopic::decoders {
    /**
     * @brief Класс для декодирования стерео кадра изображения из формата с чередованием строк
     */
    class LIBSHARED_EXPORT RowInterlacedDecoder : public StereoDecoder
    {
        public:
            RowInterlacedDecoder(StereoFormat* format, QPointF offset, QImage* source1, QImage* source2)
                : StereoDecoder(format, offset, source1, source2)
            {
            }

        protected:
            QSize fullSize(bool left);
            QImage viewSource(bool left);
    };
}
#endif // ROWINTERLACEDDECODER_H
