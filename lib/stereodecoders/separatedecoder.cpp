#include "separatedecoder.h"
#include "imagehelper.h"

#include <QImage>

using namespace stereoscopic::decoders;

QSize SeparateDecoder::fullSize(bool left)
{
    return viewRectWithOffset(left ? m_source1->size() : m_source2->size(), m_offset).size();
}

QImage SeparateDecoder::viewSource(bool left)
{
    if(left)
        return leftViewWithOffset(m_source1);
    else
        return rightViewWithOffset(m_source2);
}
