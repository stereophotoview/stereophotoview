#ifndef SEPARATEDECODER_H
#define SEPARATEDECODER_H

#include "stereodecoder.h"
#include "stereoformat.h"

using namespace stereoscopic;

namespace stereoscopic::decoders {
    /**
     * @brief Класс для декодирования стерео кадра изображения из формата с отдельными источниками
     */
    class LIBSHARED_EXPORT SeparateDecoder : public StereoDecoder
    {
        public:
            SeparateDecoder(StereoFormat* format, QPointF offset, QImage* source1, QImage* source2)
                : StereoDecoder(format, offset, source1, source2)
            {

            }

        protected:
            QSize fullSize(bool left);
            QImage viewSource(bool left);
    };
}
#endif // SEPARATEDECODER_H
