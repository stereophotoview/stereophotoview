#include "stereodecoder.h"
#include <math.h>

#include "imagehelper.h"
using namespace stereoscopic;
using namespace stereoscopic::decoders;

QSize StereoDecoder::leftFullSize()
{
    return fullSize(m_format->leftFirst());
}

QSize StereoDecoder::rightFullSize()
{
    return fullSize(!m_format->leftFirst());
}

QImage StereoDecoder::leftViewSource()
{
    return viewSource(m_format->leftFirst());
}

QImage StereoDecoder::rightViewSource()
{
    return viewSource(!m_format->leftFirst());
}

QImage StereoDecoder::leftFullView()
{
    return fullView(m_format->leftFirst());
}

QImage StereoDecoder::rightFullView()
{
    return fullView(!m_format->leftFirst());
}

QImage StereoDecoder::fullView(bool left)
{
    QImage src = viewSource(left);
    if(m_format->halfWidth() || m_format->halfHeight())
        return src.scaled(src.width() * m_format->widthFactor(), src.height() * m_format->heightFactor());
    else
        return src;
}

QImage StereoDecoder::leftViewWithOffset(QImage *leftSrc)
{
    if(leftSrc->isNull())
        return QImage(*leftSrc);
    QRect rect = viewRectWithOffset(leftSrc->size(), m_offset);
    return ImageHelper::rotated(*leftSrc, getRotationAngle(m_format->leftRotation())).copy(rect);
}

QImage StereoDecoder::rightViewWithOffset(QImage *rightSrc)
{
    if(rightSrc->isNull())
        return QImage(*rightSrc);
    QRect rect = viewRectWithOffset(rightSrc->size(), QPointF(-m_offset.x(), -m_offset.y()));
    return ImageHelper::rotated(*rightSrc, getRotationAngle(m_format->rightRotation())).copy(rect);
}

QRect StereoDecoder::viewRectWithOffset(QSize size, QPointF offset)
{
    int width = (int)round((1.0 - fabs(offset.x())) * size.width());
    int left = offset.x() < 0 ? size.width() - width : 0;
    int height = (int)round((1.0 - fabs(offset.y())) * size.height());
    int top = offset.y() < 0 ? size.height() - height : 0;
    return QRect(QPoint(left, top), QSize(width, height));
}

int StereoDecoder::getRotationAngle(StereoFormat::AngleRotation rotation)
{
    // rotation describes the position of the camera during shooting, therefore, to display, rotate the image back
    switch (rotation)
    {
        case StereoFormat::None: return 0;
        case StereoFormat::Left: return 90;
        case StereoFormat::Right: return -90;
        case StereoFormat::UpsideDown: return 180;
    }
    return 0;
}
