#ifndef STEREODECODER_H
#define STEREODECODER_H

#include "../stereoformat.h"
#include <QSize>
#include <QImage>
namespace stereoscopic::decoders {
    /**
     * @brief Базовый класс для декодирования стерео кадра изображения
     */
    class LIBSHARED_EXPORT StereoDecoder
    {
        public:
            /**
             * @brief Создаёт декодер
             * @param format Исходный стерео формат
             * @param offset Относительный сдвиг ракурсов (параллакс), который нужно сразу применить
             * @param source1 Первый источник
             * @param source2 Второй источник (опционально)
             */
            StereoDecoder(StereoFormat *format, QPointF offset, QImage* source1, QImage* source2)
            {
                m_format = format;
                m_offset = offset;
                m_source1 = source1;
                m_source2 = source2;
            }

            virtual ~StereoDecoder()
            {
            }

            /// Полный размер левого кадра
            QSize leftFullSize();

            /// Полный размер правого кадра
            QSize rightFullSize();

            /// Результирующий левый кадр
            QImage leftFullView();

            /// Результирующий правый кадр
            QImage rightFullView();

            /// Исходное левое изображение
            QImage leftViewSource();

            /// Исходное правое изображение
            QImage rightViewSource();

        protected:
            /// Формат исходника
            StereoFormat* m_format = nullptr;
            /// Первое исходное изображение
            QImage* m_source1 = nullptr;
            /// Второе исходное изображение
            QImage* m_source2 = nullptr;
            /// Относительный сдвиг ракурсов (параллакс)
            QPointF m_offset;
            virtual QSize fullSize(bool left) = 0;
            virtual QImage viewSource(bool left) = 0;
            virtual QImage fullView(bool left);

            QImage leftViewWithOffset(QImage *leftSrc);
            QImage rightViewWithOffset(QImage *rightSrc);
            QRect viewRectWithOffset(QSize size, QPointF offset);
            int getRotationAngle(StereoFormat::AngleRotation rotation);
    };
}
#endif // STEREODECODER_H
