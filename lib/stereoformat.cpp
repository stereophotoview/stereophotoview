#include "stereoformat.h"
#include <math.h>

using namespace stereoscopic;

StereoFormat::StereoFormat(QObject *parent)
    : QObject(parent)
{
    qRegisterMetaType<Layout>("StereoFormat::Layout");
}

StereoFormat::StereoFormat(const StereoFormat &src)
    : QObject(src.parent())
{
    copyMembersFrom(src);
}

StereoFormat::StereoFormat(StereoFormat::Layout layout, QObject *parent)
    : QObject(parent)
{
    m_layout = layout;
}

StereoFormat StereoFormat::StereoFormat::operator=(const StereoFormat &src)
{
    copyMembersFrom(src);
    return *this;
}

void StereoFormat::copyMembersFrom(const StereoFormat &src)
{
    m_layout = src.m_layout;
    m_leftFirst = src.m_leftFirst;
    m_separation = src.m_separation;
    m_isLoaded = src.m_isLoaded;
    m_leftRotation = src.m_leftRotation;
    m_rightRotation = src.m_rightRotation;
}

bool StereoFormat::operator==(const StereoFormat &f2)
{
    return m_layout == f2.m_layout
            && m_leftFirst == f2.m_leftFirst
            && m_leftRotation == f2.m_leftRotation
            && m_rightRotation == f2.m_rightRotation;
}

QSize StereoFormat::fullFrameSize(QSize leftSize, QSize rightSize)
{
    switch(m_layout)
    {
        case StereoFormat::Monoscopic:
            return m_leftFirst ? leftSize : rightSize;
        case StereoFormat::Horizontal:
            return QSize(leftSize.width() + rightSize.width(), std::max(leftSize.height(), rightSize.height()));
        case StereoFormat::AnamorphHorizontal:
        case StereoFormat::ColumnInterlaced:
            return QSize((leftSize.width() + rightSize.width()) / 2, std::max(leftSize.height(), rightSize.height()));
        case StereoFormat::Vertical:
            return QSize(std::max(leftSize.width(), rightSize.width()), leftSize.height() + rightSize.height());
        case StereoFormat::AnamorphVertical:
        case StereoFormat::RowInterlaced:
            return QSize(std::max(leftSize.width(), rightSize.width()), (leftSize.height() + rightSize.height()) / 2);
        case StereoFormat::AnaglyphRC:
        case StereoFormat::AnaglyphYB:
            return QSize(std::max(leftSize.width(), rightSize.width()), std::max(leftSize.height(), rightSize.height()));
        case StereoFormat::Separate:
        case StereoFormat::Default:
            return leftSize;
    }
    return QSize();
}

void StereoFormat::copyFrom(StereoFormat *src)
{
    // we do not use copyMembersFrom to notify about changes
    setLayout(src->layout());
    setLeftFirst(src->leftFirst());
    setSeparation(src->separation());
    setIsLoaded(src->isLoaded());
    setLeftRotation(src->leftRotation());
    setRightRotation(src->rightRotation());
}

