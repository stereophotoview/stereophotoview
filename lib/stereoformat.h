#ifndef STEREOFORMAT_H
#define STEREOFORMAT_H

#include <QObject>
#include <QMap>
#include <QSize>

#include "lib_global.h"

#include <QDebug>

namespace stereoscopic {
    /**
     * @brief Класс для представления стерео формата
     */
    class LIBSHARED_EXPORT StereoFormat : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(Layout layout READ layout WRITE setLayout NOTIFY layoutChanged)
            Q_PROPERTY(int separation READ separation WRITE setSeparation NOTIFY separationChanged)
            Q_PROPERTY(bool leftFirst READ leftFirst WRITE setLeftFirst NOTIFY leftFirstChanged)
            Q_PROPERTY(QString displayName READ displayName NOTIFY displayNameChanged)
            Q_PROPERTY(bool isLoaded READ isLoaded NOTIFY isLoadedChanged)
            Q_PROPERTY(AngleRotation leftRotation READ leftRotation WRITE setLeftRotation NOTIFY leftRotationChanged)
            Q_PROPERTY(AngleRotation rightRotation READ rightRotation WRITE setRightRotation NOTIFY rightRotationChanged)
            Q_PROPERTY(int widthFactor READ widthFactor NOTIFY changed)
            Q_PROPERTY(int heightFactor READ heightFactor NOTIFY changed)

        public:
            enum Layout {
                Default = -1,
                RowInterlaced = 0,
                ColumnInterlaced = 1,
                Horizontal = 2,
                AnamorphHorizontal = 3,
                Vertical = 4,
                AnamorphVertical = 5,
                AnaglyphRC = 6,
                AnaglyphYB = 7,
                Separate = 8,
                Monoscopic = 9
            };
            Q_ENUM(Layout)

            enum AngleRotation {
                None, Left, Right, UpsideDown
            };
            Q_ENUM(AngleRotation)

            StereoFormat(QObject* parent = nullptr);
            StereoFormat(const StereoFormat&);
            StereoFormat(Layout layout, QObject* parent = nullptr);

            StereoFormat operator=(const StereoFormat&);

            Layout layout() const { return m_layout; }
            StereoFormat &setLayout(Layout layout)
            {
                if(m_layout != layout)
                {
                    m_layout = layout;
                    emit layoutChanged(layout);
                    emit displayNameChanged(displayName());
                    emit changed();
                }
                return *this;
            }

            bool leftFirst() const { return m_leftFirst; }
            StereoFormat &setLeftFirst(bool leftFrst)
            {
                if(m_leftFirst != leftFrst)
                {
                    m_leftFirst = leftFrst;
                    emit leftFirstChanged(m_leftFirst);
                    emit displayNameChanged(displayName());
                }
                return *this;
            }

            bool halfWidth()
            {
                return m_layout == ColumnInterlaced || m_layout == AnamorphHorizontal;
            }
            bool halfHeight()
            {
                return m_layout == RowInterlaced || m_layout == AnamorphVertical;
            }
            int widthFactor() { return halfWidth() ? 2 : 1; }
            int heightFactor() { return halfHeight() ? 2 : 1; }
            QSize decodingSize(QSize size) { return QSize(size.width() * widthFactor(), size.height() * heightFactor()); }
            QSize codingSize(QSize size) { return QSize(size.width() / widthFactor(), size.height() / heightFactor()); }
            bool isLoaded(){ return m_isLoaded; }
            StereoFormat &setIsLoaded(bool isLoaded)
            {
                if(m_isLoaded != isLoaded)
                {
                    m_isLoaded = isLoaded;
                    emit isLoadedChanged(isLoaded);
                }
                return *this;
            }

            int separation(){ return m_separation; }
            StereoFormat &setSeparation(int value)
            {
                if(m_separation != value)
                {
                    m_separation = value;
                    emit separationChanged(m_separation);
                }
                return *this;
            }

            AngleRotation leftRotation() const { return m_leftRotation; }
            StereoFormat &setLeftRotation(AngleRotation value) {
                if(value != m_leftRotation)
                {
                    m_leftRotation = value;
                    emit leftRotationChanged();
                    emit changed();
                }
                return *this;
            }

            AngleRotation rightRotation() const { return m_rightRotation; }
            StereoFormat &setRightRotation(AngleRotation value) {
                if(value != m_rightRotation)
                {
                    m_rightRotation = value;
                    emit rightRotationChanged();
                    emit changed();
                }
                return *this;
            }

            QString displayName()
            {
                QString name = layoutName(m_layout);
                if(leftRotation() != None)
                    name += ", " + tr("first") + rotationName(leftRotation());
                if(rightRotation() != None)
                    name += ", " + tr("second") + rotationName(rightRotation());
                if(!leftFirst())
                    name += ", " + tr("reversed");
                return name;
            }

            bool operator==(const StereoFormat& f2);
            bool operator!=(const StereoFormat& f2){ return !(*this == f2); }

        signals:
            void layoutChanged(Layout);
            void separationChanged(int);
            void leftFirstChanged(bool);
            void displayNameChanged(QString);
            void isLoadedChanged(bool);
            void leftRotationChanged();
            void rightRotationChanged();
            void changed();

        public slots:

            QString layoutName(Layout layout)
            {
                return m_layoutNames[layout];
            }

            QString layoutIconSource(Layout layout)
            {
                return m_layoutIcons[layout];
            }

            QString rotationName(AngleRotation rotation)
            {
                return m_rotationNames[rotation];
            }

            QString rotationIconSource(AngleRotation rotation)
            {
                return m_rotationIcons[rotation];
            }

            void copyFrom(StereoFormat* src);

            QSize fullFrameSize(QSize leftSize, QSize rightSize);

        private:
            Layout m_layout = Layout::Monoscopic;
            bool m_isLoaded = false;
            bool m_leftFirst = true;
            int m_separation = 0;
            AngleRotation m_leftRotation = None;
            AngleRotation m_rightRotation = None;
            QSize m_fullFrameSize;

            QMap<Layout, QString> m_layoutNames = {
                { Default, tr("Default") },
                { Monoscopic, tr("Mono") },
                { Horizontal, tr("Side by Side") },
                { AnamorphHorizontal, tr("Anamorphic Side by Side") },
                { Vertical, tr("Over/Under") },
                { AnamorphVertical, tr("Anamorphic Over/Under") },
                { RowInterlaced, tr("Row Interleaved") },
                { ColumnInterlaced, tr("Column Interleaved") },
                { AnaglyphRC, tr("Anaglyph Red/Cyan") },
                { AnaglyphYB, tr("Anaglyph Yellow/Blue") },
                { Separate, tr("Separate Frames") },
            };

            QMap<Layout, QString> m_layoutIcons = {
                { Default, "" },
                { Monoscopic, "qrc:/images/layout-mono.svg" },
                { Horizontal, "qrc:/images/layout-horizontal.svg" },
                { AnamorphHorizontal, "qrc:/images/layout-anamorph-horizontal.svg" },
                { Vertical, "qrc:/images/layout-vertical.svg" },
                { AnamorphVertical, "qrc:/images/layout-anamorph-vertical.svg" },
                { RowInterlaced, "qrc:/images/layout-rowinterlaced.svg" },
                { ColumnInterlaced, "qrc:/images/layout-columninterlaced.svg" },
                { AnaglyphRC, "qrc:/images/layout-anaglyph-rc.svg" },
                { AnaglyphYB, "qrc:/images/layout-anaglyph-yb.svg" },
                { Separate, "qrc:/images/layout-separate.svg" },
            };

            QMap<AngleRotation, QString> m_rotationNames = {
                { None, tr("None") },
                { Left, tr("Left") },
                { Right, tr("Right" )},
                { UpsideDown, tr("Upside Down") },
            };

            QMap<AngleRotation, QString> m_rotationIcons = {
                { None, "qrc:/images/object-rotate-none.svg" },
                { Left, "qrc:/images/object-rotate-left.svg" },
                { Right, "qrc:/images/object-rotate-right.svg" },
                { UpsideDown, "qrc:/images/object-rotate-upsidedown.svg" },
            };

            void copyMembersFrom(const StereoFormat& src);
    };
}
#endif // STEREOFORMAT_H

