#include "stereoframe.h"

#include <QDebug>

#if DEBUG
#include <QElapsedTimer>
#endif

#include "imagehelper.h"
#include "../lib/stereocoders/coderfactory.h"
#include "colors/coloroperations.h"

using namespace stereoscopic;
using namespace stereoscopic::coders;
using namespace stereoscopic::colors;

StereoFrame::StereoFrame()
{

}

StereoFrame::StereoFrame(const QImage &source, StereoFormat format)
{
    m_format = format;
    m_firstSource = source;
}

StereoFrame::StereoFrame(const QImage &leftSource, const QImage &rightSource)
{
    m_format = StereoFormat(StereoFormat::Separate).setIsLoaded(true);
    m_firstSource = leftSource;
    m_secondSource = rightSource;
}

StereoFrame StereoFrame::asFormat(StereoFormat format)
{
    StereoFrame newFrame(m_firstSource, m_secondSource);
    newFrame.m_offset = m_offset;
    newFrame.m_format = format;
    return newFrame;
}

QSize StereoFrame::size()
{
    return fullLeftSize();
}

QSize StereoFrame::fullLeftSize()
{
    auto decoderPtr = createDecoder();
    return decoderPtr.get()->leftFullSize();
}

QSize StereoFrame::fullRightSize()
{
    auto decoderPtr = createDecoder();
    return decoderPtr.get()->rightFullSize();
}

StereoFrame StereoFrame::fitted(QSize newSize)
{
#if DEBUG
    QElapsedTimer timer;
    timer.start();
#endif

    QImage leftSrc = leftFullView();
    QImage rightSrc = rightFullView();
    QSize leftSize = ImageHelper::getEnteredImageRect(leftSrc.size(), newSize).size();
    QSize rightSize = ImageHelper::getEnteredImageRect(rightSrc.size(), newSize).size();
    StereoFrame res(leftSrc.scaled(leftSize), rightSrc.scaled(rightSize));
#if DEBUG
    //qDebug() << "fit: " << timer.elapsed() << "ms";
#endif
    return res;
}

StereoFrame StereoFrame::aligned(QPointF offset)
{
    StereoFrame newFrame = *this;
    newFrame.m_offset = QPointF(m_offset.x() + offset.x(), m_offset.y() + offset.y());
    return newFrame;
}

StereoFrame StereoFrame::rotated(double leftAngle, double rightAngle)
{
    if(fabs(leftAngle - 0) < 0.001 && fabs(rightAngle - 0) < 0.001)
        return *this;
    QImage leftImage = ImageHelper::rotated(leftFullView(), leftAngle);
    QImage rightImage = ImageHelper::rotated(rightFullView(), rightAngle);
    return modified(leftImage, rightImage);
}

StereoFrame StereoFrame::cropped(QRect rect)
{
    if(rect.isEmpty() || rect.isNull() || !rect.isValid())
        return *this;
    return modified(leftFullView().copy(rect),
                    rightFullView().copy(rect));
}

StereoFrame StereoFrame::withFeatheredBorder(int borderWidthPercents, QRect rect, QColor outerColor)
{
    if(borderWidthPercents == 0 && (rect.isEmpty() || rect.size() == size()))
        return *this;
    return modified(ImageHelper::withFeatheredBorder(leftFullView(), borderWidthPercents, rect, outerColor),
                    ImageHelper::withFeatheredBorder(rightFullView(), borderWidthPercents, rect, outerColor));
}

StereoFrame StereoFrame::adjustColorRightByLeft(int pixelStep)
{
    return modified(
                leftFullView(),
                ColorOperations::adjustImageColorsToReference(rightFullView(), leftFullView(), 2, pixelStep));
}

StereoFrame StereoFrame::adjustColorLeftByRight(int pixelStep)
{
    return modified(
                ColorOperations::adjustImageColorsToReference(leftFullView(), rightFullView(), 2, pixelStep),
                rightFullView());
}

StereoFrame StereoFrame::scaled(const double scale, Qt::TransformationMode transformMode)
{
    return scaled(size() * scale, Qt::IgnoreAspectRatio, transformMode);
}

StereoFrame StereoFrame::scaled(const QSize &size, Qt::AspectRatioMode aspectMode, Qt::TransformationMode transformMode)
{
    QImage left = leftFullView().scaled(size, aspectMode, transformMode);
    QImage right = rightFullView().scaled(size, aspectMode, transformMode);
    return modified(left, right);
}

std::shared_ptr<StereoDecoder> StereoFrame::createDecoder()
{
    return DecoderFactory::create(&m_format, &m_firstSource, &m_secondSource, m_offset);
}

QImage StereoFrame::leftFullView()
{
    auto decoderPtr = createDecoder();
    return decoderPtr.get()->leftFullView();
}

QImage StereoFrame::rightFullView()
{
    auto decoderPtr = createDecoder();
    return decoderPtr.get()->rightFullView();
}

StereoFrame StereoFrame::modified(QImage left, QImage right)
{
    StereoFrame res = StereoFrame(left, right);
    if(m_format.layout() == StereoFormat::Layout::Separate)
    {
        // Тут нельзя копировать формат, т.к. он меняется
        return res;
    }
    else
    {
        // Преобразуем новый кадр в исходный формат
        auto coder = CoderFactory::create(&m_format, &res);
        QImage image = coder.get()->createImage();
        return StereoFrame(image, m_format);
    }
}
