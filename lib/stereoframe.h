#ifndef STEREOFRAME_H
#define STEREOFRAME_H

#include <QImage>
#include <QPainter>
#include <QPair>
#include <memory>

#include "stereoformat.h"
#include "stereodecoders/decoderfactory.h"

using namespace stereoscopic::decoders;
namespace stereoscopic {
    /**
     * @brief Класс для представления стерео кадра видео или изображения
     */
    class LIBSHARED_EXPORT  StereoFrame
    {
        public:
            StereoFrame();
            StereoFrame(const QImage &source, StereoFormat format);
            StereoFrame(const QImage &leftSource, const QImage &rightSource);

            /// Возвращает стерео формат кадра
            StereoFormat format() const { return m_format; }

            /**
             * @brief Возвращает копию кадра в указанном стерео формате.
             * Если указан layout==Default и оригинальный кадр имеет формат с isLoaded==true, подставляется формат оргинального кадра
             * @param format Указанный формат
             */
            StereoFrame asFormat(StereoFormat format);

            /// Возвращает размер отображаемого кадра
            QSize size();

            /// Фозвращает отображаемый размер левого ракурса
            QSize fullLeftSize();

            /// Фозвращает отображаемый размер правого ракурса
            QSize fullRightSize();

            /**
             * @brief Возвращает новый кадр. вписаный в указанный прямоугольник.
             * Лёгкая операция. Реальное преобразование выполняется при кодировании.
             * @param newSize Размер, в который нужно вписать кадр
             * @return Новый кадр. вписаный в указанный прямоугольник.
             */
            StereoFrame fitted(QSize newSize);

            /**
             * @brief Получить копию кадра, в котором ракурсы сдвинуты относительно друг друга на указанную величину.
             * Лёгкая операция. Реальное преобразование выполняется при кодировании.
             * @param offset Значение сдвига по осям
             */
            StereoFrame aligned(QPointF offset);

            /**
             * @brief Получить копию кадра, в котором ракурсы повёрнуты относительно исходного угла на указанную величину.
             * Лёгкая операция. Реальное преобразование выполняется при кодировании.
             * @param leftAngle Угол поворота левого ракурса
             * @param rightAngle Угол поворота правого ракурса
             */
            StereoFrame rotated(double leftAngle, double rightAngle);

            /**
             * @brief Получить новый кадр путём вырезания прямоугольника из данного
             * @param rect Прямоуголник кадрирования
             */
            StereoFrame cropped(QRect rect);

            /**
             * @brief Получить новый кадр со сглаженной рамкой
             * @param borderWidth Ширина рамки
             * @param rect Прямоугольник рамки, если рамка накладывается на часть кадра
             * @param outerColor Цвет заливки пространства покруг рамки
             */
            StereoFrame withFeatheredBorder(int borderWidthPercents, QRect rect = QRect(), QColor outerColor = QColor());

            /**
             * @brief Получить новый кадр с исправленными цветами правого кадра по левому
             */
            StereoFrame adjustColorRightByLeft(int pixelStep = 1);

            /**
             * @brief Получить новый кадр с исправленными цветами левого кадра по правому
             */
            StereoFrame adjustColorLeftByRight(int pixelStep = 1);

            /**
             * @brief Получить копию кадра в указанном масштабе от исходного
             */
            StereoFrame scaled(const double scale, Qt::TransformationMode transformMode = Qt::FastTransformation);

            /**
             * @brief Получить копию кадра, масштабированного до заданного размера
             */
            StereoFrame scaled(const QSize &size, Qt::AspectRatioMode aspectMode = Qt::IgnoreAspectRatio,
                               Qt::TransformationMode transformMode = Qt::FastTransformation);

            /**
             * @brief Возвращает новый кадр, в котором левый и правый ракурсы замененны на дaнные
             * @param left Левый ракурс
             * @param right правый ракурс
             * @return
             */
            StereoFrame modified(QImage left, QImage right);

            /// Возвращает признак того, что кадр пустой
            bool isNull()
            {
                return m_firstSource.isNull();
            }

            /// Возвращает декодер данного кадра
            std::shared_ptr<StereoDecoder> createDecoder();

            /// Полноразмерное левое изображение
            QImage leftFullView();
            /// Полноразмерное правое изображение
            QImage rightFullView();

        private:
            StereoFormat m_format;
            QImage m_firstSource, m_secondSource;
            // Сдвиг ракурсов относительно друг друга
            QPointF m_offset;
    };
}
#endif // STEREOFRAME_H
