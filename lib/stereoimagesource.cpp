#include "stereoimagesource.h"

#include "imagehelper.h"
#include "stereoformat.h"

#if DEBUG
#include <QElapsedTimer>
#endif

using namespace stereoscopic;
using namespace stereoscopic::fileFormats::jps;

StereoImageSource::StereoImageSource()
    : QObject(nullptr)
{
    connect(&m_userFormat, &StereoFormat::layoutChanged, this, &StereoImageSource::handleUserFormatChanged);
    connect(&m_userFormat, &StereoFormat::leftFirstChanged, this, &StereoImageSource::handleUserFormatChanged);
    connect(&m_userFormat, &StereoFormat::leftRotationChanged, this, &StereoImageSource::handleUserFormatChanged);
    connect(&m_userFormat, &StereoFormat::rightRotationChanged, this, &StereoImageSource::handleUserFormatChanged);
    m_userFormat.setLeftRotation(StereoFormat::UpsideDown);
}


StereoImageSource::StereoImageSource(const StereoFrame &frame, StereoFormat* defUserFormat)
    : StereoImageSource()
{
    if(defUserFormat)
        m_userFormat = *defUserFormat;
    setOriginalFrame(frame);
    setOriginalFormat(frame.format());
    update();
}

StereoImageSource::StereoImageSource(StereoFormat originalFormat, StereoFormat* defUserFormat)
    : StereoImageSource()
{
    if(defUserFormat)
        m_userFormat = *defUserFormat;
    setOriginalFormat(originalFormat);
}

StereoImageSource::StereoImageSource(StereoImageSource *src)
    : StereoImageSource()
{
    m_userFormat = src->m_userFormat;
    m_originalFrame = StereoFrame(src->m_originalFrame);
    m_displayFrame = StereoFrame(src->m_displayFrame);
    m_originalFormat = src->m_originalFormat;
    m_useOriginalFormat = src->m_useOriginalFormat;
    m_leftExifHeaders = src->m_leftExifHeaders;
    m_rightExifHeaders = src->m_rightExifHeaders;
    update();
}

void StereoImageSource::setOriginalFrame(StereoFrame frame)
{
    m_originalFrame = frame;
    update();
}

StereoFormat* StereoImageSource::userFormat()
{
    return &m_userFormat;
}

StereoFormat* StereoImageSource::activeFormat()
{
    if(m_useOriginalFormat)
        return originalFormat();
    return userFormat();
}

StereoFormat *StereoImageSource::originalFormat()
{
    return &m_originalFormat;
}

void StereoImageSource::setOriginalFormat(StereoFormat format)
{
    m_originalFormat = format;
    m_useOriginalFormat = format.isLoaded();
    emit activeFormatChanged();
    emit originalFormatChanged();
    update();
}

void StereoImageSource::setUseOriginalFormat(bool value)
{
    m_useOriginalFormat = value;
    if(m_useOriginalFormat)
    {
        useOriginalFormatChanging = true;
        // don't use copySample to avoid copying isLoaded
        m_userFormat.setLayout(m_originalFormat.layout());
        m_userFormat.setLeftFirst(m_originalFormat.leftFirst());
        m_userFormat.setLeftRotation(m_originalFormat.leftRotation());
        m_userFormat.setRightRotation(m_originalFormat.rightRotation());
        useOriginalFormatChanging = false;
    }

    emit useOriginalFormatChanged();
    emit activeFormatChanged();
    emit originalFormatChanged();
    update();
}

void StereoImageSource::handleUserFormatChanged()
{
    bool originalFormatEqualUser = m_originalFormat == m_userFormat;
    setUseOriginalFormat(originalFormatEqualUser);
    emit activeFormatChanged();
    emit originalFormatChanged();
    update();
}

void StereoImageSource::update()
{
    auto oldSize = m_leftSize;
    m_displayFrame = transformFrame(m_originalFrame.asFormat(*activeFormat()));
    m_leftSize = m_displayFrame.fullLeftSize();
    m_rightSize = m_displayFrame.fullRightSize();
    if(oldSize != m_leftSize)
        onSizeChanged(oldSize, m_leftSize);
    emit changed();
}

void StereoImageSource::onSizeChanged(QSize, QSize)
{
    emit widthChanged();
    emit heightChanged();
    emit leftSizeChanged();
    emit rightSizeChanged();
}

StereoFrame StereoImageSource::transformFrame(StereoFrame frame)
{
    return frame;
}
