#ifndef STEREOIMAGESOURCE_H
#define STEREOIMAGESOURCE_H

#include <stdlib.h>
#include <QObject>
#include <QString>
#include <QImage>
#include <QDebug>
#include <memory>
#include <QtQml/qqml.h>

#include "fileFormats/jpeg/exifheaders.h"
#include "fileFormats/jpeg/qexifimageheader.h"
#include "fileFormats/jps/stereoscopicdescriptor.h"
#include "stereoframe.h"
#include "stereoformat.h"

using namespace stereoscopic;
using namespace stereoscopic::fileFormats::jps;
using namespace stereoscopic::fileFormats::jpeg;

// Этот класс не помещён в namespace stereoscopic потому, что иначе он будет недоступен в qml
namespace stereoscopic {
    /**
     * @brief Источник стерео изображения
     */
    class LIBSHARED_EXPORT StereoImageSource : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(StereoFrame* displayFrame READ displayFrame)
            Q_PROPERTY(StereoFormat* userFormat READ userFormat NOTIFY userFormatChanged)
            Q_PROPERTY(StereoFormat* originalFormat READ originalFormat WRITE setOriginalFormat NOTIFY originalFormatChanged)
            Q_PROPERTY(StereoFormat* activeFormat READ activeFormat NOTIFY activeFormatChanged)
            Q_PROPERTY(bool useOriginalFormat READ useOriginalFormat WRITE setUseOriginalFormat NOTIFY useOriginalFormatChanged)
            Q_PROPERTY(ExifHeaders* leftExifHeaders READ leftExifHeaders NOTIFY leftMetadataChanged)
            Q_PROPERTY(ExifHeaders* rightExifHeaders READ rightExifHeaders NOTIFY rightMetadataChanged)
            Q_PROPERTY(int width READ width NOTIFY widthChanged)
            Q_PROPERTY(int height READ height NOTIFY heightChanged)
            Q_PROPERTY(bool isVideo READ isVideo NOTIFY isVideoChanged)
            Q_PROPERTY(QSize leftSize READ leftSize NOTIFY leftSizeChanged)
            Q_PROPERTY(QSize rightSize READ rightSize NOTIFY rightSizeChanged)
        public:
            StereoImageSource();

            bool isVideo(){ return m_isVideo; }

            StereoImageSource(const StereoFrame &frame, StereoFormat *defUserFormat);

            StereoImageSource(StereoFormat originalFormat, StereoFormat* defUserFormat);

            StereoImageSource(StereoImageSource *src);

            /// Возвращает текущий кадр
            StereoFrame* displayFrame(){ return &m_displayFrame; }

            /// Возвращает ширину текущего кадра
            int width() { return displayFrame()->size().width(); }

            /// Возвращает высоту текущего кадра
            int height() { return displayFrame()->size().height(); }

            /// Возвращает Exif данные по левому ракурсу
            ExifHeaders* leftExifHeaders() { return m_leftExifHeaders.get(); }
            /// Устанавливает Exif данные по левому ракурсу
            void setLeftExifHeaders(QExifImageHeader &value)
            {
                m_leftExifHeaders = std::shared_ptr<ExifHeaders>(new ExifHeaders(value));
                emit leftMetadataChanged();
            }

            /// Возвращает Exif данные по правому ракурсу
            ExifHeaders* rightExifHeaders(){ return m_rightExifHeaders.get(); }
            /// Устанавливает Exif данные по правому ракурсу
            void setRightExifHeaders(QExifImageHeader &value)
            {
                m_rightExifHeaders = std::shared_ptr<ExifHeaders>(new ExifHeaders(value));
                emit rightMetadataChanged();
            }

            /// Формат источника. определённый при загружке
            StereoFormat* originalFormat();

            /// Выбранный пользователем формат источника
            StereoFormat* userFormat();

            /// Текущий формат, который используется для декодирования кадра
            StereoFormat* activeFormat();

            /// Устанавливает формат источника. определённый при загружке
            void setOriginalFormat(StereoFormat format);

            /// Возвращает признак того, что для декодирования используется формат источника, определённый при загружке
            bool useOriginalFormat(){ return m_useOriginalFormat; }
            /// Устанавливает признак того, что для декодирования используется формат источника, определённый при загружке
            void setUseOriginalFormat(bool value);

            /// Возвращает размер левого ракурса
            QSize leftSize(){ return m_leftSize; }

            /// Возвращает размер правого ракурса
            QSize rightSize(){ return m_rightSize; }

        signals:
            void isVideoChanged();
            void changed();
            void userFormatChanged();
            void originalFormatChanged();
            void activeFormatChanged();
            void leftMetadataChanged();
            void rightMetadataChanged();
            void widthChanged();
            void heightChanged();
            void useOriginalFormatChanged();
            void leftSizeChanged();
            void rightSizeChanged();

        public slots:

            /// Устанавливает текущий кадр
            void setOriginalFrame(StereoFrame frame);

        private slots:
            void handleUserFormatChanged();

        protected:
            StereoFormat m_originalFormat;
            StereoFormat m_userFormat;
            bool m_useOriginalFormat = false;
            QSize m_leftSize;
            QSize m_rightSize;

            bool m_isVideo = false;
            // Обновляет leftSource и rightSource, и вызывает сигнал changed
            void update();
            virtual void onSizeChanged(QSize oldSize, QSize newSize);
            virtual StereoFrame transformFrame(StereoFrame frame);

        private:
            StereoFrame m_originalFrame, m_displayFrame;
            std::shared_ptr<ExifHeaders> m_leftExifHeaders = std::shared_ptr<ExifHeaders>(new ExifHeaders());
            std::shared_ptr<ExifHeaders> m_rightExifHeaders = std::shared_ptr<ExifHeaders>(new ExifHeaders());
            bool useOriginalFormatChanging = false;
    };
}
#endif // STEREOIMAGESOURCE_H
