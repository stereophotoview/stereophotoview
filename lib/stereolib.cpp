#include "stereolib.h"

#include <QtQml/qqml.h>
#include "stereoimagesource.h"
#include "stereovideosource.h"
#include "fileFormats/video/videooptions.h"
#include "fileFormats/singleimageformat.h"
#include "fileFormats/mpoimageformat.h"
#include "fileFormats/videoformat.h"
#include "fileFormats/jpsimageformat.h"
#include "fileFormats/separateimageformat.h"
#include "autoalign.h"

using namespace stereoscopic;
using namespace stereoscopic::fileFormats;

void StereoLib::qmlRegisterTypes()
{
    qmlRegisterType<StereoImageSource>("StereoImage", 1, 0, "StereoImageSource");
    qmlRegisterType<StereoVideoSource>("StereoImage", 1, 0, "StereoVideoSource");
    qmlRegisterType<StereoFormat>("StereoImage", 1, 0, "StereoFormat");
    qmlRegisterType<VideoOptions>("StereoImage", 1, 0, "VideoOptions");
    qmlRegisterType<SingleImageFormat>("StereoImage", 1, 0, "SingleFileFormat");
    qmlRegisterType<MpoImageFormat>("StereoImage", 1, 0, "MPOFileFormat");
    qmlRegisterType<VideoFormat>("StereoImage", 1, 0, "VideoFileFormat");
    qmlRegisterType<JpsImageFormat>("StereoImage", 1, 0, "JpsImageFormat");
    qmlRegisterType<ExifHeaders>("StereoImage", 1, 0, "ExifHeaders");
    qmlRegisterType<AttributeValue>("StereoImage", 1, 0, "AttributeValue");
    qmlRegisterType<SeparateImageFormat>("StereoImage", 1, 0, "SeparateImageFormat");
    qmlRegisterType<AutoAlign>("StereoImage", 1, 0, "AutoAlign");
    qmlRegisterType<AlignParams>("StereoImage", 1, 0, "AlignParams");
    qmlRegisterType<MediaPlayer>("StereoImage", 1, 0, "MediaPlayer");
}
