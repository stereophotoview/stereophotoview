#ifndef STEREOLIB_H
#define STEREOLIB_H

#include "lib_global.h"
namespace stereoscopic {
    /**
     * @brief Библиотека для работы со стерео изображениями и видео
     */
    class LIBSHARED_EXPORT StereoLib
    {
    public:
            /// Регистрирует типы для qml
            static void qmlRegisterTypes();
    };
}
#endif // STEREOLIB_H
