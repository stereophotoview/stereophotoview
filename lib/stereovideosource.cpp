#include "stereovideosource.h"

#include <QPaintEngine>
#include <QPainter>
#include <../lib/mathhelper.h>
#include "colors/coloroperations.h"
#include <QImage>

using namespace stereoscopic;
using namespace stereoscopic::colors;

StereoVideoSource::StereoVideoSource()
    : StereoImageSource()
{

}

StereoVideoSource::StereoVideoSource(const QString& fileName, StereoFormat *defUserFormat, bool play)
    : StereoVideoSource()
{
    if(defUserFormat)
        m_userFormat = *defUserFormat;
    m_isVideo = true;

    if(!m_decoder.open(fileName))
        return;
    m_alignChart.setDuration(m_decoder.duration());

    readOriginalFormat();

    if(activeFormat()->layout() == StereoFormat::Separate && m_decoder.videoStream2() != nullptr)
        setOriginalFrame(StereoFrame(image(m_decoder.videoStream1()->srcFrameSize()), image(m_decoder.videoStream2()->srcFrameSize())));
    else if(m_decoder.videoStream1() != nullptr)
        setOriginalFrame(StereoFrame(image(m_decoder.videoStream1()->srcFrameSize()), activeFormat()));
    m_alignChart.setFrameSize(displayFrame()->size());

    if(play)
    {
        m_player = new MediaPlayer(this);
        connect(m_player, &MediaPlayer::videoFrameChanged, this, &StereoVideoSource::mediaPlayerVideoFrameChanged, Qt::ConnectionType::DirectConnection);
        m_player->setSource(&m_decoder);
    }
}

void StereoVideoSource::mediaPlayerVideoFrameChanged(stereoscopic::StereoFrame frame, double pts)
{
    setPts(pts);
    setHasTimestamp(m_alignChart.pointExists(m_pts));

    if(frame.format().isLoaded() && frame.format() != m_originalFormat)
    {
        setOriginalFormat(frame.format());
        setUseOriginalFormat(true);
    }
    AlignParams l = this->layout();
    setOriginalFrame(frame);
}

StereoFrame StereoVideoSource::transformFrame(StereoFrame frame)
{
    AlignParams layout = this->layout();
    if(layout.colorAdjustment() == AlignParams::ColorAdjustment::RightByLeft)
        frame = frame.adjustColorRightByLeft(m_colorAdjustPixelStep);
    if(layout.colorAdjustment() == AlignParams::ColorAdjustment::LeftByRight)
        frame = frame.adjustColorLeftByRight(m_colorAdjustPixelStep);
    frame = frame
            .rotated(layout.leftAngle(), layout.rightAngle())
            .aligned(layout.offset())
            .withFeatheredBorder(layout.borderWidthPercents(), layout.cropRect(), QColor(0,0,0, 150));
    return frame;
}

int StereoVideoSource::videoBitrate()
{
    auto stream = m_decoder.videoStream1();
    return stream != nullptr ? stream->bitrate() : 0;
}

int StereoVideoSource::audioBitrate()
{
    auto stream = m_decoder.audioStream();
    return stream != nullptr ? stream->bitrate() : 0;
}

bool StereoVideoSource::isCropped()
{
    if(m_alignChart.baseLayout().cropRect().size() != displayFrame()->size())
        return true;
    foreach(auto point, m_alignChart.data())
        if(point.second.cropRect().size() != displayFrame()->size())
            return true;
    return false;
}

QImage StereoVideoSource::image(QSize size)
{
    QImage image(size, QImage::Format_ARGB32);
    image.fill(0);
    return image;
}

void StereoVideoSource::readOriginalFormat()
{
    if(m_decoder.videoStream2() != nullptr)
    {
        setOriginalFormat(StereoFormat(StereoFormat::Separate).setIsLoaded(true));
        m_userFormat.copyFrom(&m_originalFormat);
        m_useOriginalFormat = true;
        m_leftSize = m_decoder.videoStream1()->srcFrameSize();
        m_rightSize = m_decoder.videoStream2()->srcFrameSize();
        return;
    }
    else if(m_decoder.videoStream1() != nullptr)
    {
        QString stereoMode = m_decoder.videoStream1()->metadata()["stereo_mode"];
        m_rightSize = m_leftSize = m_decoder.videoStream1()->srcFrameSize();
        if(stereoMode.length() > 0)
        {
            StereoFormat f = FFMpegHelper::stereo_format(stereoMode).setIsLoaded(true);
            setOriginalFormat(f);
            m_userFormat.copyFrom(&m_originalFormat);
            m_useOriginalFormat = true;
            return;
        }
        else if(activeFormat()->layout() == StereoFormat::Separate)
        {
            m_useOriginalFormat = true;
        }
        m_originalFormat.setLayout(StereoFormat::Monoscopic);
    }
}

void StereoVideoSource::refreshLayout()
{
    emit layoutChanged();
    update();
    m_alignChart.setFrameSize(m_leftSize);
}

void StereoVideoSource::setHasTimestamp(bool value)
{
    if(m_hasTimestamp != value)
    {
        m_hasTimestamp = value;
        emit hasTimestampChanged();
    }
}

void StereoVideoSource::setPts(double pts)
{
    if(!stereoscopic::math::equal(m_pts, pts))
    {
        m_pts = pts;
        emit ptsChanged();
    }
}

StereoVideoSource::~StereoVideoSource()
{
    if(m_player != nullptr)
    {
        disconnect(m_player, &MediaPlayer::videoFrameChanged, this, &StereoVideoSource::mediaPlayerVideoFrameChanged);
        m_player->setSource(nullptr);
    }
}

double StereoVideoSource::fps()
{
    auto stream = m_decoder.videoStream1();
    return stream != nullptr ? stream->frameRate() : 0;
}

int StereoVideoSource::audioSampleRate()
{
    auto stream = m_decoder.audioStream();
    return stream != nullptr ? stream->sampleRate() : 0;
}

int StereoVideoSource::bytesPerAudioSample()
{
    auto stream = m_decoder.audioStream();
    return stream != nullptr ? stream->sampleBytes() : 0;
}

int StereoVideoSource::audioChannels()
{
    auto stream = m_decoder.audioStream();
    return stream != nullptr ? stream->channels() : 0;
}

QVariantList StereoVideoSource::markers()
{
    QVariantList res;
    foreach(auto point, m_alignChart.data())
    {
        QVariantMap obj;
        obj["pos"] = point.first;
        obj["offsetX"] = point.second.offset().x();
        obj["offsetY"] = point.second.offset().y();
        obj["leftAngle"] = point.second.leftAngle();
        obj["rightAngle"] = point.second.rightAngle();
        res.append(obj);
    }
    return res;
}
