#ifndef STEREOVIDEOSOURCESEPARATE_H
#define STEREOVIDEOSOURCESEPARATE_H

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

#include <QDebug>
#include <QString>
#include <QMutex>
#include <QWaitCondition>
#include <QDateTime>
#include <QVariant>

#include "stereoimagesource.h"
#include "stereoframe.h"
#include "fileFormats/video/instream.h"
#include <../lib/fileFormats/video/stereovideodecoder.h>
#include "mediaplayer/mediaplayer.h"
#include "alignchart.h"

using namespace stereoscopic::fileFormats::video;
using namespace stereoscopic::mediaplayer;
namespace stereoscopic {
    /**
     * @brief Класс для представления источника стереоскопического видео
     */
    class LIBSHARED_EXPORT StereoVideoSource : public StereoImageSource
    {
            Q_OBJECT
            Q_PROPERTY(MediaPlayer* player READ player NOTIFY playerChanged)
            Q_PROPERTY(double fps READ fps NOTIFY fpsChanged)
            Q_PROPERTY(int audioSampleRate READ audioSampleRate)
            Q_PROPERTY(int bytesPerAudioSample READ bytesPerAudioSample)
            Q_PROPERTY(int audioChannels READ audioChannels)
            Q_PROPERTY(QVariantList markers READ markers NOTIFY markersChanged)
            Q_PROPERTY(bool hasTimestamp READ hasTimestamp NOTIFY hasTimestampChanged)
            Q_PROPERTY(double pts READ pts NOTIFY ptsChanged)
            Q_PROPERTY(double timeShift READ timeShift WRITE setTimeShift NOTIFY timeShiftChanged)
            Q_PROPERTY(int colorAdjustment READ colorAdjustment NOTIFY layoutChanged)

        public:
            StereoVideoSource();
            StereoVideoSource(const QString &fileName, StereoFormat* defUserFormat, bool play);
            ~StereoVideoSource();

            /// Возвращает имя видео файла
            QString fileName(){ return m_decoder.fileName(); }

            /// Возвращает номер ошибки ffmpeg.
            int error(){ return m_error; }

            /// Возвращаетп роигрыватель
            MediaPlayer* player() { return m_player; }

            /// Возвращает частоту кадров в секунду
            double fps();

            /// Возвращает количество семплов в секнду для ауио потока
            int audioSampleRate();

            /// Возвращает количество байт в одном аудио семпле
            int bytesPerAudioSample();
            int audioChannels();

            int colorAdjustment() { return (int)layout().colorAdjustment(); }

            /// Возвращает параметры выравнивания ракурсов на данный момент времени
            AlignParams layout() { return m_alignChart.get(m_pts); }

            /// Возвращает график выравнивания ракурсов
            AlignChart &layoutChart() { return m_alignChart; }

            /// Устанавливает график выравнивания ракурсов
            void setLayoutChart(AlignChart chart)
            {
                m_alignChart = chart;
                refreshLayout();
            }

            /**
            * @brief Возвращает положение текущего кадра в секундах
            * @return Время в секундах
            */
            double pts(){ return m_pts; }

            QVariantList markers();

            /**
             * @brief Устанавливает параметры расположения ракурсов
             * @param layout Требуемые изменения в расположении ракурсов
             */
            void applyAlign(AlignParams params)
            {
                // Применяем пареданные значения к графику
                m_alignChart.apply(m_pts, params);
                refreshLayout();
            }

            /**
             * @brief Добавить отметку времени в текущем положении
             */
            void addTimestamp()
            {
                m_alignChart.addPoint(m_pts);
                refreshLayout();
                emit markersChanged();
            }

            /**
             * @brief Удалить отметку времени
             */
            void removeTimestamp()
            {
                m_alignChart.removePoint(m_pts);
                refreshLayout();
                emit markersChanged();
            }

            /**
             * @brief Сбрасывает параметры выравнивания в данный момент времени в значение по умолчанию
             */
            void resetlayout()
            {
                m_alignChart.removePoint(m_pts);
                refreshLayout();
            }

            /**
             * @brief Возвращает признак наличия отметки времени на данном кадре
             * @return True, если на данном кадре установлена отметка времени
             */
            bool hasTimestamp()
            {
                return m_alignChart.pointExists(m_pts);
            }

            /**
             * @brief Сдвиг кадров второго потока относительно первого
             */
            double timeShift() {
                return m_timeShift;
            }

            /**
             * @brief Устанавливает cдвиг кадров второго потока относительно первого
             * @param value
             */
            void setTimeShift(double value) {
                m_timeShift = value;
                if(m_player != nullptr)
                    m_player->setTimeShift(value);
                emit timeShiftChanged();
            }

        signals:
            void playerChanged();
            void fpsChanged();
            void markersChanged();
            void hasTimestampChanged();
            void ptsChanged();
            void timeShiftChanged();
            void layoutChanged();

        public slots:
            /**
             * @brief Слот для приёма сигнала изменения текущего кадра плеера
             * @param frame Декодированный стерео кадр
             * @param pts Временная метка (не используется)
             */
            void mediaPlayerVideoFrameChanged(stereoscopic::StereoFrame frame, double pts);

            /// Возвращает количество бит в секунду в видео потоке
            int videoBitrate();

            /// Возвращает количество бит в секунду в аудио потоке
            int audioBitrate();

            /// Возвращает текущий прямоуголник кадрирования
            QRect cropRect() { return layout().cropRect(); }

            /// Возвращает текущий прямоуголник кадрирования
            double borderWidthPercents() { return layout().borderWidthPercents(); }

            QSize frameSizeForSave() { return m_alignChart.maxCropSize(); }
            bool isCropped();

        protected:
            virtual StereoFrame transformFrame(StereoFrame frame) override;

        private:
            int m_error = 0;
            StereoVideoDecoder m_decoder;
            MediaPlayer* m_player = nullptr;
            QDateTime startTime;
            AlignChart m_alignChart;
            QImage image(QSize size);
            const int m_colorAdjustPixelStep = 4;
            bool m_hasTimestamp = false;
            double m_pts = 0;
            double m_timeShift = 0;
            void readOriginalFormat();
            void refreshLayout();
            void setHasTimestamp(bool value);
            void setPts(double pts);

    };
}
#endif // STEREOVIDEOSOURCESEPARATE_H
