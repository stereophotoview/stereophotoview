<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>SaveVideoTask</name>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="24"/>
        <source>The destination file must not be equal to the source</source>
        <translation>Файл назначения должен отличаться от источника</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="35"/>
        <source>Can not open &quot;%1&quot;</source>
        <translation>Не удалось открыть %1</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="135"/>
        <source>Input video streams not found</source>
        <translation>Входные видео потоки не найдены</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="38"/>
        <source>Bad input format</source>
        <translation>Плохой входной формат</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="137"/>
        <source>Can not open input file</source>
        <translation>Не могу открыть входной файл</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="139"/>
        <location filename="../fileFormats/video/savevideotask.cpp" line="141"/>
        <source>Unknown decoder error</source>
        <translation>Неизвестная ошибка декодера</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="160"/>
        <source>Can not create video stream</source>
        <translation>Не удалось создать видео поток</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="162"/>
        <source>Can not open output video stream</source>
        <translation>Не удалось открыть видео поток</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="164"/>
        <source>Can not open output audio stream</source>
        <translation>Не удалось открыть аулио поток</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="166"/>
        <source>Can not write header</source>
        <translation>Не удалось записать заголовок</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="168"/>
        <source>Can not write video frame</source>
        <translation>Не удалось записать видео кадр</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="158"/>
        <source>Can not write audio frame</source>
        <translation>Не удалось записать аудио кадр</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="90"/>
        <source>Stopped</source>
        <translation>Остановлена</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.cpp" line="90"/>
        <source>Success</source>
        <translation>Готово</translation>
    </message>
</context>
<context>
    <name>fileFormats</name>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="24"/>
        <source>Image width</source>
        <translation>Ширина изображения</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="25"/>
        <source>Image length</source>
        <translation>Высота изображения</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="26"/>
        <source>Bits per sample</source>
        <translation>Bits per sample</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="27"/>
        <source>Compression</source>
        <translation>Сжатие</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="28"/>
        <source>Photometric interpretation</source>
        <translation>Представление цвета</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="29"/>
        <source>Orientation</source>
        <translation>Ориентация</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="30"/>
        <source>Samples per pixel</source>
        <translation>Samples per pixel</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="31"/>
        <source>Planar configuration</source>
        <translation>Planar configuration</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="32"/>
        <source>YCbCr SubSampling</source>
        <translation>YCbCr SubSampling</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="33"/>
        <source>X resolution</source>
        <translation>Горизонтальное разрешение</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="34"/>
        <source>Y resolution</source>
        <translation>Вертикальное разрешение</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="35"/>
        <source>Resolution unit</source>
        <translation>Единица измерения разрешения</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="36"/>
        <source>Strip offsets</source>
        <translation>Strip offsets</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="37"/>
        <source>Rows per strip</source>
        <translation>Rows per strip</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="38"/>
        <source>Strip byte counts</source>
        <translation>Strip byte counts</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="39"/>
        <source>Transfer function</source>
        <translation>Transfer function</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="40"/>
        <source>White point</source>
        <translation>White point</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="41"/>
        <source>Primary chromaciticies</source>
        <translation>Primary chromaciticies</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="42"/>
        <source>YCbCr coefficients</source>
        <translation>YCbCr coefficients</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="43"/>
        <source>YCbCr positioning</source>
        <translation>YCbCr positioning</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="44"/>
        <source>Reference black white</source>
        <translation>Reference black white</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="45"/>
        <source>Date time</source>
        <translation>Дата-время изменения</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="46"/>
        <source>Image description</source>
        <translation>Описание изображения</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="47"/>
        <source>Make</source>
        <translation>Производитель камеры</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="48"/>
        <source>Model</source>
        <translation>Модель камеры</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="49"/>
        <source>Software</source>
        <translation>ПО камеры</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="50"/>
        <source>Processing software</source>
        <translation>ПО обработки</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="51"/>
        <source>Artist</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="52"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="56"/>
        <source>Exif version</source>
        <translation>Версия Exif</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="57"/>
        <source>FlashPix version</source>
        <translation>Версия FlashPix</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="58"/>
        <source>ColorSpace</source>
        <translation>Цветовое пространство</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="59"/>
        <source>Components configuration</source>
        <translation>Components configuration</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="60"/>
        <source>Compressed bits per pixel</source>
        <translation>Compressed bits per pixel</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="61"/>
        <source>Pixel X dimension</source>
        <translation>Pixel X dimension</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="62"/>
        <source>Pixel Y dimension</source>
        <translation>Pixel Y dimension</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="63"/>
        <source>Maker note</source>
        <translation>Информация производителя</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="64"/>
        <source>User comment</source>
        <translation>Комментарий пользователя</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="65"/>
        <source>Related soundFile</source>
        <translation>Связанный звуковой файл</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="66"/>
        <source>Date time original</source>
        <translation>Дата-время снимка</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="67"/>
        <source>Date time digitized</source>
        <translation>Дата-время оцифровки</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="68"/>
        <source>SubSec time</source>
        <translation>SubSec time</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="69"/>
        <source>SubSec time original</source>
        <translation>SubSec time original</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="70"/>
        <source>SubSec time digitized</source>
        <translation>SubSec time digitized</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="71"/>
        <source>Image uniqueId</source>
        <translation>Уникальный идентификатор</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="72"/>
        <source>Exposure time</source>
        <translation>Выдержка</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="73"/>
        <source>F number</source>
        <translation>F number</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="74"/>
        <source>Exposure program</source>
        <translation>Exposure program</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="75"/>
        <source>Spectral sensitivity</source>
        <translation>Spectral sensitivity</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="76"/>
        <source>ISO speed ratings</source>
        <translation>Светочувствительность ISO</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="77"/>
        <source>Oecf</source>
        <translation>Oecf</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="78"/>
        <source>Shutter dpeed value</source>
        <translation>Выдержка</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="79"/>
        <source>Aperture value</source>
        <translation>Апертура</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="80"/>
        <source>Brightness value</source>
        <translation>Яркость</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="81"/>
        <source>Exposure bias value</source>
        <translation>Компенсация экспозиции</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="82"/>
        <source>Max aperture value</source>
        <translation>Максимальная апертура</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="83"/>
        <source>Subject distance</source>
        <translation>Дистанция</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="84"/>
        <source>Metering mode</source>
        <translation>Metering mode</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="85"/>
        <source>Light source</source>
        <translation>Источник света</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="86"/>
        <source>Flash</source>
        <translation>Вспышка</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="87"/>
        <source>Focal length</source>
        <translation>Focal length</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="88"/>
        <source>Subject area</source>
        <translation>Subject area</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="89"/>
        <source>Flash energy</source>
        <translation>Flash energy</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="90"/>
        <source>Spatial frequency response</source>
        <translation>Spatial frequency response</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="91"/>
        <source>Focal plane X resolution</source>
        <translation>Focal plane X resolution</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="92"/>
        <source>Focal Plane Y resolution</source>
        <translation>Focal Plane Y resolution</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="93"/>
        <source>Focal plane resolutionUnit</source>
        <translation>Focal plane resolutionUnit</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="94"/>
        <source>Subject location</source>
        <translation>Subject location</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="95"/>
        <source>Exposure index</source>
        <translation>Exposure index</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="96"/>
        <source>Sensing method</source>
        <translation>Sensing method</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="97"/>
        <source>File source</source>
        <translation>File source</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="98"/>
        <source>Scene type</source>
        <translation>Scene type</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="99"/>
        <source>Cfa pattern</source>
        <translation>Cfa pattern</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="100"/>
        <source>Custom rendered</source>
        <translation>Custom rendered</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="101"/>
        <source>Exposure mode</source>
        <translation>Exposure mode</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="102"/>
        <source>White balance</source>
        <translation>Баланс белого</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="103"/>
        <source>Digital zoom ratio</source>
        <translation>Digital zoom ratio</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="104"/>
        <source>Focal kength in 35mm film</source>
        <translation>Focal kength in 35mm film</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="105"/>
        <source>Scene capture type</source>
        <translation>Scene capture type</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="106"/>
        <source>Gain control</source>
        <translation>Gain control</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="107"/>
        <source>Contrast</source>
        <translation>Контраст</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="108"/>
        <source>Saturation</source>
        <translation>Насыщенность</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="109"/>
        <source>Sharpness</source>
        <translation>Резкость</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="110"/>
        <source>Device setting description</source>
        <translation>Device setting description</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="111"/>
        <source>Subject distance range</source>
        <translation>Subject distance range</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="115"/>
        <source>Gps version Id</source>
        <translation>Версия GPS</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="116"/>
        <source>Gps latitude ref</source>
        <translation>GPS полушарие широты</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="117"/>
        <source>Gps latitude</source>
        <translation>GPS широта</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="118"/>
        <source>Gps longitude ref</source>
        <translation>GPS полушарие долготы</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="119"/>
        <source>Gps longitude</source>
        <translation>GPS долгота</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="120"/>
        <source>Gps altitude ref</source>
        <translation>GPS направление долготы</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="121"/>
        <source>Gps altitude</source>
        <translation>GPS altitude</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="122"/>
        <source>Gps TimeStamp</source>
        <translation>GPS TimeStamp</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="123"/>
        <source>Gps satellites</source>
        <translation>GPS спутники</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="124"/>
        <source>Gps status</source>
        <translation>GPS статус</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="125"/>
        <source>Gps measure mode</source>
        <translation>GPS режим измерения</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="126"/>
        <source>Gps dop</source>
        <translation>GPS dop</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="127"/>
        <source>Gps dpeed Ref</source>
        <translation>GPS dpeed Ref</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="128"/>
        <source>Gps speed</source>
        <translation>GPS скорость</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="129"/>
        <source>Gps track ref</source>
        <translation>GPS track ref</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="130"/>
        <source>Gps track</source>
        <translation>GPS track</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="131"/>
        <source>Gps image direction ref</source>
        <translation>GPS image direction ref</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="132"/>
        <source>Gps image direction</source>
        <translation>GPS image direction</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="133"/>
        <source>Gps map datum</source>
        <translation>GPS Система координат</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="134"/>
        <source>Gps dest latitude ref</source>
        <translation>GPS dest latitude ref</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="135"/>
        <source>Gps dest latitude</source>
        <translation>GPS dest latitude</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="136"/>
        <source>Gps dest longitude ref</source>
        <translation>GPS dest longitude ref</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="137"/>
        <source>Gps dest longitude</source>
        <translation>GPS dest longitude</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="138"/>
        <source>Gps dest bearing ref</source>
        <translation>GPS dest bearing ref</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="139"/>
        <source>Gps dest bearing</source>
        <translation>GPS dest bearing</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="140"/>
        <source>Gps dest distance ref</source>
        <translation>GPS dest distance ref</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="141"/>
        <source>Gps dest distance</source>
        <translation>GPS dest distance</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="142"/>
        <source>Gps processing method</source>
        <translation>GPS метод обработки</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="143"/>
        <source>Gps area information</source>
        <translation>GPS area information</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="144"/>
        <source>Gps DateStamp</source>
        <translation>GPS DateStamp</translation>
    </message>
    <message>
        <location filename="../fileFormats/jpeg/exifheaders.h" line="145"/>
        <source>Gps differential</source>
        <translation>GPS differential</translation>
    </message>
    <message>
        <location filename="../fileFormats/video/savevideotask.h" line="116"/>
        <source>Waiting…</source>
        <translation>Ожидание…</translation>
    </message>
</context>
<context>
    <name>stereoscopic::StereoFormat</name>
    <message>
        <location filename="../stereoformat.h" line="141"/>
        <source>first</source>
        <translation>первый</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="143"/>
        <source>second</source>
        <translation>второй</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="145"/>
        <source>reversed</source>
        <translation>Обратный порядок</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="198"/>
        <source>Default</source>
        <translation>Исходная</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="199"/>
        <source>Mono</source>
        <translation>Моно</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="200"/>
        <source>Side by Side</source>
        <translation>Горизонтальная пара</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="201"/>
        <source>Anamorphic Side by Side</source>
        <translation>Анаморфная горизонтальная пара</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="202"/>
        <source>Over/Under</source>
        <translation>Вертикальная пара</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="203"/>
        <source>Anamorphic Over/Under</source>
        <translation>Анаморфная вертикальная пара</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="204"/>
        <source>Row Interleaved</source>
        <translation>Чередование строк</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="205"/>
        <source>Column Interleaved</source>
        <translation>Чередование столбцов</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="206"/>
        <source>Anaglyph Red/Cyan</source>
        <translation>Анаглиф красно-голубой</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="207"/>
        <source>Anaglyph Yellow/Blue</source>
        <translation>Анаглиф жёлто-синий</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="208"/>
        <source>Separate Frames</source>
        <translation>Раздельные потоки</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="226"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="227"/>
        <source>Left</source>
        <translation>Налево</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="228"/>
        <source>Right</source>
        <translation>Направо</translation>
    </message>
    <message>
        <location filename="../stereoformat.h" line="229"/>
        <source>Upside Down</source>
        <translation>Вниз головой</translation>
    </message>
</context>
<context>
    <name>stereoscopic::fileFormats::jpeg::ExifTagnames</name>
    <message>
        <source>Image width</source>
        <translation type="vanished">Ширина изображения</translation>
    </message>
    <message>
        <source>Image length</source>
        <translation type="vanished">Высота изображения</translation>
    </message>
    <message>
        <source>Bits per sample</source>
        <translation type="vanished">Bits per sample</translation>
    </message>
    <message>
        <source>Compression</source>
        <translation type="vanished">Сжатие</translation>
    </message>
    <message>
        <source>Photometric interpretation</source>
        <translation type="vanished">Представление цвета</translation>
    </message>
    <message>
        <source>Orientation</source>
        <translation type="vanished">Ориентация</translation>
    </message>
    <message>
        <source>Samples per pixel</source>
        <translation type="vanished">Samples per pixel</translation>
    </message>
    <message>
        <source>Planar configuration</source>
        <translation type="vanished">Planar configuration</translation>
    </message>
    <message>
        <source>YCbCr SubSampling</source>
        <translation type="vanished">YCbCr SubSampling</translation>
    </message>
    <message>
        <source>X resolution</source>
        <translation type="vanished">Горизонтальное разрешение</translation>
    </message>
    <message>
        <source>Y resolution</source>
        <translation type="vanished">Вертикальное разрешение</translation>
    </message>
    <message>
        <source>Resolution unit</source>
        <translation type="vanished">Единица измерения разрешения</translation>
    </message>
    <message>
        <source>Strip offsets</source>
        <translation type="vanished">Strip offsets</translation>
    </message>
    <message>
        <source>Rows per strip</source>
        <translation type="vanished">Rows per strip</translation>
    </message>
    <message>
        <source>Strip byte counts</source>
        <translation type="vanished">Strip byte counts</translation>
    </message>
    <message>
        <source>Transfer function</source>
        <translation type="vanished">Transfer function</translation>
    </message>
    <message>
        <source>White point</source>
        <translation type="vanished">White point</translation>
    </message>
    <message>
        <source>Primary chromaciticies</source>
        <translation type="vanished">Primary chromaciticies</translation>
    </message>
    <message>
        <source>YCbCr coefficients</source>
        <translation type="vanished">YCbCr coefficients</translation>
    </message>
    <message>
        <source>YCbCr positioning</source>
        <translation type="vanished">YCbCr positioning</translation>
    </message>
    <message>
        <source>Reference black white</source>
        <translation type="vanished">Reference black white</translation>
    </message>
    <message>
        <source>Date time</source>
        <translation type="vanished">Дата-время изменения</translation>
    </message>
    <message>
        <source>Image description</source>
        <translation type="vanished">Описание изображения</translation>
    </message>
    <message>
        <source>Make</source>
        <translation type="vanished">Производитель камеры</translation>
    </message>
    <message>
        <source>Model</source>
        <translation type="vanished">Модель камеры</translation>
    </message>
    <message>
        <source>Software</source>
        <translation type="vanished">ПО камеры</translation>
    </message>
    <message>
        <source>Processing software</source>
        <translation type="vanished">ПО обработки</translation>
    </message>
    <message>
        <source>Artist</source>
        <translation type="vanished">Автор</translation>
    </message>
    <message>
        <source>Copyright</source>
        <translation type="vanished">Copyright</translation>
    </message>
    <message>
        <source>Exif version</source>
        <translation type="vanished">Версия Exif</translation>
    </message>
    <message>
        <source>FlashPix version</source>
        <translation type="vanished">Версия FlashPix</translation>
    </message>
    <message>
        <source>ColorSpace</source>
        <translation type="vanished">Цветовое пространство</translation>
    </message>
    <message>
        <source>Components configuration</source>
        <translation type="vanished">Components configuration</translation>
    </message>
    <message>
        <source>Compressed bits per pixel</source>
        <translation type="vanished">Compressed bits per pixel</translation>
    </message>
    <message>
        <source>Pixel X dimension</source>
        <translation type="vanished">Pixel X dimension</translation>
    </message>
    <message>
        <source>Pixel Y dimension</source>
        <translation type="vanished">Pixel Y dimension</translation>
    </message>
    <message>
        <source>Maker note</source>
        <translation type="vanished">Информация производителя</translation>
    </message>
    <message>
        <source>User comment</source>
        <translation type="vanished">Комментарий пользователя</translation>
    </message>
    <message>
        <source>Related soundFile</source>
        <translation type="vanished">Связанный звуковой файл</translation>
    </message>
    <message>
        <source>Date time original</source>
        <translation type="vanished">Дата-время снимка</translation>
    </message>
    <message>
        <source>Date time digitized</source>
        <translation type="vanished">Дата-время оцифровки</translation>
    </message>
    <message>
        <source>SubSec time</source>
        <translation type="vanished">SubSec time</translation>
    </message>
    <message>
        <source>SubSec time original</source>
        <translation type="vanished">SubSec time original</translation>
    </message>
    <message>
        <source>SubSec time digitized</source>
        <translation type="vanished">SubSec time digitized</translation>
    </message>
    <message>
        <source>Image uniqueId</source>
        <translation type="vanished">Уникальный идентификатор</translation>
    </message>
    <message>
        <source>Exposure time</source>
        <translation type="vanished">Выдержка</translation>
    </message>
    <message>
        <source>F number</source>
        <translation type="vanished">F number</translation>
    </message>
    <message>
        <source>Exposure program</source>
        <translation type="vanished">Exposure program</translation>
    </message>
    <message>
        <source>Spectral sensitivity</source>
        <translation type="vanished">Spectral sensitivity</translation>
    </message>
    <message>
        <source>ISO speed ratings</source>
        <translation type="vanished">Светочувствительность ISO</translation>
    </message>
    <message>
        <source>Oecf</source>
        <translation type="vanished">Oecf</translation>
    </message>
    <message>
        <source>Shutter dpeed value</source>
        <translation type="vanished">Выдержка</translation>
    </message>
    <message>
        <source>Aperture value</source>
        <translation type="vanished">Апертура</translation>
    </message>
    <message>
        <source>Brightness value</source>
        <translation type="vanished">Яркость</translation>
    </message>
    <message>
        <source>Exposure bias value</source>
        <translation type="vanished">Компенсация экспозиции</translation>
    </message>
    <message>
        <source>Max aperture value</source>
        <translation type="vanished">Максимальная апертура</translation>
    </message>
    <message>
        <source>Subject distance</source>
        <translation type="vanished">Дистанция</translation>
    </message>
    <message>
        <source>Metering mode</source>
        <translation type="vanished">Metering mode</translation>
    </message>
    <message>
        <source>Light source</source>
        <translation type="vanished">Источник света</translation>
    </message>
    <message>
        <source>Flash</source>
        <translation type="vanished">Вспышка</translation>
    </message>
    <message>
        <source>Focal length</source>
        <translation type="vanished">Focal length</translation>
    </message>
    <message>
        <source>Subject area</source>
        <translation type="vanished">Subject area</translation>
    </message>
    <message>
        <source>Flash energy</source>
        <translation type="vanished">Flash energy</translation>
    </message>
    <message>
        <source>Spatial frequency response</source>
        <translation type="vanished">Spatial frequency response</translation>
    </message>
    <message>
        <source>Focal plane X resolution</source>
        <translation type="vanished">Focal plane X resolution</translation>
    </message>
    <message>
        <source>Focal Plane Y resolution</source>
        <translation type="vanished">Focal Plane Y resolution</translation>
    </message>
    <message>
        <source>Focal plane resolutionUnit</source>
        <translation type="vanished">Focal plane resolutionUnit</translation>
    </message>
    <message>
        <source>Subject location</source>
        <translation type="vanished">Subject location</translation>
    </message>
    <message>
        <source>Exposure index</source>
        <translation type="vanished">Exposure index</translation>
    </message>
    <message>
        <source>Sensing method</source>
        <translation type="vanished">Sensing method</translation>
    </message>
    <message>
        <source>File source</source>
        <translation type="vanished">File source</translation>
    </message>
    <message>
        <source>Scene type</source>
        <translation type="vanished">Scene type</translation>
    </message>
    <message>
        <source>Cfa pattern</source>
        <translation type="vanished">Cfa pattern</translation>
    </message>
    <message>
        <source>Custom rendered</source>
        <translation type="vanished">Custom rendered</translation>
    </message>
    <message>
        <source>Exposure mode</source>
        <translation type="vanished">Exposure mode</translation>
    </message>
    <message>
        <source>White balance</source>
        <translation type="vanished">Баланс белого</translation>
    </message>
    <message>
        <source>Digital zoom ratio</source>
        <translation type="vanished">Digital zoom ratio</translation>
    </message>
    <message>
        <source>Focal kength in 35mm film</source>
        <translation type="vanished">Focal kength in 35mm film</translation>
    </message>
    <message>
        <source>Scene capture type</source>
        <translation type="vanished">Scene capture type</translation>
    </message>
    <message>
        <source>Gain control</source>
        <translation type="vanished">Gain control</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation type="vanished">Контраст</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation type="vanished">Насыщенность</translation>
    </message>
    <message>
        <source>Sharpness</source>
        <translation type="vanished">Резкость</translation>
    </message>
    <message>
        <source>Device setting description</source>
        <translation type="vanished">Device setting description</translation>
    </message>
    <message>
        <source>Subject distance range</source>
        <translation type="vanished">Subject distance range</translation>
    </message>
    <message>
        <source>Gps version Id</source>
        <translation type="vanished">Версия GPS</translation>
    </message>
    <message>
        <source>Gps latitude ref</source>
        <translation type="vanished">GPS полушарие широты</translation>
    </message>
    <message>
        <source>Gps latitude</source>
        <translation type="vanished">GPS широта</translation>
    </message>
    <message>
        <source>Gps longitude ref</source>
        <translation type="vanished">GPS полушарие долготы</translation>
    </message>
    <message>
        <source>Gps longitude</source>
        <translation type="vanished">GPS долгота</translation>
    </message>
    <message>
        <source>Gps altitude ref</source>
        <translation type="vanished">GPS направление долготы</translation>
    </message>
    <message>
        <source>Gps altitude</source>
        <translation type="vanished">GPS altitude</translation>
    </message>
    <message>
        <source>Gps TimeStamp</source>
        <translation type="vanished">GPS TimeStamp</translation>
    </message>
    <message>
        <source>Gps satellites</source>
        <translation type="vanished">GPS спутники</translation>
    </message>
    <message>
        <source>Gps status</source>
        <translation type="vanished">GPS статус</translation>
    </message>
    <message>
        <source>Gps measure mode</source>
        <translation type="vanished">GPS режим измерения</translation>
    </message>
    <message>
        <source>Gps dop</source>
        <translation type="vanished">GPS dop</translation>
    </message>
    <message>
        <source>Gps dpeed Ref</source>
        <translation type="vanished">GPS dpeed Ref</translation>
    </message>
    <message>
        <source>Gps speed</source>
        <translation type="vanished">GPS скорость</translation>
    </message>
    <message>
        <source>Gps track ref</source>
        <translation type="vanished">GPS track ref</translation>
    </message>
    <message>
        <source>Gps track</source>
        <translation type="vanished">GPS track</translation>
    </message>
    <message>
        <source>Gps image direction ref</source>
        <translation type="vanished">GPS image direction ref</translation>
    </message>
    <message>
        <source>Gps image direction</source>
        <translation type="vanished">GPS image direction</translation>
    </message>
    <message>
        <source>Gps map datum</source>
        <translation type="vanished">GPS Система координат</translation>
    </message>
    <message>
        <source>Gps dest latitude ref</source>
        <translation type="vanished">GPS dest latitude ref</translation>
    </message>
    <message>
        <source>Gps dest latitude</source>
        <translation type="vanished">GPS dest latitude</translation>
    </message>
    <message>
        <source>Gps dest longitude ref</source>
        <translation type="vanished">GPS dest longitude ref</translation>
    </message>
    <message>
        <source>Gps dest longitude</source>
        <translation type="vanished">GPS dest longitude</translation>
    </message>
    <message>
        <source>Gps dest bearing ref</source>
        <translation type="vanished">GPS dest bearing ref</translation>
    </message>
    <message>
        <source>Gps dest bearing</source>
        <translation type="vanished">GPS dest bearing</translation>
    </message>
    <message>
        <source>Gps dest distance ref</source>
        <translation type="vanished">GPS dest distance ref</translation>
    </message>
    <message>
        <source>Gps dest distance</source>
        <translation type="vanished">GPS dest distance</translation>
    </message>
    <message>
        <source>Gps processing method</source>
        <translation type="vanished">GPS метод обработки</translation>
    </message>
    <message>
        <source>Gps area information</source>
        <translation type="vanished">GPS area information</translation>
    </message>
    <message>
        <source>Gps DateStamp</source>
        <translation type="vanished">GPS DateStamp</translation>
    </message>
    <message>
        <source>Gps differential</source>
        <translation type="vanished">GPS differential</translation>
    </message>
</context>
<context>
    <name>stereoscopic::fileFormats::video::SaveVideoTask</name>
    <message>
        <source>The destination file must not be equal to the source</source>
        <translation type="vanished">Файл назначения должен отличаться от источника</translation>
    </message>
    <message>
        <source>Can not open &quot;%1&quot;</source>
        <translation type="vanished">Не удалось открыть %1</translation>
    </message>
    <message>
        <source>Input video streams not found</source>
        <translation type="vanished">Входные видео потоки не найдены</translation>
    </message>
    <message>
        <source>Bad input format</source>
        <translation type="vanished">Плохой входной формат</translation>
    </message>
    <message>
        <source>Can not open input file</source>
        <translation type="vanished">Не могу открыть входной файл</translation>
    </message>
    <message>
        <source>Unknown decoder error</source>
        <translation type="vanished">Неизвестная ошибка декодера</translation>
    </message>
    <message>
        <source>Can not create video stream</source>
        <translation type="vanished">Не удалось создать видео поток</translation>
    </message>
    <message>
        <source>Can not open output video stream</source>
        <translation type="vanished">Не удалось открыть выходной видео поток</translation>
    </message>
    <message>
        <source>Can not open output audio stream</source>
        <translation type="vanished">Не удалось открыть выходной аудио поток</translation>
    </message>
    <message>
        <source>Can not write header</source>
        <translation type="vanished">Не удалось записать заголовок</translation>
    </message>
    <message>
        <source>Can not write video frame</source>
        <translation type="vanished">Не удалось записать видео кадр</translation>
    </message>
    <message>
        <source>Can not write audio frame</source>
        <translation type="vanished">Не удалось записать аудио кадр</translation>
    </message>
    <message>
        <source>Stopped</source>
        <translation type="vanished">Остановлена</translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="vanished">Готово</translation>
    </message>
    <message>
        <source>Waiting…</source>
        <translation type="vanished">Ожидание…</translation>
    </message>
</context>
</TS>
