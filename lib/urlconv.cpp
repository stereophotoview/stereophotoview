#include "urlconv.h"
#include <QFileInfo>
#include <QDir>
#include <QDebug>

using namespace stereoscopic;

UrlConv::UrlConv(QObject *parent) : QObject(parent)
{

}

QString UrlConv::getLocalFile(QUrl url)
{
    QUrl u(url);
    return u.toLocalFile();
}

QUrl UrlConv::getUrl(QString filePath)
{
    return QUrl::fromLocalFile(filePath);
}

bool UrlConv::equals(QString one, QString other)
{
#ifdef Q_OS_WIN
    return one.toUpper() == other.toUpper();
#else
    return one == other;
#endif

}

QString UrlConv::getFileName(QString filePath)
{
    if(filePath.length() == 3 && filePath.endsWith(":/"))
        return filePath.mid(0, 2);
    else
        return QFileInfo(filePath).fileName();
}

QString UrlConv::getSuffix(QString fileName)
{
    return QFileInfo(fileName).suffix();
}

QString UrlConv::changeSuffix(QString filePath, QString newSuffix)
{
    return getPathWithoutSuffix(filePath) + "." + newSuffix;
}

QString UrlConv::getPathWithoutSuffix(QString filePath)
{
    QFileInfo info(filePath);
    return QDir::cleanPath(info.path() + QDir::separator() + info.completeBaseName());
}

bool UrlConv::isDir(QString path)
{
    QFileInfo info(path);
    return info.isDir();
}

QString UrlConv::parent(QString path)
{
    QFileInfo info(path);
    QString res = info.dir().path();
    return res == path ? "" : res;
}

QString UrlConv::getDirPath(QString filePath)
{
    if(filePath.length() == 0)
        return QString();
    QFileInfo fileInfo = QFileInfo(filePath);
    QDir dir = fileInfo.dir();
    QString path = dir.path();
    return path;
}

QString UrlConv::separator()
{
    return QDir::separator();
}
