#ifndef URLCONV_H
#define URLCONV_H

#include <QObject>
#include <QUrl>

#include "lib_global.h"
namespace stereoscopic {
    /**
     * @brief Класс для преобразования url и путей файловой системы
     */
    class LIBSHARED_EXPORT  UrlConv : public QObject
    {
            Q_OBJECT
            Q_PROPERTY(QString separator READ separator)

        public:

            explicit UrlConv(QObject *parent = nullptr);

            /**
             * @brief Возвращает путь к каталогу файла
             * @param fileName Путь к файлу
             * @return Путь к каталогу
             */
            Q_INVOKABLE QString getDirPath(QString fileName);

            /**
             * @brief Возвращает используемый символ разелителя пути в файловой системе
             * @return используемый символ разелителя пути в файловой системе
             */
            QString separator();

        public slots:
            /**
             * @brief Возвращает путь к локальному файлу по url
             * @param url url файла
             * @return путь к файлу в локальной файловой системе
             */
            QString getLocalFile(QUrl url);

            /**
             * @brief Возвращает url по пути к локальному файлу
             * @param filePath Путь к файлу
             * @return url файла
             */
            QUrl getUrl(QString filePath);

            /**
             * @brief Сравнивает два пути в файловой системе
             * @param one Первый путь
             * @param other Второй путь
             * @return true, если пути одинаковые
             */
            bool equals(QString one, QString other);

            /**
             * @brief Возвращает имя файла по пути к нему в файловой системе
             * @param filePath Путь к файлу
             * @return Имя файла
             */
            QString getFileName(QString filePath);

            /**
             * @brief Возвращает расширение файла
             * @param fileName Имя файла с расширением
             * @return Расширение файла
             */
            QString getSuffix(QString fileName);

            /**
             * @brief Возвращает имя файла с указанным новым расширением
             * @param filePath Исходный путь к файлу
             * @param newSuffix Новое расширение, которое нужно применить к имени файла
             * @return Путь к файлу с новым расширеием
             */
            QString changeSuffix(QString filePath, QString newSuffix);

            /**
             * @brief Возвращает путь к файлу без расширения
             * @param Путь
             * @return Путь к файлу без расширения
             */
            QString getPathWithoutSuffix(QString filePath);

            /**
             * @brief Проверяет, является ли указанный путь каталогом
             * @param path Путь
             * @return true, если это каталог
             */
            bool isDir(QString path);

            /**
             * @brief Возвращает путь к родительскому каталогу
             * @return Путь к родительскому каталогу
             */
            QString parent(QString);

            /**
             * @brief Возвращает невалидный URL
             * @return Не валидный url
             */
            QUrl invalidUrl() { return QUrl(); }

        private:
            QString m_separator;
    };
}
#endif // URLCONV_H
