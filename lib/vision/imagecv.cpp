#include "imagecv.h"

#include "imagehelper.h"

using namespace stereoscopic::vision;

ImageCV::ImageCV()
{

}

ImageCV::ImageCV(QImage image)
{
    imageMat = createMat(image);
    m_descriptors.create(Size(0,0), 0);
}

const vector<KeyPoint> ImageCV::keyPoints()
{
    if(!detected)
        detectAndCompute();
    return m_keyPoints;
}

const Mat ImageCV::descriptors()
{
    if(!detected)
        detectAndCompute();
    return m_descriptors;
}

void ImageCV::showDebug(QString name)
{
#ifdef CV_DEBUG
    imshow(name.toStdString(), matrix());
#endif
}

Mat ImageCV::createMat(QImage src)
{
    int matrixFormat = getMatrixFormat(src.format());
    Mat mat;
    mat.create(src.height(), src.width(), matrixFormat);
    uint bytesPerLine = mat.elemSize() * src.width();
    uchar* resData = mat.data;
    for(int y = 0; y < src.height(); y++)
    {
        uchar* srcLine = src.scanLine(y);
        uchar* resLine = resData + (uint)y * bytesPerLine;
        memcpy(resLine, srcLine, bytesPerLine);
    }
    return mat;
}

void ImageCV::detectAndCompute()
{
    detected = true;
#if CV_MAJOR_VERSION < 3
    BRISK detector;
    detector.detect(imageMat, keyPoints);
    detector.compute(imageMat, keyPoints, descriptors);
#else
    auto detectorPtr = BRISK::create();
    auto detector = detectorPtr.get();
    UMat mask;
    detector->detectAndCompute(imageMat, mask, m_keyPoints, m_descriptors);
#endif
}

int ImageCV::getMatrixFormat(QImage::Format imageFormat)
{
    switch (imageFormat) {
        case QImage::Format_Grayscale8: return CV_8UC1;
        case QImage::Format_ARGB32:
            return CV_8UC4;
        default:
            throw "Not Implemented";
    }
}
