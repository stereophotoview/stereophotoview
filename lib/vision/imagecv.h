#ifndef IMAGECV_H
#define IMAGECV_H

#include <QImage>
#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>
#include "lib_global.h"

using namespace std;
using namespace cv;

namespace stereoscopic::vision {

    /**
     * @brief Computer Vision for image
     */
    class ImageCV
    {
        public:
            ImageCV();
            ImageCV(QImage image);

            const Mat matrix(){ return imageMat; }
            const QSize size() { return QSize(imageMat.cols, imageMat.rows); }
            const vector<KeyPoint> keyPoints();
            const Mat descriptors();
            void showDebug(QString name);

        private:
            Mat imageMat;
            vector<KeyPoint> m_keyPoints;
            Mat m_descriptors;
            bool detected = false;

            Mat createMat(QImage src);
            void detectAndCompute();
            int getMatrixFormat(QImage::Format imageFormat);
    };
}

#endif // IMAGECV_H
