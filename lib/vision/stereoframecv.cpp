#include "stereoframecv.h"

#include <imagehelper.h>

using namespace stereoscopic;
using namespace stereoscopic::vision;

StereoFrameCV::StereoFrameCV(StereoFrame *frame)
{
    m_leftImage = ImageCV(frame->leftFullView().convertToFormat(QImage::Format_Grayscale8));
    m_rightImage = ImageCV(frame->rightFullView().convertToFormat(QImage::Format_Grayscale8));
#ifdef CV_DEBUG
        imshow("Left", m_leftImage.matrix());
        imshow("Right", m_rightImage.matrix());
#endif

}

std::vector<DMatch> StereoFrameCV::matchingPoints()
{
    if(!detected)
        detectMatchingPoints();
    return m_matchingPoints;
}

Point2i StereoFrameCV::getDistanceBetweenMatchingPoints(DMatch m)
{
    auto leftCoords = getLeftKeyPoint(m).pt;
    auto rightCoords = getRightKeyPoint(m).pt;
    return Point2i(
                (int)(rightCoords.x - leftCoords.x),
                (int)(rightCoords.y - leftCoords.y));
}

KeyPoint StereoFrameCV::getLeftKeyPoint(DMatch m)
{
    return m_leftImage.keyPoints()[m.queryIdx];
}

KeyPoint StereoFrameCV::getRightKeyPoint(DMatch m)
{
    return m_rightImage.keyPoints()[m.trainIdx];
}

// Возвращает соответствующие точки на левом и правом изображении
void StereoFrameCV::detectMatchingPoints()
{
    detected = true;
    if(m_leftImage.descriptors().cols != m_rightImage.descriptors().cols
            || m_leftImage.descriptors().rows == 0 || m_leftImage.descriptors().rows == 0)
    {
        qWarning() << "Can not detect keypoints or compute descriptors";
        m_leftImage.showDebug("Left");
        m_leftImage.showDebug("Right");
        return;
    }

    m_matchingPoints = filterGoodMatches(detectAllMatches());
}

std::vector<DMatch> StereoFrameCV::detectAllMatches()
{
    std::vector<DMatch> matches;
#if CV_MAJOR_VERSION < 3
    BFMatcher matcher;
    matcher.match(leftFeatures.descriptors, rightFeatures.descriptors, matches);
#else
    Ptr<BFMatcher> matcher = BFMatcher::create(NORM_HAMMING, true);
    matcher.get()->match(m_leftImage.descriptors(), m_rightImage.descriptors(), matches);
#endif
    return matches;
}

vector<DMatch> StereoFrameCV::filterGoodMatches(vector<DMatch> allMatches)
{
    // Находим наиболее и наименее похожие точки
    // Условно принимаем за хорошие соответствия те, у которых разница между точками в 4 раза меньше максимальной
    // Почему просто не взять несколько первых?

    std::sort(allMatches.begin(), allMatches.end(), [](DMatch a, DMatch b){ return a.distance < b.distance; });
    auto worseMatch = minmax_element(
                allMatches.begin(), allMatches.end(),
                [](DMatch& a, DMatch& b){ return a.distance < b.distance; })
            .second;
    float maxGoodDistance = worseMatch->distance / 4;
    std::vector<DMatch>::iterator f = std::find_if(allMatches.begin(), allMatches.end(), [maxGoodDistance](DMatch m){ return m.distance >= maxGoodDistance; });
    // Ограничиваем количество хороших соответствий (от 5 до 50)
    int lastGoodMatchIndex = min(
                50,
                std::max(
                    (int)(f - allMatches.begin() - 1),
                    min(5, (int)allMatches.size() - 1)));
    std::vector<DMatch> goodMatches;
    std::copy_n(allMatches.begin(), lastGoodMatchIndex, std::back_inserter(goodMatches));
#ifdef CV_DEBUG
    Mat img_matches;
    drawMatches(m_leftImage.matrix(), m_leftImage.keyPoints(), m_rightImage.matrix(), m_rightImage.keyPoints(),
                goodMatches, img_matches, Scalar::all(-1), Scalar::all(-1),
                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
    imshow( "Good Matches", img_matches );
#endif
    return goodMatches;
}
