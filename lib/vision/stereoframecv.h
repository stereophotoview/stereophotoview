#ifndef STEREOFRAMECOMPUTERVISION_H
#define STEREOFRAMECOMPUTERVISION_H


#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>
#include "lib_global.h"


#ifdef CV_DEBUG
#include "opencv2/highgui.hpp"
#endif

#include <stereoframe.h>
#include "imagecv.h"

using namespace std;
using namespace cv;
using namespace stereoscopic;
using namespace stereoscopic::vision;

namespace stereoscopic::vision {
    /**
     * @brief Computer Vision for stereo frame
     */
    class StereoFrameCV
    {
        public:
            /** Computer Vision for stereo frame */
            StereoFrameCV(StereoFrame* frame);

            ImageCV leftImage(){ return m_leftImage; }
            ImageCV rightImage(){ return m_rightImage; }
            /** Получить соответствующие точки на левом и правом изображении */
            vector<DMatch> matchingPoints();
            Point2i getDistanceBetweenMatchingPoints(DMatch m);
            KeyPoint getLeftKeyPoint(DMatch m);
            KeyPoint getRightKeyPoint(DMatch m);
            QSize size() { return leftImage().size(); }
        private:
            ImageCV m_leftImage, m_rightImage;
            vector<DMatch> m_matchingPoints;
            bool detected = false;

            void detectMatchingPoints();
            vector<DMatch> detectAllMatches();
            vector<DMatch> filterGoodMatches(vector<DMatch> allMatches);
    };
}
#endif // STEREOFRAMECOMPUTERVISION_H
