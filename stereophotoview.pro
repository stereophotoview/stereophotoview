TEMPLATE=subdirs
SUBDIRS=lib conv gui test
gui.depends=lib
conv.depends=lib
test.depends=lib

include(common.pri)

DISTFILES += README.md \
    README_RU.md

include(deployment.pri)
