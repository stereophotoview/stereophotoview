TEMPLATE = app

QT += testlib
QT -= gui

TARGET = test
CONFIG -= app_bundle
CONFIG += testcase no_testcase_installs c++17

TEMPLATE = app

SOURCES +=  \
    test_alignchart.cpp

# Подключаем сторонние библиотеки
include(../external_libs.pri)

DEPENDPATH += ../lib
INCLUDEPATH += ../lib
windows {
    LIBS += -lstereophotoview1
    CONFIG(debug):LIBS += -L../lib/debug
    CONFIG(release):LIBS += -L../lib/release
}
unix {
    LIBS += -L../lib -lstereophotoview

    QMAKE_RPATHDIR += $ORIGIN/../lib
}

HEADERS += \
    test_alignchart.h
