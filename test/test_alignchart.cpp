#include <QTest>
#include "test_alignchart.h"
#include "../lib/alignchart.h"

using namespace stereoscopic;
using namespace stereoscopic::test::gui;

Test_AlignChart::Test_AlignChart(QObject *parent)
    : QObject(parent)
{

}

void Test_AlignChart::applyAll()
{
    QSize framesize(1024, 768);
    // Тестовые данные. <момент времени,параметры>
    QList<QPair<double, AlignParams>> testData { QPair<double, AlignParams>(0, AlignParams()),
                QPair<double, AlignParams>(1, AlignParams(QPointF(10, 20), 1, 2)),
                QPair<double, AlignParams>(2, AlignParams(QPointF(-10, -20), -1, -2)),
                QPair<double, AlignParams>(3, AlignParams(QPointF(-20, -30), -3, -4)),
                QPair<double, AlignParams>(4, AlignParams(QPointF(-20, -30), -3, -4)),
                QPair<double, AlignParams>(5, AlignParams(QRect(10, 10, 100, 100), 5)),
                QPair<double, AlignParams>(6, AlignParams(QRect(5, 5, 80, 80), 6))};

    // Применяем тестовые данные без создания точек
    AlignChart chart;
    chart.setDuration(30);
    chart.setFrameSize(framesize);
    foreach(auto testPoint, testData)
        chart.apply(testPoint.first, testPoint.second);

    auto data = chart.data();

    // apply должны действовать на весь интервал времени, список точек должен остаться пустым
    QCOMPARE(data.size(), 0);

    // Значения выравнивания на всём интервале дожно равняться сумме переданных значений
    AlignParams expectedAlign;
    foreach(auto testPoint, testData)
    {
        expectedAlign.setoffset(expectedAlign.offset() + testPoint.second.offset());
        expectedAlign.setLeftAngle(expectedAlign.leftAngle() + testPoint.second.leftAngle());
        expectedAlign.setRightAngle(expectedAlign.rightAngle() + testPoint.second.rightAngle());

        // Координаты прямоугольника кадрирования складыаются
        expectedAlign.setCropRect(
                    QRect(expectedAlign.cropRect().left() + testPoint.second.cropRect().left(),
                          expectedAlign.cropRect().right() + testPoint.second.cropRect().right(),
                          0, 0));
    }

    // Размеры прямоугольника кадрирования - это размеры последнего кадрирования
    auto lastCropRect = testData.last().second.cropRect();
    auto expectedCropRect = expectedAlign.cropRect();
    expectedCropRect.setWidth(lastCropRect.width());
    expectedCropRect.setHeight(lastCropRect.height());
    expectedAlign.setCropRect(expectedCropRect);

    // Толщина рамки - толщина последней рамки
    expectedAlign.setBorderWidthPercents(testData.last().second.borderWidthPercents());

    // Проверяем значение выравнивания по контрольным точкам
    QList<double> checkPoints { 0, 5, 10, 15, 20, 25, 30 };
    foreach(double checkPoint, checkPoints)
    {
        AlignParams l = chart.get(checkPoint);
        QCOMPARE(l.offset(), expectedAlign.offset());
        QCOMPARE(l.leftAngle(), expectedAlign.leftAngle());
        QCOMPARE(l.rightAngle(), expectedAlign.rightAngle());
    }
}

void Test_AlignChart::applyPoints()
{
    QSize framesize(1024, 768);
    // Тестовые данные. <момент времени,параметры>
    QList<QPair<double, AlignParams>> testData {
        QPair<double, AlignParams>(10, AlignParams(QPointF(10, 20), 1, 2)),
                QPair<double, AlignParams>(20, AlignParams(QPointF(-10, -20), -1, -2).with(AlignParams(QRect(10, 10, 100, 100), 5))),
                QPair<double, AlignParams>(30, AlignParams(QPointF(-20, -30), -3, -4).with(AlignParams(QRect(5, 5, 80, 80), 7)))
    };

    // Применяем тестовые данные на точки
    AlignChart chart;
    chart.setDuration(30);
    chart.setFrameSize(framesize);

    foreach(auto testPoint, testData)
        chart.addPoint(testPoint.first);
    foreach(auto testPoint, testData)
        chart.apply(testPoint.first, testPoint.second);

    auto data = chart.data();

    // Должны автоматически создаваться граничные точки
    QCOMPARE(data.size(), testData.length() + 1);

    // Проверяем значения в указанных точках
    QCOMPARE(data[0].first, 0.0);
    QCOMPARE(data[0].second.offset(), QPointF(0, 0));
    QCOMPARE(data[0].second.leftAngle(), 0.0);
    QCOMPARE(data[0].second.rightAngle(), 0.0);
    QCOMPARE(data[0].second.cropRect(), QRect(QPoint(0, 0), framesize));

    QCOMPARE(data[1].first, 10.0);
    QCOMPARE(data[1].second.offset(), QPointF(10, 20));
    QCOMPARE(data[1].second.leftAngle(), 1.0);
    QCOMPARE(data[1].second.rightAngle(), 2.0);

    QCOMPARE(data[2].first, 20.0);
    QCOMPARE(data[2].second.offset(), QPointF(-10, -20));
    QCOMPARE(data[2].second.leftAngle(), -1.0);
    QCOMPARE(data[2].second.rightAngle(), -2.0);
    QCOMPARE(data[2].second.cropRect(), QRect(10, 10, 100, 100));
    QCOMPARE(data[2].second.borderWidthPercents(), 5.0);

    QCOMPARE(data[3].first, 30.0);
    QCOMPARE(data[3].second.offset(), QPointF(-20, -30));
    QCOMPARE(data[3].second.leftAngle(), -3.0);
    QCOMPARE(data[3].second.rightAngle(), -4.0);
    QCOMPARE(data[3].second.cropRect(), QRect(5, 5, 80, 80));
    QCOMPARE(data[3].second.borderWidthPercents(), 7.0);
}

void Test_AlignChart::applyAllWithPoints()
{
    QSize framesize(1024, 768);
    AlignParams first(QPointF(10, 20), 1, 2);
    AlignChart chart;
    chart.setDuration(30);
    chart.setFrameSize(framesize);
    // Добавляем одну контрольную точку
    chart.addPoint(15);
    chart.apply(15, first);

    AlignParams diff(QPointF(1, 1), 1, 1);

    // Применяем значения к другим моментам времени и проверяем, что значения всех точек меняются на эту величину
    chart.apply(12, diff);
    QCOMPARE(chart.data()[0].second.offset(), diff.offset());
    QCOMPARE(chart.data()[0].second.leftAngle(), diff.leftAngle());
    QCOMPARE(chart.data()[0].second.rightAngle(), diff.rightAngle());

    QCOMPARE(chart.data()[1].second.offset(), first.with(diff).offset());
    QCOMPARE(chart.data()[1].second.leftAngle(), first.with(diff).leftAngle());
    QCOMPARE(chart.data()[1].second.rightAngle(), first.with(diff).rightAngle());

    QCOMPARE(chart.data()[2].second.offset(), diff.offset());
    QCOMPARE(chart.data()[2].second.leftAngle(), diff.leftAngle());
    QCOMPARE(chart.data()[2].second.rightAngle(), diff.rightAngle());
}

void Test_AlignChart::getByPoints()
{
    QSize framesize(1024, 768);
    AlignChart chart;
    chart.setDuration(30);
    chart.setFrameSize(framesize);

    chart.addPoint(10);
    chart.addPoint(20);
    chart.apply(10, AlignParams(QPointF(10, 20), 1, 2));
    chart.apply(10, AlignParams(QRect(10, 10, 100, 100), 7));
    chart.apply(20, AlignParams(QPointF(-10, -20), -1, -2));
    chart.apply(20, AlignParams(QRect(5, 5, 80, 80), 5));
    chart.apply(30, AlignParams(QPointF(-20, -30), -3, -4));

    AlignParams l = chart.get(5);
    QCOMPARE(l.offset(), QPointF(5, 10));
    QCOMPARE(l.leftAngle(), 0.5);
    QCOMPARE(l.rightAngle(), 1.0);
    QCOMPARE(l.cropRect(), QRect(5, 5, 562, 434));
    QCOMPARE(l.borderWidthPercents(), 3.5);

    l = chart.get(10);
    QCOMPARE(l.cropRect(), QRect(10, 10, 100, 100));
    QCOMPARE(l.borderWidthPercents(), 7.0);

    l = chart.get(15);
    QCOMPARE(l.offset(), QPointF(0, 0));
    QCOMPARE(l.leftAngle(), 0.0);
    QCOMPARE(l.rightAngle(), 0.0);

    l = chart.get(25);
    QCOMPARE(l.offset(), QPointF(-15, -25));
    QCOMPARE(l.leftAngle(), -2.0);
    QCOMPARE(l.rightAngle(), -3.0);

    l = chart.get(20);
    QCOMPARE(l.offset().x(), -10.0);
    QCOMPARE(l.offset().y(), -20.0);
    QCOMPARE(l.leftAngle(), -1.0);
    QCOMPARE(l.rightAngle(), -2.0);
    QCOMPARE(l.cropRect(), QRect(5, 5, 80, 80));
    QCOMPARE(l.borderWidthPercents(), 5.0);

    l = chart.get(30);
    QCOMPARE(l.offset().x(), -20.0);
    QCOMPARE(l.offset().y(), -30.0);
    QCOMPARE(l.leftAngle(), -3.0);
    QCOMPARE(l.rightAngle(), -4.0);
}

void Test_AlignChart::removePoint()
{
    QSize framesize(1024, 768);
    AlignChart chart;
    chart.setDuration(30);
    chart.setFrameSize(framesize);

    chart.addPoint(10);
    chart.addPoint(20);
    chart.addPoint(30);
    chart.apply(10, AlignParams(QPointF(10, 20), 1, 2));
    chart.apply(20, AlignParams(QPointF(-10, -20), -1, -2));
    chart.apply(30, AlignParams(QPointF(-20, -30), -3, -4));

    chart.removePoint(20);

    auto data = chart.data();

    QCOMPARE(data.size(), 3);

    QCOMPARE(data[0].first, 0.0);
    QCOMPARE(data[0].second.offset(), QPointF(0, 0));
    QCOMPARE(data[0].second.leftAngle(), 0.0);
    QCOMPARE(data[0].second.rightAngle(), 0.0);

    QCOMPARE(data[1].first, 10.0);
    QCOMPARE(data[1].second.offset(), QPointF(10, 20));
    QCOMPARE(data[1].second.leftAngle(), 1.0);
    QCOMPARE(data[1].second.rightAngle(), 2.0);

    QCOMPARE(data[2].first, 30.0);
    QCOMPARE(data[2].second.offset(), QPointF(-20, -30));
    QCOMPARE(data[2].second.leftAngle(), -3.0);
    QCOMPARE(data[2].second.rightAngle(), -4.0);

    // Крайние точки нельзя удалить
    chart.removePoint(0);
    QCOMPARE(chart.data().size(), 3);
    chart.removePoint(30);
    QCOMPARE(chart.data().size(), 3);

    // Удаление последней промежуточной точки ведёт к удалению всех точек
    chart.removePoint(10);
    QCOMPARE(chart.data().size(), 0);
}

void Test_AlignChart::pointExists()
{
    QSize framesize(1024, 768);
    AlignChart chart;
    chart.setDuration(30);
    chart.setFrameSize(framesize);

    chart.addPoint(10);
    chart.addPoint(20);
    chart.apply(0, AlignParams());
    chart.apply(10, AlignParams(QPointF(10, 20), 1, 2));
    chart.apply(20, AlignParams(QPointF(-10, -20), -1, -2));
    chart.apply(30, AlignParams(QPointF(-20, -30), -3, -4));
    QCOMPARE(chart.pointExists(0), true);
    QCOMPARE(chart.pointExists(10), true);
    QCOMPARE(chart.pointExists(20), true);

    QCOMPARE(chart.pointExists(-10), false);
    QCOMPARE(chart.pointExists(5), false);
    QCOMPARE(chart.pointExists(12), false);
}

QTEST_MAIN(stereoscopic::test::gui::Test_AlignChart)
