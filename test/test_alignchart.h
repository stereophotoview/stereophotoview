#ifndef TEST_ALIGNCHART_H
#define TEST_ALIGNCHART_H

#include <QObject>
#include <QtTest>

namespace stereoscopic::test::gui {
    class Test_AlignChart : public QObject
    {
        Q_OBJECT
    public:
        explicit Test_AlignChart(QObject *parent = 0);

    private slots:
            /**
             * @brief Многократное применение параметров без создания точек
             */
            void applyAll();

            /**
             * @brief Многократное применение параметров с созданием точек
             */
            void applyPoints();

            /**
             * @brief Применение парамтеров ко всему видео при наличии точек
             */
            void applyAllWithPoints();

            /**
             * @brief Получение параметров в произвольные моменты времени при налии сохраннённых точек
             */
            void getByPoints();

            /**
             * @brief Удаление точки
             */
            void removePoint();

            /**
             * @brief Проверка функцуии pointExists
             */
            void pointExists();
    };
}

#endif // TEST_ALIGNCHART_H
